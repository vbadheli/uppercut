<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Home extends MY_Controller {



  public function __construct() {

    parent::__construct();

    $this->load->model('home_model');


  }



  public function index() {

  //$this->load->view('search', $this->data);

    if(user_logged_in()){
         $usertype=get_session('type');
         //echo "<pre>";print_r($usertype);exit;
        if($usertype=="owner")
        {
          redirect('shop/shop/homesalon');
        }
        if($usertype=="user")
        {
          $this->load->view('search', $this->data);
        }
       //redirect('dashboard');
   }else{
     $this->load->view('search', $this->data);
    }

 }

 public function login() {

  if(user_logged_in()){

   redirect(base_url());

 }else{

   $this->load->view('shop/login', $this->data);

 }

}
public function rate($id) {

  if(!user_logged_in()){

   redirect(base_url());

 }else{
  $data = $this->db->where('id',$id)->get('shop_list')->row();
  $this->load->view('shop/rate', ['data'=>$data]);

}

}
public function store_rating() {

  if(!user_logged_in()){

   redirect(base_url());

 }else{
  $post= $this->input->post();

  unset($post['submit']);
  if($this->db->insert('feedback',$post))
  {
    $this->session->set_flashdata('feedback','Rating accepted. <Br> Thanks for sharing your feedback');

    $this->session->set_flashdata('feedback_class', 'alert-success');
    redirect(base_url('rate_a_salon'));
  }else
  {
    $this->session->set_flashdata('feedback','Sorry we are unable to update your rating, please try again');

    $this->session->set_flashdata('feedback_class', 'alert-danger');
    redirect(base_url('rate_a_salon'));
  }
}

}



public function dashboard() {

 if(user_logged_in()) {

  $this->load->view('shop/dashboard', $this->data);

}else{

  set_flashdata('message', "Oops! Your username and password didn't match.", 'danger');

  redirect('login');

}

}
public function salon($id) {

 if(user_logged_in()) {
   $this->load->model('signup_model'); 
   $this->data['obj'] = $this->signup_model;
   $this->load->view('shop/salon', $this->data);

 }else{

  set_flashdata('message', "Oops! Your username and password didn't match.", 'danger');

  redirect('login');

}

}
public function edit_salon($id) {

 if(user_logged_in()) {
  $data = $this->db->where('id',$id)->get('shop_list')->row();
  $this->load->view('shop/editsaloon', ['data'=>$data]);

}else{

  set_flashdata('message', "Oops! Your username and password didn't match.", 'danger');

  redirect('login');

}

}
public function owner()
{
  if(user_logged_in()) {
    if(!empty($this->input->post('name') && $this->input->post('about_me')))
    {


      $config = [

        'upload_path' =>    './uploads',

        'allowed_types' =>    'jpg|png|jpeg',

      ];

      $this->load->library('upload', $config);
      if($this->upload->do_upload('picture')){

        $post = $this->input->post();
    // unset($post['submit']);
        $data = $this->upload->data();
        $image_path =  $data['raw_name'] . $data['file_ext'];
        $post['image'] = $image_path;
        $post['service_offered'] = isset($post['service_offered'])?json_encode($post['service_offered']):"";
        $post['days'] = json_encode($post['days']);
        $post['parking'] = isset($post['parking'])?json_encode($post['parking']):"" ;
        $post['type'] = $post['type'];
        $post['shop_user'] = get_session('userid');
        unset($post['submit']);
    // echo "<pre>"; print_r($post);exit;
        if($this->db->insert('shop_list',$post))
        {
          $insert_id = $this->db->insert_id();
          $session_data = array(
            'salonid' => $insert_id,
          ); 
          set_sessions($session_data);     
          $this->load->model('signup_model'); 
          $this->data['obj'] = $this->signup_model;    
          $this->data['divcongrats'] = 1; 
          //set_flashdata('divcongrats', "1", 'danger');   
          $this->load->view('shop/homesalon',$this->data); 
        // $this->session->set_flashdata('divcongrats', "1", 'danger');
       // redirect('shop/shop/saloncongrats',$this->data);
        }
        else
        {
          redirect('shop/shop/saloncongrats',$this->data);
        }
      }else
      {
        redirect('shop/shop/saloncongrats',$this->data);
      }
    }else
    {
     redirect('shop/shop/saloncongrats',$this->data);    
   }
 }else{

  set_flashdata('message', "Oops! Your username and password didn't match.", 'danger');

  redirect('login');

}


}

public function dashboard2($key) {





  $result = $this->home_model->search_result();



  // debug($result);

  $getbyid = $this->home_model->getshopbyid($key);

  $this->data['getbyid'] = $getbyid;

  // debug($this->data);

  if(isset($result) && count($result)>0){

   $this->data['result'] = $result;

   $this->load->view('shop/dashboard2', $this->data);

 }else{

  set_flashdata('message', 'Sorry, no shops found ', 'danger');

  $this->load->view('search', $this->data);

}



}

public function signup() {

  $this->load->view('shop/signup', $this->data);

}
public function usersignup() {

  $this->load->view('shop/usersignup', $this->data);

}



public function restpassword() {

  $this->load->view('shop/resetpass', $this->data);

}
public function about() {

  $this->load->view('about_us', $this->data);

}
public function contact() {

  $this->load->view('contact_us', $this->data);

}
public function terms() {

  $this->load->view('terms_condition', $this->data);

}



public function newpassword() {

  $this->load->view('shop/newpassword', $this->data);

}
public function change_pass()
{
 if(user_logged_in()){
   $this->load->model('Signup_model');
   $old = $this->input->post('oldpassword');
   $new = $this->input->post('newpassword');
   $pass = md5($old);
   $match = $this->db->where('id',get_session('userid'))->get('shop_signup')->row()->password;

   if($pass === $match && $this->Signup_model->change_passd(get_session('userid'),$new)){

    $this->session->set_flashdata('feedback','Updated successfully');

    $this->session->set_flashdata('feedback_class', 'alert-success');
    redirect('newpass');
  }
  else
  {
    $this->session->set_flashdata('feedback','Passowrd not matching');

    $this->session->set_flashdata('feedback_class', 'alert-danger');
    redirect('newpass');
  }

}else{
  redirect('login');

}



}
public function check_pass($first,$second)
{

  if ($second == md5($first))
  {
    return TRUE;    

  }
  else if(password_verify($second, $first))
  {
    return TRUE;
  }
  else
  {
    return FALSE;
  }

}
public function missyou() {

 if(user_logged_in()){

  $this->load->view('shop/missu', $this->data);

}else{

  redirect('login');

}

}



public function addshop(){

  if(!user_logged_in()) {

    redirect('login');

  }

  $this->load->view('shop/addshop', $this->data);

}



public function will_missyou(){



 if(user_logged_in()){

  $this->load->view('shop/will_missyou', $this->data);

}else{

  redirect('home');

}

}



public function map(){

 $this->load->view('shop/map', $this->data);

}



public function mapbyid($data){

  $this->data['latlan']=$data;

  $result = $this->home_model->search_result();

  if(isset($result) && count($result)>0){

   $this->data['result'] = $result;

 }

 $this->load->view('shop/mapbyid', $this->data);

}



}