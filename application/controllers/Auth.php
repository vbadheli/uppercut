<?php defined('BASEPATH') OR exit('No direct script access allowed');



class Auth extends MY_Controller {



	public function __construct() {

		parent::__construct();

		$this->load->model('Signup_model');

		$this->load->library('user_agent');

	}



	function login() {

		$post = $this->input->post();

		set_session('timeZone', $post['timeZone']);

		if(isset($_POST['opt_check'])) {

			$request = array(

				'email' => $post['email']

			);

			$response = postParamRecord(LOGIN_OTP, $request);

			if($response['status'] == 'success') {

				set_session('otp', $response['message']);

				set_session('email', $post['email']);

				$output = array(

					'error' => false,

					'login' => false,

					'message' => ajax_alert('Verification code has been sent to your email ID. Kindly check your email.', 'success'),

				);

			} else {

				$output = array(

					'error' => true,

					'message' => ajax_alert($response['reason'], 'danger'),

				);

			}

		} else {

			$request = array(

				'email' => $post['email'],

				'password' => $post['password']

			);

			$response = postParamRecord(LOGIN, $request);



			if($response['status'] == 'success') {

				$this->set_login($response['data']);

				$output = array(

					'error' => false,

					'message' => '',

					'login' => true,

				);

			} else {

				$output = array(

					'error' => true,

					'message' => ajax_alert($response['reason'], 'danger'),

				);

			}

		}

		json_output(json_encode($output));

	}

	public function account($email,$code)
	{
		if(!empty($email) && !empty($code))
		{
			if($this->db->where('email',$email)->get('shop_signup')->row()->activation_code == $code)
			{
				$this->db->set('verified_status',1)->where('email',$email)->update('shop_signup');
				$this->session->set_flashdata('feedback','Congratulations your account is active. Now you can login');

				$this->session->set_flashdata('feedback_class', 'alert-success');
				redirect(base_url());
			}else
			{
				$this->session->set_flashdata('feedback','Oops, we are unable to activate your account.');

				$this->session->set_flashdata('feedback_class', 'alert-danger');
				redirect(base_url());
			}
		}
		else
		{
			redirect(base_url());
		}
	}

	function set_login($data) {

		unset_userLogin_sessions();

		set_login_sessions($data);

		/*$response = getParamRecord(CHECK_PROFILE, array(

			'consultantId' => get_session('user_id')

		));

		if($response['status'] == 'success') {

			if($response['message'] != '') {

				set_session('checkProfile', $response['message']);

				set_session('checkSlot', $response['slot']);

			} else {

				set_session('checkProfile', '');

				set_session('checkSlot', '');

			}

		}*/

	}

	public function reset_password($email,$activation_code)
	{
		if(!empty($email) && !empty($activation_code))
		{
			$row = $this->db->where('email',$email)->where('activation_code',$activation_code)->get('shop_signup');
			if($row->num_rows() > 0)
			{
				$data['email'] = $email;
				$data['activation_code'] = $activation_code;
				$this->load->view('shop/resetpassword', ['data'=>$data]);
			}else
			{
			$this->session->set_flashdata('feedback','Wrong authentication code, please try again');

			$this->session->set_flashdata('feedback_class', 'alert-danger');

			redirect(base_url());
			}
		}
		else
		{
			redirect(base_url());
		}
	}
	public function update_password()
	{
		if(!empty($this->input->post('email')) && !empty($this->input->post('activation_code')) && $this->input->post('password'))
		{
			if($this->db->set('password',md5($this->input->post('password')))->where('email',$this->input->post('email'))->where('activation_code',$this->input->post('activation_code'))->update('shop_signup'))
			{
				$this->session->set_flashdata('feedback','Updated successully');

				$this->session->set_flashdata('feedback_class', 'alert-success');
				redirect(base_url());
			}
			else{
				$this->session->set_flashdata('feedback','Failed to update ');

				$this->session->set_flashdata('feedback_class', 'alert-danger');
				redirect(base_url());
			}
		}else
		{
			redirect(base_url());
		}
	}
	function otp_login() {

		$post = $this->input->post();

		$otp = get_session('otp');

		$email = get_session('email');

		if($post['otp'] == $otp && $post['email'] == $email) {

			$output = array(

				'error' => false,

				'message' => '',

				'first_time' => true

			);

			$this->set_login_session($email);

		} else {

			$message = "Wrong OTP";

			$output = array(

				'error' => true,

				'message' => ajax_alert($message, 'danger'),

			);

		}

		json_output(json_encode($output));

	}



	function set_login_session($email) {

		$request = array(

			'email' => $email

		);

		$response = getParamRecord(AFTER_LOGIN, $request);

		if($response['status'] == 'success') {

			unset_userLogin_sessions();

			set_login_sessions($response['data']);

		}



		/*$response = getParamRecord(CHECK_PROFILE, array(

			'consultantId' => get_session('user_id')

		));

		if($response['status'] == 'success') {

			if($response['message'] != '') {

				set_session('checkProfile', $response['message']);

				set_session('checkSlot', $response['slot']);

			} else {

				set_session('checkProfile', '');

				set_session('checkSlot', '');

			}

		}*/

	}



	function resend_otp() {

		$request = array(

			'email' => $_POST['email']

		);

		$response = getParamRecord(LOGIN_OTP, $request);

		if($response['status'] == 'success') {

			set_session('otp', $response['message']);

			set_session('email', $_POST['email']);

			$output = array(

				'error' => false,

				'message' => ajax_alert('Verification code has been sent to your email ID:<a href="#" style="color: #0000cc;">('.$_POST['email'].')</a>. Kindly check your email ID.', 'success'),

			);

		}

		json_output(json_encode($output));

	}



	function forgot() {

		$email = $this->input->post('forgot-email');

		$request = array(

			'email' => $email,

			'link' => base_url('auth/change_password/')

		);

		$response = postParamRecord(FORGOT_CON_PWD, $request);



		if($response['status'] == 'success') {

			$output = array(

				'error' => false,

				'message' => ajax_alert('Reset password link has been sent to your email ID:<a href="#" style="color: #0000cc;">('.$email.')</a>. Kindly check your email.', 'success'),

			);

		} else {

			$output = array(

				'error' => false,

				'message' => ajax_alert($response['reason'], 'danger'),

			);

		}

		json_output(json_encode($output));

	}



	function check_mobile() {

		 $request = array(

			 'mobileno' => $_GET['mobile']

		 );

		$response = getParamRecord(MOBILE_EMAIL_VALIDATE, $request);

		if($response['status'] == 'success') {

			echo 'false';

		} else {

			echo 'true';

		}

	}



	function check_email() {

		$request = array(

			'email' => $_GET['email']

		);

		$response = getParamRecord(MOBILE_EMAIL_VALIDATE, $request);

		if($response['status'] == 'success') {

			echo 'true';

		} else {

			echo 'false';

		}

	}



	function load_login_register() {

		$data = array();

		$message = array(

			'error' => false,

			'message' => $this->load->view('frontend/login_register', $data, true),

		);

	}



	function load_forgot_password() {

		$data = array();

		$message = array(

			'error' => false,

			'message' => $this->load->view('frontend/forgot_password', $data, true),

		);

		json_output(json_encode($message));

	}



	function index() {

		$inclusions = array('validate');

		$this->data['inclusions'] = inclusions($inclusions);

		$this->data['title'] = get_title('Authentication');

		load_frontend_page('frontend/auth', $this->data);

	}



	function admin_login($id) {

        if (!admin_logged_in()) {

            redirect('admin');

        } else {

            $result = getParamRecord(CON_PROFILE_GET.'/'.custom_decode($id), array());

            set_login_sessions($result['data']);

            redirect(base_url('dashboard'));

        }

	}



	function change_password($token) {

		$inclusions = array('validate');

		$this->data['inclusions'] = inclusions($inclusions);

		$this->data['title'] = get_title('Change password');

		if (isset($_POST['change_password'])) {

			$response = postParamRecord(CHNAGE_CON_PWD, array (

				'email' => get_session('email'),

				'password' => $_POST['password']

			));

			if($response['status'] == 'success') {

				set_flashdata('message', 'Your password has been changed successfully.', 'success');

				redirect(base_url('login-registration'));

			}else{

				set_flashdata('message', $response['reason'], 'success');

				redirect(base_url('auth/change_password'.$token));

			}

		} else {

			$response = getParamRecord(AFTER_LOGIN, array (

				'token' => $token

			));

			if($response['status'] == 'success') {

				$this->data['profile'] = $response['data'];

			} else {

				set_flashdata('message', $response['reason'], 'danger');

				redirect(base_url('login-registration'));

			}

		}

		$this->load->view('change-password', $this->data);

	}



	function register() {

		$post = $this->input->post();

		$request = array(

			'salutation' => 'Dr.',

			'firstname' => $post['name'],

			'mobile' => $post['mobile'],

			'country_code' => '+61',

			'email' => $post['email'],

			'password' => $post['password'],

			"time_zone" => date_default_timezone_get(),

			'verification_link' => base_url('auth/verify'),

			'clinic_name' => $post['clinic_name']

		);

		$response = postParamRecord(REGISTRATION, $request);

		if($response['status'] == 'success') {

			$output = array(

				'error' => false,

				'message' => ajax_alert('Registration verification link has been sent to your email ID:<a href="#" style="color: #0000cc;">('.$post['email'].')</a>. Kindly check your email and verify the account.', 'info'),

			);

		} else {

			$output = array(

				'error' => true,

				'message' => ajax_alert($response['reason'], 'danger'),

			);

		}

		json_output(json_encode($output));

	}



	function verify() {

		$inclusions = array('validate');

		$this->data['title'] = 'Congratulation';

		if(isset($_GET['token'])) {

			$response = getParamRecord(AFTER_LOGIN, array (

				'token' => $_GET['token'],

			));

			if($response['status'] == 'success') {

				set_flashdata('message', 'Your account has been verified successfully.', 'success');

				set_session('first_time', true);

				redirect(base_url('login-registration'));

			}else{

				set_flashdata('message', 'Verification token expired', 'success');

				redirect(base_url());

			}

		}else{

			set_flashdata('message', 'Verification token expired', 'success');

			redirect(base_url());

		}

	}



	function logout() {

		unset_login_sessions();

		redirect(base_url());

	}



	function userLogout() {

		unset_userLogin_sessions();

		redirect(base_url());

	}



	function password_change() {

		$response = postParamRecord(CHNAGE_CON_PWD, array (

			'id' => get_session('user_id'),

			'email' => get_session('email'),

			'old_password' => $_POST['old_password'],

			'password' => $_POST['new_password']

		));



		if($response['status'] == 'success') {

			$session_data = array('user_id', 'logged_in');

			unset_session($session_data);

			set_flashdata('message', 'Your have changed your login password. Kindly login using new password.', 'info');

			$output = array(

				'error' => false,

				'message' => ajax_alert("Your have changed your login password. Kindly login using new password.", 'info'),

			);

		}else{

			$output = array(

				'error' => false,

				'message' => ajax_alert($response['reason'], 'danger'),

			);

		}

		json_output(json_encode($output));

	}



	// Start Facebook login

	function fb_login() {

		include FCPATH.'assets/facebook/src/Facebook/autoload.php';



		$fb = new Facebook\Facebook([

			'app_id' => $this->facebook['app_id'],

			'app_secret' => $this->facebook['secret'],

			'default_graph_version' => 'v2.2',

		]);



		$helper = $fb->getRedirectLoginHelper();



		$permissions = ['email'];

		$loginUrl = $helper->getLoginUrl(base_url('fb_callback'), $permissions);

		redirect($loginUrl);

	}



	function fb_callback() {

		include FCPATH.'assets/facebook/src/Facebook/autoload.php';



		$fb = new Facebook\Facebook([

			'app_id' => $this->facebook['app_id'],

			'app_secret' => $this->facebook['secret'],

			'default_graph_version' => 'v2.2',

		]);



		$helper = $fb->getRedirectLoginHelper();



		try {

			$accessToken = $helper->getAccessToken();

			$response = $fb->get('/me?fields=id,name,email,firstname,lastname', $accessToken);

			$user = $response->getGraphUser();

			$data = array(

				'platform' => 'facebook',

				'fb_id' => $user['id'],

				'name' => $user['name'],

				'email' => @$user['email']

			);

			if( !empty($data['email']) ) {

				$user = $this->login_model->social_login($data);

				if( $user ) {

					redirect(base_url());

				} else {

					redirect(base_url());

				}

			} else {

				$this->session->set_flashdata('facebook-error', 'success');

				redirect(base_url());

			}

		} catch(Facebook\Exceptions\FacebookResponseException $e) {

		  // When Graph returns an error

		  echo 'Graph returned an error: ' . $e->getMessage();

		  exit;

		} catch(Facebook\Exceptions\FacebookSDKException $e) {

		  // When validation fails or other local issues

		  echo 'Facebook SDK returned an error: ' . $e->getMessage();

		  exit;

		}



		if (! isset($accessToken)) {

		  if ($helper->getError()) {

			header('HTTP/1.0 401 Unauthorized');

			echo "Error: " . $helper->getError() . "\n";

			echo "Error Code: " . $helper->getErrorCode() . "\n";

			echo "Error Reason: " . $helper->getErrorReason() . "\n";

			echo "Error Description: " . $helper->getErrorDescription() . "\n";

		  } else {

			header('HTTP/1.0 400 Bad Request');

			echo 'Bad request';

		  }

		  exit;

		}



		// Logged in

		// The OAuth 2.0 client handler helps us manage access tokens

		$oAuth2Client = $fb->getOAuth2Client();



		// Get the access token metadata from /debug_token

		$tokenMetadata = $oAuth2Client->debugToken($accessToken);



		// Validation (these will throw FacebookSDKException's when they fail)

		$tokenMetadata->validateAppId($this->facebook['app_id']); // Replace {app-id} with your app id

		// If you know the user ID this access token belongs to, you can validate it here

		//$tokenMetadata->validateUserId('123');

		$tokenMetadata->validateExpiration();



		if (! $accessToken->isLongLived()) {

		  // Exchanges a short-lived access token for a long-lived one

		  try {

			$accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);

		  } catch (Facebook\Exceptions\FacebookSDKException $e) {

			echo "<p>Error getting long-lived access token: " . $helper->getMessage() . "</p>\n\n";

			exit;

		  }



		  echo '<h3>Long-lived</h3>';

		  var_dump($accessToken->getValue());

		}



		redirect(base_url());

	}

	// End





	// Start Google login

	function set_gplus() {

		include FCPATH.'assets/gplus/src/Google_Client.php';

		include FCPATH.'assets/gplus/src/contrib/Google_PlusService.php';

		include FCPATH.'assets/gplus/src/contrib/Google_Oauth2Service.php';



		$api = new Google_Client();

		$api->setApplicationName("MaximHot100"); // Set Application name

		$api->setClientId($this->gplus['client_id']); // Set Client ID

		$api->setClientSecret($this->gplus['secret']); //Set client Secret

		$api->setAccessType('offline'); // Access method

		$api->setScopes(array('https://www.googleapis.com/auth/userinfo.email', 'https://www.googleapis.com/auth/userinfo.profile'));

		$api->setRedirectUri(base_url('gp_callback'));

		return $api;

	}



	function gp_login() {

		$api = $this->set_gplus();

		$service = new Google_PlusService($api);

		$url = $api->createAuthUrl();

		redirect($url);

	}



	function gp_callback() {

		$api = $this->set_gplus();

		$oauth2 = new Google_Oauth2Service($api);

		if( isset($_GET['code']) ) {

			$api->authenticate($_GET['code']);

			if ( $api->getAccessToken() ) {

				$user_data = $oauth2->userinfo->get();

				$data = array(

					'platform' => 'google',

					'g_id' => $user_data['id'],

					'name' => $user_data['name'],

					'email' => $user_data['email']

				);

				if( !empty($data['email']) ) {

					$user = $this->login_model->social_login($data);

					if( $user ) {

						redirect(base_url());

					} else {

						redirect(base_url());

					}

				} else {

					redirect(base_url());

				}

			}

		}

		redirect('login');

	}

	//End



	function user_login() {

		$post = $this->input->post();

		set_session('timeZone', $post['timeZone']);

		$request = array(

			'name' => $post['name'],

			'email' => $post['email'],

			'profile_pic' => $post['photo'],

			'registerBy' => "WEB"

		);

		//debug($request, false);

		$response = postParamRecord(USER_LOGIN, $request);

		//debug($response);

		if($response['status'] == 'success') {

			unset_login_sessions();

			set_userLogin_sessions($response['data']);

			$output = array(

				'error' => false,

				'message' => '',

				'login' => true,

			);

		} else {

			$output = array(

				'error' => true,

				'message' => ajax_alert($response['reason'], 'danger'),

			);

		}

		json_output(json_encode($output));

	}



}