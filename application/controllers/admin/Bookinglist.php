<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Bookinglist extends MY_Controller {

    public function __construct() {
        parent::__construct();
       $this->load->model('admin/MBookinglist');
    }

    function index() {
        if (!admin_logged_in()) {
            redirect('admin');
        }
        $includes = array('validate', 'summernote', 'iCheck', 'bootbox', 'datatable','input_mask');
        $this->data['inclusions'] = inclusions($includes);
        $this->data['page_title'] = "Booking user";
        $result = $this->MBookinglist->get_data();
        $this->data['result'] = $result;
        load_backend_page('backend/bookinglist', $this->data);
    }
}
