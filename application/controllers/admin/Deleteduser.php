<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Deleteduser extends MY_Controller {

    public function __construct() {
        parent::__construct();
       $this->load->model('admin/deleted_user');
    }

    function index() {
        if (!admin_logged_in()) {
            redirect('admin');
        }
        $includes = array('validate', 'summernote', 'iCheck','datatable');
        $this->data['inclusions'] = inclusions($includes);
        $this->data['page_title'] = "Deleted user";
        $result = $this->deleted_user->get_data();
        $this->data['result'] = $result;
        load_backend_page('backend/deleteduser', $this->data);
    }
}
