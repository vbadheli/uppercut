<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Shopuser extends MY_Controller
{

  public function __construct() {
    parent::__construct();
    if (!admin_logged_in()) {
      redirect('admin');
    }
    $this->load->model('admin/shop_model');
  }

  function index() {
    $inclusions = array('validate', 'bootbox', 'datatable','input_mask');
    $this->data['inclusions'] = inclusions($inclusions);
    $this->data['page_title'] = "Shop Users";
    $response = $this->shop_model->shopList();
    
    if($response){

      $this->data['result'] = $response;
    }
    load_backend_page('backend/shop/index', $this->data);
  }

  function add_services() {
   
   
    $this->data['page_title'] = "Add Services";
    
    load_backend_page('backend/shop/add_services', $this->data);
  }

     public function ajax_edit($id)
    {
       $this->load->model('admin/shop_model');
      $data = $this->shop_model->get_user_id($id);

      echo json_encode($data);
    }

  function edituser()
  {
    if (!admin_logged_in()) {
      redirect('admin');
    }
   $data = $this->input->post();
   unset($data['password']);
   $result = $this->db->where('id',$data['id'])->update('shop_signup',$data);

      if ($result) {
        set_flashdata('message', 'Updated Successfully', 'success');

      } else {
       set_flashdata('message', 'Failed to update', 'danger');

     }
      redirect('admin/shopuser');
  
  }
  function changepassword($id,$value)

  {
    if (!admin_logged_in()) {
      redirect('admin');
    }
    
    $result = $this->db->where('id',$id)->set('password',md5($value))->update('shop_signup');
      if ($result) {
         echo json_encode('TRUE');

      } else {
        echo json_encode('FALSE');

     }
    

  }
  function deleteAccount($value){

    if (!admin_logged_in()) {
      redirect('admin');
    }
        // debug($value);        
    if (isset($value)) {
      $result = $this->shop_model->deleteAccount($value);
      if ($result) {
        set_flashdata('message', 'Your Account is deleted successfully', 'danger');

      } else {
       set_flashdata('message', 'Failed to delete this account, please try to signup', 'danger');

     }
      redirect('shoplist');
   }
 }
 //admin booking delete
 function deleteAccountAdmin($value){

    if (!admin_logged_in()) {
      redirect('admin');
    }
        // debug($value);        
    if (isset($value)) {
      $result = $this->shop_model->deleteAccountAdmin($value);
      if ($result) {
        set_flashdata('message', 'Your Account is deleted successfully', 'danger');

      } else {
       set_flashdata('message', 'Failed to delete this account, please try to signup', 'danger');

     }
       redirect('admin/Bookinglist');
   }
 }
   function deleteservice($value){

    if (!admin_logged_in()) {
      redirect('admin');
    }
        // debug($value);        
    if (isset($value)) {
      $result = $this->shop_model->deleteservice($value);
      if ($result) {
        set_flashdata('message', 'Your service is deleted successfully', 'danger');

      } else {
       set_flashdata('message', 'Failed to delete this service', 'danger');

     }
      redirect('services');
   }
 }


 function deleteuser($value){

     echo $value;

  if (!admin_logged_in()) {
    redirect('admin');
  }
        // debug($_POST['userid']);        
  if (isset($value)) {
    $result = $this->shop_model->deleteuser($value);
    if ($result) {

      set_flashdata('message', 'Your Account is deleted successfully', 'danger');

    } else {
      set_flashdata('message', 'Failed to delete this account, please try to signup', 'danger');

    }
    redirect('admin/shopuser');

  }
}
 function approve($value){

    // echo $value;

  if (!admin_logged_in()) {
    redirect('admin');
  }
        // debug($_POST['userid']);        
  if (isset($value)) {
    $result = $this->shop_model->approveuser($value);
    if ($result) {

      set_flashdata('message', 'Your Account is Approved successfully', 'success');

    } else {
      set_flashdata('message', 'Failed to approve this account, please try to signup', 'danger');

    }
    redirect('admin/shopuser');

  }
}
 function disable($value){

    // echo $value;

  if (!admin_logged_in()) {
    redirect('admin');
  }
        // debug($_POST['userid']);        
  if (isset($value)) {
    $result = $this->shop_model->disableuser($value);
    if ($result) {

      set_flashdata('message', 'Your Account is disabled successfully', 'danger');

    } else {
      set_flashdata('message', 'Failed to disable this account, please try to signup', 'danger');

    }
    redirect('admin/shopuser');

  }
}



function shop() {

  $inclusions = array('validate', 'bootbox', 'datatable',"input_mask","fancybox");
  $this->data['inclusions'] = inclusions($inclusions);
  $this->data['page_title'] = "List fo shops";
  $response = $this->shop_model->shop();
  if($response){
    $this->data['result'] = $response;
  }
  load_backend_page('backend/shop/shoplist', $this->data);

}
function editshop() {

  $inclusions = array();
  $this->data['inclusions'] = inclusions($inclusions);
  $this->data['page_title'] = "Edit Shop";
  $this->load->helper('form');
  load_backend_page('backend/shop/editshop', $this->data);

}
//admin list
function editBookingListAdmin() {

  $inclusions = array();
  $this->data['inclusions'] = inclusions($inclusions);
  $this->data['page_title'] = "Edit Booking";
  $this->load->helper('form');
  load_backend_page('backend/shop/editBookingList', $this->data);

}

function edit_profile_admin($id)
{
    //echo "<pre>";print_r($id);exit;
      $post['service_type'] = $this->input->post('service_type');
      $post['date'] = $this->input->post('date');
      $post['time'] = $this->input->post('time');
      $post['remark'] = $this->input->post('remark');
      $post['status'] = $this->input->post('status');
      //$id = ['id'];
     
         $response = $this->db->where('id',$id)->update('bookings',$post); 
     
     
    if( $response == TRUE )
    {

      $this->session->set_flashdata('feedback','Updated Successully');

      $this->session->set_flashdata('feedback_class', 'alert-success');
      redirect('admin/Bookinglist');

    }
    else
    {
      $this->session->set_flashdata('feedback','Failed  to UPdate ');

      $this->session->set_flashdata('feedback_class', 'alert-danger');
      redirect('admin/Bookinglist');
    }
  
    
}

function edit_profile()
{
  $this->load->library('form_validation');
    $config = [

    'upload_path' =>    './uploads',

    'allowed_types' =>    'jpg|png|jpeg',

    ];

    $this->load->library('upload', $config);
    $this->form_validation->set_rules('name','Name','required');
    
    $this->form_validation->set_error_delimiters("<p class='text-danger'>","</p>");
   
    if($this->upload->do_upload('picture') || $this->form_validation->run()){

      $post = $this->input->post();
      $post['days'] = json_encode($this->input->post('days'));
      $post['service_offered'] = json_encode($this->input->post('service_offered'));
      $id = $post['id'];
      unset($post['shop_user']);
      unset($post['id']);
      unset($post['submit']);
      $data = $this->upload->data();
      $image_path =  $data['raw_name'] . $data['file_ext'];
      $post['image'] = $image_path;
      if($post['days']){}else{ unset($post['days']); }
      if($post['image'])
      {

      }else
      {
        unset($post['image']);
      }
    if(count($id))
    {
      
      $response = $this->db->where('id',$id)->update('shop_list',$post);
    }
    else
    {
     
     
    }

    if( $response == TRUE )
    {

      $this->session->set_flashdata('feedback','Updated Successully');

      $this->session->set_flashdata('feedback_class', 'alert-success');
      redirect('shoplist');
    }
    else
    {
      $this->session->set_flashdata('feedback','Failed  to UPdate ');

      $this->session->set_flashdata('feedback_class', 'alert-danger');
      redirect('shoplist');
    }
    }
    else
    {
      $this->session->set_flashdata('feedback','Failed  to Upload Details ');

      $this->session->set_flashdata('feedback_class', 'alert-danger');

      redirect('shoplist');
    }
}
function services() {

  $inclusions = array('validate', 'bootbox', 'datatable',"input_mask","fancybox");
  $this->data['inclusions'] = inclusions($inclusions);
  $this->data['page_title'] = "Services";
  $response = $this->shop_model->services();
  if($response){
    $this->data['result'] = $response;
  }
  load_backend_page('backend/shop/services', $this->data);

}
function feedback() {
  $inclusions = array('validate', 'bootbox', 'datatable',"input_mask","fancybox");
  $this->data['inclusions'] = inclusions($inclusions);  
  $this->data['page_title'] = "Feedback";
  $response = $this->shop_model->feedback();
  if($response){
    $this->data['result'] = $response;
  }
  load_backend_page('backend/shop/feedback', $this->data);

}
function addservice()
{
    $name =  $this->input->post();
    unset($name['submit']);
     if($this->db->insert('services',$name)){
        $message='Service is updated Successfully';
      }else{
        $message='Shop is failed to add ';
      }
      set_flashdata('message',$message, 'info');
    redirect('services');
}

function addShop() {
  $url = upload('', $_FILES["attachment"]);
  $mobile = unmask_mobile($_POST['mobileno']);
  $request_array = array(
    "name"=>$_POST['name'],
    "mobileno"=>$mobile,
    "website"=>$_POST['website'],
    "about_me"=>$_POST['about_me'],
    "profile_pic"=>$url,
    "user_address"=>$_POST['street'],
    "city"=>$_POST['city'],
    "state"=>$_POST['state'],
    "postalcode"=>$_POST['postalcode'],
    "country"=>$_POST['country'],
    "lat"=>$_POST['lat'],
    "lon"=>$_POST['lon'],
    "service_offered"=>$_POST['service_offered'],
  "days"=>$_POST['days'],
  "parking"=>$_POST['parking'],
  "type"=>$_POST['type'],
  "closing_time "=>date("Y-m-d h:i:s a", strtotime($_POST['closing_time '])),
  "opening_time"=>date("Y-m-d h:i:s a", strtotime($_POST['opening_time'])),
  
    'is_created_by_admin'=>1,'id'=>custom_decode($_POST['userid']),
    'userid'=>NULL);

// print_r(date("Y-m-d h:i:s a", strtotime($_POST['closing_time '])));
// die();
    // echo $url;
    // debug($request_array);


  if (isset($_POST['mobileno'])) {
    $result = $this->shop_model->addShop($request_array);
    if($result) {

      if(!empty($request_array['id'])){
        $message='Shop is updated Successfully';
      }else{
        $message='Shop is added Successfully';
      }
      set_flashdata('message',$message, 'info');
    }else{
      set_flashdata('message', "Failed to add new shop, please try again", 'danger');
    }
    redirect('shoplist');
  }
}


function adduser(){

 $url = upload('', $_FILES["attachment"]);
 $mobile = unmask_mobile($_POST['mobileno']);
 $request_array = array(
  "name"=>$_POST['email'],
  "mobileno"=>$mobile,
  "password"=>$_POST['password'],
  'is_created_by_admin'=>1);

 $result = $this->shop_model->addnewuser($request_array);
 if($result) {

  $message='New user is added Successfully';
  set_flashdata('message',$message, 'info');
}else{
  set_flashdata('message', "Failed to add new user, please try again", 'danger');
}
redirect('admin/shopuser');

}

}