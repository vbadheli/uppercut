<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

    public function __construct() {
        parent::__construct();
       // $this->load->model('admin/dashboard_model');
    }

    function index() {
        if (!admin_logged_in()) {
            redirect('admin');
        }
        // $response = getParamRecord(ADMIN_DASHBOARD, array());
        //debug($response, false);
        // if($response['status'] == 'success') {
        //     $this->data['total'] = $response['data'];
        // }
        $includes = array('validate', 'summernote', 'iCheck');
        $this->data['inclusions'] = inclusions($includes);
        $this->data['page_title'] = "Dashboard";
        //debug($this->data['total'], false);
        load_backend_page('backend/dashboard/index', $this->data);
    }

    function invite()
    {
        if(isset($_POST['submit'])) {
            $combine = array_combine($this->input->post('name'), $this->input->post('email'));
            $result = postParamRecord(EMAIL, $combine);
            if($result['status'] == 'success'){
                set_flashdata('message', 'Email successfully sent', 'success');
            }else{
                set_flashdata('message', $result['reason'], 'danger');
            }
            redirect('admin');
        }
    }

    function commissions() {
        
    }

}
