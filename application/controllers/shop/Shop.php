<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shop extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('signup_model');
		$this->load->model('shop_model');
		$this->load->model('Booking_model');
	}

	public function profile($name)
	{


		if(!empty($name))
		{

			$this->data['result'] = $this->db->where('name',urldecode($name))->get('shop_list')->row();
			if(user_logged_in()){
				$this->db->set('user_id',get_session('userid'))->set('shop_id',$this->data['result']->id)->insert('recent_view');
			}
			
			$this->data['result']->rating = $this->calculate_rating($this->data['result']->id); 
			
			$this->load->view('shop/shop_public',$this->data); 
		}
		else
		{
			redirect(base_url());
		}
	}
	function calculate_rating($id)
	{
		$total = 0;
		$data = $this->db->where('shop_id',$id)->get('feedback')->result();
		if(!empty($data[0]->id))
		{
			$i = 0;
			foreach($data as $r)
			{
				$i++;
				$total = $total + $r->rating;
			}

			$av = $total / $i  ;
			$av = number_format($av, 1, '.', '');
			return $av;
		}
		return 0;
		

	}
	function signup() {

		$id= $this->input->Post('email');		
		if (isset($id)) {
			
			if($this->db->where('email',$id)->get('shop_signup')->row()->email == "")
			{		

				$activation_code = random_code();
				$subject = 'Email Confirmation | UPPERCUT';
				$message = 'Click on this link to activate your Account:'.base_url().'auth/account/'.$_POST['email'].'/'.$activation_code.'';
				$emailresult = $this->sendEmail($_POST['email'],$subject,$message);
				$_POST['activation_code'] = $activation_code;
				$result = $this->signup_model->signup();
				//echo "<pre>";print_r($emailresult);exit;

				if($result){

					$this->session->set_flashdata('feedback','Account created successfully. Wait for an email to activate your account.');

					$this->session->set_flashdata('feedback_class', 'alert-success');
					redirect(base_url());
				}else {

					$this->session->set_flashdata('feedback','Email address is already registered with us. Please use a
						different email or try login.');

					$this->session->set_flashdata('feedback_class', 'alert-danger');
					redirect(base_url());
				}	
			}
			else
			{
				$this->session->set_flashdata('feedback','Email address is already registered with us. Please use a
					different email or try login.');

				$this->session->set_flashdata('feedback_class', 'alert-danger');
				redirect(base_url());
			}
		}else

		{
			$this->session->set_flashdata('feedback','Invalid email.');

			$this->session->set_flashdata('feedback_class', 'alert-danger');
			redirect(base_url());
		}
		
	}

	function usersignup() {

		if (isset($_POST['email'])) {
			if($this->db->where('email',$_POST['email'])->get('shop_signup')->row()->email == "")
			{

				$activation_code =  mt_rand(1000, 9999);
				$subject = 'Email Confirmation | UPPERCUT';
				$message = 'Your Activation Code is:'.$activation_code.'';
				$emailresult = $this->sendEmail($_POST['email'],$subject,$message);
				$_POST['activation_code'] = $activation_code;
				$result = $this->signup_model->usersignup();

				if($result ){

					$this->session->set_flashdata('feedback','Check your registered email and enter the code here');

					$this->session->set_flashdata('feedback_class', 'alert-success');
					redirect(base_url('shop/shop/authenticate'));
				}else {

					$this->session->set_flashdata('feedback','Email address is already registered with us. Please use a
						different email.');

					$this->session->set_flashdata('feedback_class', 'alert-danger');
					redirect(base_url());
				}	
			}
			else
			{
				$this->session->set_flashdata('feedback','Email address is already registered with us. Please use a
					different email.');

				$this->session->set_flashdata('feedback_class', 'alert-danger');
				redirect(base_url());
			}
		}else

		{
			$this->session->set_flashdata('feedback','Invalid Email.');

			$this->session->set_flashdata('feedback_class', 'alert-danger');
			redirect(base_url());
		}
		
	}

	function login() {

		if (isset($_POST['email'])) {
			$result = $this->signup_model->login();
			if ($result) {
				$usertype=get_session('type');
				if($usertype=="owner")
				{
					// redirect('shop/shop/shopbookinglist/all');
					redirect('shop/shop/saloncongrats');
				}
				redirect('/');
			} else {
				set_flashdata('message', "Oops! Your username and password didn't match.", 'danger');
				redirect('login');
			}
		} 
	}
	public function authenticate()
	{
		
		
		if(get_session('code') != "") { 
			$this->load->view('shop/authenticate'); 
		}else
		{
			redirect(base_url());
		}
		
	}
	public function rating()
	{
		if(user_logged_in()){
			$this->load->helper('form');
			$this->load->view('shop/rating');
		}
		else
		{
			redirect(base_url());
		}
	}
	public function feedback($id)
	{
		if(user_logged_in()){
			$this->load->helper('form');
	       $recent = $this->db->where('shop_id',$id)->order_by('id','DESC')->get('feedback')->result(); 
	       
	       if(!empty($recent))
	       {
	       	$this->load->view('shop/feedback',['recent'=>$recent]);
	       }else
	       {
	       	redirect(base_url());
	       }
			
		}
		else
		{
			
		}
	}
	public function createsaloon()
	{

		if($this->input->post('1') != "" && $this->input->post('2') != "" && $this->input->post('3') != "" && $this->input->post('4') != "")
		{

			$email = $this->input->post('email'); 
			$result = $this->db->where('email',get_session('email'))->get('shop_signup')->row();
			$string = $this->input->post('1').$this->input->post('2').$this->input->post('3').$this->input->post('4');
			
			$id = $result->id;
			$type = $result->type;
			$code =$result->activation_code;
			if($result->activation_code == $string)
			{
			    $dd =  $this->db->set('verified_status',1)->where('email',get_session('email'))->update('shop_signup');
			    
				$session_data = array(
					'userid' => $id,
					'email'=>$email,
					'type' => $type,
					'logged_in' => 1
				); 

				set_sessions($session_data);
				redirect('shop/shop/membership'); 
			}else
			{
				$this->session->set_flashdata('feedback','Incorrect code');

				$this->session->set_flashdata('feedback_class', 'alert-danger');

				if(get_session('code') != "") {
					$this->load->view('shop/authenticate'); 
				}else
				{
					redirect(base_url());
				}
			}

			
		}else
		{
			$this->session->set_flashdata('feedback','Invalid email.');

			$this->session->set_flashdata('feedback_class', 'alert-danger');

			if($this->session->get_session('code') != "") {
				$this->load->view('shop/authenticate'); 
			}else
			{
				redirect(base_url());
			}
		}
		
	}
	public function update_user()
	{

		if($this->input->post('username') != "" && $this->input->post('email') != "" && $this->input->post('phone') != "" )
		{
			$username = $this->input->post('username');
			$email = $this->input->post('email');
			$phone = $this->input->post('phone');
			$id = get_session('userid');

			if($this->db->set('username',$username)->set('email',$email)->set('phone',$phone)->where('id',$id)->update('shop_signup'))
			{
				$data = $this->db->where('id',$id)->get('shop_signup')->row();

				$session_data = array(

					'userid' => $id,

					'email'=>$data->email,
					'type' => $data->type,
					'code' => $data->code,

					'logged_in' => 1

				); 

				set_sessions($session_data);
				$this->session->set_flashdata('feedback','Profile updated');

				$this->session->set_flashdata('feedback_class', 'alert-success');
				redirect('profile');
			}else
			{
				$this->session->set_flashdata('feedback','Profile updation failed');

				$this->session->set_flashdata('feedback_class', 'alert-danger');
				redirect('profile');
				
			}

			
		}else
		{
			$this->session->set_flashdata('feedback','Profile updation failed');

			$this->session->set_flashdata('feedback_class', 'alert-danger');
			redirect('profile');
		}
		
	}
	public function MembershipData()
	{
		$data = $_POST;		
		foreach ($data['choose'] as $key => $package) {
			$expiry_date_str = null;
			if($data['spnperiod'][$key] != 0)
			{
				$expiry_date = date_add( date_create( date("Y-m-d", time())), date_interval_create_from_date_string($data['spnperiod'][$key]));
				$expiry_date_str = $expiry_date->format("Y-m-d H:i:s");
			}
			$request_array = array(						
				"package_id"=> $data['txtid'][$key],
				"validity_period"=> $data['spnperiod'][$key],
				"prices"=> $data['spnprice'][$key],
				'expiry_date'=> $expiry_date_str,
				'created_on'=>date('Y-m-d H:i:s',time()),
				'user_id'=>get_session('userid'));			
			
			// echo "<pre>"; var_dump($request_array);exit;
			$result = $this->Booking_model->MembershipData($request_array);			
			if($result) {
				$message='Membership created successfully';
				set_flashdata('message',$message, 'info');
			} else{
				set_flashdata('message', "Failed to add new shop, please try again", 'danger');
			}
		}
		redirect('shop/shop/saloonprofilenew');
	}

	public function saloonprofilenew()
	{
		$this->load->view('shop/paymentsuccessful');
	}
	public function saloonprofile()
	{
		$this->load->view('shop/saloonprofile');
	}
	public function userprofile()
	{
		$this->load->view('shop/userprofile');
	}
	
	public function membership()
	{
		$this->load->model('Booking_model'); 		
     	$data['result'] = $this->Booking_model->MembershipStatus();
     	$this->load->view('shop/membership', $data);
	}

	public function activity()
	{
		$this->load->view('shop/activity');
	}
	public function setting()
	{
		$this->load->view('shop/setting');
	}

	public function saloonlist()
	{
		$this->load->model('signup_model'); 
      $this->data['obj'] = $this->signup_model;
		$this->load->view('shop/saloonlist', $this->data);
	}
	public function editprofile(){
		$this->load->helper('form');
		$this->load->view('shop/editprofile', $this->data);
	}
	function resetPass() {

		$data = $this->input->post();
		$email = $data['email'];
		
		$activation_code = random_code();
		// debug($data);
		
		$ans = $this->db->where('email',$email)->get('shop_signup');
		if($ans->num_rows() > 0){
		$mail = $this->signup_model->change_dd($_POST['email'],$activation_code);
		$activation_code = $this->db->where('email',$_POST['email'])->get('shop_signup')->row()->activation_code;
		// sending forgot password email
		$subject = 'Reset Password | UPPERCUT';
		$message = 'Click on this link to activate your Account:'.base_url().'auth/reset_Password/'.$_POST['email'].'/'.$activation_code.'';
			$result = $this->sendEmail($_POST['email'],$subject,$message);
			if ($result) {
				echo "Check your email";
			} 
			else
			{
				echo "Failed to reset the password";
			}
		}
		else{
			echo "Can't Find Your Email";
		}
		
		
		
	}

	function changepass() {
		
		if(user_logged_in()){
			$data=array('email',get_session('email'));
			array_pop($_POST);
			array_push($_POST['email']=get_session('email'));
		}

		if (isset($_POST['email'])) {

			$result = $this->signup_model->change_password();

			if ($result) {
				$output = array(
					'error' => false,
					'message' =>"This email is present"
				);
			} else {
				$output = array(
					'error' => true,
					'message' => 'This email does not exist, please try signup'
				);
			}
			json_output(json_encode($output));
		} 
	}
	public function edituser($id)
	{
		$data = $this->db->where('id',$id)->get('shop_signup')->row();
		$this->load->view('shop/edituser',['data'=>$data]);
	}
	public function change_pass()
	{
		
		$old = md5($this->input->post('password'));
		$match = $this->db->where('id',get_session('userid'))->get('shop_signup')->row()->passowrd;
		if($old == $match && $this->db->set('password',md5($this->input->post('newpassword')))->where('id',get_session('userid')->update('shop_signup')))
		{

			$this->session->set_flashdata('feedback','Updated successfully');

			$this->session->set_flashdata('feedback_class', 'alert-success');
			redirect('newpass');
		}
		else
		{
			$this->session->set_flashdata('feedback','Passowrd not matching');

			$this->session->set_flashdata('feedback_class', 'alert-danger');
			redirect('newpass');
		}
		
	}
	public function uploadimage()
	{
		$config = [

			'upload_path'	=>		'./uploads',

			'allowed_types'	=>		'jpg|png|jpeg',

		];

		$this->load->library('upload', $config);
		if($this->upload->do_upload('picture')){
			$post = $this->input->post();
			unset($post['submit']);
			$data = $this->upload->data();
			$image_path =  $data['raw_name'] . $data['file_ext'];
			$post['image'] = $image_path;
			$id = $this->db->where('shop_user',get_session('userid'))->set('image',$post['image'])->update('shop_list');
			if($id)
			{
				$this->session->set_flashdata('feedback','Updated successully');

				$this->session->set_flashdata('feedback_class', 'alert-success');
				redirect('editprofile');
			}
			else
			{
				$this->session->set_flashdata('feedback','Failed to update ');

				$this->session->set_flashdata('feedback_class', 'alert-danger');
				redirect('editprofile');
			}
		}else{
			$this->session->set_flashdata('feedback','Failed to update ');

			$this->session->set_flashdata('feedback_class', 'alert-danger');
			redirect('editprofile');
		}
	}
	public function update_saloon($id)
	{
		if(user_logged_in()) {
			if(!empty($this->input->post('name') && $this->input->post('about_me')))
			{


				$config = [

					'upload_path' =>    './uploads',

					'allowed_types' =>    'jpg|png|jpeg',

				];

				$this->load->library('upload', $config);
				$post = $this->input->post();
				
				if($this->upload->do_upload('picture')){
					$data = $this->upload->data();
					$image_path =  $data['raw_name'] . $data['file_ext'];
					$post['image'] = $image_path;
				}else
				{
					$post['image'] = $this->db->where('id',$id)->get('shop_list')->row()->image;
				}
					$post['service_offered'] = json_encode($post['service_offered']);
					$post['days'] = json_encode($post['days']);
					$post['parking'] = json_encode($post['parking']);
					$post['shop_user'] = get_session('userid');
					unset($post['submit']);
					if($this->db->where('id',$id)->update('shop_list',$post))
					{
						$this->session->set_flashdata('feedback','Updated successully');

						$this->session->set_flashdata('feedback_class', 'alert-success');
						redirect('shop/shop/saloonlist');
					}
					else
					{
						$this->session->set_flashdata('feedback','Oops, failed to update');

						$this->session->set_flashdata('feedback_class', 'alert-danger');
						redirect('shop/shop/saloonlist');
					}
				
			}else
			{
				$this->session->set_flashdata('feedback','Oops, failed to update');

				$this->session->set_flashdata('feedback_class', 'alert-danger');
				redirect('shop/shop/saloonlist');    
			}
		}else{

			set_flashdata('message', "Oops! Your username and password didn't match.", 'danger');

			redirect('login');

		}


	}
	public function edit_profile()
	{

		$this->load->library('form_validation');
		$config = [

			'upload_path'	=>		'./uploads',

			'allowed_types'	=>		'jpg|png|jpeg',

		];

		$this->load->library('upload', $config);
		$this->form_validation->set_rules('name','Name','required');
		
		$this->form_validation->set_error_delimiters("<p class='text-danger'>","</p>");
		$id = $this->db->where('shop_user',get_session('userid'))->get('shop_list')->row();
		if($this->upload->do_upload('picture') || $this->form_validation->run()){

			$post = $this->input->post();
			$post['days'] = json_encode($this->input->post('days'));
			$post['service_offered'] = json_encode($this->input->post('service_offered'));
			unset($post['submit']);
			$data = $this->upload->data();
			$image_path =  $data['raw_name'] . $data['file_ext'];
			$post['image'] = $image_path;
			if($post['days']){}else{ unset($post['days']); }
			if($post['image'])
			{

			}else
			{
				unset($post['image']);
			}
			if(count($id))
			{

				$response = $this->db->where('shop_user',get_session('userid'))->update('shop_list',$post);
			}
			else
			{
				$post['shop_user'] = get_session('userid');
				$response = $this->db->insert('shop_list',$post);
			}

			if( $response == TRUE )
			{

				$this->session->set_flashdata('feedback','Updated successully');

				$this->session->set_flashdata('feedback_class', 'alert-success');
				redirect('editprofile');
			}
			else
			{
				$this->session->set_flashdata('feedback','Failed to update ');

				$this->session->set_flashdata('feedback_class', 'alert-danger');
				redirect('editprofile');
			}
		}
		else
		{
			$this->session->set_flashdata('feedback','Failed to upload details ');

			$this->session->set_flashdata('feedback_class', 'alert-danger');

			redirect('editprofile');
		}
	}
	public function send(){
		$emailid="saadmirza009@gmail.com";
		$message="this is test email";
		$result = $this->sendEmail($emailid, $message);
		print_r($result);
		
	}
	public function send_mail_to()
	{
		$code	= $this->input->post('name');
		$email	= $this->input->post('email');
		$subject = 'Email Confirmation | UPPERCUT';
		$message = 'Your Activation Code is:'.$code.'';
		$emailresult = $this->sendEmail($email,$subject,$message);
		if($emailresult)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
		
	}
	public function sendEmail($emailid,$subject,$message){   
  // Email configuration
		$config = Array(
			'protocol' => 'smtp',
			'smtp_host' => 'mail.uppercut.shop',
		'smtp_user' => 'hello@uppercut.shop', // change it to yours
	     'smtp_pass' => 'iNh41r~w371;', // change it to yours
	     'smtp_port' => 25,
	     'mailtype' => 'html',
	     'charset' => 'iso-8859-1',
	 ); 
		
		$this->load->library('email', $config);
		$this->email->from('hello@uppercut.shop', "UPPERCUT");
		$this->email->to($emailid);
		$this->email->set_newline("\r\n");
		$this->email->subject($subject);
		$this->email->message($message);		
		if($this->email->send()  == TRUE){     
			return TRUE;
		} 
		else{
			return FALSE;
		} 
	}
	function deleteAccount(){

		$reason = $this->input->post()['reason'];
		$date = $this->input->post()['date'];

		if(user_logged_in()){
			$result = $this->signup_model->deleteAccount(get_session('userid'));
			if ($result) {
				$this->db->set('name',$result->username)->set('email',$result->email)->set('number',$result->phone)->set('reason',$reason)->set('date',$date)->insert('deleted_account');
				unset_admin_sessions();
				$this->session->sess_destroy();
				$output = array(
					'error' => false,
					'message' =>"Your account has been deleted successfully"
				);
			} else {
				$output = array(
					'error' => true,
					'message' => 'Failed to delete this account.'
				);
			}

			json_output(json_encode($output));
		}
		else
		{
			redirect('home');
		}


	}

	function addShop()
	{		
		// debug($_POST);
		$url = upload('', $_FILES["attachment"]);
		$mobile = $_POST['contact'];
		$request_array = array(
			"name"=>$_POST['shopname'],
			"mobileno"=>$mobile,
			"website"=>$_POST['website'],
			"about_me"=>$_POST['about_me'],
			"profile_pic"=>$url,
			"user_address"=>$_POST['street'],
			"city"=>$_POST['city'],
			"state"=>$_POST['state'],
			"postalcode"=>$_POST['postalcode'],
			"country"=>$_POST['country'],
			"lat"=>$_POST['lat'],
			"lon"=>$_POST['lon'],
			'is_created_by_admin'=>0,
			'userid'=>get_session('userid'),
			'id'=>custom_decode($_POST['userid']));
		// echo $url;
		// debug($request_array);
		if (isset($_POST['contact'])) {
			$result = $this->shop_model->addShop($request_array);
			if($result) {

				if(!empty($request_array['id'])){
					$message='Shop details updated successfully';
				}else{
					$message='Shop added successfully';
				}
				set_flashdata('message',$message, 'info');
			}else{
				set_flashdata('message', "Failed to add new shop, please try again", 'danger');
			}
			redirect('addshop');
		}
	}



	function logout() {
		unset_admin_sessions();
		$this->session->sess_destroy();
		redirect('home');
	}
	//service booking
	public function service_booking($id)
	{
		//echo $id; exit;
		$data['id']=$id;
		if(user_logged_in()){
			//$this->load->helper('form');
			$this->load->view('shop/service_booking',$data);
		}
		else
		{
			redirect(base_url());
		}
	}
	 function sbooking() {	 	
	 		$request_array = array(			
			"salonid"=>$_POST['salonid'],
			"service_type"=>$_POST['service_type'],
			"date"=>$_POST['date'],
			"time"=>$_POST['time'],
			'created_at'=>date('Y-m-d H:i:s',time()),			
			'userid' => get_session('userid'));
		if (isset($_POST['salonid'])) {
			$result = $this->Booking_model->addBooking($request_array);			
			if($result) {

				if(!empty($request_array['salonid'])){
					$message='Booking details updated successfully';
				}else{
					$message='Booking added successfully';
				}
				set_flashdata('message',$message, 'info');
			}else{
				set_flashdata('message', "Failed to add new shop, please try again", 'danger');
			}
			redirect('shop/shop/mybooking');
		}
	}
	function sbookingajax() {	 	
	 		$request_array = array(			
			"salonid"=>$_POST['salonid'],
			"service_type"=>$_POST['service_type'],
			"date"=>$_POST['date'],
			"time"=>$_POST['time'],
			'created_at'=>date('Y-m-d H:i:s',time()),			
			'userid' => get_session('userid'));
			$result=0;
		if (isset($_POST['salonid'])) {
			$result = $this->Booking_model->addBookingajax($request_array);						
			
			//redirect('shop/shop/mybooking');
		}
			echo $result;
	}
	//booking list Admin 
	function sbookingAdmin() {	 	
	 		$request_array = array(			
			"salonid"=>$_POST['salonid'],
			"service_type"=>$_POST['service_type'],
			"date"=>$_POST['date'],
			"time"=>$_POST['time'],
			"remark"=>$_POST['remark'],
			'created_at'=>date('Y-m-d H:i:s',time()),			
			'userid' => 1);
			//echo "<pre>";print_r($request_array);exit;
		if (isset($_POST['salonid'])) {
			$result = $this->Booking_model->addBookingAdmin($request_array);			
			if($result) {

				if(!empty($request_array['salonid'])){
					$message='Booking details updated successfully';
				}else{
					$message='Booking added successfully';
				}
				set_flashdata('message',$message, 'info');
			}else{
				set_flashdata('message', "Failed to add new shop, please try again", 'danger');
			}
			redirect('admin/Bookinglist');
		}
	}

	function shopbookinglist($type) {

		$this->load->model('Booking_model'); 
		if($type =='all')
     		$data['result'] = $this->Booking_model->BookingAcceptReject();
     	else if($type =='upcoming')
     		$data['result'] = $this->Booking_model->getUpcomingBookings();
     	else if($type =='ongoing')
     		$data['result'] = $this->Booking_model->getOnGoingBookings();

     	// echo "<pre>"; print_r($data);exit;
			$this->load->view('shop/shopbookinglist', $data);
	
	}
	function shopbookingrecords() {

		$this->load->model('Booking_model'); 		
     	$result=$this->data['obj'] = $this->Booking_model->BookingRecords();
		$this->load->view('shop/shopbookingrecords', $result);	
	}

	function ShopbookingAcceptReject($id) 
	{	
	
	  	$data['id']=$id;
		if(user_logged_in()){
		
			$this->load->view('shop/ShopbookingAccept',$data);
		}
		else
		{
			redirect(base_url());
		}
	}
	function shopBookingAccept($id) {
		//echo "<pre>";print_r($id);exit;
			$request_array = array(
	 		"id"=>$id,
	 		'updated_at'=>date('Y-m-d H:i:s',time()),			
			'status' => 1,
			'remark'=>'Confirm Your Booking');
			$result = $this->Booking_model->shopBookingAccept($request_array);
			$data['divbookingaccept'] = 3;		
			if($result) {				
				if(!empty($request_array['id'])){
					$message='Booking details updated successfully';
				}else{
					$message='Booking added successfully';
				}
				set_flashdata('message',$message, 'info');
			}else{
				set_flashdata('message', "Failed to add new shop, please try again", 'danger');
			}
			$this->load->view('shop/homesalon',$data);
			//redirect('shop/shop/homesalon',$data);
		}
	
	function ShopbookingReject($id) 
	{	
	  	$data['id']=$id;
	  	$data['divbookingreject'] = 4;
	  	// echo "<pre>";print_r($id);exit;
		if(user_logged_in()){
		
			$this->load->view('shop/homesalon',$data);
		}
		else
		{
			redirect(base_url());
		}	  	

	}
	function ShopbookingRejectF($id) {
	 		$remark =$_POST['choose'];
	 		if($remark=='Other')
			{
				$remark=$_POST['txtreason'];
			}
	 		$request_array = array(			
			"id"=>$id,
			'remark'=> $remark,						
			'updated_at'=>date('Y-m-d H:i:s',time()),						
			'status' => 2);
			//echo "<pre>";print_r($request_array);exit;		
			$result = $this->Booking_model->shopBookingRejectF($request_array);
			$data['divbookingaccept'] = 3;
			//$data1['id'] = $id;						
			if($result) {

				if(!empty($request_array['id'])){
					$message='Booking details updated successfully';
				}else{
					$message='Booking added successfully';
				}
				set_flashdata('message',$message, 'info');
			}else{
				set_flashdata('message', "Failed to add new shop, please try again", 'danger');
			}
			$this->load->view('shop/homesalon',$data);
			//redirect('shop/shop/bookingsuccessfully',$data);
		
	}
	public function mybooking()
	{
		if(user_logged_in()){
			
			$this->load->model('Booking_model'); 		
     		$data['result'] = $this->Booking_model->mybookingM();
			$this->load->view('shop/mybooking', $data);
		}
		else
		{
			redirect(base_url());
		}
	}
	
	public function bookingStatus($id)
	{
		if(user_logged_in()){
			
			$this->load->model('Booking_model'); 		
     		$bookingdata = $this->Booking_model->BookingStatus($id);
     		$data['result']= $bookingdata;
     		//echo "<pre>";print_r($bookingdata->userid);exit;
     		$data['userdata'] = $this->db->where('id',$bookingdata->userid)->get('shop_signup')->row(); 

     		if($bookingdata != null && $bookingdata->status == 1)
     		{
     			$this->load->view('shop/BookingConfirmed', $data);
     		}
     		else if($bookingdata != null && $bookingdata->status == 2)
     		{
				$this->load->view('shop/BookingCancelled', $data);
     		}
     		else if($bookingdata != null && $bookingdata->status == 0)
     		{
				// $this->load->view('shop/BookingAwaiting', $data);
				$this->load->view('shop/BookingCancelled', $data);
     		}
     		else
     			redirect(base_url());
		}
		else
		redirect(base_url());
	}
	function saloncongrats() {
		$data['result'] = $this->Booking_model->getOnGoingBookings();
		$this->load->view('shop/homesalon',$data);
	}
	function bookingsuccessfully() {
		
		$this->load->view('shop/bookingsuccessfully');	
	}
	function adminactivesalonprofile() {
		
		$this->load->view('shop/adminactivesalonprofile');	
	}
	
	
	//test booking function start
	function testbookingrequest() {

		$d = new DateTime('+1day');
		$tomorrow = $d->format('Y/m/d h.i.s');
	 		$request_array = array(			
			"salonid"=> get_session('salonid'),
			"service_type"=>'Mens Hair Cut',
			"date"=>$tomorrow,
			"time"=>'2 PM',
			'created_at'=>date('Y-m-d H:i:s',time()),			
			'userid' => -1);
			 //echo "<pre>";print_r($request_array);exit;
	 		//$userdata = $this->db->where('status',0)->where('userid',-1)->where('salonid',get_session('salonid'))->get('bookings')->row();
			//if($userdata->salonid!=get_session('salonid') && $userdata->status!= 0 && $userdata->userid!= -1)
			$result = $this->Booking_model->addBooking($request_array);
			$data['divtestbooking'] = 2;			
			$insert_id = $this->db->insert_id();
			$data['insert_id'] = $insert_id;
			
			//echo "<pre>";print_r($data);exit;			
			if($result) {
				if(!empty($request_array['salonid'])){
					$message='Booking details updated successfully';
				}else{
					$message='Booking added successfully';
				}
				set_flashdata('message',$message, 'info');
			}else{
				set_flashdata('message', "Failed to add new shop, please try again", 'danger');
			}
			//redirect('shop/shop/saloncongrats',$data);
			//shop/shop/ShopbookingAcceptReject/.$insert_id,
			$this->load->view('shop/homesalon',$data);
	}	
	//test booking function end
	public function NotificationAlert()
	{
			$this->load->model('Booking_model'); 		
     		$data= $this->Booking_model->Notification();
			echo $data;
		
	}
	public function loaddemo()
	{
		$this->load->view('shop/loaddemo');
	}
}
