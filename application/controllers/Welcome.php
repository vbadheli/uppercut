<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {

        $this->load->view('index');
    }

    public function error_404()
    {
        $this->load->view('404', array(
            'heading' => 'Error 404',
            'message' => 'Page not found!'
        ));
    }
    
    public function newfunction()
	{
	$this->load->view('/wip/1_98');
     }
}
