<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends MY_Controller {

	public function __construct() {
		parent::__construct();
   $this->load->model('home_model');
 }

 public function index() {

    // debug($_POST);

   $request= $this->input->post();
  //echo "<pre>";print_r($request);exit;

   if(!empty($request['search']) || !empty($request['lat']))
   {
    if(!empty($request['lat']))
    {
      $request['search'] = $this->Get_Address_From_Google_Maps($request['lat'], $request['lon']);
     // echo "<pre>";print_r($request);exit;
      if(!empty($request['search'])){

        $result = $this->home_model->search_result_lt($request['search']);
        

      }else
      {
        set_flashdata('message', 'Snap! enter a location or choose your own location to find.', 'danger');
        return redirect(base_url());
      }
      
    }
    else
    {
      $result = $this->home_model->search_result($request['search']);
      
    }



    if(!empty($result) && count($result)>0){   
      $result['search']=$request['search'];
      $this->session->set_userdata('search', $result['search']); 
      $this->data['result'] = $result;
      $this->load->model('signup_model'); 
      $this->data['obj'] = $this->signup_model;
      $this->load->view('shop/search_result', $this->data);

    }else{
     set_flashdata('message', 'Snap! we didnt find any salon near you. Try typing a different location in the search field.', 'danger');

     return redirect(base_url());
   }
 }
 else{
   set_flashdata('message', 'Snap! enter location or choose your own location to find.', 'danger');
   return redirect(base_url());
 }
}

public function service($name,$lat,$lon)
{
  $name = urldecode($name);
  if(!empty($lat))
  {
    $address = $this->Get_Address_From_Google_Maps($lat, $lon);
  }else
  {
    $address = "";
  }
  $result = $this->home_model->search_service($name,$address);
  if(!empty($result) && count($result)>0){   
    $name=$name;
    $this->session->set_userdata('search', $name); 
    $this->data['result'] = $result;
    $this->load->model('signup_model'); 
    $this->data['obj'] = $this->signup_model;
    $this->load->view('shop/search_result', $this->data);

  }else{
   set_flashdata('message', 'Snap! we didnt find any salon near you. Try typing a different location in the search field.', 'danger');

   return redirect(base_url());
 }
}
public function error_404() {
  $this->load->view('404', array(
    'heading' => 'Error 404',
    'message' => 'Page not found!'
  ));
}
public function Get_Address_From_Google_Maps($lat, $lon) {

  $url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$lon&sensor=false&key=AIzaSyA8G1mE0PUuoN79b3iqE1PB27H0IisuCAg";

// Make the HTTP request
  $data = @file_get_contents($url);

// Parse the json response
  $jsondata = json_decode($data);
  $status = $jsondata->status;

  if($status=="OK")
    return $jsondata->results[0]->formatted_address;
  else
    return false;
}
public function service_booking()
{
    $this->load->view('shop/service_booking');
    //$name = urldecode($name);
    /*if(!empty($lat))
    {
      $address = $this->Get_Address_From_Google_Maps($lat, $lon);
    }else
    {
      $address = "";
    }
    $result = $this->home_model->search_service($name,$address);
       if(!empty($result) && count($result)>0){   
      $name=$name;
      $this->session->set_userdata('search', $name); 
      $this->data['result'] = $result;
      $this->load->model('signup_model'); 
      $this->data['obj'] = $this->signup_model;
      $this->load->view('shop/search_result', $this->data);
*/
    //}else{
      // set_flashdata('message', 'Snap! we didnt find any salon near you. Try typing a different location in the search field.', 'danger');
      
     // return redirect(base_url());
    //}
}

    public function searchCust()
    {
        
      $request=$_POST['phone'];
      //$this->input->post();
        //print_r($request);
        //exit;
        
      if(!empty($request)){

        $result['data']= $this->home_model->search_customer($request);
        
          $this->load->view('shop/searchCustomer',$result);

        

      }else
      {
        set_flashdata('message', 'Snap! enter a location or choose your own location to find.', 'danger');
        return redirect(base_url());
      }
      
    
    }


}
