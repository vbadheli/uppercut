<?php $this->load->view('public_header'); ?>
<style >
  .left-nav {
    position: fixed;
    overflow: hidden;
    width: 295px;
    z-index: 3333;
    height: 100%;
    /* background: rgb(247, 247, 247); */
    /* background: rgba(255, 0, 0, 0.26); */
   }
   .hide-nav .bottom-3 {
    
  }
  .height100
  {
    height: 100% !important;
  }
  .height10{
    height: 10%;
   }
  .bottom-3
  {
    bottom: 16%;
  }
  
  
  /* Thulasi CSS */
.h3-mod {
	font-size: 18px;
	color: grey;
	font-weight: 100;
}  
  
.contact-p {
	padding-left: 25% ;
	padding-right: 10px;
	font-size: 12px;
	color: #464646;
}

.heading-1 {
	font-size: 24px !important;
	font-weight: 100;
	letter-spacing: -0.5px;	
	margin-bottom: 15px;
	margin-top: 60%;
	color: black;
	
}

.body-text {
	font-size: 12px;
	color: #888 !important;
	font-weight: 100;
	line-height: 16px;
	margin-bottom: 5px;
	padding-right: 5%;
}

.heading-4 {
	font-size: 13px !important;
	font-weight: 500;
	letter-spacing: 0px;	
}


/* Thulasi CSS ends */
  
  
   @media only screen and (max-width: 900px) {
   .left-nav {
    position: fixed;
    width: 100%;
    z-index: 3333;
    height: 12%;
   }
   .height10{
    height: 14%;
   }
   .mx-auto {
   padding-top: 59px;
}
   }
</style>

<div class="container">
        <div class="row">
        

              <div class="mx-auto">
                
                <div class="form-body mx-auto contact-p">
                <br>
                <h3 class="heading-1">About Us</h3>
                
                  <p class="body-text">Search nearby salon, spa & parlour <br><br>

Have you ever wasted hours waiting at the salon for your turn? I have. <br><br>

A project born out of a need to save time.<br><br>

Looking at the receptionist for nearly 90 mins and wondering when she will call my name. During those 90 minutes, I saw customers enquiring about a service and the wait time. Few left immediately after hearing that they have to wait for 45 mins to an hour to get a service. Some waited for 30mins and left. But few poor things like me waited patiently for my turn. Among them, I waited the longest 90 mins and didn’t get a service coz the receptionist forgot that I enquired about a service. How unfortunate but fortunate enough to kickstart this project.<br><br>

Didn't find what you were looking for? Let us know we will fix it. 

hello@uppercut.shop <br><br><br><br></p>

                </div>
              </div>
              <!--/col-->
            </div>
            <!--/row-->
          </div>
          <!--/col-->
        </div>
        <!--/row-->
      </div>

      <?php $this->load->view('layouts/footer'); ?>
      <script type='text/javascript' src='<?php echo base_url('assets/admin/js/backend/main.js'); ?>'></script>
    

