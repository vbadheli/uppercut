<?php $this->load->view('public_header'); ?>
<style >
.font-chg{
	font-family: 'Comfortaa', cursive;
}
.row{
	margin-left:0 !important;
}
.col-md-12 .row{
	padding-left:25px;
}
.note-head-wrap{
	position: absolute;
	top: 0;
	left: 0;
	right: 0;
	padding-bottom: 15px;
}
.note-head-arr{
	width: 55px;
	display: inline-block;
	float: left;
	padding-top: 18px;
	padding-left: 15px;
}
.note-head-arr span{
	color:#ffffff;
}
.note-head-text{
	width: calc(100% - 55px);
	text-transform: uppercase;
	color: #ffffff;
	display: inline-block;
	float: left;
	padding-top: 24px;
	font-size: 16px;
}
.note-head-text span{
	display: inline-block;
	width: 100%;
	font-size: 12px;
	color:#ffffff;
}
.bg-overlay{
	background: #aa00ff;
	position: absolute;
	top: 0;
	left: -15px;
	right:0;
	bottom:0;
	height: 540px;
	z-index: -1;
	border-radius: 25px;
}
.item-wrap{
	padding-top: 200px;
	padding-right: 5%;
}
.customer{
	font-size: 24px;
	color:#ffffff;
	margin-top:30px;
	font-weight: bold;
	text-transform: uppercase;
}
.customrid{
	font-size: 18px;
	color:#ffffff;
	margin-bottom: 30px;
}
.hair-cut{
	font-size: 18px;
	color:#ffffff;
}
.duration{
	font-size: 18px;
	color:#ffffff;
}
.date{
	font-size: 18px;
	color:#ffffff;
}
.time{
	font-size: 18px;
	color:#ffffff;
}
.controls{
	padding:20px 0;
}
.controls span{
	color: #ffffff;
	padding-right: 30px;
}
</style>

<?php //$this->load->view('open'); ?>

<div class="note-head-wrap">
	<div class="note-head-arr"><span class="fa fa-home fa-2x"></span></div>
	<div class="note-head-text font-chg">Booking Awaiting</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="row">

				<div class="bg-overlay">Booking Response Awaiting</div>
				<div class="item-wrap font-chg">
					<div class="oops-msg">
						Your request is awaiting saloon response
					</div>
					
					<div class="customer"><?php echo $userdata->username; ?></div>
					<!-- <div class="customrid">locality name</div> -->
					<div class="hair-cut"><?php echo $result->service_type; ?></div>
					<div class="duration">199 | 30 mins</div>
					<div class="date"><?php echo $result->date; ?></div>
					<div class="time"><?php echo $result->time; ?></div>
					<div class="controls">
						<a href="#"><span class="fa fa-map fa-2x"></span></a>
						<a href="#"><span class="fa fa-phone fa-2x"></span></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php //$this->load->view('close'); ?>

<?php //$this->load->view('layouts/footer'); ?>

