<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Shop Search Result</title>

  <link rel='shortcut icon' href='/uploads/favicon.ico' type='image/x-icon' />

  <meta name="viewport" content="width=device-width, minimum-scale=1.0" />
  <link href="<?php  echo base_url('assets/vendor/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
  <link href="<?php  echo base_url('assets/vendor/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600|Roboto:300" rel="stylesheet">
  <link href="<?php  echo base_url('assets/vendor/magnific-popup/magnific-popup.css'); ?>" rel="stylesheet">
  <link href="<?php  echo base_url('assets/css/style.css'); ?>" rel="stylesheet">
   <!--  Scroll bar for desktop version -->
  <link rel="stylesheet" media="screen and (min-width: 900px)" type="text/css" href="<?php  echo base_url('assets/css/scroll.css'); ?>">
  <link rel="stylesheet" media="screen and (min-width: 900px)" href="<?php  echo base_url(); ?>assets/css/desktop.css">
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-100486865-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments)};
    gtag('js', new Date());
    gtag('config', 'UA-100486865-1');
  </script>
  <style type="text/css">

  i {
    border: solid white;
    border-width: 0 3px 3px 0;
    display: inline-block;
    padding: 3px;
  }
  span.fa-angle-left {
    color: black;
    font-size: 24px;
    font-weight: 100;
    padding-top: 10px;
    padding-left: 5px;
    padding-right: 5px;
    
  }
  .toggleTab {
    width: 35px;
    /* background-color: #1f1e21; */
    height: 25px;
    position: absolute;
    top: 5px;
    left: 15px;
    z-index: 999999;
    cursor: pointer;
    display: inline-block;
  }
  .left {
    transform: rotate(135deg);
    -webkit-transform: rotate(135deg);
  }
  b, strong {
    font-weight: 500;
  }
  h1 {
    font-weight: 300;
    font-size: 42px;
    color: #4a4a4a;
    letter-spacing: 0px;
  }
  .round {
    border: 1px solid #9a9a9a !important;
    border-radius: 5px;
    padding-left: 15px;
    height: 68px;
  }
  .form-body .btn {
    border-radius: 100px;
  }
  .btn-green {
    background-color: #007bff;
  }
  .filter_search_area a
  {
    color: black;
  }
  .filter_search_area .active 
  {
    color:red;
  }
  .row {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: 0px;
    margin-left: 0px;
  }
  .brand-name {
    margin-bottom: .5rem;
    font-family: inherit;
    font-weight: 500;
    line-height: 1;
    color: grey;
    font-size: 18px;
    font-style: normal;
    margin-top: 12px;
    margin-left: -37px;
    	font-size: 12px;
	color: #878787 !important;
	font-weight: 100;
   	padding-top: 10px;
   	text-transform: uppercase;
  }
  .badge
  {
    vertical-align: top;
    margin-left: 6px;
    color: black;
  }
  .badge-info {
    color: black;
    background-color: #d3fb00;
  }
  .color-box
  {
    border:1px solid #d3fb00;
    border-radius: 50px;
  }
  .filter_search_area
  {
    margin-bottom: 25px;
  }
  .shop_list_area ul
  {
    list-style-type: none;
  }
  .shop_list_area ul li
  {
    border-bottom: 1px dashed grey;
    margin-bottom: 20px;
  }
  .color-box .col-4
  {
    padding: 8px;
    text-align: center;
    color: grey;
  }
  .span-color
  {
    background-color: #d3fb00;
    color: black;
    margin-right: 55%;
    padding: 0px 4px 0px 4px;
    font-size: 12px;
    border-radius: 2px;
  }
  a
  {
    color: black;
  }
  .greypara
  {
    color: grey;
    margin-top: 0;
    margin-bottom: 4px;
  }
  .form-check-input {
    position: inherit;
    margin-top: .25rem;
    margin-left: -20px;
  }
  .testimonial-group > .row1 {
  overflow-x: auto;
  white-space: nowrap;
  }
  .testimonial-group > .row1 > .col-11 {
    display: inline-block;
    float: none;
    margin-bottom: 15px;
  }
  
/* Thulasi CSS starts */
.c-padding {
	padding-left: 10%;
}

.pt-6 {
	padding-top: 20vh !important; 
	/* padding-bottom: 40% !important; */
}

.grey {
	font-size: 12px;
	color: #878787 !important;
	font-weight: 100;
}

.shop-name-h1 {
	font-size: 18px;
	font-weight: 500;
	margin-bottom: 3px;
	color: black;
	font-family: 'Comfortaa', open sans;
		
}

.text-info {
	font-size: 14px;
	color: #3D8DFB !important;	
}

.icon-h4 {
	font-size: 24px;
	margin-top: 0px;
}

.icon-h5 {
	font-size: 18px;
	padding-right: 5px;
	margin-top: 5px;
	padding-top: 5px;
}

/* Thulasi CSS ends */  
  
  
</style>
</head>
<body>
  <div class="boxing" id="boxing">
  <div id="loader"></div>
  <!-- Navbar starts from here -->
  <div class="left-nav">
    <div class="top-body">
      <div class="toggleTab">
       <a href="<?php echo base_url(''); ?>"><span class="fa fa-angle-left" aria-hidden="true"></span></a>
     </div>
     <span class="brand-name">
       <?= $this->session->userdata('search'); ?>
     </span>

   </div>


 </div>
 <!-- Navbar ends here -->
 <div class="container">
  <div class="">
         
        <br>
        <Br>
        <Br>
        <!-- Original Code Body starts from here -->

        <div class="testimonial-group">

          
          <div class="row1" style="padding: 0;display: block;">
            <Br>
            <Br>
            <?php foreach($result as $dt){ ?>
            <?php if(!empty($dt->name)){ ?>

            <div class="col-11 pt-6" >
              <span style="color: black; font-size: 13px;" class="locality greypara grey"><i style="color: #40e111;" class="fa fa-star" aria-hidden="true"></i><?= $obj->calculate_rating($dt->id); ?></span>
              <div >
                <a href="<?= base_url(); ?>shop/shop/profile/<?= urlencode($dt->name); ?>" target="_blank">
                <img style="height:150px; width: 100%;
                border-radius: 5px;margin-bottom: 15px;" src="<?= base_url(); ?>uploads/<?= $dt->image; ?>">
                </a>
                <Br>
                <div class="row" style="white-space: initial;">
                <div class="col-6" style="padding: 0px;">
                  <h3 class="shop-name-h1"><?= $dt->name; ?></h3>
                  
                  <h6 class="locality greypara grey"><?= $dt->locality_name; ?></h6>
                  <!-- <h6 class="greypara">4.0 kms</h6> -->
                </div>
                <div class="col-6" style="padding: 0px; margin-top: 0px;"> 
                 &nbsp&nbsp
                  <span style="float:right;" >
                    <a class="icon-h5" href="https://www.google.com/maps/dir/<?php echo $dt->lat.','.$dt->lon; ?>/" id="link">
                    <i style="color: dodgerblue;" class="fa fa-map" aria-hidden="true"></i></a>&nbsp
                  <a class="icon-h5" href="tel:<?= $dt->mobileno; ?>">
                    <i style="color: dodgerblue;" class="fa fa-phone" aria-hidden="true"></i></a>
                </span>
                <!-- <span>
                <p style="color: red; font-size: 20px;" class="pull-right">
                  <a href="" style="color: red;" ><i  class="fa fa-cut" aria-hidden="true"></i>
                    <br>Book</a>
                </span> -->
                </p>
                </div>                
              </div>

              <div class="col-12">
                   <div class="col-6">                     
                    
                    </div>
                    <div class="col-6" style="padding-left: 130px;">
                      <i style="color: dodgerblue;" class="fa fa-cut" aria-hidden="true"></i>
                      <a href="<?= base_url(); ?>shop/shop/service_booking/<?= $dt->id; ?>" name='booking' id='booking' class="btn btn-danger">Book</a>
                     
                    </div>
              </div>
              </div>
            </div>
            <?php } ?>
            <?php }  ?>
          </div>
        </div>
      </div>

      <!-- BOdy code ends here  -- >

       <!--/col-->

     </div>

     <!--/row-->

   </div>



   <!-- container -->

   <?php   $this->load->view('layouts/footer'); ?>
   <script>
     $('.your-checkbox').prop('indeterminate', true)
   </script>
 <script type="text/javascript">
          if ("geolocation" in navigator){
            console.log("reached");
            navigator.geolocation.getCurrentPosition(function(position){ 
              var lat=position.coords.latitude; 
              var lang=position.coords.longitude;
              if(lat && lang){
                $('#link').attr("href", "https://www.google.com/maps/dir/<?php echo $result->lat.','.$result->lon; ?>/"+lat+","+lang);
                 $('#link2').attr("href", "https://www.google.com/maps/dir/<?php echo $result->lat.','.$result->lon; ?>/"+lat+","+lang);
                  $("#lon").val(lang);
                 $("#lat").val(lat);
              }else{
                
                $('#link').attr("href", "https://www.google.com/maps/dir/<?php echo $result->lat.','.$result->lon; ?>/");
                $('#link2').attr("href", "https://www.google.com/maps/dir/<?php echo $result->lat.','.$result->lon; ?>/");
              }
            
            
          },function(error) {
       
           console.log('test only');
            $('#link').attr("href", "https://www.google.com/maps/dir/<?php echo $result->lat.','.$result->lon; ?>/");
            $('#link2').attr("href", "https://www.google.com/maps/dir/<?php echo $result->lat.','.$result->lon; ?>/");
           });
          }else{
            console.log("Browser doesn't support geolocation!");
            $('#link').attr("href", "https://www.google.com/maps/dir/<?php echo $result->lat.','.$result->lon; ?>/");
            $('#link2').attr("href", "https://www.google.com/maps/dir/<?php echo $result->lat.','.$result->lon; ?>/");
          }

      </script>
