<?php $this->load->view('public_header'); ?>
<style type="text/css">


b, strong {
  font-weight: 500;
}
h1 {
  font-weight: 300;
  font-size: 42px;
  color: #4a4a4a;
  letter-spacing: 0px;
}
.filter_search_area a
{
  color: black;
}
.filter_search_area .active 
{
  color:red;
}
.row {
  display: -ms-flexbox;
  display: flex;
  -ms-flex-wrap: wrap;
  flex-wrap: wrap;
  margin-right: 0px;
  margin-left: 0px;
}
.badge
{
  vertical-align: top;
  margin-left: 6px;
  color: black;
}
.badge-info {
  color: black;
  background-color: #d3fb00;
}
.round
{
  border: 1px solid #9a9a9a !important;
  border-radius: 50px;
  padding-left: 15px;
  height: 45px;
}
.form-body .btn {
  border-radius: 100px;
}
.btn-green {
  background-color: #007bff;
}
.color-box
{
  border:1px solid #d3fb00;
  border-radius: 50px;
}
.filter_search_area
{
  margin-bottom: 25px;
}
.shop_list_area ul
{
  list-style-type: none;
}
.shop_list_area ul li
{
  border-bottom: 1px dashed grey;
  margin-bottom: 10px;
}
.color-box .col-4
{
  padding: 8px;
  text-align: center;
  color: grey;
}
.span-color
{
  background-color: #d3fb00;
  color: black;
  margin-right: 55%;
  padding: 0px 4px 0px 4px;
  font-size: 12px;
  border-radius: 2px;
}
a
{
  color: black;
}

/* Thulasi CSS starts */

.shop-name-h1 {
	font-size: 18px;
	font-weight: 500;
	margin-bottom: 3px;
	color: black;
	font-family: 'Comfortaa', open sans;

}

.btn-text {
	letter-spacing: 5px;
}

.body-text {
	font-size: 14px;
	color: #4A4A4A !important;
	font-weight: 400;
}

.left-pad {
	padding-left: 3%;
	padding-right: 3%;
}

.form-group {
	margin-bottom: 5px;
}

.col-11 {
	max-width: 100% !important;
}

.btn-text3 {
  font-size: 12px !important; 
  letter-spacing: 4px;
  font-weight: 100 !important;
  padding-left: 40px;
  padding-right: 40px;
}

.btn {
	padding-top: 11px !important;
}

.shop-name {
	font-size: 20px;
	margin-bottom: 0px;
	text-transform: uppercase;	
}

.grey {
	font-size: 12px;
	color: #878787 !important;
	font-weight: 100;
}

.bottom-mar-pad {
	margin-bottom: 50px;
}

.pad-r-8 {
	padding-right: 8px;
}

.heading-1 {
	font-size: 20px;
	margin-bottom: -6px;
	font-weight: 700;
	margin-top: 5px;
	text-transform: initials;
	letter-spacing: -1px;
}

.icon-small {
	font-size: 14px;
	color: #aaa;
}

.list-group {
	padding-left: 22px;
}

@media only screen and (max-width: 420px) {
  .pt-6 {
    padding-top: 70% !important;
  }
  .btn-text3 {
    font-size: 14px !important; 
    letter-spacing: 4px;
    font-weight: 100 !important;
    padding-left: 40px;
    padding-right: 40px;
  }


  /* Thulasi CSS ends */   
  
</style>
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="row">
        <div class="item-wrap font-chg">
          <div class="item">
           <table class="table table-hover table-bordered display">
            <thead><br><br>
              <tr>
                <th>Your Booking Records</th>                                                   
              </tr>
            </thead>
            <tbody>
              <?php
              // if(isset($result)):
                // echo "<pre>"; print_r($result);exit;
                foreach ($result as $value):  ?>
                 <?php $id = $this->db->where('id',$value->salonid)->get('shop_list')->row(); ?>
                 <?php $id1 = $this->db->where('id',$value->userid)->get('shop_signup')->row(); ?>
                  <tr>                                   
                   <td>                                 
                   <a href="<?= base_url('shop/shop/bookingStatus/'.$value->id)?>">
                    <div>
                    <?php echo "Salon Name:-".$id->name; ?><br>
                    <?php echo "Date:-".$value->date ?><br>
                    <?php echo "Time:-".$value->time; ?><br>
                    <?php echo "Remark:-".$value->remark; ?><br>
                  </div>
                  </a>
                   </td>
                 </tr>
                <?php  endforeach; 
                // endif; ?>
             </tbody>
           </table>
         </div>
       </div>
     </div>
   </div>
 </div>
</div>

<!--/container-->

<?php $this->load->view('layouts/footer'); ?>

