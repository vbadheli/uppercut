<?php $this->load->view('public_header'); ?>
<style >
  .left-nav {
    position: fixed;
    overflow: hidden;
    width: 295px;
    z-index: 3333;
    height: 100%;
    /* background: rgb(247, 247, 247); */
    /* background: rgba(255, 0, 0, 0.26); */
   }
   .hide-nav .bottom-3 {
    
  }
  .height100
  {
    height: 100% !important;
  }
  .height10{
    height: 10%;
   }
  .bottom-3
  {
    bottom: 16%;
  }
   @media only screen and (max-width: 900px) {
   .left-nav {
    position: fixed;
    width: 100%;
    z-index: 3333;
    height: 12%;
   }
   .height10{
    height: 14%;
   }
   .mx-auto {
   padding-top: 59px;
}
   }
   
/* Thulasi CSS starts */
.btn-text {
	letter-spacing: 5px;
}

.btn {
    border-radius: 100px !important;
    letter-spacing: 5px !important;
    font-size: 12px;
}

.body-text {
	font-size: 14px;
	color: #4A4A4A !important;
	font-weight: 400;
}

.left-pad {
	padding-left: 3%;
	padding-right: 3%;
}

.form-group {
	margin-bottom: 5px;
}

.grey {
	font-size: 12px;
	color: #878787 !important;
	font-weight: 100;
}

.round
{
  border: 1px solid #9a9a9a !important;
    border-radius: 50px;
    padding-left: 15px;
    height: 45px;
    font-size: 12px; 
}

/* Thulasi CSS ends */    
   
</style>
<div class="container">
	<div class="row">

		<div class="mx-auto">
			<Br>
			<br>
			<h1 class="py-2 pb-1 text-center" style="font-size: 32px;">We Will Miss You</h1>
			<p class="text-center ptext">Alrighty, before you go, please let us know why you are deleting your account</p>
			<br>
			<div class="form-body">
				<form class="form"  role="form" autocomplete="off" id="deleteAccount">
					<input type="hidden" name="date" value="<?= date('d-m-Y'); ?>">
					<div class="form-group">
						<label class="sr-only" for="reason">select a reason</label>
						<!-- <input type="text" class="" id="reason" name="reason" placeholder="Select a reason" required> -->
						<select class="form-control form-control-lg rounded-0 round" id="reason" name="reason" placeholder="Select a reason" required>
							<option value=""> Select a reason</option>
							<option value="Less number of customers from website">Less number of customers from website </option>
							<option value="Privacy">Privacy</option>
							<option value="I have created another account for my shop">I have created another account for my shop</option>
							<option value="Too many email notifications">Too many email notifications</option>
							<option value="I don’t find this website useful">I don’t find this website useful</option>
							<option value="">I can’t change my name</option>
							<option value="Other">Other</option>
						</select>	

					</div>
					<br>
					<button type="submit" class="btn btn-text3 mb-2 btn-red btn-sz">DELETE MY ACCOUNT</button>
					<button type="button"  onClick="window.location='<?php echo base_url(); ?>'"  class="btn btn-text3 btn-green btn-sz">Cancel</button>
				</form>
				<div class="form-url">

				</div>
			</div>
		</div>
		<!--/col-->

		<!--/col-->
	</div>
	<!--/row-->
</div>

<?php $this->load->view('layouts/footer'); ?>
<script type="text/javascript">
	

	$('#deleteAccount').validate({
		rules:{
			reason:{
				required: true,
			}
		},
		errorPlacement: function(error, element) {},
		highlight: function(element) {
			$(element).parent('div').addClass('has-error');
		},
		unhighlight: function(element) {
			$(element).parent('div').removeClass('has-error');
		},
		submitHandler: function() { 
			var formData=$("#deleteAccount").serialize();
			// console.log(formData);
			// return false;
			signUp(formData);
		}
	});

	function signUp(formData) {
		$.ajax({
			url: "<?php echo base_url('delAccount'); ?>",
			type: "POST",
			data:formData,
			dataType: "JSON",
			beforeSend: function () {
				$('#loader').addClass('loader');
			},
			complete: function () {
				$('#loader').removeClass('loader');
			},
			success: function (response) {
				console.log(response);
				window.location.href="<?php echo base_url('home'); ?>";
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				console.log(textStatus);
				window.location.href="<?php echo base_url('home'); ?>";
			}
		});
	}
</script>