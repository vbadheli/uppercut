<?php $this->load->view('public_header'); ?>
  <style type="text/css">
   
    
   b, strong {
    font-weight: 500;
  }
  h1 {
    font-weight: 300;
    font-size: 42px;
    color: #4a4a4a;
    letter-spacing: 0px;
  }
  .filter_search_area a
  {
    color: black;
  }
  .filter_search_area .active 
  {
    color:red;
  }
  .row {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: 0px;
    margin-left: 0px;
}
.badge
{
  vertical-align: top;
  margin-left: 6px;
  color: black;
}
.badge-info {
    color: black;
    background-color: #d3fb00;
    }
    .round
{
  border: 1px solid #9a9a9a !important;
    border-radius: 50px;
    padding-left: 15px;
    height: 45px;
}
.form-body .btn {
    border-radius: 100px;
}
.btn-green {
    background-color: #007bff;
}
.color-box
{
  border:1px solid #d3fb00;
  border-radius: 50px;
}
.filter_search_area
{
  margin-bottom: 25px;
}
.shop_list_area ul
{
  list-style-type: none;
}
.shop_list_area ul li
{
  border-bottom: 1px dashed grey;
  margin-bottom: 10px;
}
.color-box .col-4
{
      padding: 8px;
      text-align: center;
      color: grey;
}
  .span-color
  {
    background-color: #d3fb00;
    color: black;
      margin-right: 55%;
    padding: 0px 4px 0px 4px;
    font-size: 12px;
    border-radius: 2px;
  }
  a
  {
    color: black;
  }

/* Thulasi CSS starts */

.shop-name-h1 {
	font-size: 18px;
	font-weight: 500;
	margin-bottom: 3px;
	color: black;
	font-family: 'Comfortaa', open sans;
		
}

.btn-text {
	letter-spacing: 5px;
}

.body-text {
	font-size: 14px;
	color: #4A4A4A !important;
	font-weight: 400;
}

.left-pad {
	padding-left: 3%;
	padding-right: 3%;
}

.form-group {
	margin-bottom: 5px;
}

.col-11 {
	max-width: 100% !important;
}

.btn-text3 {
    font-size: 12px !important; 
    letter-spacing: 4px;
    font-weight: 100 !important;
    padding-left: 40px;
    padding-right: 40px;
}

.btn {
	padding-top: 11px !important;
}

.shop-name {
	font-size: 20px;
	margin-bottom: 0px;
	text-transform: uppercase;	
}

.grey {
	font-size: 12px;
	color: #878787 !important;
	font-weight: 100;
}

.bottom-mar-pad {
	margin-bottom: 50px;
}

.pad-r-8 {
	padding-right: 8px;
}

.heading-1 {
	font-size: 20px;
	margin-bottom: -6px;
	font-weight: 700;
	margin-top: 5px;
	text-transform: initials;
	letter-spacing: -1px;
}

.icon-small {
	font-size: 14px;
	color: #aaa;
}

.list-group {
	padding-left: 22px;
}

@media only screen and (max-width: 420px) {
.pt-6 {
    padding-top: 70% !important;
}
.btn-text3 {
    font-size: 14px !important; 
    letter-spacing: 4px;
    font-weight: 100 !important;
    padding-left: 40px;
    padding-right: 40px;
}


/* Thulasi CSS ends */   
  
  </style>

<div class="container">
  <div class="">
          
        <br>
        <Br>
        <Br>
      <!-- Original Code Body starts from here -->
      <!-- <a href="<?=  base_url(); ?>shop/shop/membership"> -->
      <!-- <a href="#">
      <div id="membership-box" class=" row justify-content-end">
        
        <span class="span-color">Advanced</span>
        <div class="color-box col-11">
          
          <div class="row ">
            <div class="col-4">
              <h6>4000<Br>Rupees</h6>
            </div>
            <div class="col-4">
              <h6>4000<Br>Rupees</h6>
            </div>
            <div class="col-4">
              <h6>4000<Br>Rupees</h6>
            </div>
            
          </div>
        </div>

      </div>
      </a> -->
      <br>
      <div class="row justify-content-end">
        <!-- <div class="filter_search_area col-11">
          <ul class="nav">
            <li class="nav-item">
              <a class="nav-link active" href="#"><i class="fa fa-plus fa-2x" aria-hidden="true" ></i><span class="badge badge-pill badge-info">Pro</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link " href="#"><i class="fa fa-search fa-2x" aria-hidden="true" ></i></a>
            </li>
            <li class="nav-item">
              <a class="nav-link " href="#"><i class="fa fa-filter fa-2x" aria-hidden="true" ></i></a>
            </li>
          </ul>
        </div> -->
        <Br>
        <Br>
        <div class="shop_list_area col-11">

          <ul class="list-group">
            <?php $list = $this->db->where('shop_user',get_session('userid'))->get('shop_list')->result(); ?>
            <?php if(!empty($list)){ ?>
            <?php foreach($list as $k): ?>

            <li>
              <a href="<?= base_url(); ?>home/salon/<?=$k->id?>">
                <!-- home/salon/<?= $k->id; ?>-->
                <h3 class="heading-1"><?= $k->name; ?></h3>
              </a>
              <span class="locality grey"><?= $k->locality_name; ?></span>
              <br><br>            
              <div class="row" >            
              <div class="col-6" style=" padding-left:0; max-width: 30%;">
              <p><i class="fa fa-eye pad-r-8 icon-small" aria-hidden="true"></i><?= $k->counter; ?></p>
              </div>
              <div class="col-4">
              <p><i class="fa fa-star-o pad-r-8 icon-small" aria-hidden="true"></i><?= $obj->calculate_rating($k->id); ?></p>              
              </div>
              </div>
            </li>
          <?php endforeach; ?>
          </ul>
          <?php }else{ ?>
          <h6 class=" text-center body-text pt-6" style="    margin-bottom: 20px;">Click to create a saloon profile</h6>

     <div class="form-body mx-auto">
      

      <div class="form-url">

       <a  action="<?= base_url(); ?>shop/shop/saloonprofile" href="<?= base_url(); ?>shop/shop/saloonprofile" style="font-size: 15px;" class="btn btn-text3 btn-green btn-sz">Create a Saloon</a>

      </div>

     </div>

          <?php } ?>
        </div>
      </div>

      <!-- BOdy code ends here  -- >

         <!--/col-->

       </div>

       <!--/row-->

     </div>



     <!--/container-->

     <?php $this->load->view('layouts/footer'); ?>


