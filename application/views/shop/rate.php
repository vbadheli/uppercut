<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Salon Rating1</title>
  <meta name="viewport" content="width=device-width, minimum-scale=1.0" />
  <link href="<?php  echo base_url('assets/vendor/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
  <link href="<?php  echo base_url('assets/vendor/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600|Roboto:300" rel="stylesheet">
  <link href="<?php  echo base_url('assets/vendor/magnific-popup/magnific-popup.css'); ?>" rel="stylesheet">
  <link href="<?php  echo base_url('assets/css/style.css'); ?>" rel="stylesheet">
   <!--  Scroll bar for desktop version -->
  <link rel="stylesheet" media="screen and (min-width: 900px)" type="text/css" href="<?php  echo base_url('assets/css/scroll.css'); ?>">
  <link rel="stylesheet" media="screen and (min-width: 900px)" href="<?php  echo base_url(); ?>assets/css/desktop.css">
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-100486865-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments)};
    gtag('js', new Date());
    gtag('config', 'UA-100486865-1');
  </script>
  <style type="text/css">

  i {
    border: solid white;
    border-width: 0 3px 3px 0;
    display: inline-block;
    padding: 3px;
  }
  span.fa-angle-left {
    color: black;
    font-size: 24px;
    font-weight: 100;
    padding-top: 10px;
    padding-left: 5px;
    padding-right: 5px;
    
    }
  .toggleTab {
    width: 35px;
    /* background-color: #1f1e21; */
    height: 25px;
    position: absolute;
    top: 5px;
    left: 15px;
    z-index: 999999;
    cursor: pointer;
    display: inline-block;
}
  .left {
    transform: rotate(135deg);
    -webkit-transform: rotate(135deg);
  }
  b, strong {
    font-weight: 500;
  }
  h1 {
    font-weight: 300;
    font-size: 42px;
    color: #4a4a4a;
    letter-spacing: 0px;
  }
  .round {
    border: 1px solid #9a9a9a !important;
    border-radius: 5px;
    padding-left: 15px;
    height: 68px;
}
.form-body .btn {
    border-radius: 100px;
}
.star-rating {
  line-height:32px;
  font-size:1.75em;
}

.star-rating .fa-star{color: black;}
.btn-green {
    background-color: #007bff;
}
  .filter_search_area a
  {
    color: black;
  }
  .filter_search_area .active 
  {
    color:red;
  }
  .row {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: 0px;
    margin-left: 0px;
  }
  .brand-name {
    margin-bottom: .5rem;
    font-family: inherit;
    font-weight: 500;
    line-height: 1.1;
    	font-size: 12px;
	color: #878787 !important;
	font-weight: 100;
   	padding-top: 10px;
    font-style: normal;
    margin-top: 12px;
    margin-left: -37px;
}
  .badge
  {
    vertical-align: top;
    margin-left: 6px;
    color: black;
  }
  .badge-info {
    color: black;
    background-color: #d3fb00;
  }
  .color-box
  {
    border:1px solid #d3fb00;
    border-radius: 50px;
  }
  .filter_search_area
  {
    margin-bottom: 25px;
  }
  .shop_list_area ul
  {
    list-style-type: none;
  }
  .shop_list_area ul li
  {
    border-bottom: 1px dashed grey;
    margin-bottom: 20px;
  }
  .color-box .col-4
  {
    padding: 8px;
    text-align: center;
    color: grey;
  }
  .span-color
  {
    background-color: #d3fb00;
    color: black;
    margin-right: 55%;
    padding: 0px 4px 0px 4px;
    font-size: 12px;
    border-radius: 2px;
  }
  a
  {
    color: black;
  }
  .greypara
  {
    color: grey;
    margin-top: 0;
    margin-bottom: 4px;
  }
  .form-check-input {
    position: inherit;
    margin-top: .25rem;
    margin-left: -20px;
}

/* Thulasi CSS starts */

.text-info {
	font-size: 14px;
	color: #3D8DFB !important;
	line-height: 15px !important;	
}

.rate-1 {
	text-align: right;
	line-height: 15px;
	padding-right: 0; 
}

.shop-name-2 {
	font-size: 18px;
	font-weight: 100;	
}

.grey {
	font-size: 12px;
	color: #878787 !important;
	font-weight: 100;
}

.body-text {
	font-size: 14px;
	color: #4A4A4A !important;
	font-weight: 400;
}

.placeholder-txt {
	font-size: 14px; 
	color:#9b9b9b; 
	font-weight:100;
}

.btn-text {
	letter-spacing: 5px;
}

/* Thulasi CSS ends */

</style>
</head>
<body>
  <div class="boxing" id="boxing">
  <div id="loader"></div>
  <!-- Navbar starts from here -->
  <div class="left-nav">
    <div class="top-body">
      <div class="toggleTab">
       <a href="<?php echo base_url(); ?>"><span class="fa fa-angle-left" aria-hidden="true"></span></a>
     </div>
     <span class="brand-name">
     RATE THIS SALON
    </span>

  </div>


</div>
<Br>
<Br>
<Br>
<Br>

<!-- Navbar ends here -->
<div class="container">
  <div class="">
          <!--
          <div class="col-md-12">
            <div class=" shop-login-signup pull-right" >
              <span>SHOP</span>
              <div class="login-signup-link" >
                <ul>
                  <li><a href="<?php echo base_url("login"); ?>">Login</a></li>
                  <li><a href="<?php echo base_url("signup"); ?>">Signup</a></li>
                </ul>
              </div>
            </div>
          </div>
        -->
        
        <Br>
        <!-- Original Code Body starts from here -->
       
        <div class="row ">
          
          <Br>
          <Br>
 
          <div class="shop_list_area col-12">
<!--
          <div class="col-12">
           <h2 class="shop-name-2"><?= $data->name; ?></h2>
           <h5 class="grey"><?= $data->locality_name; ?></h5>
         </div> 
-->
           <Br>
           
        <form method="POST" action="<?= base_url(); ?>home/store_rating" >
            <input type="hidden" name="user_id" value="<?= get_session('userid'); ?>">
            <input type="hidden"  name="date" value="<?= date('d-m-Y'); ?>">
            <input type="hidden" name="shop_id" value="<?= $data->id; ?>">
          <div class="container">
            <div class="row">
              <div class="col-12" style="padding-left:0;">
                <div class="star-rating">
                  <span class="fa fa-star-o" data-rating="1"></span>
                  <span class="fa fa-star-o" data-rating="2"></span>
                  <span class="fa fa-star-o" data-rating="3"></span>
                  <span class="fa fa-star-o" data-rating="4"></span>
                  <span class="fa fa-star-o" data-rating="5"></span>
                  <input type="hidden" required name="rating" class="rating-value" value="0">
                </div>
              </div>
            </div>
             <Br>
         <div class="form-group">
            <label for="exampleFormControlTextarea1" class="body-text">Would you like to say anything to the Salon Owner</label>
            <textarea name="feedback" placeholder="Your Comments" class="form-control placeholder-txt" required id="exampleFormControlTextarea1" rows="3"></textarea>
          </div>
          <div class="form-body">
            <button type="submit"  class="btn btn-text btn-green btn-sz">Submit</button>
          </div>
          </div>
          </form>
          </div>
        </div>
       
        <!-- BOdy code ends here  -- >

         <!--/col-->

       </div>

       <!--/row-->

     </div>

</div>

     <!-- container -->

     <?php   $this->load->view('layouts/footer'); ?>
     <script>
       $('.your-checkbox').prop('indeterminate', true)
     </script>
     <script>
       var $star_rating = $('.star-rating .fa');

      var SetRatingStar = function() {
        return $star_rating.each(function() {
          if (parseInt($star_rating.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
            return $(this).removeClass('fa-star-o').addClass('fa-star');
          } else {
            return $(this).removeClass('fa-star').addClass('fa-star-o');
          }
        });
      };

      $star_rating.on('click', function() {
        $star_rating.siblings('input.rating-value').val($(this).data('rating'));
        return SetRatingStar();
      });

      SetRatingStar();
      $(document).ready(function() {

      });
     </script>

