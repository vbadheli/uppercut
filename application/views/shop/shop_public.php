<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Salon profile</title>
  
  <link rel='shortcut icon' href='/uploads/favicon.ico' type='image/x-icon' />
  
  <meta name="viewport" content="width=device-width,initial-scale=1" />
  <link href="<?php  echo base_url('assets/vendor/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
  <link href="<?php  echo base_url('assets/vendor/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600|Roboto:300" rel="stylesheet">
  <link href="<?php  echo base_url('assets/vendor/magnific-popup/magnific-popup.css'); ?>" rel="stylesheet">
  <link href="<?php  echo base_url('/assets/css/style.css'); ?>" rel="stylesheet">
  <!--  Scroll bar for desktop version -->
  <link rel="stylesheet" media="screen and (min-width: 900px)" type="text/css" href="<?php  echo base_url('assets/css/scroll.css'); ?>">
  <link rel="stylesheet" media="screen and (min-width: 900px)" href="<?php  echo base_url('/assets/css/desktop.css'); ?>">

<!--   <link rel="stylesheet" media="screen and (min-width: 900px)" href="<?php  echo base_url(); ?>assets/css/desktop.css">
-->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-100486865-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments)};
    gtag('js', new Date());
    gtag('config', 'UA-100486865-1');
  </script>
  <style type="text/css">

  i {
    border: solid white;
    border-width: 0 3px 3px 0;
    display: inline-block;
    padding: 3px;
  }
  /* span.fa-angle-left {
    color: black;
    font-size: 45px;
    font-weight: lighter;
  } */	
  
  span.fa-angle-left {
    color: black;
    font-size: 24px;
    font-weight: 100;
    padding-top: 10px;
    padding-left: 0px;
    padding-right: 5px;
    
  }
  
  .toggleTab {
    width: 35px;
    /* background-color: #1f1e21; */
    height: 25px;
    position: absolute;
    top: 5px;
    left: 15px;
    z-index: 999999;
    cursor: pointer;
    display: inline-block;
  }
  .left {
    transform: rotate(135deg);
    -webkit-transform: rotate(135deg);
  }
  b, strong {
    font-weight: 500;
  }
  #days span
  {
    padding-right: 10px;
  }
  #bookmarks
  {
    font-size: 10px !important;
	color: #878787 !important;
	font-weight: 100;
	text-transform: uppercase;
	letter-spacing: 1px;

    
  }

  .round {
    border: 1px solid #9a9a9a !important;
    border-radius: 5px;
    padding-left: 15px;
    height: 68px;
  }
  .form-body .btn {
    border-radius: 100px;
  }
  .btn-green {
    background-color: #007bff;
  }
  .filter_search_area a
  {
    color: black;
  }
  .filter_search_area .active 
  {
    color:red;
  }
  
  /*
  #cord
  {
    padding-left: 90px;
  }  
  */

  #cord a
  {
    padding-right: 15px;
  }
  
.row {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: 0px;
    margin-left: 0px;
  }
.brand-name {
    margin-bottom: .5rem;
    font-family: inherit;
    font-weight: 700;
    line-height: 1;
    color: black;
    font-size: 24px;
    font-style: normal;
    margin-left: 0px; 
    padding-bottom: 5px;
    padding-right: 8%;
    margin-bottom: 0;
    font-family: roboto, 'Comfortaa', open sans;
    margin-top: 0;
  }
  .badge
  {
    vertical-align: top;
    margin-left: 6px;
    color: black;
  }
  .badge-info {
    color: black;
    background-color: #d3fb00;
  }
  .color-box
  {
    border:1px solid #d3fb00;
    border-radius: 50px;
  }
  .filter_search_area
  {
    margin-bottom: 25px;
  }
  .shop_list_area ul
  {
    list-style-type: none;
  }
  .shop_list_area ul li
  {
    border-bottom: 1px dashed grey;
    margin-bottom: 20px;
  }
  /* #bottomnavbar i
  {
    font-size: 35px;
  } */
  .color-box .col-4
  {
    padding: 8px;
    text-align: center;
    color: grey;
  }
  .span-color
  {
    background-color: #d3fb00;
    color: black;
    margin-right: 55%;
    padding: 0px 4px 0px 4px;
    font-size: 12px;
    border-radius: 2px;
  }
  a
  {
    color: black;
  }
  .greypara
  {
    color: grey;
    margin-top: 0;
    margin-bottom: 4px;
  }
  .form-check-input {
    position: inherit;
    margin-top: .25rem;
    margin-left: -20px;
  }
  #bottomnavbar
  {
    border-top: 0.1px solid #808080c4;;
    padding: 15px 15px 0px 15px;
  }

 <!--  @media only screen and (max-width: 900px)
  {
    
  }
  
 --> 
  .left-nav .top-body {
    position: absolute;
    width: auto;
    height: auto; 
    top: 0px;
    padding-left: 13%; 
    background-color:unset; 
  }

/* Thulasi start */

/* Typography */
.h1 {
	font-size: 28px; !important;
	font-weight: 500 !important;
	letter-spacing: -1px;	
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;	
	color: #333;
}

.h2{
	font-size: 24px;
}

.h3{
	font-size: 12px; !important;
	font-weight: 400 !important;
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
	color: #555;
}

.h4{
	font-size: 16px;
}

/* Body text */
.h5{
	font-size: 12px; !important;
	font-weight: 100 !important;
	letter-spacing: 0.3px;
}

/* info */
.h6{
	font-size: 10px; !important;
	font-weight: 100 !important;
	letter-spacing: 0px !important;
}





/* Typo 50 opacity */

.opacity50 {	
	color: #888;		
}

.uppercase {
	text-transform: uppercase;
}


/* Typography ends */

.hyperlink {
	color: dodgerblue;
	font-size: 10px;
}
 
.text-label {
	font-size: 12px;
	font-weight: 100;
	color: #000;
	opacity: 0.6;
	letter-spacing: 0;
}

.grey {
	font-size: 12px !important;
	color: #878787 !important;
	font-weight: 100;
}


.icon-h4 {
	font-size: 16px !important;
	padding-right: 5px;
	margin-top: 5px;
	padding-top: 5px;
	color: dodgerblue !important;
}

.icon-h3 {
	font-size: 20px !important;
	padding-right: 5px;
	margin-top: 5px;
	padding-top: 5px;
	color: dodgerblue !important;
}

.icon-h2 {
	font-size: 32px !important;
	padding-right: 5px;
	margin-top: 5px;
	padding-top: 5px;
	color: dodgerblue !important;
}

.body-text {
	font-size: 13px;
	color: #4A4A4A !important;
	font-weight: 300;
	line-height: 16px;
}


.remove-mar-pad {
	margin-bottom: 0!important;
}

.working-days {
	font-size: 12px;
	padding-right: 5px;
}

/* .img-blk-bg { background-color: #000000; height: 200px; width: 100%;border-radius: 0px 0px 50px 50px; }
.img-blk-bg img { opacity: 0.7; }
}
*/

.rem-pad {
	padding-top: 0px !important ;
} 

.left-pad {
	padding-left: 25%;
}

.col-12 {
	padding-right: 0px;
}

.overlap {
	position: relative;
}

.left-pad-adjust {
	padding-left: 15%;
}

.pad-top {
	padding-top: 50px;
}


@media only screen and (max-width: 900px) {

/* Typography */
.h1 {
	font-size: 28px; !important;
	font-weight: 500 !important;
	letter-spacing: -1px;	
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;	
	color: #333;
}

.h2{
	font-size: 24px;
}

.h3{
	font-size: 18px; !important;
	font-weight: 400 !important;
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
	color: #555;
}

.h4{
	font-size: 16px;
}

/* Body text */
.h5{
	font-size: 13px; !important;
	font-weight: 100 !important;
	letter-spacing: 0.3px;
}

/* info */
.h6{
	font-size: 11px; !important;
	font-weight: 100 !important;
	letter-spacing: 0px !important;
}


.left-nav {
      position: relative;
      width: 100%;
      z-index: 3333;
      height: 10%;
    }

.body-text {
	font-size: 16px;
	color: #000 !important;
	font-weight: 400;
}

.grey-bright {
	font-size: 12px;
	color: #878787 !important;
	font-weight: 100;
}


.icon-h4 {
	font-size: 20px !important;
	padding-right: 5px;
	margin-top: 5px;
	padding-top: 5px;
	color:dodgerblue !important;
}

.icon-h3 {
	font-size: 24px !important;
	padding-right: 5px;
	margin-top: 5px;
	padding-top: 5px;
	color:dodgerblue !important;
}

.icon-h2 {
	font-size: 32px !important;
	padding-right: 5px;
	margin-top: 5px;
	padding-top: 5px;
	color:dodgerblue !important;
}

.remove-mar-pad {
	margin-bottom: 0!important;
}

.b-bar-pad
  {
    border-top: 0.1px solid #808080c4;;
    padding: 15px 15px 15px 15px !important;
  }
  
  
.rem-pad {
	padding-top: 0px !important;
}    
}


/* Thulasi end */  
  
  
</style>
</head>
<body>
  <?php $plat = 0; $plong= 0 ?>
  <div class="boxing clearfix" id="boxing">
    <?php $p = $this->db->where('id',$result->id)->get('shop_list')->row()->counter; ?>
    <?php $p++; $this->db->set('counter',$p)->where('id',$result->id)->update('shop_list'); ?>
    <div id="loader"></div>
    
    
    
    <!-- Navbar starts from here -->
    <div class="left-nav clearfix img-blk-bg">

<!--       <img class="salon-img" style="height: 200px; width: 100%; border-radius: 0px 0px 50px 50px;" src="<?= base_url(); ?>uploads/<?= $result->image; ?>">
-->


      <div class="top-body" style="padding-top: 0px; height:200px;">
        <div class="toggleTab">
   <a href="<?php echo base_url('search'); ?>"><span style="color: black;" class="fa fa-angle-left" aria-hidden="true"></span></a>
       </div>
       
<!--    <div style="height: 200px; position: absolute;">   
       <span class="brand-name">
         <b><?= $result->name; ?></b>
       </span>
      
       <br>
       <span style="color: white; " class="locality greypara " ><i style="color: #40e111;border: none; margin-left: 0px;" class="fa fa-star" aria-hidden="true"></i><?= $result->rating; ?></span>
    </div>   
 -->       

       
     </div>
   <?php $plat = $result->lat; $plong = $result->lon; ?>

   </div>
   <!-- Navbar ends here -->
   
   		<div>
   			<div class="col-6 left-pad pad-top" style="margin-bottom: 15px;">          		
       			<span style="color: black; margin-bottom: -10px;" class="locality greypara body-text" ><i style="color: #40e111; border: none; margin-left: 0px; padding-left:0; " class="fa fa-star" aria-hidden="true"></i><?= $result->rating; ?></span>         		       		
       			<b class="h1"><?= $result->name; ?></b>
    			</div>           
          		<div>
          		<img class="salon-img" style="width: 100%;" src="<?= base_url(); ?>uploads/<?= $result->image; ?>">
			</div>
  		</div>
   
   
   <div class="container clearfix rem-pad left-pad-adjust" style="clear: both; margin-bottom: 74px; padding-top:200px;" >
    <div class="col-12 clearfix">         
      <div class="col-12" style="margin-top: 15px;"> 
        <div>
          <!-- <span style="font-size: 25px;"><a href="#"><i style="color: dodgerblue;" class="fa fa-map icon-h5" aria-hidden="true"></i></a>
          </span> -->
          <div class="row">
          
            <div class="col-6"style="padding: 0;">                        
              <b class="h3"><?= $result->locality_name; ?></b>
                        
            <div style="margin-top: -10px;">  <p style="padding-top: -10px"><a id="link" href="#" class="hyperlink">Get direction</a></p> 
              <!-- <p class="body-text remove-mar-pad grey" style="font-size:11px !important; font-weight: 300;" id="distance">3.2 Kms</p>  -->
            </div>   
            </div>
                         

            
            
          </div>  
<!--          <hr style=" margin-top:0;"> -->

		<!-- <div class="col-6 pull-right text-right" style="padding: 0;">  -->             
              	<div style="margin-bottom: 25px; margin-top: 10px;">
              	<p class="h6 opacity50 uppercase">Parking</p>	
              	<?php $parking = json_decode($result->parking); ?>
              	
              	<div style="margin-top: -8px;">
              	<span>

                <?php foreach($parking as $parka){ ?>
                <?php if($parka == "bike") { ?>
                <i style="color: grey;" class="fa fa-motorcycle"></i>
                <?php } ?>
                <?php 
                if($parka == "car"){ ?>
                <i style="color: grey;" class="fa fa-car"></i>
                <?php } 
                if($parka == "bus"){ ?>
                <i style="color: grey;" class="fa fa-bus"></i>
                <?php } ?>
                <?php  } ?>
              	</span>
              	</div>
              	</div>
           







<!--          <hr style=" margin-top:0;"> -->
          <div>
            <p class="h6 opacity50 uppercase" >Timings</p>                      

            <p id="days">
              <?php if($result->days != "null" ){  ?>
              <?php $su = "#ccc";$sa = "#ccc";$fr = "#ccc";
              $mo = "#ccc"; $tu = "#ccc";$we = "#ccc";$th = "#ccc"; ?>
              <?php $dayss =  json_decode($result->days); ?>
              <?php foreach ($dayss as $key ) {  ?>
              <?php if($key == "Su"){ ?>
              <?php $su = "black"; ?>
              <?php } ?>
              <?php if($key == "Mo"){ ?>
              <?php $mo = "black"; ?>
              <?php } ?>
              <?php if($key == "Tu"){ ?>
              <?php $tu = "black"; ?>
              <?php } ?>
              <?php if($key == "We"){ ?>
              <?php $we = "black"; ?>
              <?php } ?>
              <?php if($key == "Th"){ ?>
              <?php $th = "black"; ?>
              <?php } ?>
              <?php if($key == "Fr"){ ?>
              <?php $fr = "black"; ?>
              <?php } ?>
              <?php if($key == "Sa"){ ?>
              <?php $sa = "black"; ?>
              <?php } ?>

              <?php } } ?>
              
              <div style="margin-top: -20px;">
              <span class="h4" style="margin-right: 5px; color:<?= $su; ?>;">Su</span>
              <span class="h4" style="margin-right: 5px;color:<?= $mo; ?>;">Mo</span>
              <span class="h4" style="margin-right: 5px;color:<?= $tu; ?>;">Tu</span>
              <span class="h4" style="margin-right: 5px;color:<?= $we; ?>;">We</span>
              <span class="h4" style="margin-right: 5px;color:<?= $th; ?>;">Th</span>
              <span class="h4" style="margin-right: 5px;color:<?= $fr; ?>;">Fr</span>
              <span class="h4" style="margin-right: 5px;color:<?= $sa; ?>;">Sa</span>
            </p>
            
            <div class="row">
              <div class="col-6" style="padding-left:0;">
                <p class="h6 opacity50 uppercase" style="margin-bottom: 0;" >Opening</p>
                <p class="h4"><?= $result->opening_time; ?></p>
                <!-- <p class="body-text"><?= $result->closing_time; ?></p> -->
              </div>
              <div class="col-6">
                <p class="h6 opacity50 uppercase" style="margin-bottom: 0;">Closing</p>
                <!-- <p class="body-text"><?= $result->opening_time; ?></p> -->
                <p class="h4"><?= $result->closing_time; ?></p>
              </div>
            </div>
          </div> 
          </div>
<!--          <hr>  -->
	<br>
          <div class="services">
            <p class="h6 opacity50 uppercase" >Services</p>
            
            <?php $services = json_decode($result->service_offered); ?>
            <?php foreach($services as $key){ ?>
            <p class="h4"><?= $key; ?></p>
            <?php } ?>
          </div>  
<!--          <hr>  -->
	<br>	
          <div>
            <p class="h6 opacity50 uppercase">Contact Details</p>
            <p class="h4"><a href="tel:<?= $result->mobileno; ?>"><!-- <i style="color: dodgerblue;" class="fa fa-phone">--></i><?= $result->mobileno; ?></a></p>
            
            <!-- <p id="bookmarks">Address</p> -->
            <p class="h4"><?= $result->user_address; ?></p>
            <p style="color: dodgerblue; font-size: 12px;"><a style="color: dodgerblue;" href="<?= $result->website; ?>">Visit Website</a></p>

          </div> 
<!--          <hr>  -->

<br>
          <div>
            <p class="h6 opacity50 uppercase">About us</p>
            <p class="h4" style="word-wrap: break-word;"><?= $result->about_me; ?></p>
          </div>
          <Br>
          <Br>
          <Br>
          <Br>

        </div>
      </div>
    </div>

    <!-- BOdy code ends here  -- >

     <!--/col-->

   </div>

   <div id="bottomnavbar" class="fixed-bottom" style="background-color: white;  border-top: 1px rgba(0, 0, 0, 0.07) solid;
   z-index: 99;  box-shadow: 0 0 30px rgba(0, 0, 0, 0.1); width:100%; padding-bottom: 15px;" >
   <p >
     <!-- <a href="<?php echo base_url('search'); ?>"><span class="fa fa-angle-left icon-h2" style="color:dodgerblue;" aria-hidden="true"></span></a> -->
     <span id="cord" >
      <?php $userid= $this->db->where('shop_id',$result->id)->where('user_id',get_session('userid'))->get('feedback')->row(); ?>
      <?php if(empty($userid->id)) { ?>
      <a style="float: left;" href="<?= base_url(); ?>home/rate/<?= $result->id; ?>"><i class="fa fa-star icon-h3" style="color:dodgerblue;" ></i></a>
      <?php } ?>
      <!-- <a style="float: right;" href="#" id="link2"><i class="fa fa-map icon-h4" style="color:dodgerblue;"></i></a> -->
      
      <a style="float:right;" href="tel:<?= $result->mobileno; ?>"><i class="fa fa-phone icon-h3" style="color:dodgerblue;"></i> <p class="h5" style="font-size: 12px; color: dodgerblue; padding-right: 15px;">Call for booking &nbsp;</p></a>
    </span> 
    
    <!-- style="float:right;"  -->
    
  </p>
</div>
<!--/row-->

</div>



<!-- container -->
<script type="text/javascript">

  if ("geolocation" in navigator){

    navigator.geolocation.getCurrentPosition(function(position){ 
      var lat=position.coords.latitude; 
      var lang=position.coords.longitude;
      var plat = "<?= $plat; ?>";
      var plang = "<?= $plong; ?>"
      if(lat && lang){
        $('#link').attr("href", "https://www.google.com/maps/dir/<?php echo $result->lat.','.$result->lon; ?>/"+lat+","+lang);
        $('#link2').attr("href", "https://www.google.com/maps/dir/<?php echo $result->lat.','.$result->lon; ?>/"+lat+","+lang);
        var p1 = new google.maps.LatLng(lat, lang);
         var p2 = new google.maps.LatLng(plat, plang);

        $('#distance').html(calcDistance(p1,p2)+"kms");
        console.log(calcDistance(p1,p2));
        $("#lon").val(lang);
        $("#lat").val(lat);
      }else{

        $('#link').attr("href", "https://www.google.com/maps/dir/<?php echo $result->lat.','.$result->lon; ?>/");
        $('#link2').attr("href", "https://www.google.com/maps/dir/<?php echo $result->lat.','.$result->lon; ?>/");
      }


    },function(error) {

     console.log('test only');
     $('#link').attr("href", "https://www.google.com/maps/dir/<?php echo $result->lat.','.$result->lon; ?>/");
     $('#link2').attr("href", "https://www.google.com/maps/dir/<?php echo $result->lat.','.$result->lon; ?>/");
   });
  }else{
    console.log("Browser doesn't support geolocation!");
    $('#link').attr("href", "https://www.google.com/maps/dir/<?php echo $result->lat.','.$result->lon; ?>/");
    $('#link2').attr("href", "https://www.google.com/maps/dir/<?php echo $result->lat.','.$result->lon; ?>/");
  }

</script>

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&libraries=geometry"></script>

<script>
//calculates distance between two points in km's
function calcDistance(p1, p2) {
  return (google.maps.geometry.spherical.computeDistanceBetween(p1, p2) / 1000).toFixed(2);
}

</script>


<?php   $this->load->view('layouts/footer'); ?>


