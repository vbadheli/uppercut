<?php $this->load->view('public_header'); ?>
<style >
.pt-6 {
  padding-top: 70px !important;
}
#image
{
      position: absolute;     
    margin: 0;
    padding: 0;
    width: 100%;
    height: 150px;
    outline: none;
    opacity: 0;
}
#drag p
{
   
  width: 100%;
  text-align: center;
  line-height: 170px;
  color: black;
  font-family: Arial;
}
#drag
{  
  /* height: 150px;
  border: 2px dashed #212529; */
}
.round
{
  border: 1px solid #9a9a9a !important;
  border-radius: 50px;
  padding-left: 15px;
  height: 45px;
}
.form-body .btn {
  border-radius: 100px;
}
#opening_time,#closing_time
{
  margin-left: 20px;
    width: 30%;
}
#days span
{
  float: left;
}
#days input
{
  float: right;
}
#days .toggle
{
   float: right;
}
.btn-green {
  background-color: #007bff;
}
select:-internal-list-box option:checked
{
  background-color: #70b12d;
  color: white;
}
.form-body input {
    font-weight: 100;
    border-top: 0px;
    border-left: 0px;
    border-right: 0px;
    padding-left: 0px;
    font-size: 14px;
    color: black;
    width:100%;
    }
.boot
{
  width: 100%;
}


/* Thulasi CSS starts */
.btn-text {
	letter-spacing: 5px;
}

.body-text {
	font-size: 13px;
	color: #4A4A4A !important;
	font-weight: 400;
	line-height: 16px;
}

.hyper {
	font-size: 13px;
	color: dodgerblue;
}

.left-pad {
	padding-left: 3%;
	padding-right: 3%;
}

.form-group {
	margin-bottom: 5px;
}

.row {
	display: flow-root !important;
}

.text-info {
	font-size: 11px;
	color: #ccc; 
}

.heading-1 {
	font-size: 18px !important;
	font-weight: 700;
	letter-spacing: -1px;
}


.text-label {
	font-size: 12px;
	font-weight: 100;
	color: #000;
	opacity: 0.6;
	letter-spacing: 0;
}

.txt-area-10 {
	border-radius: 10px;
}

textarea {
	border-radius: 5px; 
	padding: 5px;
	border:1px solid #aaa;
	font-size: 13px;
	color: #4A4A4A !important;
	font-weight: 400;
	line-height: 16px !important;
}



/* form starting stylings */

.group 			  { 
  position:relative;
  margin-bottom:25px; 
}

input 	{
  font-size:14px;
  font-weight: 100 !important;
  padding:10px 10px 5px 5px;
  display:block;
  width: 100%;
  border:none;
  border-bottom:1px solid #757575;
}

.placeholder {
  font-size: 14px;
  font-weight: 200;
  color: #aaa;
}

input:focus { outline:none; }

/* LABEL ====*/

label 	{
  color:#999; 
  font-size:14px;
  font-weight:normal;
     position: absolute;
  pointer-events:none;
  left:5px;
  top:10px;
  transition:0.2s ease all; 
  -moz-transition:0.2s ease all; 
  -webkit-transition:0.2s ease all;
}

/* active state */
input:valid ~ label {
  top:-5px;
  left: auto;
  font-size:11px;
  color: #ccc;
} 


input:focus ~ label{
  top:-10px;
  left: auto;
  font-size:11px;
  color: #007bff;
} 


/* BOTTOM BARS ================================= */
.bar 	{ position:relative; display:block; width:100%; }
.bar:before, .bar:after 	{
  content:'';
  height:2px; 
  width:0;
  bottom:1px; 
  position:absolute;
  background: #007bff; 
  transition:0.2s ease all; 
  -moz-transition:0.2s ease all; 
  -webkit-transition:0.2s ease all;
}
.bar:before {
  left:50%;
}
.bar:after {
  right:50%; 
}

/* active state */
input:focus ~ .bar:before, input:focus ~ .bar:after {
  width:50%;
} 

/* HIGHLIGHTER ================================== */
.highlight {
  position:absolute;
  height:60%; 
  width:100px; 
  top:25%; 
  left:0;
  pointer-events:none;
  opacity:0.5;
}

/* active state */
input:focus ~ .highlight {
  -webkit-animation:inputHighlighter 0.3s ease;
  -moz-animation:inputHighlighter 0.3s ease;
  animation:inputHighlighter 0.3s ease;
} 

/* ANIMATIONS ================ */
@-webkit-keyframes inputHighlighter {
	from { background:#5264AE; }
  to 	{ width:0; background:transparent; }
}
@-moz-keyframes inputHighlighter {
	from { background:#5264AE; }
  to 	{ width:0; background:transparent; }
}
@keyframes inputHighlighter {
	from { background:#5264AE; }
  to 	{ width:0; background:transparent; }
}


/* input styling ends */

/* Checkbox styling */
* {
  box-sizing: border-box;
}

/*
body {
  margin: 0;
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
}


body {
  background: #f5f5f5;
  font: 1em sans-serif;
}
*/

.checkbox, .radio {
  position: relative;
  cursor: pointer;
  padding: 0.2em;
}
.checkbox::selection, .radio::selection {
  background: transparent;
}
.checkbox input + span, .radio input + span {
  background: white;
  content: "";
  display: inline-block;
  margin: 0 0.5em 0 0;
  padding: 0;
  vertical-align: middle;
  width: 2em;
  height: 2em;
  transform: translate3d(0, 0, 0);
  	border: 1px solid #ddd;
  -webkit-backface-visibility: hidden;
}
.checkbox input + span::after, .radio input + span::after {
  content: "";
  display: block;
  transform: scale(0);
  transition: transform 0.2s;
}

.checkbox input:active + span, .radio input:active + span {
  box-shadow: 0 4px 8px rgba(0, 0, 0, .15);
}
.checkbox input:focus + span, .radio input:focus + span {
  box-shadow: 0 0 0 3px lightblue;
}
.checkbox input:checked + span::after, .radio input:checked + span::after {
  transform: scale(1);
}
.checkbox input, .radio input {
  position: absolute;
  cursor: pointer;
  opacity: 0;
}
.checkbox input + span {
  border-radius: 2px;
}
.checkbox input + span::after {
  background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz48IURPQ1RZUEUgc3ZnIFBVQkxJQyAiLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4iICJodHRwOi8vd3d3LnczLm9yZy9HcmFwaGljcy9TVkcvMS4xL0RURC9zdmcxMS5kdGQiPjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeD0iMHB4IiB5PSIwcHgiIHdpZHRoPSI2NHB4IiBoZWlnaHQ9IjY0cHgiIHZpZXdCb3g9IjAgMCA2NCA2NCIgZW5hYmxlLWJhY2tncm91bmQ9Im5ldyAwIDAgNjQgNjQiIHhtbDpzcGFjZT0icHJlc2VydmUiPjxnPjxwb2x5Z29uIHBvaW50cz0iMTMuNzA3LDMyLjI5MyAxMi4yOTMsMzMuODU0IDI0LjI5Myw0NiAyNS43MDcsNDYgNDkuNzA3LDIxLjg1NCA0OC4yOTMsMjAuMzY2IDI1LDQzLjYyMyAiLz48L2c+PC9zdmc+) no-repeat center;
  background-size: contain;
  width: 2em;
  height: 2em;
}
.radio input + span {
  border-radius: 100%;
}
.radio input + span::after {
  border-radius: 100%;
  margin: 0.65em;
  width: 0.75em;
  height: 0.75em;
}
.radio input:checked + span::after {
  background: black;
}

@media screen and (min-width: 768px) {
  .checkbox:hover input + span, .radio:hover input + span {
    box-shadow: 0 2px 4px rgba(0, 0, 0, .15);
  }
}

/* checkbox styling ends */


/* Thulasi CSS ends */


</style>
<div class="container">
  <div class="">
          
        <div class="col-md-12">
          <div class="row">
            <div class="py-6 mx-auto">
               <p class="pt-6 text-center"> </p>
              <!--
               <p class="pt-6 text-center" style="color: grey;">Step 3</p> 
              <h5 class=" text-center" style="    margin-bottom: 20px;">Creating Your Saloon Profile</h5> -->

              <div class="form-body mx-auto">
                <form class="form" method="POST" role="form" action="<?php echo base_url(); ?>shop/shop/update_saloon/<?= $this->uri->segment(3); ?>" enctype="multipart/form-data" autocomplete="off" id="formSignup">
                
                  <!-- <form class="form" role="form" action="<?php echo base_url("shop/Shop") ?>" autocomplete="off" id="formSignup"> -->
                    
       
       
                    
     	<div class="group">      
      		<input type="text" class="placeholder" name="name" value="<?= $data->name; ?>"  required autofocus="autofocus">
      		<span class="highlight"></span>
      		<span class="bar"></span>
      		<label class="placeholder">Salon Name</label>
   	</div>
                  
        <div class="group">      
      		<input type="text" class="placeholder" name="website" value="<?= $data->website; ?>"  required >
      		<span class="highlight"></span>
      		<span class="bar"></span>
      		<label class="placeholder">Website</label>
   	</div> 
   	
   	
   	<h4><b class="heading-1">Contact details</b></h4> 
   	<div class="group">      
      		<input type="number" class="placeholder" name="mobileno" value="<?= $data->mobileno; ?>"  required oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"    
    maxlength = "10">
      		<span class="highlight"></span>
      		<span class="bar"></span>
      		<label class="placeholder">Mobile Number</label>
   	</div>
   	
   	<div class="group">      
      		<input type="text" class="placeholder" name="locality_name" value="<?= $data->locality_name; ?>"  required >
      		<span class="highlight"></span>
      		<span class="bar"></span>
      		<label class="placeholder">Locality</label>
   	</div> 
   	
   	<div class="group">      
      		<input type="text" class="placeholder" name="user_address" value="<?= $data->user_address; ?>"  required >
      		<span class="highlight"></span>
      		<span class="bar"></span>
      		<label class="placeholder">Address</label>
   	</div>
   	           
	<h4><b class="heading-1">Map Coordinates</b></h4>
        <p style="color: grey;" class="body-text">This section helps your customers to navigate to your Saloon without going in the wrong direction</p>
        <p><a class="hyper" id="currentlocation" href="javascript:void(0)">Click to get coordinates</a></p>
        <br>                    
                    
        <div class="group">      
      		<input type="text" class="placeholder" name="lat" value="<?= $data->lat; ?>"  required >
      		<span class="highlight"></span>
      		<span class="bar"></span>
      		<label class="placeholder">Latitude</label>
   	</div>
   	
   	<div class="group">      
      		<input type="text" class="placeholder" name="lon" value="<?= $data->lon; ?>"  required >
      		<span class="highlight"></span>
      		<span class="bar"></span>
      		<label class="placeholder">Longitude</label>
   	</div>
        
        	    
        <h4><b class="heading-1">Select Service Categories</b></h4>                  
           	<div class="form-group">
                    <select name="service_offered[]" multiple class="form-control" id="exampleFormControlSelect2">                    
                    <?php  $services = $this->db->get('services')->result(); ?>
                      <?php if(!empty($services)){ ?>
                      <?php foreach($services as $s){ ?>
                      <?php if(strpos($data->service_offered, $s->name) !== false){ ?>
                      <option selected value="<?= $s->name; ?>"><?= $s->name; ?></option>
                      <?php }else{ ?>
                       <option value="<?= $s->name; ?>"><?= $s->name; ?></option>
                      <?php } ?>
                      <?php } } ?>
                    </select>
                </div>
        
        <BR><br>
        <h4><b class="heading-1">Working Days and Saloon Timings</B></h4>
        <!-- <p style="color: grey;">This section helps your customers to navigate to your Saloon without going in the wrong direction</p> -->
        <div class="group">      
      		<input type="time" class="placeholder" name="opening_time" value="<?= $data->opening_time; ?>"  required >
      		<span class="highlight"></span>
      		<span class="bar"></span>
      		<label class="placeholder">Opening Time</label>
   	</div>
   	
   	<div class="group">      
      		<input type="time" class="placeholder" name="closing_time" value="<?= $data->closing_time; ?>"  required >
      		<span class="highlight"></span>
      		<span class="bar"></span>
      		<label class="placeholder">Closing Time</label>
   	</div>
        
        <Br>
 <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet"> 
                  <div class="form-group" id="days">
       	<h4><B class="heading-1">Working Days</B></h4>
                    <?php if($data->days != "null" ){  ?>
            <?php $su = "grey";$sa = "grey";$fr = "grey";
            $mo = "grey"; $tu = "grey";$we = "grey";$th = "grey"; ?>
            <?php $dayss =  json_decode($data->days); ?>
            <?php foreach ($dayss as $key ) {  ?>
            <?php if($key == "Su"){ ?>
            <?php $su = "Checked"; ?>
            <?php } ?>
            <?php if($key == "Mo"){ ?>
            <?php $mo = "Checked"; ?>
            <?php } ?>
            <?php if($key == "Tu"){ ?>
            <?php $tu = "Checked"; ?>
            <?php } ?>
            <?php if($key == "We"){ ?>
            <?php $we = "Checked"; ?>
            <?php } ?>
            <?php if($key == "Th"){ ?>
            <?php $th = "Checked"; ?>
            <?php } ?>
            <?php if($key == "Fr"){ ?>
            <?php $fr = "Checked"; ?>
            <?php } ?>
            <?php if($key == "Sa"){ ?>
            <?php $sa = "Checked"; ?>
            <?php } ?>    
            <?php } } ?>
                   <label class="boot body-text" style="position: relative; pointer-events: auto;"><span>Sunday</span><input type="checkbox" name="days[]" value="Su" <?= $su; ?> data-toggle="toggle"></label> 
                   <Br>
                   <label class="boot body-text" style="position: relative; pointer-events: auto;"><span>Monday</span><input type="checkbox" name="days[]" value="Mo" <?= $mo; ?>  data-toggle="toggle"></label> 
                   <br>
                   <label class="boot body-text" style="position: relative; pointer-events: auto;"><span>Tuesday</span><input type="checkbox" name="days[]" value="Tu" <?= $tu; ?>  data-toggle="toggle"></label> 
                   <br>
                   <label class="boot body-text" style="position: relative; pointer-events: auto;"><span>Wednesday</span><input type="checkbox" name="days[]" value="We" <?= $we; ?>  data-toggle="toggle"></label> 
                   <br>
                   <label class="boot body-text" style="position: relative; pointer-events: auto;"><span>Thursday</span><input type="checkbox" name="days[]" value="Th" <?= $th; ?>  data-toggle="toggle"></label> 
                   <br>
                   <label class="boot body-text" style="position: relative; pointer-events: auto;"><span>Friday</span><input type="checkbox" name="days[]" value="Fr" <?= $fr; ?>  data-toggle="toggle"></label> 
                   <br>
                   <label class="boot body-text" style="position: relative; pointer-events: auto;"><span>Saturday</span><input type="checkbox" name="days[]" value="Sa" <?= $sa; ?> data-toggle="toggle"></label> 
                  </div>
                  
  		  <br>
                  <br>
            <h4><B class="heading-1">Parking</B></h4>
                  <!-- <p style="color: grey;">This section helps your customers to navigate to your Saloon without going in the wrong direction</p> -->
                  <?php if($data->parking != "null" ){  ?>
            <?php $bike = "";$car = "";$common = "";$no = ""; ?>
            <?php $parking =  json_decode($data->parking); ?>
            <?php foreach ($parking as $key ) {  ?>
            <?php if($key == "car"){ ?>
            <?php $car = "Checked"; ?>
            <?php } ?>
            <?php if($key == "bike"){ ?>
            <?php $bike = "Checked"; ?>
            <?php } ?>
            <?php if($key == "common"){ ?>
            <?php $common = "Checked"; ?>
            <?php } ?>
            <?php if($key == "no"){ ?>
            <?php $no = "Checked"; ?>
            <?php } ?>
            
    
           <?php } } ?>
                <!-- <div class="row" style="display: flex;">
                  <div class="col-3">
                    <div class="form-check"> -->
                    
		
		<label class="checkbox body-text" style="pointer-events: auto;">
    		<input name="parking[]" value="car" id="check1" type="checkbox" <?= $car; ?> onchange="console.log('changed');">
    		<span></span>
    		Car
		</label><br>
		
		<label class="checkbox body-text" style="pointer-events: auto;">
    		<input name="parking[]" value="bike" id="check1" type="checkbox" <?= $bike; ?> onchange="console.log('changed');">
    		<span></span>
    		Bike
		</label><br>
		
		<label class="checkbox body-text" style="pointer-events: auto;">
    		<input name="parking[]" value="common" id="check1" type="checkbox" <?= $common; ?> onchange="console.log('changed');">
    		<span></span>
    		Common parking
		</label><br>
		
		<label class="checkbox body-text" style="pointer-events: auto;">
    		<input name="parking[]" value="no" id="check1" type="checkbox" <?= $no; ?> onchange="console.log('changed');">
    		<span></span>
    		No parking
		</label><br>
		
		
		<br>
                <Br>
                <div class="clearfix"></div>              
           <h4><B class="heading-1">Saloon Image</B></h4>
                <div class="form-group" id="drag"> 
                  <input type="file" class="imagefor" name="picture"  id="image">
                  <p><img width="100%"  src="<?= base_url(); ?>uploads/<?= $data->image; ?>" ></p>
                </div>                
                <br>	
                                   
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                          
                    
                  <!--  
                    <div class="form-group">                
                      <label class="sr-only" for="email">Salon Name</label>
                      <input type="text" class="form-control  form-control-lg rounded-0 round" name="name" id="name" placeholder="Shop Name" value="<?= $data->name; ?>" required autofocus="autofocus">  
                    </div>                   
                    
                    <div class="form-group">

                      <label class="sr-only" for="website">Website</label>

                      <input type="text" class="form-control  form-control-lg rounded-0" name="website" id="website" placeholder="Website" value="<?= $data->website; ?>" required autofocus="autofocus">
                    </div>                  
                    
                    <h4><b>Address</b></h4>
                    <div class="form-group">

                      <label class="sr-only" for="contactno">Contact No</label>

                      <input type="text" class="form-control  form-control-lg rounded-0" name="mobileno" id="contactno" value="<?= $data->mobileno; ?>" placeholder="Contact Number" required autofocus="autofocus">

                    </div>
                    <div class="form-group">

                      <label class="sr-only" for="locality">Locality</label>

                      <input type="text" class="form-control  form-control-lg rounded-0" value="<?= $data->locality_name; ?>" name="locality_name" id="locality" placeholder="Locality" required autofocus="autofocus">

                    </div>
                    <div class="form-group">

                      <label class="sr-only" for="streetaddress">Street Address</label>

                      <input type="text" class="form-control  form-control-lg rounded-0" value="<?= $data->user_address; ?>" name="user_address" id="streetaddress" placeholder="Street Address" required autofocus="autofocus">

                    </div>
                    
                   
                    <br>
                    <Br>
                    <h4><b>Map Coordinates</b></h4>
                    <p style="color: grey;">This section helps your customers to navigate to your Saloon without going in the wrong direction</p>
                    <p><a id="currentlocation" href="javascript:void(0)">Click to Get Coordinates</a></p>
                    <br>
                    <div class="form-group">

                      <label class="sr-only" for="latitude">Latitude</label>

                      <input type="number" class="form-control  form-control-lg rounded-0" value="<?= $data->lat; ?>" name="lat" id="lat" placeholder="Latitude" required autofocus="autofocus">

                    </div>
                    <div class="form-group">

                      <label class="sr-only" for="longitude">Longitude</label>

                      <input type="number" class="form-control  form-control-lg rounded-0" value="<?= $data->lon; ?>" name="lon" id="lon" placeholder="Longitude" required autofocus="autofocus">

                    </div>
                    
                     
                    <Br>
                    <Br>
                    <h4><b>Select Service Categories</b></h4>
                   
                    <div class="form-group">
                    <select name="service_offered[]" multiple class="form-control" id="exampleFormControlSelect2">
                     
                    <?php  $services = $this->db->get('services')->result(); ?>
                      <?php if(!empty($services)){ ?>
                      <?php foreach($services as $s){ ?>
                      <?php if(strpos($data->service_offered, $s->name) !== false){ ?>
                      <option selected value="<?= $s->name; ?>"><?= $s->name; ?></option>
                      <?php }else{ ?>
                       <option value="<?= $s->name; ?>"><?= $s->name; ?></option>
                      <?php } ?>

                      <?php } } ?>
                    </select>
                  </div>
                  
                  
                  
                  <Br>
                  <br>
                  <h4><B>Working Days and Saloon Timings</B></h4>
                  <p style="color: grey;">This section helps your customers to navigate to your Saloon without going in the wrong direction</p>
                    
                  <div class="form-group"> 
                      <label>Opening Time <input type="text" value="<?= $data->opening_time; ?>" placeholder="9:30 AM" id="opening_time" name="opening_time" class="" required></label>
                      
                    </div>
                    <div class="form-group"> 
                      <label>Closing Time <input type="text" value="<?= $data->closing_time; ?>" placeholder="12:30 PM" id="closing_time" name="closing_time" class="" required></label>                      
                    </div>
                   
                    
                    <Br>
 <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

                  <div class="form-group" id="days">
                    <h4><B>Working Days</B></h4>
                    <Br>
                    <?php if($data->days != "null" ){  ?>
            <?php $su = "grey";$sa = "grey";$fr = "grey";
            $mo = "grey"; $tu = "grey";$we = "grey";$th = "grey"; ?>
            <?php $dayss =  json_decode($data->days); ?>
            <?php foreach ($dayss as $key ) {  ?>
            <?php if($key == "Su"){ ?>
            <?php $su = "Checked"; ?>
            <?php } ?>
            <?php if($key == "Mo"){ ?>
            <?php $mo = "Checked"; ?>
            <?php } ?>
            <?php if($key == "Tu"){ ?>
            <?php $tu = "Checked"; ?>
            <?php } ?>
            <?php if($key == "We"){ ?>
            <?php $we = "Checked"; ?>
            <?php } ?>
            <?php if($key == "Th"){ ?>
            <?php $th = "Checked"; ?>
            <?php } ?>
            <?php if($key == "Fr"){ ?>
            <?php $fr = "Checked"; ?>
            <?php } ?>
            <?php if($key == "Sa"){ ?>
            <?php $sa = "Checked"; ?>
            <?php } ?>
    
           <?php } } ?>
                   <label class="boot" style="position: relative;"><span>Sunday</span><input type="checkbox" name="days[]" value="Su" <?= $su; ?> data-toggle="toggle"></label> 
                   <Br>
                   <label class="boot" style="position: relative;"><span>Monday</span><input type="checkbox" name="days[]" value="Mo" <?= $mo; ?>  data-toggle="toggle"></label> 
                   <br>
                   <label class="boot" style="position: relative;"><span>Tuesday</span><input type="checkbox" name="days[]" value="Tu" <?= $tu; ?>  data-toggle="toggle"></label> 
                   <br>
                   <label class="boot" style="position: relative;"><span>Wednesday</span><input type="checkbox" name="days[]" value="We" <?= $we; ?>  data-toggle="toggle"></label> 
                   <br>
                   <label class="boot" style="position: relative;"><span>Thirsday</span><input type="checkbox" name="days[]" value="Th" <?= $th; ?>  data-toggle="toggle"></label> 
                   <br>
                   <label class="boot" style="position: relative;"><span>Friday</span><input type="checkbox" name="days[]" value="Fr" <?= $fr; ?>  data-toggle="toggle"></label> 
                   <br>
                   <label class="boot" style="position: relative;"><span>Saturday</span><input type="checkbox" name="days[]" value="Sa" <?= $sa; ?> data-toggle="toggle"></label> 
                  </div>
                   
                  
                  <Br>
                  <br>
                  <h4><B>Additional Information</B></h4>
                  <p style="color: grey;">This section helps your customers to navigate to your Saloon without going in the wrong direction</p>
                  <br>
                  <?php if($data->parking != "null" ){  ?>
            <?php $bike = "";$car = "";$common = "";$no = ""; ?>
            <?php $parking =  json_decode($data->parking); ?>
            <?php foreach ($parking as $key ) {  ?>
            <?php if($key == "car"){ ?>
            <?php $car = "Checked"; ?>
            <?php } ?>
            <?php if($key == "bike"){ ?>
            <?php $bike = "Checked"; ?>
            <?php } ?>
            <?php if($key == "common"){ ?>
            <?php $common = "Checked"; ?>
            <?php } ?>
            <?php if($key == "no"){ ?>
            <?php $no = "Checked"; ?>
            <?php } ?>
            
    
           <?php } } ?>
                <div class="row" style="display: flex;">
                  <div class="col-3">
                    <div class="form-check">
                      <input style="height: 35px;width: 35px;" class="form-check-input" name="parking[]" value="bike" type="checkbox" <?= $bike; ?> id="gridCheck">
                      <label style="margin-left: 10px;padding-top: 10px;" class="form-check-label" for="gridCheck">
                        Bike
                      </label>
                    </div>
                  </div>
                  <div class="col-2">
                    
                  </div>
                   <div class="col-6">
                    <div class="form-check">
                      <input style="height: 35px;width: 35px;" class="form-check-input" <?= $car; ?> name="parking[]" value="car" type="checkbox" id="gridCheck">
                      <label style="margin-left: 5px;padding-top: 10px;" class="form-check-label" for="gridCheck">
                        Car
                      </label>
                    </div>
                  </div>
                </div>
                <Br>
                <div class="row" style="display: flex;">
                  <div class="col-6">
                    <div class="form-check">
                      <input style="height: 35px;width: 35px;" class="form-check-input" <?= $common; ?> name="parking[]" value="common" type="checkbox" id="gridCheck">
                      <label style="margin-left: 5px;padding-top: 10px;" class="form-check-label" for="gridCheck">
                        Common Parking
                      </label>
                    </div>
                  </div>
                  
                   <div class="col-6">
                    <div class="form-check">
                      <input style="height: 35px;width: 35px;" class="form-check-input" <?= $no; ?> name="parking[]" value="no" type="checkbox" id="gridCheck">
                      <label style="margin-left: 10px;padding-top: 10px;" class="form-check-label" for="gridCheck">
                       No Parking
                      </label>
                    </div>
                  </div>
                </div>
                 -->
                 
            
            
               <!--  
                
                <br>
                <Br>
                <div class="clearfix"></div>                
                <h4><B>Saloon Image</B></h4>
                <div class="form-group" id="drag"> 
                  <input type="file" class="imagefor" name="picture"  id="image">
                  <p><img height="180" src="<?= base_url(); ?>uploads/<?= $data->image; ?>" ></p>
                </div>
                <br>
                
               -->
            
            
            
            
            
            
            
            
            
            
                
                <div class="form-group">

                      <label class="sr-only" for="Describe about Your Saloon">Describe About Your Salon</label>
                      <textarea class="body-text" style="width: 100%; height: 180px;" maxlength="250" rows="6" class="form-control  form-control-lg rounded-0 txt-area-10 text-label" name="about_me" id="gBann" placeholder="Describe about Your Saloon" required onKeyUp="toCount('gBann','uBann','{CHAR} characters remaining',250);" ><?= $data->about_me; ?></textarea>
			<span id="uBann" class="text-info">250 characters                    
		</div>
                    <Br>
                <Br>
                <!-- <div class="form-group">
                  <label for="exampleInputColor1">Color</label>
                  <input type="color" name="color" class="form-control" id="exampleInputColor1" required>
                </div> -->
                    <div class="form-group form-group3">

                      <div class="text-center terms">

                       <p>By signing up you agree to <span><a href="<?= base_url(); ?>terms_condition">Terms</a></span></p>

                     </div>

                   </div>

                   <button type="submit" style="font-size: 13px;" class="btn btn-text btn-green btn-sz">UPDATE</button>

                 </form>

                

               </div>

             </div>

             <!--/col-->

           </div>

           <!--/row-->

         </div>

         <!--/col-->

       </div>

       <!--/row-->

     </div>



     <!--/container-->

     <?php $this->load->view('layouts/footer'); ?>

    
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="<?php  echo base_url('assets/admin/datepicker/moment.min.js'); ?>"></script>
<script src="<?php  echo base_url('assets/admin/datepicker/datetimepicker.min.js'); ?>"></script>
<script >

  $("#opening_time").datetimepicker({
pickDate: false
});
</script>
<script >
  $("#closing_time").datetimepicker({
pickDate: false
});
</script>
<style >
  .inputDnD {
  .form-control-file {
    position: relative;
    width: 100%;
    height: 100%;
    min-height: 6em;
    outline: none;
    visibility: hidden;
    cursor: pointer;
    background-color: #c61c23;
    box-shadow: 0 0 5px solid currentColor;
    &:before {
      content: attr(data-title);
      position: absolute;
      top: 0.5em;
      left: 0;
      width: 100%;
      min-height: 6em;
      line-height: 2em;
      padding-top: 1.5em;
      opacity: 1;
      visibility: visible;
      text-align: center;
      border: 0.25em dashed currentColor;
      transition: all 0.3s cubic-bezier(.25, .8, .25, 1);
      overflow: hidden;
    }
    &:hover {
      &:before {
        border-style: solid;
        box-shadow: inset 0px 0px 0px 0.25em currentColor;
      }
    }
  }
}

// PRESENTATIONAL CSS
body {
  background-color: #f7f7f9;
}
</style>
<script >
  function readUrl(input) {
  if (input.files && input.files[0]) {
    let reader = new FileReader();
    reader.onload = (e) => {
      let imgData = e.target.result;
      let imgName = input.files[0].name;
      $('#drag p').text(imgName + " file(s) selected");
      input.setAttribute("data-title", imgName);
      console.log(e.target.result);
    }
    reader.readAsDataURL(input.files[0]);
  }

}
</script>
<script >
  $(document).ready(function(){
  $('.imagefor').change(function () {
    console.log('reached');
    $('#drag p').text(this.files.length + " file(s) selected");
  });
});
</script>
<script type="text/javascript">
        $("#currentlocation").click(function(){
          if ("geolocation" in navigator){
            navigator.geolocation.getCurrentPosition(function(position){ 
              var lat=position.coords.latitude; 
              var lang=position.coords.longitude;
              $("#lat").val(lat);
              $("#lon").val(lang);
            
          });
          }else{
            console.log("Browser doesn't support geolocation!");
          }

        });
      </script>
     
     
<!-- character count -->      
<script type="text/javascript">
function toCount(entrance,exit,text,characters) {  
    var entranceObj=document.getElementById(entrance);  
    var exitObj=document.getElementById(exit);  
    var length=characters - entranceObj.value.length;  
    if(length <= 0) {  
    length=0;  
    text='<span class="disable"> '+text+' <\/span>';  
    entranceObj.value=entranceObj.value.substr(0,characters);  
    }  
    exitObj.innerHTML = text.replace("{CHAR}",length);  
    }
    </script>

<!-- character count ends -->    