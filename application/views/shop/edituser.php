<?php $this->load->view('public_header'); ?>
<style >
  .left-nav {
    position: fixed;
    overflow: hidden;
    width: 295px;
    z-index: 3333;
  
   }
   .hide-nav .bottom-3 {
    
  }
  .btn-green {
    background-color: #007bff;
}
  .height100
  {
    height: 100% !important;
  }
  .height10{
    height: 10%;
   }
  .bottom-3
  {
    bottom: 16%;
  }
  .grey
  {
    color: grey;
  }
  p
  {
        font-weight: bold;
    color: black;
  }
   @media only screen and (max-width: 900px) {
   .left-nav {
    position: fixed;
    width: 100%;
    z-index: 3333;
    height: 12%;
   }
   .height10{
    height: 14%;
   }
   .mx-auto {
   padding-top: 59px;
}
   }
   
/* Thulasi CSS starts */
.pt-6 {
	padding-top: 20% !important;
}

.pt-26 {
	padding-top: 30% !important;
}

.text-info {
	font-size: 14px;
	color: #3D8DFB !important;	
}

.text-danger {
	font-size: 14px;
	color: #FF3E2D !important;	
}

.text-success {
	font-size: 10px;
	color:#70B12D !important;
}

.grey {
	font-size: 12px;
	color: #878787 !important;
	font-weight: 100;
}

.body-text {
	font-size: 14px;
	color: #000 !important;
	font-weight: 400;
}

.round
{
  border: 1px solid #9a9a9a !important;
    border-radius: 50px;
    padding-left: 15px;
    height: 45px;
    font-size: 12px; 
}

.left-pad {
	padding-left: 3%;
	padding-right: 3%;
}

.btn {
    border-radius: 100px;
    letter-spacing: 5px;
}

.rem-pad-mar {
	padding-left: 0;
	padding-right: 0;
}
   
</style>

<div class="container">
        <div class="row">
       



              <div class="mx-auto container">
                <Br>
                
                <div class="col-md-12 left-pad pt-6">
                  <form class="form" method="POST" action="<?= base_url(); ?>shop/shop/update_user">
                  <div class="col-md-12 rem-pad-mar">
                    <p class="text-center" style="color: grey; font-weight: 100;">Edit User</p>
                    <Br>
                    
                    <div class="form-group">
                      <!-- <label class="grey">Username</label> -->
                      <input type="text" placeholder="Username" name="username" class="form-control round " required value="<?= $data->username; ?>">
                    </div>
                    <div class="form-group">
                      <!-- <label class="grey">Email</label> -->
                      <input type="email" placeholder="Email" name="email" value="<?= $data->email; ?>" required class="form-control round">
                    </div>
                    <div class="form-group">
                      <!-- <label class="grey">Phone Number</label> -->
                      <input type="text" placeholder="Phone" name="phone" value="<?= $data->phone; ?>" required class="form-control round">
                    </div>
                    <div>
                      
                    </div>
                  </div>
                  <div class="form-group">
                    <button type="submit" class="btn btn-text btn-green btn-sz">Update</button>
                    
                  </div>
                  </form>
                  
                </div>
                
              </div>
              <!--/col-->
            </div>
            <!--/row-->
          </div>
          <!--/col-->
        </div>
        <!--/row-->
      </div>

      <?php $this->load->view('layouts/footer'); ?>
      <script type='text/javascript' src='<?php echo base_url('assets/admin/js/backend/main.js'); ?>'></script>
    

