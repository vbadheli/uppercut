<?php $this->load->view('public_header'); ?>
<style >
  .left-nav {
    position: fixed;
    overflow: hidden;
    width: 295px;
    z-index: 3333;
  
   }
   #edituser
   {
        position: fixed;
    width: 100%;
   }
   .hide-nav .bottom-3 {
    
  }
  .height100
  {
    height: 100% !important;
  }
  .height10{
    height: 10%;
   }
  .bottom-3
  {
    bottom: 16%;
  }
  .grey
  {
    color: grey;
  }
  p
  {
        font-weight: bold;
    color: black;
  }
   @media only screen and (max-width: 900px) {
   .left-nav {
    position: fixed;
    width: 100%;
    z-index: 3333;
    height: 12%;
   }
   .height10{
    height: 14%;
   }
   .mx-auto {
   padding-top: 59px;
}
   }
   
/* Thulasi CSS starts */
.pt-6 {
	padding-top: 20% !important;
	padding-left: 44px; 
}

.pt-26 {
	padding-top: 30% !important;
}

.text-info {
	font-size: 14px;
	color: #3D8DFB !important;	
}

.text-danger {
	font-size: 14px;
	color: #FF3E2D !important;	
}

.text-success {
	font-size: 10px;
	color:#70B12D !important;
}

.grey {
	font-size: 12px;
	color: #878787 !important;
	font-weight: 100;
}

.body-text {
	font-size: 14px;
	color: #000 !important;
	font-weight: 400;
}

/* Thulasi CSS ends */
</style>

<div class="container">
        <div class="row">
        

<?php $id = get_session('userid');  ?>
<?php $data = $this->db->where('id',$id)->get('shop_signup')->row(); ?>

              <div class="mx-auto container">
                <Br>
                <div class="col-md-12 pt-6">
                  <div class="col-md-12">
                    <span class="text-success">ACTIVE</span>
                    <Br>
                    <span class="grey">USER NAME</span>
                    <p class="body-text"><?= $data->username; ?></p>
                    
             
                    <span class="grey">EMAIL</span>
                    <p class="body-text"><?= $data->email; ?></p>
                    
                    <span class="grey">PHONE NUMBER</span>
                    <p class="body-text"><?= $data->phone; ?></p>
                    
                    
                    <br>
                    <div class="pt-26">
                    

                    <h5><a class="text-info" href="<?= base_url(); ?>edituser/<?= $data->id; ?>">EDIT PROFILE</a></h5>
                    <br>
                    
                    <h5>
                    <a class="text-danger" href="<?= base_url(); ?>deleteaccount">DELETE PROFILE</a></h5>
                    </div>
                  </div>
                  
                </div>
                
              </div>
              <!--/col-->
            </div>
            <!--/row-->
          </div>
          <!--/col-->
        </div>
        <!--/row-->
      </div>

      <?php $this->load->view('layouts/footer'); ?>
      <script type='text/javascript' src='<?php echo base_url('assets/admin/js/backend/main.js'); ?>'></script>
    

