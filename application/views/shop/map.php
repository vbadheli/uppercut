  <!DOCTYPE html>

  <html lang="en">

  <head>

    <meta charset="UTF-8">
      <meta charset="UTF-8">
  <meta charset="utf-8">
  <meta http-equiv="content-type" content="text/html;charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" id="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Shop</title>

    <!-- Bootstrap core CSS -->

    <link href="<?php  echo base_url('assets/vendor/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <!-- Custom styles for this template -->

    <link href="<?php  echo base_url('assets/css/style.css'); ?>" rel="stylesheet">

    <link href="<?php  echo base_url('assets/vendor/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600|Roboto:300" rel="stylesheet">

    <!-- Plugin CSS -->

    <link href="<?php  echo base_url('assets/vendor/magnific-popup/magnific-popup.css'); ?>" rel="stylesheet">

    <!-- Custom styles for this template -->

    <link href="<?php  echo base_url('assets/css/style.css'); ?>" rel="stylesheet">

   

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-100486865-1"></script>

    <script>

      window.dataLayer = window.dataLayer || [];

      function gtag(){dataLayer.push(arguments)};

      gtag('js', new Date());

      gtag('config', 'UA-100486865-1');

    </script>

    <style type="text/css">

      i {

        border: solid white;

        border-width: 0 3px 3px 0;

        display: inline-block;

        padding: 3px;

      }

      .left {

        transform: rotate(135deg);

        -webkit-transform: rotate(135deg);

      }
      .left-nav
      {
        padding: 0;background: white;overflow: hidden;
      }
      .googlemap
      {
        width: 100%;height: 637px;
      }
      #mobileresult
   {
    display: none;
   }
      @media only screen and (max-width: 900px) {
   .left-nav {
    position: fixed;
    width: 100%;
    z-index: 3333;
    height: 9%;
   }
   #result
   {
    display: none;
   }
   #mobileresult
   {
   display: inline;
    background: white;
    clear: both;
    float: left;
    width: 100%;
    padding-bottom: 30px;
   }
   .height100 #result
   {display: initial;}
   .left-nav
   {
    overflow: initial;
   }
   .googlemap
   {
    width: 100%;height: 400px;
    position: relative;
   }
   .Shop_name div.left_border {
    display: none;
   }
   }

    </style>

  </head>

  <body style="height: 100%;">

    <div id="loader"></div>

    <!--left nav-->
    <div class="left-nav"  >

    <div class="top-body">
      <div class="toggleTab">
        <div class="tog-icon"></div>
        <div class="tog-icon"></div>
      </div>
    <span class="brand-name">
    <a class="brand-logo" href="<?php echo base_url('home'); ?>"><img src="<?php echo base_url('assets/img/logo.png'); ?>"></a>
    </span>
    </div>
    <div id="result">

    <div class="form-dash toggled-nav">

      <form class="form" role="form" autocomplete="off"  id="formLogin" action="<?php echo base_url('search'); ?>" method="POST">



        <div class="form-group search-box">
          <?php if(!empty($this->session->userdata('search'))){ ?>
          <input type="search" class="form-control form-control-lg" name="search" id="address" placeholder="Search by shop name or locality" value="<?php echo $this->session->userdata('search'); ?>" >

          <?php }else{ ?>
          <input type="search" class="form-control form-control-lg" name="search" id="address" placeholder="Search by shop name or locality" value="" >
          <?php  } ?>


          <input type="hidden" name="lat" value="" id="lat">

          <input type="hidden" name="lon" value="" id="lon">    

          <button type="submit" class="search-dash-btn"><i class="fa fa-search"></i></button>

        </div>

      </form>

      <div class="form-url"  style="display: none;" > 
        <p class="text-center" ><span class="map"><i class="fa fa-map-marker"></i> <a href="javascript:void(0)" id="currentlocation">Use my current location</a></span></p>

      </div>

    </div>

    <div class="row dashhrow">
      <?php if(isset($result)){ ?>
      <div class="col-sm-12 search-result">Search result <span class="num_of_search_result">  <?php  echo count($result) - 1 ; ?></span></div>
        <?php }else{ ?>
        <div class="col-sm-12 search-result">Search result <span class="num_of_search_result"> </span></div>
        <?php } ?>
      

    </div>

    <!-- <div class="leftbar"> -->

    <?php 

    if(isset($result)):     foreach ($result as $key => $value):    ?>
    <?php if(isset($value->id) ){  ?>
    <div onClick="window.location.href= '<?php echo base_url('dashboard2'); ?>/<?php echo base64_encode($value->id); ?>' " style="cursor: pointer;" >
      
     <div class="col-md-12 marg10">

      <div class="Shop_name"><div class="left_border"></div> <span><a href="<?php echo base_url('dashboard2');?>/<?php echo base64_encode($value->id); ?>" style="color: black;font-weight: 600 "> <?php echo $value->name; ?></a></span></div>

    </div>

    <div class="row dashhrow">
      <?php if(isset($value->city)){ ?>
      <div class="col-sm-6 shop_area"><?php echo $value->city; ?></div>
      <?php }else{ ?>
      <?php } ?>
      <div class="col-sm-6 kms"></div>

    </div>

  </div>
    <?php } ?>


<?php endforeach; endif; ?>
  </div>
</div>


<div class="main">
  <div class="">
    <div class="col-md-12" style="padding: 0px;">
      <div class="">
        <div class=" header-body" style="border-bottom: none;    height: 0px;padding: 0px;">
          <div class="" href="" >
            <div class="mr-3" id="oval">
              <div class="toggleTab man">
            <div class="tog-icon"></div>
            <div class="tog-icon"></div>
          </div>
            </div>
            <div class="s-name">
              <h1><a href="<?= base_url(); ?>editprofile"></a></h1>
            </div>
          </div>
          <!-- <div class="coll">
            <div class="btn-body">
              <button class="bttn bttn-blue">SAVE</button>
              <button class="bttn bttn-border-blue">CANCEL</button>
            </div>
          </div> -->
        </div>

  <div class="row">

    <div id="googleMap" class="googlemap col-sm-0 col-xs-12" style="">

    </div>
    <div id="mobileresult" class="col-sm-0 col-xs-12"> 

    <div class="form-dash">

      <form class="form" role="form" autocomplete="off"  id="formLogin" action="<?php echo base_url('search'); ?>" method="POST">



        <div class="form-group search-box">
          <?php if(isset($result)){ ?>
          <input type="search" class="form-control form-control-lg" name="search" id="address" placeholder="Search by shop name or locality" value="<?php echo $result['search']; ?>" >
          <?php }else{ ?>
          <input type="search" class="form-control form-control-lg" name="search" id="address" placeholder="Search by shop name or locality" value="" >
          <?php  } ?>


          <input type="hidden" name="lat" value="" id="lat">

          <input type="hidden" name="lon" value="" id="lon">    

          <button type="submit" class="search-dash-btn"><i class="fa fa-search"></i></button>

        </div>

      </form>

      <div class="form-url"  style="display: none;" > 
        <p class="text-center" ><span class="map"><i class="fa fa-map-marker"></i> <a href="javascript:void(0)" id="currentlocation">Use my current location</a></span></p>

      </div>

    </div>

    <div class="row dashhrow">
      <?php if(isset($result)){ ?>
      <div class="col-sm-12 search-result">Search result <span class="num_of_search_result">  <?php  echo count($result)-1; ?></span></div>
        <?php }else{ ?>
        <div class="col-sm-12 search-result">Search result <span class="num_of_search_result"> </span></div>
        <?php } ?>
      

    </div>

    <!-- <div class="leftbar"> -->

    <?php 

    if(isset($result)):     foreach ($result as $key => $value):    ?>
    <?php if(isset($value->id) ){  ?>
    <div onClick="window.location.href= '<?php echo base_url('dashboard2'); ?>/<?php echo base64_encode($value->id); ?>' " style="cursor: pointer;" >
      
     <div class="col-md-12 marg10">

      <div class="Shop_name"><div class="left_border"></div> <span><a href="<?php echo base_url('dashboard2');?>/<?php echo base64_encode($value->id); ?>" style="color: black;font-weight: 600 "> <?php echo $value->name; ?></a></span></div>

    </div>

    <div class="row dashhrow">
      <?php if(isset($value->city)){ ?>
      <div class="col-sm-6 shop_area"><?php echo $value->city; ?></div>
      <?php }else{ ?>
      <?php } ?>
      <div class="col-sm-6 kms"></div>

    </div>

  </div>
    <?php } ?>


<?php endforeach; endif; ?>
  </div>

  </div>


</div>
</div>
</div>
</div>

</div>

<!--/container-->

<?php $this->load->view('layouts/footer'); ?>

<script type='text/javascript' src='<?php echo base_url('assets/admin/js/backend/main.js'); ?>'></script> 

<!-- <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8G1mE0PUuoN79b3iqE1PB27H0IisuCAg&callback=initMap"> -->

</script>

<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8G1mE0PUuoN79b3iqE1PB27H0IisuCAg&callback=myMap"></script> -->

<script src="https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyA8G1mE0PUuoN79b3iqE1PB27H0IisuCAg" type="text/javascript"></script>
<?php if(isset($result)){ ?>
<script>



  var maplat = <?=json_encode($result);?>;

  console.log(maplat);

  // var myLatLng = "";



  // $.each(maplat,function(index,value){

  //   console.log(value.profile_pic);

  //   myLatLng={lat:value.lan, lng: value.lon};

  //   console.log(myLatLng);

  // });



  // function initMap() {

  //   var myLatLng = {lat: 28.5585116, lng: 77.36598600000002};



  //   var map =     new google.maps.Map(document.getElementById('googleMap'), {

  //     zoom: 4,

  //     center: myLatLng

  //   });



  //   var marker = new google.maps.Marker({

  //     position: myLatLng,

  //     map: map,

  //     title: 'CodeAspire software company'

  //   });

  // }

  var map = new google.maps.Map(document.getElementById('googleMap'), {

    zoom: 4,

    center: new google.maps.LatLng(28.5585116, 77.36598600000),

    mapTypeId: google.maps.MapTypeId.ROADMAP

  });

  var map, infoWindow;
    
        infoWindow = new google.maps.InfoWindow;
   if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };

            infoWindow.setPosition(pos);
            infoWindow.setContent('Your Location');
            infoWindow.open(map);
             marker = new google.maps.Marker({
            position: new google.maps.LatLng(position.coords.latitude, position.coords.longitude),
            map: map,
            title: 'Your Location'
          });
            map.setCenter(pos);
          }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
          });
        } else {
          // Browser doesn't support Geolocation
          handleLocationError(false, infoWindow, map.getCenter());
        }
  var infowindow = new google.maps.InfoWindow();



  var marker;

  var index;

  $.each(maplat,function(index,value){

    marker = new google.maps.Marker({

      position: new google.maps.LatLng(value.lat, value.lon),

      map: map

    });

    google.maps.event.addListener(marker, 'click', (function(marker, index) {

      return function() {

        infowindow.setContent(value.city);

        infowindow.open(map, marker);

      }

    })(marker, index));

  });
   
      
      
      
</script>

<?php }else{ ?>
<script>



  var map = new google.maps.Map(document.getElementById('googleMap'), {

    zoom: 4,

    center: new google.maps.LatLng(28.5585116, 77.36598600000),

    mapTypeId: google.maps.MapTypeId.ROADMAP

  });

  var map, infoWindow;
    
        infoWindow = new google.maps.InfoWindow;
   if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };

            infoWindow.setPosition(pos);
            infoWindow.setContent('Your Current Location');
            infoWindow.open(map);
             marker = new google.maps.Marker({
            position: new google.maps.LatLng(position.coords.latitude, position.coords.longitude),
            map: map,
            title: 'Your Location'
          });
            map.setCenter(pos);
          }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
          });
        } else {
          // Browser doesn't support Geolocation
          handleLocationError(false, infoWindow, map.getCenter());
        }
  var infowindow = new google.maps.InfoWindow();    
      
</script>
<?php } ?>

<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8G1mE0PUuoN79b3iqE1PB27H0IisuCAg&callback=myMap"></script> -->





<script type="text/javascript">

  $("#currentlocation").click(function(){

    if ("geolocation" in navigator){

      navigator.geolocation.getCurrentPosition(function(position){ 

        var lat=position.coords.latitude; 

        var lang=position.coords.longitude;

        $("#lat").val(lat);

        $("#lon").val(lang);

        console.log(lang);

        console.log(lat);

        $("#formLogin").submit();

      });

    }else{

      console.log("Browser doesn't support geolocation!");

    }



  });

</script>
<!-- Bootstrap core JavaScript -->



      <script type='text/javascript' src='<?php echo base_url('assets/admin/js/backend/main.js'); ?>'></script>
      <script type="text/javascript">
        $("#currentlocation").click(function(){
          if ("geolocation" in navigator){
            navigator.geolocation.getCurrentPosition(function(position){ 
              var lat=position.coords.latitude; 
              var lang=position.coords.longitude;
              $("#lat").val(lat);
              $("#lon").val(lang);
            // console.log(lang);
            // console.log(lat);
            $("#formLogin").submit();
          });
          }else{
            console.log("Browser doesn't support geolocation!");
          }

        });
      </script>

