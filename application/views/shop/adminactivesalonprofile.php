<?php $this->load->view('public_header'); ?>
<style >
.font-chg{
	font-family: 'Comfortaa', cursive;
}
.row{
	margin-left:0 !important;
}
.col-md-12 .row{
	padding-left:25px;
}
.note-head-wrap{
	position: absolute;
	top: 0;
	left: 0;
	right: 0;
	padding-bottom: 15px;
}
.note-head-arr{
	width: 55px;
	display: inline-block;
	float: left;
}
.note-head-arr span{
	border: 2px solid #272727;
	display: inline-block;
	width: 30px;
	height: 12px;
	border-left: none;
	border-right: none;
	margin: 28px 0 0 12px;
}
.note-head-text{
	width: calc(100% - 55px);
	text-transform: uppercase;
	color: #a1a1a1;
	display: inline-block;
	float: left;
	padding-top: 24px;
	font-size: 16px;
}
.note-head-text span.one{
	position: absolute;
	right: 10px;
	font-size: 20px;
	color: #000000;
	top: 22px;
}
.note-head-text span.two{
	position: absolute;
	right: 12px;
	font-size: 16px;
	color: #fff;
	top: 24px;
}
.item-wrap{
	padding-top:80px;
	padding-right: 5%;
	width: 100%;
}
.stats{
	margin-bottom: 25px;
}
.stats-top{
	font-size: 12px;
	color:#a4a4a4;
}
.stats-bottom{
	border: 1px solid #b9b9b9;
	border-radius: 40px;
	display: inline-block;
	margin-left: -35px;
	padding:5px 10px 5px 30px;
	width: calc(100% + 35px);
}
.stats-block{
	display: inline-block;
	width: 33.3333%;
	float: left;
}
.stats-block .figur{
	display: inline-block;
	width: 100%;
	color:#292929;
	font-size: 14px;
}
.stats-block .desc{
	display: inline-block;
	width: 100%;
	color:#a4a4a4;
	font-size: 12px;
}
.search-otp-label{
	margin-top: 10px;
	color: #000000;
	font-size: 13px;
	margin-bottom: 10px;
}
.or-label{
	margin-top: 10px;
	color: #000000;
	font-size: 13px;
	margin-bottom: 10px;
}
.otp-input-wrap{
	display: inline-block;
	width: 100%;
}
.otp-input-wrap input, .otp-input-wrap button{
	display: inline-block;
	float: left;
	height: 50px;
	width: calc(20% - 10px);
	border:1px solid #8e8e8e;
	border-radius: 5px;
	margin-right: 10px;
	text-align: center;
}
.otp-input-wrap button{
	background: #000000;
	color: #fff;
	border-color:#000000;
}
.phone-wrap input{
	display: inline-block;
	float: left;
	height: 40px;
	width: calc(80% - 10px);
	border:1px solid #8e8e8e;
	border-radius: 5px;
	margin-right: 10px;
	text-align: center;
	padding:0px 10px;
}
.phone-wrap button{
	display: inline-block;
	float: left;
	height: 40px;
	width: calc(20% - 10px);
	border:1px solid #8e8e8e;
	border-radius: 5px;
	background: #000000;
	color: #fff;
	border-color:#000000;
}
.icon-links{
	display: inline-block;
	width: 100%;
	margin-bottom: 30px;
}
.icon-links .section{
	padding-top: 30px;
	display: inline-block;
	width: 25%;
}
.icon-links .section .icon{
	text-align: left;
}
.icon-links .section .text{
	text-align: left;
	font-size: 11px;
	padding-top: 4px;
	text-transform: uppercase;
}
.gray-label{
	text-transform: uppercase;
	color:#cacaca;
	font-size: 13px;
}
.gray-label:last-child{
	margin-top: 20px;
}
.gray-label a{
	text-transform: none;
	color:#3d8dfb;
	font-size: 12px;
	margin-left: 10px;
}
.desk-wrapper{
	display: inline-block;
	width: 100%;
}
.desk-wrapper .desk{
	display: inline-block;
	width: 25%;
	border-right: 1px solid #ccc;
	float: left;
	font-size: 12px;
	margin:6px 0 10px 0;
	text-align: center;
}
.desk-wrapper .desk:nth-child(4n+1){
	text-align: left;
}
.desk-wrapper .desk:nth-child(4n+4){
	border:none;
}
.desk-wrapper .desk.active{
	color:#e99705;
}
.desk-wrapper .desk.expired{
	color:#c4c4c4;
}
.item{
	border-bottom: 2px dashed #999999;
	margin-bottom:30px;
	padding-bottom: 20px;
}
.item .time{
	color: #000000;
	font-size: 18px;
}
.item .cut-type{
	color: #999999;
font-size: 14px;
padding: 2px 0;
}
.item .price{
	color: #999999;	
	font-size: 15px;
}
.item .customer-name{
	color: #000000;
	font-size: 16px;
	padding-top: 40px;
}

.item .customer-note{
	color: #999999;	
	font-size: 14px;
	padding: 0 0 6px 0;
	margin-top: 15px;
}
.item .customer-desc{
	color: #515151;
	font-size: 14px;
	line-height: 20px;
}
.item .controls{
	padding-top: 20px;
}
.item .controls a{
	margin-right: 22px;
	color: #3d8dfb;
	font-size: 14px;
}
.item .controls a span.fa{
	font-size: 18px;
}
.my-badge-wrap{
	display: inline-block;
	width: 100%;
	padding: 15px 0;
}
.my-badge{
	background: #ff3f2e;
	color: #fff;
	padding: 4px 18px 5px 10px;
	font-size: 10px;
	float: left;
	position: relative;
	text-transform: uppercase;
}
.my-badge::after{
	content: '';
	float: right;
	position: absolute;
	border-right: 18px solid white;
	border-top: 12px solid #ff3f2e;
	border-bottom: 12px solid #ff3f2e;
	top: 0;
	border-radius: 0px 0px 0px 0px;
	margin-left: 15px;
}
.my-badge.ongoing{background:#ffa402;}
.my-badge.ongoing::after{border-top-color:#ffa402;border-bottom-color:#ffa402;}
.my-badge.refund{background:#ff3f2e;}
.my-badge.refund::after{border-top-color:#ff3f2e;border-bottom-color:#ff3f2e;}
.my-badge.cancelled{background:#ff3f2e;}
.my-badge.cancelled::after{border-top-color:#ff3f2e;border-bottom-color:#ff3f2e;}
.my-badge.completed{background:#70b12d;}
.my-badge.completed::after{border-top-color:#70b12d;border-bottom-color:#70b12d;}

</style>

<?php //$this->load->view('open'); ?>

<div class="note-head-wrap">
	<div class="note-head-arr"><span>&nbsp;</span></div>
	<div class="note-head-text font-chg">
		<img src="https://uppercut.shop/assets/img/logo.svg">
		<span class="one"><span class="fa fa-bell"></span></span>
		<span class="two"><span class="fa fa-bell"></span></span>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="row">

				<div class="item-wrap font-chg">
					<div class="stats">
						<div class="stats-top">
							Today
						</div>
						<div class="stats-bottom">
							<div class="stats-block">
								<div class="figur">50,000</div>
								<div class="desc">Revenue</div>
							</div>
							<div class="stats-block">
								<div class="figur">20K</div>
								<div class="desc">Bookings</div>
							</div>
							<div class="stats-block">
								<div class="figur">5</div>
								<div class="desc">Chairs</div>
							</div>
						</div>
					</div>
					<div class="search-otp-label">Search booking with OTP</div>
					<div class="otp-input-wrap">
						<input type="text">
						<input type="text">
						<input type="text">
						<input type="text">
						<button><span class="fa fa-search"></span></button>
					</div>
					<div class="or-label">(or)</div>
					<div class="phone-wrap">
						<input type="text" placeholder="Search with phone number">
						<button><span class="fa fa-search"></span></button>
					</div>
					<div class="icon-links">
						<div class="section">
							<div class="icon"><?php echo str_repeat('&nbsp;',2); ?><span class="fa fa-asterisk"></span></div>
							<div class="text">Chair</div>
						</div>
						<div class="section">
							<div class="icon"><?php echo str_repeat('&nbsp;',4); ?><span class="fa fa-list-alt"></span></div>
							<div class="text">Bookings</div>
						</div>
						<div class="section">
							<div class="icon"><?php echo str_repeat('&nbsp;',4); ?><span class="fa fa-comment"></span></div>
							<div class="text">Feedback</div>
						</div>
					</div>

					<div class="gray-label">Barber Chair <a href="#">View All</a></div>
					<div class="desk-wrapper">
						<div class="desk active">Desk 1</div>
						<div class="desk expired">Desk 2</div>
						<div class="desk">Desk 3</div>
						<div class="desk">Desk 4</div>
						<div class="desk">Desk 5</div>
						<div class="desk active">Desk 6</div>
						<div class="desk">Desk 7</div>
						<div class="desk">Desk 8</div>
					</div>
					<div class="gray-label">On-Going Bookings <a href="#">View All</a></div>

					<div class="item">
						<div class="my-badge-wrap"><span class="my-badge ongoing">Ongoing</span></div>
						<div class="time font-chg">5:00 PM</div>
						<div class="cut-type font-chg">Men's - hair cut</div>
						<div class="price font-chg">₹ 199 | 30 mins</div>
						<div class="customer-name font-chg">Customer Name</div>
						<div class="controls">
							<a href="#"><span class="fa fa-cut fa-2x"></span> Stop</a>
						</div>
					</div>

					<div class="item">
						<div class="time font-chg">5:00 PM</div>
						<div class="cut-type font-chg">Men's - hair cut</div>
						<div class="price font-chg">₹ 199 | 30 mins</div>
						<div class="customer-name font-chg">Customer Name</div>
						<div class="customer-note font-chg">Note From Customer</div>
						<div class="customer-desc font-chg">The resulting cornea flap is created at a precise depth and dia.<a href="#">Read More</a></div>
						<div class="customer-note font-chg">Internal Note</div>
						<div class="customer-desc font-chg">The resulting cornea flap is created at a precise depth and dia.<a href="#">Read More</a></div>
						<div class="controls">
							<a href="#"><span class="fa fa-rocket fa-2x"></span> Start</a>
							<a href="#"><b>OTP</b>-Resend</a>
							<a href="#"><span class="fa fa-cut fa-2x"></span></a>
							<a href="#"><span class="fa fa-close fa-2x"></span></a>
						</div>
					</div>
					
				</div>

			</div>
		</div>
	</div>
</div>

<?php //$this->load->view('close'); ?>

<?php $this->load->view('layouts/footer'); ?>

