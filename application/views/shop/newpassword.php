<?php load_view('public_header'); ?>
<style>
	.btn-green {
    background-color: #007bff;
}

/* Thulasi CSS starts */
.body-text {
	font-size: 14px;
	color: #4A4A4A !important;
	font-weight: 400;
}

.round
{
  border: 1px solid #C2C2C2 !important;
    border-radius: 50px;
    padding-left: 15px;
    height: 40px;
}

.form-body .btn {
    border-radius: 100px;
}

.pt-6 {
    padding-top: 60% !important;
}

.btn-text3 {
    font-size: 12px !important; 
    letter-spacing: 4px;
    font-weight: 100 !important;
    padding-left: 40px;
    padding-right: 40px;
}

@media only screen and (max-width: 420px) {
.pt-6 {
    padding-top: 60% !important;
}
.btn-text3 {
    font-size: 14px !important; 
    letter-spacing: 4px;
    font-weight: 100 !important;
    padding-left: 40px;
    padding-right: 40px;
}


.p-3 {
	padding: 0px !important;
}

}

/* Thulasi CSS ends */ 
</style>
<BR>
<BR>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<?php if( $feedback = $this->session->flashdata('feedback')): 

			$feedback_class = $this->session->flashdata('feedback_class');

			?>

			<div class="row">

				<div class="col-lg-9 col-lg-offset-1">

					<div class="alert alert-dismissible <?= $feedback_class ?>">

						<?= $feedback ?>

					</div>

				</div>

			</div>

		<?php endif; ?>
			<div class="row">
				<div class="py-6 mx-auto">
					<!-- <h1 class="pt-5 pb-3 text-center">Reset Password</h1> -->
					
					<p class="pt-6 text-center" >Change password</p>
					<!-- <p class="text-center ptext">8 characters, 1 uppercase, 1 lowercase<br>, and 1 number in your password</p> -->
					
					<div class="form-body ml-3 p-3 mx-auto" >
						<form class="form" action="<?= base_url(); ?>change_pass" method="POST" role="form" autocomplete="off" id="changePassword">
							<div class="form-group">
								<label class="sr-only" for="oldpassword">Old password</label>
								<input type="password" class="form-control round form-control-lg rounded-0" id="oldpassword" name="oldpassword" placeholder="Old password" required autocomplete="old-password">
							</div>
							<div class="form-group">
								<label class="sr-only" for="newpassword">New password</label>
								<input type="password" class="form-control round form-control-lg rounded-0" id="newpassword" name="newpassword" placeholder="New password" required autocomplete="new-password">
							</div>
							<button type="submit" class="btn btn-text3 btn-green btn-sz">Set new password</button>
						</form>
						<div class="form-url">
							
						</div>
					</div>
				</div>
				<!--/col-->
			</div>
			<!--/row-->
		
</div>
<!--/container-->
<?php $this->load->view('layouts/footer'); ?>
