<?php load_view('layouts/header'); ?>
<!-- <div class="form-dash ">
  <form class="form" role="form" autocomplete="off" method="post" action="<?php //echo base_url('search'); ?>" id="formLogin">
    <div class="form-group search-box">
    <input type="search" class="form-control form-control-lg" name="search" id="address" placeholder="Search by shop name or locality" >
      <input type="hidden" name="lat" value="" id="lat">
      <input type="hidden" name="lon" value="" id="lon">    
      <button type="submit" class="search-dash-btn"><i class="fa fa-search"></i></button>
    </div>
  </form>
</div>
<div class="row dashhrow">
  <div class="col-sm-8">Search result <span>  &nbsp;<?php  //echo count($result); ?></span></div>
  <div class="col-sm-4"><i class="fa fa-filter marg35" aria-hidden="true"></i></div>

</div> -->
<style>
   @media only screen and (max-width: 900px) {
  .px-6 {
    padding-left: 10px;
    padding-right: 10px;
    padding-top: 83px;
}

.ml-6 {
    margin-left: 0px;
}
.coll {
    display: inline-block;
    width: 100%;
}
}
</style>
<?php 

  // echo '<pre>';
  // print_r($result);
  // echo '</pre>';

if(isset($result)):     foreach ($result as $key => $value):    ?>
<div class="col-md-12 marg10">
  <div class="col-md-8"> <span ><a href="javascript:void(0)" style="color: black;font-weight: 600 "> <?php echo $value->name; ?></a></span></div>
</div>
<div class="row dashhrow">
  <div class="col-sm-6"><?php echo $value->city; ?></div>
  <div class="col-sm-6">3.2 km away</div>
</div>
<hr>
<?php endforeach; endif; ?>
</div>
<?php  $shop = $this->db->where('shop_user',get_session('userid'))->get('shop_list')->row(); ?>
<?php if(count($shop)){ ?>
<div class="main">
  <div class="">
    <div class="col-md-12">
      <div class="ml-6 px-6">
        <div class="py-2 header-body">
          <div class="coll" href="<?= base_url(); ?>editprofile" >
            <div class="oval mr-3" id="oval">
              <div class="toggleTab man">
            <div class="tog-icon"></div>
            <div class="tog-icon"></div>
          </div>
            </div>
            <div class="s-name">
              <h1><a href="<?= base_url(); ?>editprofile"><?= $shop->name; ?></a></h1>
            </div>
          </div>
          <!-- <div class="coll">
            <div class="btn-body">
              <button class="bttn bttn-blue">SAVE</button>
              <button class="bttn bttn-border-blue">CANCEL</button>
            </div>
          </div> -->
        </div>
        <div class="row mt-4 fa-icon" style="margin: 0px;">
          <div class="col-sm-4">
            <div class="map-kms">
              <p style="padding-left: 26px;"><i class="fa fa-map-o"></i><span></span></p>

              <p class="map-link"><a href="" id="link" target="__BLANK">Get Directions <img src="<?= base_url(); ?>assets/img/download.png" ></a></p>
            </div>
            <div class="service">
              <p><span>Service offered</span></p>
              <?php foreach (json_decode($shop->service_offered) as $sey ) { ?>
               <p><?= $sey; ?></p>
              <?php  } ?>
            </div>
            <div class="parking">
              <p><span>Parking</span></p>
              <p>
                <?php if($shop->parking == "Bike") { ?>
                 <i class="fa fa-motorcycle"></i>
                <?php }else if($shop->parking == "Car"){ ?>
                <i class="fa fa-car"></i>
                 <?php }else if($shop->parking == "Bus"){ ?>
                <i class="fa fa-bus"></i>
                <?php }else{ ?>
                <?= $shop->parking; ?>
                <?php } ?>
                </p>
                
              </div>
              <div class="address">
                <p><span>Address</span></p>
                <p><?= $shop->user_address; ?></p>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="contact">
                <p><i class="fa fa-phone"></i><span>Contact</span></p>
                <p><a href="tel:<?= $shop->mobileno; ?>"><?= $shop->mobileno; ?></a></p>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="img">
                <img src="<?= base_url(); ?>uploads/<?= $shop->image; ?>" class="img-responsive" style="width: 100%;    border-radius: 15px;" alt="">
              </div>
              <div class="mt-4 result-text">
                <p><?= $shop->about_me; ?></p>
              </div>
              <div class="date-time">
                <p><i class="fa fa-clock-o"></i><span><?= $shop->opening_time; ?> - <?= $shop->closing_time; ?></span></p>
                <?php if($shop->days != "null" ){  ?>
                <p><?php $dayss =  json_decode($shop->days); ?>
                  <?php foreach ($dayss as $key ) {
                 echo $key.'  ';
                } }else { } ?></p>
              </div>
              <div class="site-url">
                <p><i class="fa fa-globe"></i><span><?= $shop->website; ?></span></p>
              </div>
            </div>
          </div>
        </div>
        <!--/col-->
      </div>
      <!--/col-->
    </div>
    <!--/row-->
  </div>

  <?php }else{ ?>
  <div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="ml-6 px-6">
        <div class="py-2 header-body">
          <div class="coll">
            <div class="oval mr-3">
            </div>
            <div class="s-name">
              <h1>Shop name 1</h1>
              <p>Locality name</p>
            </div>
          </div>
          <!-- <div class="coll">
            <div class="btn-body">
              <button class="bttn bttn-blue">SAVE</button>
              <button class="bttn bttn-border-blue">CANCEL</button>
            </div>
          </div> -->
        </div>
        <div class="row no-gutters mt-4 fa-icon">
          <div class="col-sm-4">
            <div class="map-kms">
              <p style="padding-left: 26px;"><i class="fa fa-map-o"></i><span></span></p>

              <p class="map-link"><a href="" id="link" target="__BLANK">Get Directions <img src="<?= base_url(); ?>assets/img/download.png" ></a></p>
            </div>
            <div class="service">
              <p><span>Service offered</span></p>
              <p>Service name 1</p>
              <p>Service name 2</p>
              <p>Service name 3</p>
              <p>Service name 4</p>
              <p>Service name 5</p>
              <p>Service name 6</p>
            </div>
            <div class="parking">
              <p><span>Parking</span></p>
              <p><i class="fa fa-motorcycle"></i> 
                <i class="fa fa-car"></i></p>
              </div>
              <div class="address">
                <p><span>Address</span></p>
                <p>23, 5th main road, EC</p>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="contact">
                <p><i class="fa fa-phone"></i><span>Contact</span></p>
                <p>080 9483 8473</p>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="img">
                <img src="" alt="">
              </div>
              <div class="mt-4 result-text">
                <p>The resulting corneal ﬂap is created at a precise depth and diameter pre-determined by the surgeon. As occurs with a mechanical microkeratome, a small section of tissue at one edge o</p>
              </div>
              <div class="date-time">
                <p><i class="fa fa-clock-o"></i><span>8:00 AM - 8:00 PM</span></p>
                <p>Sa Su Mo Tu We Th Fr</p>
              </div>
              <div class="site-url">
                <p><i class="fa fa-globe"></i><span>www.website.com</span></p>
              </div>
            </div>
          </div>
        </div>
        <!--/col-->
      </div>
      <!--/col-->
    </div>
    <!--/row-->
  </div>
  <?php } ?>
  <!--/container-->
  <?php $this->load->view('layouts/footer'); ?>
  <script type='text/javascript' src='<?php echo base_url('assets/admin/js/backend/main.js'); ?>'></script>

  <script type="text/javascript">

    $('#formLogin').validate({
      rules:{
        address:{
          required: true,
        },
      },
      errorPlacement: function(error, element) {},
      highlight: function(element) {
        $(element).parent('div').addClass('has-error');
      },
      unhighlight: function(element) {
        $(element).parent('div').removeClass('has-error');
      },
    });
  </script>

   <script type="text/javascript">
       
          if ("geolocation" in navigator){
            
            navigator.geolocation.getCurrentPosition(function(position){ 
              var lat=position.coords.latitude; 
              var lang=position.coords.longitude;
              if(lat && lon){
                $('#link').attr("href", "https://www.google.com/maps/dir/<?php echo $shop->lat.','.$shop->lon; ?>/"+lat+","+lang);
                  $("#lon").val(lang);
                 $("#lat").val(lat);
              }else{
                console.log('test only');
                $('#link').attr("href", "https://www.google.com/maps/dir/<?php echo $shop->lat.','.$shop->lon; ?>/");
              }
            
            
          },function(error) {
       
           console.log('test only');
                $('#link').attr("href", "https://www.google.com/maps/dir/<?php echo $shop->lat.','.$shop->lon; ?>/");
           });
          }else{
            console.log("Browser doesn't support geolocation!");
            $('#link').attr("href", "https://www.google.com/maps/dir/<?php echo $shop->lat.','.$shop->lon; ?>/");
          }

      </script>