<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta charset="UTF-8">
  <meta charset="utf-8">
  <meta http-equiv="content-type" content="text/html;charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" id="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Shop</title>
  <!-- Bootstrap core CSS -->
  <link href="<?php  echo base_url('assets/vendor/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
  <!-- Custom fonts for this template -->
  <link href="<?php  echo base_url('assets/vendor/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600|Roboto:300" rel="stylesheet">
  <!-- Plugin CSS -->
  <link href="<?php  echo base_url('assets/vendor/magnific-popup/magnific-popup.css'); ?>" rel="stylesheet">
  <!-- Custom styles for this template -->
  <link href="<?php  echo base_url('assets/css/style.css'); ?>" rel="stylesheet">
 
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-100486865-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments)};
    gtag('js', new Date());
    gtag('config', 'UA-100486865-1');
  </script>
  <style type="text/css">
    i {
      border: solid white;
      border-width: 0 3px 3px 0;
      display: inline-block;
      padding: 3px;
    }
    .left {
      transform: rotate(135deg);
      -webkit-transform: rotate(135deg);
    }
  </style>
</head>
<body>
  <div id="loader"></div>
  <!--left nav-->
  <div class="left-nav">
   <div class="top-body">
    <div class="toggleTab">
     <a href="<?php echo base_url('home'); ?>"><span class="fa fa-angle-left" aria-hidden="true"></span></a>
   </div>
      <!-- <div class="toggleTab">
        <div class="tog-icon"></div>
        <div class="tog-icon"></div>
      </div> -->
      <span class="brand-name">
        <a class="brand-logo" href="<?php echo base_url('home'); ?>"><img src="<?php echo base_url('assets/img/logo.png'); ?>"></a>
      </span>
    </div>
    <div class="form-dash ">
      <form class="form" role="form" autocomplete="off" method="post" action="<?php //echo base_url('search'); ?>" id="formLogin">
        <div class="form-group search-box">
          <input type="search" class="form-control form-control-lg" name="search" id="address" placeholder="Search by shop name or locality" >
          <input type="hidden" name="lat" value="" id="lat">
          <input type="hidden" name="lon" value="" id="lon">    
          <button type="submit" class="search-dash-btn"><i class="fa fa-search"></i></button>
        </div>
      </form>
    </div>
    <div class="row dashhrow">
      <div class="col-sm-8 search_result">Search result <span class="num_of_search_result">  <?php  echo count($result); ?></span></div>
      <div class="col-sm-4"><i class="fa fa-filter marg35" aria-hidden="true"></i></div>
    </div>
    <div class="leftbar">
      <?php 
      if(isset($result)):     foreach ($result as $key => $value):    ?>
      <div onClick="window.location.href='<?php echo base_url('dashboard2'); ?>'/<?php echo base64_encode($value->id); ?>" style="cursor: pointer;" >
       <div class="col-md-12 marg10">
        <div class="Shop_name"><div class="left_border"></div> <span><a href="<?php echo base_url('dashboard2');?>/<?php echo base64_encode($value->id); ?>" style="color: black;font-weight: 600 "> <?php echo $value->name; ?></a></span></div>
      </div>
      <div class="row dashhrow">
        <div class="col-sm-6 shop_area"><?php echo $value->city; ?></div>
        <div class="col-sm-6 kms">3.2 km away</div>
      </div>
    </div>
    <!-- <hr> -->
  <?php endforeach; endif; ?>
</div>
</div>
<div class="container-fluid">
 <!--  <?php echo $result=explode('-', $latlan) ?> -->
  <div class="ml-6">
    <div id="googleMap" class="googlemap"></div>
  </div>
</div>
<!--/container-->
<?php $this->load->view('layouts/footer'); ?>

<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8G1mE0PUuoN79b3iqE1PB27H0IisuCAg&callback=myMap"></script> -->
<script src="https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyA8G1mE0PUuoN79b3iqE1PB27H0IisuCAg" type="text/javascript"></script>
<script>
  var maplat = <?=json_encode($result);?>;
  var map = new google.maps.Map(document.getElementById('googleMap'), {
    zoom: 4,
    center: new google.maps.LatLng(maplat[0], maplat[1]),
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });
   var map, infoWindow;
    
        infoWindow = new google.maps.InfoWindow;
   if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };

            infoWindow.setPosition(pos);
            infoWindow.setContent('Your Location');
            infoWindow.open(map);
             marker = new google.maps.Marker({
            position: new google.maps.LatLng(position.coords.latitude, position.coords.longitude),
            map: map,
            title: 'Your Location'
          });
            map.setCenter(pos);
          }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
          });
        } else {
          // Browser doesn't support Geolocation
          handleLocationError(false, infoWindow, map.getCenter());
        }
  var infowindow = new google.maps.InfoWindow();
  var marker;
  var index;
  $.each(maplat,function(index,value){
    // console.log(value);
    console.log(maplat[0]);
    
    marker = new google.maps.Marker({
      position: new google.maps.LatLng(maplat[0], maplat[1]),
      map: map
    });
  });
</script>
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8G1mE0PUuoN79b3iqE1PB27H0IisuCAg&callback=myMap"></script> -->


