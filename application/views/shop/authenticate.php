<?php $this->load->view('public_header'); ?>
<style >
  .pt-6 {
    padding-top: 150px;
}
.round {
    border: 1px solid #9a9a9a !important;
    border-radius: 10px;
    padding-left: 15px;
    height: 68px;
}
.form-body .btn {
    border-radius: 100px;
}
.btn-green {
    background-color: #007bff;
}
.row
{
      display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: -15px;
    margin-left: -15px;
}
.alter
{
  width: 100%;
}
.col, .col-1, .col-10, .col-11, .col-12, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-auto, .col-lg, .col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-auto, .col-md, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-auto, .col-sm, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-auto, .col-xl, .col-xl-1, .col-xl-10, .col-xl-11, .col-xl-12, .col-xl-2, .col-xl-3, .col-xl-4, .col-xl-5, .col-xl-6, .col-xl-7, .col-xl-8, .col-xl-9, .col-xl-auto {
    position: relative;
    width: 100%;
    min-height: 1px;
    padding-right: 10px;
    padding-left: 10px;
}
body
    {
      background: url(<?= base_url(); ?>assets/img/login_bg.png) no-repeat;
    }
    
/* Thulasi CSS starts */
.btn-text {
	letter-spacing: 5px;
}

.body-text {
	font-size: 14px;
	color: #4A4A4A !important;
	font-weight: 400;
}

.left-pad {
	padding-left: 3%;
	padding-right: 3%;
}

.form-group {
	margin-bottom: 5px;
}

.pt-6 {
    padding-top: 50% !important;
}

.btn-text3 {
    font-size: 12px !important; 
    letter-spacing: 4px;
    font-weight: 100 !important;
    padding-left: 40px;
    padding-right: 40px;
}

@media only screen and (max-width: 420px) {
.pt-6 {
    padding-top: 40% !important;
}
.btn-text3 {
    font-size: 14px !important; 
    letter-spacing: 4px;
    font-weight: 100 !important;
    padding-left: 40px;
    padding-right: 40px;
}


.p-3 {
	padding: 0px !important;
}


/* Thulasi CSS ends */     
    
    
</style>


<div class="container">
        <div class="">
          <!--
          <div class="col-md-12">
            <div class=" shop-login-signup pull-right" >
              <span>SHOP</span>
              <div class="login-signup-link" >
                <ul>
                  <li><a href="<?php echo base_url("login"); ?>">Login</a></li>
                  <li><a href="<?php echo base_url("signup"); ?>">Signup</a></li>
                </ul>
              </div>
            </div>
          </div>
        -->
          <div class="col-md-12">
            <div class="">
    <div class="py-6 mx-auto">
      <p class="pt-6 text-center" style="color: grey;">Step 2</p>
     <h4 class="text-center" >Validating Your Account</h4>
     <p class="text-center body-text" style="color: grey;margin-bottom: 20px;" >We have send you an activation code to your registered email address. </p>
     <div class="form-body mx-auto">
      <form class="form" method="POST" role="form" action="<?php echo base_url("shop/shop/createsaloon") ?>" autocomplete="off" id="formSignup">
      <!-- <form class="form" role="form" action="<?php echo base_url("shop/Shop") ?>" autocomplete="off" id="formSignup"> -->
        <input type="text" name="email" style="display: none;" class="hidden" id="email" value="<?= get_session('email'); ?>">
        <input type="text"  style="display: none;" class="hidden" id="code" value="<?= get_session('code'); ?>">
    <div class="row">

      <div class="col-3 col-sm col-lg col-xl">
       <div class="form-group">
        <input type="number" onkeyup="moveOnMax(this,'2')" class="form-control round form-control-lg rounded-0" name="1" id="phone" 
       maxlength = "1" required >
     </div>
   </div>
   <div class="col-3 col-sm col-lg col-xl">
     <div class="form-group">
          <input type="number" onkeyup="moveOnMax(this,'3')" id="2" class="form-control round form-control-lg rounded-0" name="2"   
       maxlength = "1" required >
     </div>
   </div>
    <div class="col-3 col-sm col-lg col-xl">
     <div class="form-group">
      
          <input type="number" class="form-control round form-control-lg rounded-0" name="3" id="3"  onkeyup="moveOnMax(this,'4')"
       maxlength = "1" required >
     </div>
   </div>
    <div class="col-3 col-sm col-lg col-xl">
     <div class="form-group">
          <input type="number" class="form-control round form-control-lg rounded-0" name="4" id="4"  
       maxlength = "1" required >
       </div>
     </div>
   </div>
       <div class="form-group form-group3">

        <div class="text-center terms">

         <p>Didn't Receive the Code <span><a href="javascript:void(0)" id="resend">Resend</a></span></p>
        
        </div>
      <div class="row " id="alertnoti" style="display: none;"  >
        <div class="alter">
         <div class="alert alert-success alert-dismissible fade show" role="alert">
            Your code is successfully sent.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        </div>
      </div>
      <div class="row" id="alertnotid" style="display: none;"  >
        <div class="alter">
         <div class="alert alert-danger alert-dismissible fade show" role="alert">
            You reached your maximum limit. Please try after sometime.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        </div>
      </div>
       </div>

       <button type="submit"  class="btn btn-text btn-green btn-sz">Authenticate</button>

      </form>

      <div class="form-url">

       <p class="text-center">Already have an account? <span><a href="<?php echo base_url('login');?>">LOGIN</a></span></p>

      </div>

     </div>

    </div>

    <!--/col-->

   </div>

   <!--/row-->

  </div>

  <!--/col-->

 </div>

 <!--/row-->

</div>



<!--/container-->

<?php $this->load->view('layouts/footer'); ?>

<script type="text/javascript">



 $('#formSignup').validate({

  rules:{

   email:{

    required: true,

    email: true

   },

   phone:{

    required: true,

    number:true,

    minlength:1,

    maxlength:1,

   },

   password: {

    required: true

   }

  },



  messages: { 

   email:{ 

    required:'' 

   },

   phone:{ 

    required:'' 

   }

  },



  submitHandler: function() { 



   var formData=$("#formSignup").serialize();

   signUp(formData);

  }

 });




</script>
<script >
function moveOnMax(field,nextFieldID){
  if(field.value.length >= field.maxLength){
    document.getElementById(nextFieldID).focus();
  }
}
</script>

<script >
  var i = 1;
  $('#resend').click(function() {
    if(i <= 3){
      var code = $('#code').val();
    var mail = $('#email').val();
    $.ajax({

   url: "<?php echo base_url('shop/shop/send_mail_to'); ?>",

   type: "POST",

   data:({ name : code , email : mail }),


   beforeSend: function () {

    $('#loader').addClass('loader');

   },

   complete: function () {

    $('#loader').removeClass('loader');

   },

   success: function (response) {

    if (response.error == false) {

      $('#alertnotidf').fadeIn('slow',function(){
      $('#alertnotidf').delay(5000).fadeOut();
    });

    }else{

     $('#alertnoti').fadeIn('slow',function(){
      $('#alertnoti').delay(5000).fadeOut();
    });

    }

   },

   error: function (XMLHttpRequest, textStatus, errorThrown) {

    console.log(errorThrown);

   }

  });
  i = i+ 1;
  }else
  {
    $('#alertnotid').fadeIn('slow',function(){
      $('#alertnotid').delay(5000).fadeOut();
    });
  }
    
});
</script>