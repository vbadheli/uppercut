<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Uppercut</title>
  
  <link rel='shortcut icon' href='/uploads/favicon.ico' type='image/x-icon' />
  
  <meta name="viewport" content="width=device-width, minimum-scale=1.0" />
  <link href="<?php  echo base_url('assets/vendor/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
  <link href="<?php  echo base_url('assets/vendor/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600|Roboto:300" rel="stylesheet">
  <link href="<?php  echo base_url('assets/vendor/magnific-popup/magnific-popup.css'); ?>" rel="stylesheet">
  <link href="<?php  echo base_url('assets/css/style.css'); ?>" rel="stylesheet">
 <!--  Scroll bar for desktop version -->
  <link rel="stylesheet" media="screen and (min-width: 900px)" type="text/css" href="<?php  echo base_url('assets/css/scroll.css'); ?>">
  <link rel="stylesheet" media="screen and (min-width: 900px)" href="<?php  echo base_url(); ?>assets/css/desktop.css">
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-100486865-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments)};
    gtag('js', new Date());
    gtag('config', 'UA-100486865-1');
  </script>
  <style type="text/css">

  i {
    border: solid white;
    border-width: 0 3px 3px 0;
    display: inline-block;
    padding: 3px;
  }
  span.fa-angle-left {
    color: black;
    font-size: 24px;
    font-weight: lighter; 
    padding-top: 13px;
}
  .toggleTab {
    width: 35px;
    /* background-color: #1f1e21; */
    height: 25px;
    position: absolute;
    top: 5px;
    left: 15px;
    z-index: 999999;
    cursor: pointer;
    display: inline-block;
}
  .left {
    transform: rotate(135deg);
    -webkit-transform: rotate(135deg);
  }
  b, strong {
    font-weight: 500;
  }
  h1 {
    font-weight: 300;
    font-size: 42px;
    color: #4a4a4a;
    letter-spacing: 0px;
  }
  .round {
    border: 1px solid #9a9a9a !important;
    border-radius: 5px;
    padding-left: 15px;
    height: 68px;
}
.form-body .btn {
    border-radius: 100px;
}
.btn-green {
    background-color: #007bff;
}
  .filter_search_area a
  {
    color: black;
  }
  .filter_search_area .active 
  {
    color:red;
  }
  .row {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: 0px;
    margin-left: 0px;
  }
  .brand-name {
    font-size: 16px;
    margin-bottom: .5rem;
    font-family: inherit;
    font-weight: 100;
    line-height: 1.1;
    color: black;
    font-style: normal;
    margin-top: 12%;
    margin-left: -25px;
    
}
  .badge
  {
    vertical-align: top;
    margin-left: 6px;
    color: black;
  }
  .badge-info {
    color: black;
    background-color: #d3fb00;
  }
  .color-box
  {
    border:1px solid #d3fb00;
    border-radius: 50px;
  }
  .filter_search_area
  {
    margin-bottom: 25px;
  }
  .shop_list_area ul
  {
    list-style-type: none;
  }
  .shop_list_area ul li
  {
    border-bottom: 1px dashed grey;
    margin-bottom: 20px;
  }
  .color-box .col-4
  {
    padding: 8px;
    text-align: center;
    color: grey;
  }
  .span-color
  {
    background-color: #d3fb00;
    color: black;
    margin-right: 55%;
    padding: 0px 4px 0px 4px;
    font-size: 12px;
    border-radius: 2px;
  }
  .box
  {

       /* box-shadow: 0 0 9px #eee; */
        padding-top: 0px;
        width: 50px;
    padding-left: 0px;
    padding-bottom: 20px;
    margin-right: -10px;
    border-radius: 0px 20px 20px 0px;
        text-align: center;
  }
  a
  {
    color: black;
  }
  .greypara
  {
    color: grey;
    margin-top: 0;
    margin-bottom: 4px;
  }
  .form-check-input {
    position: inherit;
    margin-top: .25rem;
    margin-left: -20px;
}
.nav-link {
    display: block;
       padding: 15px 5px 20px 5px;
}
.active
{
  text-decoration: underline;
  text-decoration-color: red;
}
.box span 
{
  font-size: 22px;
}
.box a
{
  font-size: 11px;
}

.left-nav .top-body {
	height: 0 !important;
}


/* Thulasi CSS starts */

.shop-name {
	font-size: 20px;
	margin-bottom: 3px;
	font-weight: 700;
	margin-top: 5px;
	text-transform: uppercase;
	letter-spacing: -1px;
}

.grey {
	font-size: 11px;
	color: #aaa !important;
	font-weight: 100;
}
.text-info {
	font-size: 14px;
	color: #3D8DFB !important;	
}

.rem-pad-mar {
	margin-bottom: 2px;
	margin-top: 5px;
	font-size: 0;
}

.img-1 {
    width: 100%; 
    border-radius: 3px; 
    margin-bottom: 20px; 
}

.locality {
	margin-bottom: 20px;
	text-transform: initial;
}

.stats{
	font-size:12px; 
	margin-bottom:-5px; 
	font-weight: 800;	
}

.heading-2 {
	font-size: 12px;
	font-weight: 100;
	text-transform: uppercase;
	letter-spacing: 0px;
}

.add-pad-mar {
	margin-bottom: 5px;
}

/* Thulasi CSS ends */

</style>
</head>
<?php $id  = $this->uri->segment(3); ?>
<?php if(empty($id))
{
  redirect(base_url());
}
$data = $this->db->where('id',$id)->get('shop_list')->row(); 
?>
<body>
  <div class="boxing" id="boxing">

  <div id="loader"></div>
  <!-- Navbar starts from here -->
  <div class="left-nav">
    <div class="top-body">
      <div class="toggleTab">
       
     </div>
<!--
     <span class="brand-name">
     Dashboard
     
       <?= $data->name; ?> 
-->
     </span>
     

  </div>


</div>

       
        <Br>
        <!-- Original Code Body starts from here -->
       
        <div class="">
          
          
          <div class="row">
               
          <div class="col-2" style="padding-left: 0;padding-right: 0;" id="left">
            <nav class="nav flex-column box">
               <!--
                 <a class="nav-link active" style="font-size:10px;" href="<?= base_url(); ?>home/salon/<?= $this->uri->segment(3); ?>"><span class="fa fa-tachometer" aria-hidden="true" style="font-size: 16px;"></span><br>Dash</a>
                <a class="nav-link" href="#"><span class="fa fa-cut" aria-hidden="true"></span>Booking</a>
                <a class="nav-link" href="#"><span class="fa fa-credit-card" aria-hidden="true"></span>Transaction</a>  
                <a class="nav-link disabled" href="<?= base_url(); ?>shop/shop/feedback/<?= $data->id; ?>"><span class="fa fa-comments" aria-hidden="true"></span>Feedback</a> -->
            </nav>
          </div>
          <div class="col-10" style="padding-left: 25px;" id="right">
          
         <a href="<?php echo base_url('shop/shop/saloonlist'); ?>"><span class="fa fa-angle-left" aria-hidden="true"></span></a> 
          <h3 class="rem-pad-mar"><b class="shop-name"><?= $data->name; ?></b></h3>
          <h5 class="locality grey"><?= $data->locality_name; ?></h5>
          
            <img class="img-1" src="<?= base_url(); ?>uploads/<?= $data->image; ?>" >
            
           
            
<!--
            <span id="grey">Store Manager</span>         
            <?php $store = $this->db->where('id',$data->shop_user)->get('shop_signup')->row(); ?>
-->
            
	<div class="add-pad-mar"> <b class="heading-2">Statistics</b> </div>
            <div class="row" >            
              <div class="col-6" style=" padding-left:0; max-width: 35%;">              
                <h5 class="stats"><?= $data->counter; ?></h5>
                <span class="grey">Page view</span>
              </div>

              <div class="col-4">
                 <h5 class="stats"><?= $obj->calculate_rating($data->id); ?>&nbsp<span class="fa fa-caret-up text-success" aria-hidden="true"></span></h5>
                 <span class="grey">Rating</span>
              </div>


<!-- <div class="col-4">
                 <h5 style="margin-bottom: 0px;">20 k</h5>
                 <span class="grey">Booking</span>
              </div> 
-->
            </div>
 
 
            <br> 
           
            <h5><?php if(!empty($store)){ $store->email; }?> <span><a  style="font-size: 10px;" href="<?= base_url(); ?>home/edit_salon/<?= $this->uri->segment(3); ?>" class="text-info">EDIT SHOP</a></span></h5>  
          </div>
          </div>
        </div>
       
        <!-- BOdy code ends here  -- >

         <!--/col-->

       </div>

       <!--/row-->

     </div>
</div>


     <!-- container -->

     <?php   $this->load->view('layouts/footer'); ?>
     <script>
       $('.your-checkbox').prop('indeterminate', true)
     </script>

