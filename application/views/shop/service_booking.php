<?php $this->load->view('public_header'); ?>
<style >
.font-chg{
	font-family: 'Comfortaa', cursive;
}
.row{
	margin-left:0 !important;
}
.col-md-12 .row{
	padding-left:25px;
}
.note-head-wrap{
	position: absolute;
	top: 0;
	left: 0;
	right: 0;
	padding-bottom: 15px;
}
.note-head-arr{
	width: 55px;
	display: inline-block;
	float: left;
}
.note-head-arr span::before{
	content: '';
	border-right: 16px solid #0b0b0b;
	border-top: 14px  solid transparent;
	border-bottom: 14px  solid transparent;
	float: left;
	position: absolute;
	margin-top: 20px;
	margin-left: 20px;
}
.note-head-arr span::after{
	content: '';
	border-right: 16px solid #fff;
	border-top: 14px  solid transparent;
	border-bottom: 14px  solid transparent;
	float: left;
	position: absolute;
	margin-top: 20px;
	margin-left: 17px;
}
.note-head-text{
	width: calc(100% - 55px);
	text-transform: uppercase;
	color: #a1a1a1;
	display: inline-block;
	float: left;
	padding-top: 24px;
	font-size: 16px;
}
.item-wrap{
	padding-top: 80px;
	padding-right: 5%;
	width: 100%;
}
.booking-request-wrap{
	width: 100%;
	margin-top: 60px;
}
.booking-request-wrap a{
	background: #000000;
	color:#fff;
	border-radius: 50px;
	display: inline-block;
	width: 100%;
	padding:13px 10px;
	text-align: center;
	text-transform: uppercase;
	font-size: 14px;
	letter-spacing: 2px;
}
.combo{
	width: 100%;
	border:none;
	border-bottom: 2px solid #ccc;
	padding:5px 0;
	color:#a4a4a4;
}

.service-combo-wrap{
	position: relative;
}
.service-combo-wrap::after{
	content: '';
	width: 20px;
	background: #fff;
	position: absolute;
	right: 0;
	bottom: 2px;
	height: 35px;
}
.service-combo-wrap > span{
	position: absolute;
	right: 0;
	top: 6px;
	z-index: 1;
}
.date-wrap{
	display: inline-block;
	width: 100%;
	margin-top: 30px;
	position: relative;
}
.date-wrap .date-label{
	color: #cccccc;
	transform: rotate(-90deg);
	display: inline-block;
	margin-left: -46px;
	top: 13px;
	position: absolute;
	font-size: 14px;
}
.date-wrap .week-day-wrap{
	display: inline-block;
	width: 100%;
}
.date-wrap .week-day-wrap span{
	display: inline-block;
	width: 15.99%;
	float: left;
	color:#7f7f7f;
	font-size: 16px;
	text-align: center;
}
.date-wrap .week-day-wrap span:first-child{
	text-align: left;
	width: 10%;
}
.date-wrap .week-day-wrap span:last-child{
	text-align: right;
	width: 10%;
}
.date-wrap .week-day-wrap span.disabled{
	color:#e0e0e0;
}

.date-wrap .day-num-wrap{
	display: inline-block;
	width: 100%;
}
.date-wrap .day-num-wrap span{
	display: inline-block;
	width: 15.99%;
	float: left;
	color:#000000;
	font-size: 16px;
	text-align: center;
	text-decoration: underline;
}
.date-wrap .day-num-wrap span:first-child{
	text-align: left;
	width: 10%;
}
.date-wrap .day-num-wrap span:last-child{
	text-align: right;
	width: 10%;
}
.date-wrap .day-num-wrap span.disabled{
	color:#e0e0e0;
	text-decoration: none;
}
.date-wrap .day-num-wrap span.publish{
	color: #ffffff;
	background: #7fba41;
	text-decoration: none;
	border-radius: 50%;
	padding: 3px 0px 3px 9px;
	margin: 0px 6px 0 -6px;
}
.time-wrap{
	display: inline-block;
	width: 100%;
	margin-top: 30px;
	position: relative;
}
.time-wrap .time-label{
	color: #cccccc;
	transform: rotate(-90deg);
	display: inline-block;
	left: -46px;
	top: 13px;
	position: absolute;
	font-size: 14px;
}
.time-wrap span{
	display: inline-block;
	width: 30%;
	float: left;
	text-align: center;
	color: #000000;
	font-size: 15px;
	margin-bottom: 15px;
}
.time-wrap span:nth-child(4n+1){
	text-align: left;
	width: 20%;
}
.time-wrap span:nth-child(4n+4){
	text-align: right;
	width: 20%;
}
.time-wrap span.disabled{
	color:#cccccc;
}
.time-wrap span.publish{
	background: #7fba41;
	color: #fff;
	padding: 5px 0;
	margin: -5px 0 -5px 0;
	border-radius: 50px;
}
#loading{
	
    width: 0px;
    top: 280px;
    right: 81%;
    position:fixed;
    z-index:9999;
    height: $(window).height() + 'px',
    background:url("<?php echo base_url('assets/img/Uppercut_logo.png'); ?>") no-repeat center center rgba(0,0,0,0.25)
}


</style>

<?php //$this->load->view('open'); ?>

<div class="note-head-wrap">
	<div class="note-head-arr"><span>&nbsp;</span></div>
	<div class="note-head-text font-chg">Service Booking</div>
</div>

<div class="container">
	<form method="POST" action="" id="frmdata">
		<div class="row">
			<div class="col-md-12">
				<div class="row" >
					<div class="item-wrap font-chg">
						<div class="service-combo-wrap" id='servicecombowrapid'>
							<select class="combo" id='service_type' name='service_type' required="">
								<option value="" disabled="disabled">Select a service</option>
								<option name="MensHairCut" value="MensHairCut">Men's Hair Cut</option>
								<option name="WomensHairCut" value="WomensHairCut">Women's Hair Cut</option>
								<option name="HairCutKids" value="HairCutKids">Hair Cut Kids</option>
								<option name="EyebrowThreading" value="EyebrowThreading">Eyebrow Threading</option>
							</select>
							<span><span class="fa fa-angle-down"></span></span>
						</div>
						<div class="date-wrap" id="datewrapid">
							<span class="date-label">Date</span>
							<div class="week-day-wrap">
								<input type='hidden' name='date' id='bookingdate' value="">
								<input type='hidden' name='time' id='bookingtime' value="">
								<input type='hidden' name='salonid' id='salonid' value="<?php echo $id ?>">
								<?php
								for($i=0; $i<=6; $i++)
								{
									echo "<span>".date('D',strtotime("$i day"))."</span>";
								}
								?>
							<!-- <span>M</span>
							<span>T</span>
							<span>W</span>
							<span>T</span>
							<span class="disabled">F</span>
							<span class="disabled">S</span>
							<span class="disabled">S</span> -->
						</div>
						<div class="day-num-wrap">							
							<!-- <span onclick="selectBookingDate(this, 5)" class="publish">5</span>
							<span onclick="selectBookingDate(this, 6)">6</span>
							<span onclick="selectBookingDate(this, 7)">7</span>
							<span onclick="selectBookingDate(this, 8)">8</span>
							<span onclick="selectBookingDate(this, 9)" >9</span><!-- class="disabled" -->
							<!-- <span onclick="selectBookingDate(this, 10)" >10</span>
								<span onclick="selectBookingDate(this, 11)" >11</span> - -->

								<?php
								for($i=0; $i<=6; $i++)
								{
									echo "<span onclick='selectBookingDate(this,\"" . date('Y-m-d', strtotime("$i day")) . "\")'>".date('d',strtotime("$i day"))."</span>";
									//echo date('Y-m-d: D',strtotime("$i day"))."<br>";

								}
								?>
							</div>
						</div>
						<div class="time-wrap" id="timewrapid">
							<span onclick="selectBookingTime(this,'8 AM')">8 AM</span>
							<span onclick="selectBookingTime(this, '9 AM')">9 AM</span>
							<span onclick="selectBookingTime(this, '10 AM')">10 AM</span>
							<span onclick="selectBookingTime(this, '11 AM')">11 AM</span>
							<span onclick="selectBookingTime(this, '12 AM')">12 AM</span>
							<span onclick="selectBookingTime(this, '1 PM')">1 PM</span>
							<span onclick="selectBookingTime(this, '2 PM')">2 PM</span>
							<span onclick="selectBookingTime(this, '3 PM')">3 PM</span>
							<span onclick="selectBookingTime(this, '4 PM')">4 PM</span>
							<span onclick="selectBookingTime(this, '5 PM')">5 PM</span>
							<span onclick="selectBookingTime(this, '6 PM')">6 PM</span>
							<span onclick="selectBookingTime(this, '7 PM')">7 PM</span>
							<span onclick="selectBookingTime(this, '8 PM')">8 PM</span>
							<span onclick="selectBookingTime(this, '9 PM')">9 PM</span>
							<span onclick="selectBookingTime(this, '10 PM')">10 PM</span>
							<div class="time-label">Time</div>
						</div>
						<div class="booking-request-wrap">
							<input type="Submit" name='btnsubmit' class='btn btn-success' id='btnsubmit' value="Send Booking Request">
							<!-- <a href="#">Send Booking Request</a> -->
							<!--  data-toggle="modal" data-target="#bookingrequest" -->
						</div>						
					</div>
				</div>
			</div>
		</div>
	</form>
	<div id="loading"></div>
</div>


<?php //$this->load->view('close'); ?>
<?php $this->load->view('layouts/footer'); ?>

<script type="text/javascript">
	function selectBookingDate(current, date)
	{
	// alert(date);
	// console.log($(current));
	$(".day-num-wrap").children().each(function(e,i){
		$(i).removeClass('publish');
	})
	$(current).addClass('publish');
	$("#bookingdate").val(date);

}
function selectBookingTime(current, time)
{
	// alert(date);
	// console.log($(current));
	$(".time-wrap").children().each(function(u,t){
		$(t).removeClass('publish');
	})
	$(current).addClass('publish');
	$("#bookingtime").val(time);

}
$(document).ready(function(){
	$("#btnsubmit").click(function(event) {
		event.preventDefault();
		$("#btnsubmit").attr("disabled", true);
		$('#loading').html('<img src="<?php echo base_url('assets/img/ajax-loader.gif'); ?>"> loading...');       
		var service_type = $("#service_type").val();
		var salonid = $("#salonid").val();
		var date = $("#bookingdate").val();
		var time = $("#bookingtime").val();
		$.ajax({
			type: "POST",
			url: "<?php echo base_url("shop/shop/sbookingajax"); ?>",
			data: "service_type=" + service_type + "&salonid=" + salonid + "&date=" + date + "&time=" + time,
			success: function(data){
				
				if(data > 0)
				{						
					setTimeout(function () 
					{
						//window.location="<?php // echo base_url('shop/shop/mybooking/'); ?>" + data;
						//$('#loading').html('<img src="<?php //echo base_url('assets/img/loading.gif'); ?>">');
						window.location="<?php echo base_url('shop/shop/bookingStatus/'); ?>" + data;
					}, 60000);
					
				}	
				//$("#ajaxStart").attr("disabled", false);		

			}
		});
	});
});

</script>

