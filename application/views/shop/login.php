<?php $this->load->view('public_header'); ?>
<style >
  .pt-6 {
    padding-top: 230px;
}
.round
{
  border: 1px solid #C2C2C2 !important;
    border-radius: 50px;
    padding-left: 15px;
    height: 40px;
}
.form-body .btn {
    border-radius: 100px;
}
.btn-green {
    background-color: #007bff;
}
body
    {
      background: url(<?= base_url(); ?>assets/img/login_bg.png) no-repeat;
    }

/* Thulasi CSS starts */
.a-mod {
	color: #3D8DFB;
	font-size: 13px;
}

.loginh1
{
	    padding-top: 200px !important;
}

.form-group {
	margin-bottom: 15px;
}

.error {
	color: red;
	font-size: 13px !important;
	padding-left: 20px;
}

@media only screen and (max-width: 420px) {
.pt-6 {
    padding-top: 50% !important;
}
}

/* Thulasi CSS ends */    
    
</style>
<div class="container">
        <div class="">
          <!--
          <div class="col-md-12">
            <div class=" shop-login-signup pull-right" >
              <span>SHOP</span>
              <div class="login-signup-link" >
                <ul>
                  <li><a href="<?php echo base_url("login"); ?>">Login</a></li>
                  <li><a href="<?php echo base_url("signup"); ?>">Signup</a></li>
                </ul>
              </div>
            </div>
          </div>
        -->
          <div class="col-md-12">
        <div class="row">
        <div class="py-6 mx-auto">

          <!-- <h1 class="pt-6 text-center loginh1" style="margin-bottom: 6px;">Login</h1> -->
          
          <p class="pt-6 text-center" style="color: grey;">Login</p>
          <div class="form-body mx-auto">
            <?php echo get_flashdata('message'); ?>
            
            <form class="form" role="form" autocomplete="off" method="post" action="<?php echo base_url('shoplogin');?>" id="formLogin">
              <div class="form-group">
                <label class="sr-only" for="email">Email</label>
                <input type="email" class="form-control round form-control-lg rounded-0" name="email" id="email" placeholder="Email" required>
              </div>
              <div class="form-group">
                <label class="sr-only" for="password">Password</label>
                <input type="password" class="form-control round form-control-lg rounded-0" id="password" name="password" placeholder="Password" required autocomplete="new-password">
              </div>
              <div class="form-group form-group3">
                
                <div class="text-center"><a class="a-mod" href="<?php echo base_url('restpass'); ?>">Forgot Password</a></div>
              </div>
              <button type="submit" class="btn btn-text btn-green btn-sz">Login</button>
            </form>
            <div class="form-url">
              <p class="text-center">Don't have an account? <span><a class="a-mod" href="<?php echo base_url('signup');?>">sign up</a></span></p>


            </div>
          </div>
        </div>
        <!--/col-->
      </div>
      <!--/row-->
    </div>
    <!--/col-->
  </div>
  <!--/row-->
</div>
<!--/container-->
<?php $this->load->view('layouts/footer'); ?>
<script type="text/javascript">

  $('#formLogin').validate({
    rules:{
      email:{
        required: true,
        email: true
      },
      password: {
        required: true
      }
    },
      messages: { 
email:{ 
required:'Please enter a valid email id ' 
}
},
    
  });


</script>