<?php load_view('layouts/header'); ?>
</div>
<div class="main">
  <div class="">
    <div class="col-md-12">
      <div class="">
        <div class="py-2 header-body" style="border-bottom: none;">
          <div class="" href="" >
            <div class="mr-3" id="oval">
             <div class="toggleTab man">
            <div class="tog-icon"></div>
            <div class="tog-icon"></div>
          </div>
            </div>
            <div class="s-name">
              <h1><a href="<?= base_url(); ?>editprofile"></a></h1>
            </div>
          </div>
          <!-- <div class="coll">
            <div class="btn-body">
              <button class="bttn bttn-blue">SAVE</button>
              <button class="bttn bttn-border-blue">CANCEL</button>
            </div>
          </div> -->
        </div>
				<div class="mx-auto">
					<h1 class="pt-5 pb-3 text-center">Add Shop</h1>
					<div class="form-body">
						<?php echo get_flashdata('message'); ?>
						<form class="form" role="form" autocomplete="off"  method="post" id="addform" action="<?php echo base_url('shop/shop/addShop'); ?>" enctype="multipart/form-data">
							<div class="form-group">
								<label class="sr-only" for="shopname">Shop name</label>
								<input type="text" class="form-control form-control-lg rounded-0" id="shopname" name="shopname" placeholder="Shop name" required autocomplete="old-password">
							</div>
							<div class="form-group">
								<label class="sr-only" for="website">Website</label>
								<input type="text" class="form-control form-control-lg rounded-0" id="website" name="website" placeholder="Enter website" required autocomplete="new-password">
							</div>
							<div class="form-group">
								<label class="sr-only" for="contact">Contact Number</label>
								<input type="number" class="form-control form-control-lg rounded-0" id="contact" name="contact" placeholder="Enter contact" required autocomplete="new-password">
							</div>	
							<div class="form-group">
								<label class="sr-only" for="address">Search On Google</label>
								<input type="hidden" name="lat" value="" id="lat">
								<input type="hidden" name="lon" value="" id="lon">
								<input class="form-control" id="address" name="address" placeholder="address" autocomplete/>
							</div>	

							<div class="form-group">
								<label class="sr-only" for="location">Address</label>
								<input type="text" class="form-control" id="street" name="street" placeholder="street address" value="">
							</div>
							<div class="form-group">
								<label  class="sr-only" for="country" class="">Country</label>
								<input type="text" class="form-control" id="country" name="country" placeholder="country" value="">
							</div>
							<div class="form-group">
								<label class="sr-only"   for="city" class="">City</label>
								<input type="text" class="form-control" id="city" name="city" placeholder="city" value="">
							</div>
							<div class="form-group">
								<label class="sr-only" for="state" class="">State</label>
								<input type="text" class="form-control" id="state" name="state" placeholder="state" value="">
							</div>
							<div class="form-group">
								<label class="sr-only" for="postalcode" class="">Post Code</label>
								<input type="text" class="form-control" id="postalcode" name="postalcode" placeholder="post code" value="">
							</div>					

							<div class="form-group">
								<label class="sr-only" for="about">About Shop</label>
								<textarea class="form-control" id="about" name="about_me" rows="3" placeholder="Enter company details" required></textarea>
							</div>
							<div class="form-group"  id="errordiv">
								<div class="btn btn-default btn-file">
									<i class="fa fa-paperclip"></i> Select Image
									<input type="file" id="fUpload" name="attachment" required>
								</div>
								<div> 
									<p class="help-block" id="file-info">Max. 1MB</p>
								</div>
							</div>
							<button type="submit" class="btn btn-text3 btn-orng btn-sz">Add new shop</button>
						</form>
						
					</div>
				</div>
				<!--/col-->
			</div>
			<!--/row-->
		</div>	
		<!--/col-->
	</div>
	<!--/row-->
</div>

<!--/container-->
<?php $this->load->view('layouts/footer'); ?>
<script type="text/javascript">
	
	$(document).ready(function(){
		var url=window.location.href;
		var email = window.location.href.substring(url.lastIndexOf('#')+1);

		$('#changePassword').validate({
			rules:{
				shopname:{
					required: true,
				},
				website:{
					required: true,					
				},
				contact:{
					required: true,					
				},			
				location:{
					required: true,					
				},
				country:{
					required: true,					
				},
				city:{
					required:true
				},
				state:{
					required:true
				},
				about:{
					required:true,
				},
				street:{
					required:true
				},
				attachment:{
					required:true
				}
			},
		});
	});

</script>

<script type='text/javascript' src='<?php echo base_url('assets/admin/js/backend/main.js'); ?>'></script>

