<?php $this->load->view('public_header'); ?>
<style >
  .pt-6 {
    padding-top: 100px !important;
}
.round
{
  border: 1px solid #9a9a9a !important;
    border-radius: 50px;
    padding-left: 15px;
    height: 45px;
}
.form-body .btn {
    border-radius: 100px;
}
.btn-green {
    background-color: #007bff;
}
body
    {
      background: url(<?= base_url(); ?>assets/img/login_bg.png) no-repeat;
    }
    
/* Thulasi CSS starts */
.a-mod {
	color: #3D8DFB;
	font-size: 13px;
}

.btn-text {
	letter-spacing: 5px;
}

.body-text {
	font-size: 14px;
	color: #4A4A4A !important;
	font-weight: 400;
}

.error {
	color: red;
	font-size: 13px !important;
	padding-left: 20px;
}

@media only screen and (max-width: 420px) {
.pt-6 {
    padding-top: 25% !important;
}
}

/* Thulasi CSS ends */ 

    
</style>
<div class="container">
        <div class="">
          <!--
          <div class="col-md-12">
            <div class=" shop-login-signup pull-right" >
              <span>SHOP</span>
              <div class="login-signup-link" >
                <ul>
                  <li><a href="<?php echo base_url("login"); ?>">Login</a></li>
                  <li><a href="<?php echo base_url("signup"); ?>">Signup</a></li>
                </ul>
              </div>
            </div>
          </div>
        -->
          <div class="col-md-12">
            <div class="row">
    <div class="py-6 mx-auto">
      <p class="pt-6 text-center" style="color: grey;">Step 1</p>
     <h6 class=" text-center body-text" style="margin-bottom: 20px;">Let's start by creating an account for you</h6> 

     <div class="form-body mx-auto">
      <form class="form" method="POST" role="form" action="<?php echo base_url("shop/shop/usersignup") ?>" autocomplete="off" id="formSignup">
      <!-- <form class="form" role="form" action="<?php echo base_url("shop/Shop") ?>" autocomplete="off" id="formSignup"> -->

       <div class="form-group">

        <label class="sr-only" for="email">email</label>

        <input type="email" class="form-control round form-control-lg rounded-0" name="email" id="email" placeholder="Email" required autofocus="autofocus">

       </div>

       <div class="form-group">

        <label class="sr-only" for="phone">Phone Number</label>

        <input type="number" class="form-control round form-control-lg rounded-0" name="phone" id="phone" placeholder="Phone Number" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
    type = "number"
    maxlength = "10" required>

       </div>

       <div class="form-group">

        <label class="sr-only" for="password">Password</label>

        <input type="password" class="round form-control form-control-lg rounded-0" id="password" name="password" placeholder="Password" required autocomplete="new-password">

       </div>

       <div class="form-group form-group3">

        <div class="text-center terms">

         <p style="font-size:10px;">By signing up you agree to <span><a class="a-mod" style="font-size:10px;" href="<?= base_url(); ?>terms_condition">terms & conditions</a></span></p>

        </div>

       </div>

       <button type="submit" style="font-size: 13px;" class="btn btn-text btn-green btn-sz">Create Account</button>

      </form>

      <div class="form-url">

       <p class="text-center">Already have an account? <span><a href="<?php echo base_url('login');?>">LOGIN</a></span></p>

      </div>

     </div>

    </div>

    <!--/col-->

   </div>

   <!--/row-->

  </div>

  <!--/col-->

 </div>

 <!--/row-->

</div>



<!--/container-->

<?php $this->load->view('layouts/footer'); ?>

<script type="text/javascript">



 $('#formSignup').validate({

  rules:{

   email:{

    required: true,

    email: true

   },

   phone:{

    required: true,

    number:true,

    minlength:10,

    maxlength:10,

   },

   password: {

    required: true

   }

  },



  messages: { 

   email:{ 

    required:'Please enter a valid email id ' 

   },

   phone:{ 

    required:'Please enter a valid phone no.' 

   }

  },



  submitHandler: function() { 



   var formData=$("#formSignup").serialize();

   signUp(formData);

  }

 });


</script>


