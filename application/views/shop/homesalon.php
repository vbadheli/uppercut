<?php $this->load->view('public_header'); ?>
<style >
.font-chg{
	font-family: 'Comfortaa', cursive;
}
.row{
	margin-left:0 !important;
}
.col-md-12 .row{
	padding-left:25px;
}
.note-head-wrap{
	position: absolute;
	top: 0;
	left: 0;
	right: 0;
	padding-bottom: 15px;
}
.note-head-arr{
	width: 55px;
	display: inline-block;
	float: left;
}
.note-head-arr span{
	border: 2px solid #272727;
	display: inline-block;
	width: 30px;
	height: 12px;
	border-left: none;
	border-right: none;
	margin: 28px 0 0 12px;
}
.note-head-text{
	width: calc(100% - 55px);
	text-transform: uppercase;
	color: #a1a1a1;
	display: inline-block;
	float: left;
	padding-top: 24px;
	font-size: 16px;
}
.note-head-text span.one{
	position: absolute;
	right: 10px;
	font-size: 20px;
	color: #000000;
	top: 22px;
}
.note-head-text span.two{
	position: absolute;
	right: 12px;
	font-size: 16px;
	color: #fff;
	top: 24px;
}
.note-head-text span.notif{
	color: #fff;
	background: red;
	border-radius: 50%;
	font-size: 8px;
	padding: 3px;
	position: absolute;
	right: 5px;
	top: 18px;
	border: none;
}

.item-wrap{
	/*padding-top:850px;*/
	padding-top:340px;
	padding-right: 5%;
	width: 100%;
}
.success-msg{
	color: #000000;
	background: #d3fb00;
	position: absolute;
	left: -15px;
	right: 0;
	padding: 20px 15px 20px 55px;
	font-size: 12px;
	/*top: 325px;*/
	top: 75px;
}
.success-msg p{
	font-size: 16px;
	margin: 0;
}
.success-msg p:first-child{
	margin-top: 10px;
}
.success-msg .accept-button{
	color: #fff;
	background: #000000;
	padding: 7px;
	width: calc(50% - 5px);
	margin-top: 20px;
	border: none;
	text-transform: uppercase;
	letter-spacing: 1px;
	font-size: 10px;
	border-radius: 7px;
	margin-right: 10px;
	float: left;
}
.success-msg .reject-button{
	color: #000000;
	background: transparent;
	padding: 6px;
	width: calc(50% - 5px);
	margin-top: 20px;
	border: none;
	text-transform: uppercase;
	letter-spacing: 1px;
	font-size: 10px;
	border-radius: 7px;
	float: left;
	border:1px solid #000000;
}
.stats{
	margin-bottom: 25px;
}
.stats-top{
	font-size: 12px;
	color:#a4a4a4;
}
.stats-bottom{
	border: 1px solid #b9b9b9;
	border-radius: 40px;
	display: inline-block;
	margin-left: -35px;
	padding:5px 10px 5px 30px;
	width: calc(100% + 35px);
}
.stats-block{
	display: inline-block;
	width: 33.3333%;
	float: left;
}
.stats-block .figur{
	display: inline-block;
	width: 100%;
	color:#292929;
	font-size: 14px;
}
.stats-block .desc{
	display: inline-block;
	width: 100%;
	color:#a4a4a4;
	font-size: 12px;
}
.search-otp-label{
	margin-top: 10px;
	color: #000000;
	font-size: 13px;
	margin-bottom: 10px;
}
.or-label{
	margin-top: 10px;
	color: #000000;
	font-size: 13px;
	margin-bottom: 10px;
}
.otp-input-wrap{
	display: inline-block;
	width: 100%;
}
.otp-input-wrap input, .otp-input-wrap button{
	display: inline-block;
	float: left;
	height: 50px;
	width: calc(20% - 10px);
	border:1px solid #8e8e8e;
	border-radius: 5px;
	margin-right: 10px;
	text-align: center;
}
.otp-input-wrap button{
	background: #000000;
	color: #fff;
	border-color:#000000;
}
.phone-wrap input{
	display: inline-block;
	float: left;
	height: 40px;
	width: calc(80% - 10px);
	border:1px solid #8e8e8e;
	border-radius: 5px;
	margin-right: 10px;
	text-align: center;
	padding:0px 10px;
}
.phone-wrap button{
	display: inline-block;
	float: left;
	height: 40px;
	width: calc(20% - 10px);
	border:1px solid #8e8e8e;
	border-radius: 5px;
	background: #000000;
	color: #fff;
	border-color:#000000;
}
.icon-links{
	display: inline-block;
	width: 100%;
	margin-bottom: 30px;
}
.icon-links .section{
	padding-top: 30px;
	display: inline-block;
	width: 25%;
}
.icon-links .section .icon{
	text-align: left;
}
.icon-links .section .text{
	text-align: left;
	font-size: 11px;
	padding-top: 4px;
	text-transform: uppercase;
}
.gray-label{
	font-size: 14px;
	color:#cdcdcd;
	padding:4px 0;
}
.desk{
	font-size: 14px;
	color:#70b12d;
	text-transform: uppercase;
	margin-bottom: 20px;
}
.no-booking{
	font-size: 10px;
	margin-bottom: 30px;
	margin-top: 5px;
	color: #939393;
}
.success-msg1 {
	color: #000000;
	background: #a8e767;
	position: absolute;
	left: -15px;
	right: 0;
	padding: 20px 15px 20px 55px;
	font-size: 12px;
	top: 75px;
	top: 75px;
}
.success-msg1 .send-button {
    color: #fff;
    background: #000000;
    padding: 7px;
    width: 100%;
    margin-top: 20px;
    border: none;
    text-transform: uppercase;
    letter-spacing: 1px;
}

button {
    cursor: pointer;
}
.test-success-msg{
	color: #000000;
	background: #a8e767;
	position: absolute;
	left: -15px;
	right: 0;
	padding: 15px 15px 15px 55px;
	font-size: 13px;
	/*top: 525px;*/
	top: 75px;
	font-weight: bold;
}
.test-success-msg span{
	font-size: 22px;
	position: absolute;
	left: 10px;
	top: 6px;
}
.warning-msg{
	color: #000000;
	background: #f7c204;
	position: absolute;
	left: -15px;
	right: 0;
	padding: 15px 15px 15px 55px;
	font-size: 12px;
	/*top: 590px;*/
	top: 126px;
}
.warning-msg span{
	font-size: 20px;
	position: absolute;
	left: 10px;
	top: 14px;
}
.test-reject-success-msg{
	color: #000000;
	background: #a8e767;
	position: absolute;
	left: -15px;
	right: 0;
	padding: 15px 15px 15px 55px;
	font-size: 13px;
	top: 685px;
	top: 75px;
	font-weight: bold;
}
.test-reject-success-msg span{
	font-size: 22px;
	position: absolute;
	left: 10px;
	top: 6px;
}
.test-reject-success-msg .submit-button{
	color: #fff;
	background: #000000;
	padding: 7px;
	width: calc(50% - 5px);
	margin-top: 20px;
	border: none;
	text-transform: uppercase;
	letter-spacing: 1px;
	font-size: 10px;
	border-radius: 7px;
	margin-right: 10px;
	float: left;
}
.test-reject-success-msg .cancel-button{
	color: #000000;
	background: transparent;
	padding: 6px;
	width: calc(50% - 5px);
	margin-top: 20px;
	border: none;
	text-transform: uppercase;
	letter-spacing: 1px;
	font-size: 10px;
	border-radius: 7px;
	float: left;
	border:1px solid #000000;
}
.gray-label{
	text-transform: uppercase;
	color:#cacaca;
	font-size: 13px;
}
.gray-label:last-child{
	margin-top: 20px;
}
.gray-label a{
	text-transform: none;
	color:#3d8dfb;
	font-size: 12px;
	margin-left: 10px;
}
.item{
	border-bottom: 2px dashed #999999;
	margin-bottom:30px;
	padding-bottom: 20px;
}
.item .time{
	color: #000000;
	font-size: 18px;
}
.item .cut-type{
	color: #999999;
font-size: 14px;
padding: 2px 0;
}
.item .price{
	color: #999999;	
	font-size: 15px;
}
.item .customer-name{
	color: #000000;
	font-size: 16px;
	padding-top: 40px;
}

.item .customer-note{
	color: #999999;	
	font-size: 14px;
	padding: 0 0 6px 0;
	margin-top: 15px;
}
.item .customer-desc{
	color: #515151;
	font-size: 14px;
	line-height: 20px;
}
.item .controls{
	padding-top: 20px;
}
.item .controls a{
	margin-right: 22px;
	color: #3d8dfb;
	font-size: 14px;
}
.item .controls a span.fa{
	font-size: 18px;
}

</style>

<?php //$this->load->view('open'); ?>

<div class="note-head-wrap">
	<div class="note-head-arr"><span>&nbsp;</span></div>
	<div class="note-head-text font-chg">
		<img src="https://uppercut.shop/assets/img/logo.svg">
		<span class="one"><span class="fa fa-bell"></span></span>
		<span class="two"><span class="fa fa-bell"></span></span>
		<span class="notif">58</span>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="row">

				<!-- <h2><?php //echo $this->session->flashdata('divcongrats'); ?></h2>  -->
				<?php
				if(isset($divcongrats)): ?>
				<div class="success-msg1 font-chg" id="divcongrats">
					<span><span class="fa fa-check-circle"></span></span>
					Congrats!!! your salon profile is active. Now you can accept booking.
					<BR><BR>
					1.Keep time slots updated.<br>
					2.Be ready to accept booking via sms and portal.
					<br><br>
					Let's send test booking request.
					<a href="<?php echo base_url('shop/shop/testbookingrequest'); ?>"><button class="send-button">Send</button></a>
				</div>
			<?php endif;?>			
			
			<?php				
				if(isset($divtestbooking)):?>
					<input type='hidden' name='bookingid' id='bookingid' value="<?= $insert_id ?>">
				<?php $id = $this->db->where('id',$insert_id)->get('bookings')->row();  ?>	
				<div class="success-msg font-chg" id="divtestbooking">
					Test Booking
					<p><?= $id->service_type ?></p>
					<p><?= $id->date ?> (today)</p>
					<p><?= $id->time ?></p>
					<a href="<?= base_url(); ?>shop/shop/ShopbookingAccept/<?= $id->id; ?>" class="accept-button">Accept</a>
					<a href="<?= base_url(); ?>shop/shop/ShopbookingReject/<?= $id->id; ?>" class="reject-button">Reject</a>
				</div>
			<?php endif;?>
			<?php
				if(isset($divbookingaccept)):?>
				<div class="row" id="divbookingaccept">
				<div class="test-success-msg font-chg" id="divbookingacceptSuccess">
					<span><span class="fa fa-check-circle"></span></span>
					Test booking accepted successfully !
				</div>
				<div class="warning-msg font-chg" id="divwarning">					
						<span><span class="fa fa-warning"></span></span>
					Thank you for submitting. Our team is reviewing your profile for activation within 48 hours.
				</div>
			</div>
			<?php endif;?>
			<?php
				if(isset($divbookingreject)):?>
				<div class="test-reject-success-msg font-chg" id="divbookingreject">
					<form role="form" method="post" action="<?php echo base_url(); ?>shop/shop/ShopbookingRejectF/<?= $id; ?>" enctype="multipart/form-data" id="formReject">
					New Booking
					<select class="form-control" id="rejectreason" name="choose" required="">
		              <option value="">Select reason for rejecting order</option>
		              <option>Already Book This Scheduled.</option>
		              <option>Holiday On This Day.</option>
		              <option>Your Book Desk Already Book.</option>
		              <option>Other</option>
		            </select>
            		<br>
          			 <div class="form-group hide" id="OtherReason">
          			   <label  for="txtreason">
          			    Reason of Rejected
          			  </label>
          			  <textarea class="form-control" placeholder="Enter Your Reason" type="text" cols="30" rows="3" name="txtreason" id="txtreason"></textarea>              
          			</div>          			
					<button class="submit-button">Submit</button>
					<button class="cancel-button">Cancel</button>
				</form>
				</div>
			<?php endif;?>
				
			
				<div class="item-wrap font-chg">
					 <div class="stats">
						<div class="stats-top">
							Today
						</div>
					
						<!--<div class="stats-bottom">
							<div class="stats-block">
								<div class="figur">50,000</div>
								<div class="desc">Revenue</div>
							</div>
							<div class="stats-block">
								<div class="figur">20K</div>
								<div class="desc">Bookings</div>
							</div>
							<div class="stats-block">
								<div class="figur">5</div>
								<div class="desc">Chairs</div>
							</div>
						</div> -->
					</div>
					<!-- <div class="search-otp-label">Search booking with OTP</div>
					<div class="otp-input-wrap">
						<input type="text">
						<input type="text">
						<input type="text">
						<input type="text">
						<button><span class="fa fa-search"></span></button>
					</div>
					<div class="or-label">(or)</div> -->
					<div class="phone-wrap">
						<form action="<?php echo base_url(); ?>search/searchCust" method="post">
						<input type="text" placeholder="Search with phone number" name="phone">
						<button ><span class="fa fa-search"></span></button>
						</form>
					</div>
					<div class="icon-links">
						<!-- <div class="section">
							<div class="icon"><?php echo str_repeat('&nbsp;',2); ?><span class="fa fa-asterisk"></span></div>
							<div class="text">Chair</div>
						</div> -->
						<div class="section">
							<a href="<?php echo base_url('shop/bookings/all')?>"><div class="icon"><?php echo str_repeat('&nbsp;',4); ?><span class="fa fa-list-alt"></span></div>
							<div class="text">Bookings</div></a>
						</div>
						<div class="section">
							<a href="<?php ?>"><div class="icon"><?php echo str_repeat('&nbsp;',4); ?><span class="fa fa-comment"></span></div>
							<div class="text">Feedback</div></a>
						</div>
					</div>
					<div class="gray-label">On-Going Bookings <a href="<?= base_url(); ?>shop/bookings/all">View All</a></div>

					<?php if(count($result)>0):
						$i=0;
							foreach($result as $item):
								if($i==0):
									$i++;?>
					<div class="item">
						<div class="my-badge-wrap"><span class="my-badge ongoing">Ongoing</span></div>
						<div class="time font-chg"><?= $item->time?></div>
						<div class="cut-type font-chg"><?= $item->service_type?></div>
						<div class="price font-chg">₹ 199 | 30 mins</div>
						<div class="cut-type font-chg"><?= $item->date ?></div>
						<div class="customer-name font-chg">Customer Name</div>
						<div class="controls">
							<a href="#"><span class="fa fa-cut fa-2x"></span> Stop</a>
						</div>
					</div>
				<?php else:?>
					<div class="item">
						<div class="time font-chg"><?= $item->time?></div>
						<div class="cut-type font-chg"><?= $item->service_type?></div>
						<div class="price font-chg">₹ 199 | 30 mins</div>
						<div class="cut-type font-chg"><?= $item->date ?></div>
					</div>
				
				<?php endif;?>

				<?php endforeach;?>
				<?php else:?>
					<div class="item">
						<div class="my-badge-wrap"><span class="my-badge ongoing">No Bookings found</span></div>
					</div>
				<?php endif;?>
					<!--<div class="item">
						<div class="time font-chg">5:00 PM</div>
						<div class="cut-type font-chg">Men's - hair cut</div>
						<div class="price font-chg">₹ 199 | 30 mins</div>
						<div class="customer-name font-chg">Customer Name</div>
						<div class="customer-note font-chg">Note From Customer</div>
						<div class="customer-desc font-chg">The resulting cornea flap is created at a precise depth and dia.<a href="#">Read More</a></div>
						<div class="customer-note font-chg">Internal Note</div>
						<div class="customer-desc font-chg">The resulting cornea flap is created at a precise depth and dia.<a href="#">Read More</a></div>
						<div class="controls">
							<a href="#"><span class="fa fa-rocket fa-2x"></span> Start</a>
							<a href="#"><b>OTP</b>-Resend</a>
							<a href="#"><span class="fa fa-cut fa-2x"></span></a>
							<a href="#"><span class="fa fa-close fa-2x"></span></a>
						</div>
					</div>-->
					<!-- <div class="gray-label">Barber chair slots</div>
					<div class="desk">Desk 1</div>
					<div class="gray-label">On-going bookings</div>
					<div class="no-booking">No booking yet</div> -->
				</div>
			</div>
			
		</div>
	</div>
</div>

<?php //$this->load->view('close'); ?>

<?php $this->load->view('layouts/footer'); ?>

<script type="text/javascript">
  $(document).ready(function() {
  	$("#OtherReason").hide();
    $("#rejectreason").on("change", function(){
      var selectedreason = $(this).val();
      $("#OtherReason").hide();
      if(selectedreason.toLowerCase() == 'other')
      {
        $("#OtherReason").show();
      }
    });
  });

 </script> 
