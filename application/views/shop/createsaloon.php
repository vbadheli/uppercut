<?php $this->load->view('public_header'); ?>
<style >
.pt-6 {
    padding-top: 150px;
}
.round
{
  border: 1px solid #9a9a9a !important;
    border-radius: 50px;
    padding-left: 15px;
    height: 45px;
}
.form-body .btn {
    border-radius: 100px;
}
.btn-green {
    background-color: #007bff;
}
body
    {
      background: url(<?= base_url(); ?>assets/img/login_bg.png) no-repeat;
    }
    
/* Thulasi CSS starts */
.btn-text {
	letter-spacing: 5px;
}

.body-text {
	font-size: 14px;
	color: #4A4A4A !important;
	font-weight: 400;
}

.left-pad {
	padding-left: 3%;
	padding-right: 3%;
}

.form-group {
	margin-bottom: 5px;
}

/* Thulasi CSS ends */     
    
</style>
<div class="container">
        <div class="">
          
          <div class="col-md-12">
            <div class="row">
    <div class="py-6 mx-auto">
      <!-- <p class="pt-6 text-center" style="color: grey;">Step 3</p> -->
     <h6 class=" text-center body-text" style="margin-bottom: 20px;">Click to create your saloon profile</h6>

     <div class="form-body mx-auto">
      

      <div class="form-url">

       <a  action="<?= base_url(); ?>shop/shop/saloonprofile" href="<?= base_url(); ?>shop/shop/saloonprofile" style="font-size: 14px; " class="btn btn-text btn-green btn-sz">Create a Saloon</a>

      </div>

     </div>

    </div>

    <!--/col-->

   </div>

   <!--/row-->

  </div>

  <!--/col-->

 </div>

 <!--/row-->

</div>



<!--/container-->

<?php $this->load->view('layouts/footer'); ?>

