<?php load_view('public_header'); ?>
<style>
	.btn-green {
    background-color: #007bff;
}
</style>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="py-6 mx-auto">
					<Br>
					<Br>
					<h1 class="py-5 text-center">Reset Password</h1>
					<p class="text-center ptext">Enter your New Password Here</p>
					<p class="alert alert-success" style="display: none;" id="alert"></p>
					<div class="form-body py-6 mx-auto">
						<form class="form" action="<?= base_url(); ?>auth/update_password" method="post" role="form" autocomplete="off" id="formrestpassword">
							<div class="form-group">
								<label class="sr-only" for="password">New Password</label>
								<input type="password" class="form-control form-control-lg rounded-0" name="password" id="password" placeholder="New Password" required>
							</div>
						<input type="hidden" value="<?= $data['email']; ?>" name="email">
						<input type="hidden" value="<?= $data['activation_code']; ?>" name="activation_code">

							<button type="submit" class="btn btn-text3 btn-green btn-sz">Reset password</button>
						</form>
						<div class="form-url">
							
						</div>
					</div>
				</div>
				<!--/col-->
			</div>
			<!--/row-->
		
	<!--/row-->
</div>
<!--/container-->
<?php $this->load->view('layouts/footer'); ?>


