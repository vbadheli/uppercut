<?php $this->load->view('public_header'); ?>
<style >
.font-chg{
  font-family: 'Comfortaa', cursive;
}
.row{
  margin-left:0 !important;
}
.col-md-12 .row{
  padding-left:25px;
}
.note-head-wrap{
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  padding-bottom: 15px;
}
.note-head-arr{
  width: 55px;
  display: inline-block;
  float: left;
}
.note-head-arr span::before{
  content: '';
  border-right: 16px solid #0b0b0b;
  border-top: 14px  solid transparent;
  border-bottom: 14px  solid transparent;
  float: left;
  position: absolute;
  margin-top: 20px;
  margin-left: 20px;
}
.note-head-arr span::after{
  content: '';
  border-right: 16px solid #fff;
  border-top: 14px  solid transparent;
  border-bottom: 14px  solid transparent;
  float: left;
  position: absolute;
  margin-top: 20px;
  margin-left: 17px;
}
.note-head-text{
  width: calc(100% - 55px);
  text-transform: uppercase;
  color: #a1a1a1;
  display: inline-block;
  float: left;
  padding-top: 15px;
  font-size: 16px;
}
.note-head-text span.date{
  display: inline-block;
  width: 100%;
  font-size: 12px;
  color:#3b3b3b;
}
.note-head-text span.icon{
  float: right;
  margin-right: 25px;
  font-size: 20px;
  color: #000000;
}
.note-head-text span.cname{
  display: inline-block;
  max-width: 68%;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}
.item-wrap{
  padding-top:80px;
  padding-right: 5%;
  width: 100%;
}
.stats{
  margin-bottom: 25px;
}
.stats-top{
  font-size: 12px;
  color:#a4a4a4;
}
.stats-bottom{
  border: 1px solid #b9b9b9;
  border-radius: 40px;
  display: inline-block;
  margin-left: -35px;
  padding:5px 10px 5px 30px;
  width: calc(100% + 35px);
}
.stats-block{
  display: inline-block;
  width: 33.3333%;
  float: left;
}
.stats-block .figur{
  display: inline-block;
  width: 100%;
  color:#292929;
  font-size: 14px;
}
.stats-block .desc{
  display: inline-block;
  width: 100%;
  color:#a4a4a4;
  font-size: 12px;
}
.search-otp-label{
  margin-top: 10px;
  color: #000000;
  font-size: 13px;
  margin-bottom: 10px;
}
.or-label{
  margin-top: 10px;
  color: #000000;
  font-size: 13px;
  margin-bottom: 10px;
}
.otp-input-wrap{
  display: inline-block;
  width: 100%;
}
.otp-input-wrap input, .otp-input-wrap button{
  display: inline-block;
  float: left;
  height: 50px;
  width: calc(20% - 10px);
  border:1px solid #8e8e8e;
  border-radius: 5px;
  margin-right: 10px;
  text-align: center;
}
.otp-input-wrap button{
  background: #000000;
  color: #fff;
  border-color:#000000;
}
.phone-wrap input{
  display: inline-block;
  float: left;
  height: 40px;
  width: calc(80% - 10px);
  border:1px solid #8e8e8e;
  border-radius: 5px;
  margin-right: 10px;
  text-align: center;
  padding:0px 10px;
}
.phone-wrap button{
  display: inline-block;
  float: left;
  height: 40px;
  width: calc(20% - 10px);
  border:1px solid #8e8e8e;
  border-radius: 5px;
  background: #000000;
  color: #fff;
  border-color:#000000;
}
.icon-links{
  display: inline-block;
  width: 100%;
  margin-bottom: 30px;
}
.icon-links .section{
  padding-top: 30px;
  display: inline-block;
  width: 25%;
}
.icon-links .section .icon{
  text-align: left;
}
.icon-links .section .text{
  text-align: left;
  font-size: 11px;
  padding-top: 4px;
  text-transform: uppercase;
}
.gray-label{
  text-transform: uppercase;
  color:#cacaca;
  font-size: 13px;
}
.gray-label:last-child{
  margin-top: 20px;
}
.gray-label a{
  text-transform: none;
  color:#3d8dfb;
  font-size: 12px;
  margin-left: 10px;
}
.desk-wrapper{
  display: inline-block;
  width: 100%;
}
.desk-wrapper .desk{
  display: inline-block;
  width: 25%;
  border-right: 1px solid #ccc;
  float: left;
  font-size: 12px;
  margin:6px 0 10px 0;
  text-align: center;
}
.desk-wrapper .desk:nth-child(4n+1){
  text-align: left;
}
.desk-wrapper .desk:nth-child(4n+4){
  border:none;
}
.desk-wrapper .desk.active{
  color:#e99705;
}
.desk-wrapper .desk.expired{
  color:#c4c4c4;
}
.item{
  border-bottom: 2px dashed #999999;
  margin-bottom:30px;
  padding-bottom: 20px;
}
.item .time{
  color: #000000;
  font-size: 18px;
}
.item .cut-type{
  color: #999999;
font-size: 14px;
padding: 2px 0;
}
.item .price{
  color: #999999; 
  font-size: 15px;
}
.item .customer-name{
  color: #000000;
  font-size: 16px;
  padding-top: 40px;
}

.item .customer-note{
  color: #999999; 
  font-size: 14px;
  padding: 0 0 6px 0;
  margin-top: 15px;
}
.item .customer-desc{
  color: #515151;
  font-size: 14px;
  line-height: 20px;
}
.item .rating{
  margin: 10px 0;
}
.item .rating span{
  font-size: 20px;
  color:#70b12d;
  margin-right: 8px;
}
.item .controls{
  padding-top: 20px;
}
.item .controls a{
  margin-right: 22px;
  color: #3d8dfb;
  font-size: 14px;
}
.item .controls a.stop{
  color:#ff3624;
}
.item .controls a.right{
  float: right;
  margin-right: 0;
  margin-left: 30px;
}
.item .controls a span.fa{
  font-size: 18px;
}
.my-badge-wrap{
  display: inline-block;
  width: 100%;
  padding: 15px 0;
}
.my-badge{
  background: #ff3f2e;
  color: #fff;
  padding: 4px 18px 5px 10px;
  font-size: 10px;
  float: left;
  position: relative;
  text-transform: uppercase;
}
.my-badge::after{
  content: '';
  float: right;
  position: absolute;
  border-right: 18px solid white;
  border-top: 12px solid #ff3f2e;
  border-bottom: 12px solid #ff3f2e;
  top: 0;
  border-radius: 0px 0px 0px 0px;
  margin-left: 15px;
}
.my-badge.ongoing{background:#ffa402;}
.my-badge.ongoing::after{border-top-color:#ffa402;border-bottom-color:#ffa402;}
.my-badge.refund{background:#ff3f2e;}
.my-badge.refund::after{border-top-color:#ff3f2e;border-bottom-color:#ff3f2e;}
.my-badge.cancelled{background:#c2c2c2;}
.my-badge.cancelled::after{border-top-color:#c2c2c2;border-bottom-color:#c2c2c2;}
.my-badge.completed{background:#70b12d;}
.my-badge.completed::after{border-top-color:#70b12d;border-bottom-color:#70b12d;}
.stats{
  margin-bottom: 30px;
}
.stats .heading{
  color:#dcdcdc;
  font-size: 14px;
  padding: 6px 0;
}
.stats .action-wrap span{
  padding-right: 25px;
}
.stats .action-wrap span a{
  text-decoration:none;
  color:#64a4fc;
  font-size: 14px;
}
.stats .action-wrap span.active a{
  color:#000000;
  border-bottom:5px solid #000000;
}
</style>

<?php //$this->load->view('open'); ?>

<link href="http://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>

<div class="note-head-wrap">  
  <div class="note-head-arr"><span>&nbsp;</span></div>
  <div class="note-head-text font-chg"><span class="cname">Bookings</span><span class="icon"><span class="fa fa-search"></span></span><span class="icon"><span class="fa fa-calendar"></span></span><span class="icon"><span class="fa fa-arrow-right"></span></span><span class="date">25 Mar 2018</span></div>
</div>

<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="row">

        <div class="item-wrap font-chg">         
      <h5>Yours Booking List</h5>
                    <div class="item">
           <table class="table table-hover table-bordered display" id="example" datatable=""  width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Date Time</th>
                                     <th>User Name</th>                                     
                                </tr>
                            </thead>
                            <tbody>
                                <?php

                                if(isset($result)): 
                                    foreach ($result as $key => $value):  ?>
                                       <?php $id = $this->db->where('id',$value->salonid)->get('shop_list')->row(); ?>
                                       <?php $id1 = $this->db->where('id',$value->userid)->get('shop_signup')->row(); ?>
                                <tr>                                   
                                   <td><?php echo $value->date ?><?php echo "<br>Time:-".$value->time; ?><br>
                                     <a  href="<?= base_url(); ?>shop/shop/ShopbookingAcceptReject/<?= $value->id; ?>"  data-id="<?php echo custom_encode($value->id); ?>" class="badge badge-info"><i class="fa fa-edit"></i></a>
                                     <a  href="<?= base_url(); ?>shop/shop/ShopbookingReject/<?= $value->id; ?>" data-id="<?php echo custom_encode($value->id); ?>" class="badge badge-danger delete_user"><i class="fa fa-trash"></i></a>
                                   </td>                                   
                                   <td><?php echo $id1->username; ?></td>
                                   
                               </tr>
                           <?php  endforeach; endif; ?>
                       </tbody>
                   </table>
            </div>
            
          
        </div>

      </div>
    </div>
  </div>
</div>




<?php //$this->load->view('close'); ?>
<!-- <script type="text/javascript">
  $(document).ready(function(){ 
      //alert("hi");
      $('#example').DataTable(); 
  });
</script> -->
<?php $this->load->view('layouts/footer'); ?>
