<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Shop</title>
  <meta name="viewport" content="width=device-width, minimum-scale=1.0" />
  <link href="<?php  echo base_url('assets/vendor/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
  <link href="<?php  echo base_url('assets/vendor/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600|Roboto:300" rel="stylesheet">
  <link href="<?php  echo base_url('assets/vendor/magnific-popup/magnific-popup.css'); ?>" rel="stylesheet">
  <link href="<?php  echo base_url('assets/css/style.css'); ?>" rel="stylesheet">
  <!--  Scroll bar for desktop version -->
  <link rel="stylesheet" media="screen and (min-width: 900px)" type="text/css" href="<?php  echo base_url('assets/css/scroll.css'); ?>">
  <link rel="stylesheet" media="screen and (min-width: 900px)" href="<?php  echo base_url(); ?>assets/css/desktop.css">
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-100486865-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments)};
    gtag('js', new Date());
    gtag('config', 'UA-100486865-1');
  </script>
  <style type="text/css">

  i {
    border: solid white;
    border-width: 0 3px 3px 0;
    display: inline-block;
    padding: 3px;
  }
  span.fa-angle-left {
    color: black;
    font-size: 45px;
    font-weight: lighter;
  }
  .toggleTab {
    width: 35px;
    /* background-color: #1f1e21; */
    height: 25px;
    position: absolute;
    top: 5px;
    left: 15px;
    z-index: 999999;
    cursor: pointer;
    display: inline-block;
  }
  .left {
    transform: rotate(135deg);
    -webkit-transform: rotate(135deg);
  }
  b, strong {
    font-weight: 500;
  }
  h1 {
    font-weight: 300;
    font-size: 42px;
    color: #4a4a4a;
    letter-spacing: 0px;
  }
  .round {
    border: 1px solid #9a9a9a !important;
    border-radius: 5px;
    padding-left: 15px;
    height: 68px;
  }
  .form-body .btn {
    border-radius: 100px;
  }
  .btn-green {
    background-color: #007bff;
  }
  .filter_search_area a
  {
    color: black;
  }
  .filter_search_area .active 
  {
    color:red;
  }
  .row {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: 0px;
    margin-left: 0px;
  }
  .brand-name {
    margin-bottom: .5rem;
    font-family: inherit;
    font-weight: 500;
    line-height: 1.1;
    color: grey;
    font-style: normal;
    margin-top: 12px;
    margin-left: -37px;
  }
  .badge
  {
    vertical-align: top;
    margin-left: 6px;
    color: black;
  }
  .badge-info {
    color: black;
    background-color: #d3fb00;
  }
  .color-box
  {
    border:1px solid #d3fb00;
    border-radius: 50px;
  }
  .filter_search_area
  {
    margin-bottom: 25px;
  }
  .shop_list_area ul
  {
    list-style-type: none;
  }
  .shop_list_area ul li
  {
    border-bottom: 1px dashed grey;
    margin-bottom: 20px;
  }
  .color-box .col-4
  {
    padding: 8px;
    text-align: center;
    color: grey;
  }
  .span-color
  {
    background-color: #d3fb00;
    color: black;
    margin-right: 55%;
    padding: 0px 4px 0px 4px;
    font-size: 12px;
    border-radius: 2px;
  }
  a
  {
    color: black;
  }
  .greypara
  {
    color: grey;
    margin-top: 0;
    margin-bottom: 4px;
  }
  .form-check-input {
    position: inherit;
    margin-top: .25rem;
    margin-left: -20px;
  }
</style>
</head>
<body>
  <div class="boxing" id="boxing">
    <div id="loader"></div>
    <!-- Navbar starts from here -->
    <div class="left-nav">
      <div class="top-body">
        <div class="toggleTab">
         <a href="<?php echo base_url('shop/shop/saloonlist'); ?>"><span class="fa fa-angle-left" aria-hidden="true"></span></a>
       </div>
       <span class="brand-name">
        Membership

      </span>

    </div>


  </div>
  <!-- Navbar ends here -->
  <div class="container">
    <div class="">
          <!--
          <div class="col-md-12">
            <div class=" shop-login-signup pull-right" >
              <span>SHOP</span>
              <div class="login-signup-link" >
                <ul>
                  <li><a href="<?php echo base_url("login"); ?>">Login</a></li>
                  <li><a href="<?php echo base_url("signup"); ?>">Signup</a></li>
                </ul>
              </div>
            </div>
          </div>
        -->
        <br>
        <Br>
        <Br>
        <!-- Original Code Body starts from here -->

        <div class="row ">

          <Br>
          <Br>
          <form class="form" method="POST" role="form" action="<?php echo base_url("shop/shop/MembershipData") ?>" autocomplete="off" id="formmembership">
                
             <div class="shop_list_area col-12">
              <?php                   
            foreach ($result as $value) {
             if($value->valid_period== 0 )
             {
               $Period="lifetime";
             }
             else 
             {
               $Period=$value->valid_period." months";
             }
             if($value->prices== 0 )
             {
               $Price="FREE";
             }
             else 
             {
               $Price=$value->prices;
             }

             ?>  
              <ul class="list-group">
                <li class="col-12">
                  <div class="left">
                    
                  </div>
                  <div class="right" >
                    <input type="hidden" name="txtid[<?= $value->id?>]" class="form-control" id="txtid" value="<?= $value->id ?>">
                    <input type="hidden" id="spnprice" name="spnprice[<?= $value->id?>]" class="form-control"  value="<?= $Price ?>">
                    <input type="hidden" id="spnperiod" name="spnperiod[<?= $value->id?>]" class="form-control" value="<?= $Period ?>">
                    <h3><input type="checkbox" name="choose[<?= $value->id?>]" value=1 class="form-check-input" id="" <?= $value->id==1?"checked disabled":""?> required> 
                      <span class="bold"><?= $value->title ?></span> <span class="text-success"></span>
                    </h3>
                    <h5><span class="bold"><?= $Price ?></span>
                      <span class="text-danger"></span> For <?= $Period ?>
                    </h5>
                    <p class="greypara"><?= $value->description ?></p>
                  </div>
                </li>              
              </ul>
          <?php } ?>   
              <p class="greypara">To Pay ₹499 one salon per year</p>
              <div class="form-body">
                <button type="submit" id=""  class="btn btn-text btn-green btn-sz">Activate</button>
              </div>
            </div>
          
        </form>
      </div>


      <!-- BOdy code ends here  -- >

       <!--/col-->

     </div>

     <!--/row-->

   </div>


 </div>
 <!-- container -->

 <?php   $this->load->view('layouts/footer'); ?>
 <script>
   $('.your-checkbox').prop('indeterminate', true)
 </script>

