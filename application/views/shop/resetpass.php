<?php load_view('public_header'); ?>
<style>
	.btn-green {
    background-color: #007bff;
}

/* Thulasi starts */ 
.round
{
    border: 1px solid #C2C2C2 !important;
    border-radius: 50px !important;
    padding-left: 15px;
    height: 40px;
}

.pt-6 {
    padding-top: 100% !important;
}

.body-text {
	font-size: 14px;
	color: #4A4A4A !important;
	font-weight: 300;
}

.btn-text3 {
    font-size: 12px !important; 
    letter-spacing: 4px;
    font-weight: 100 !important;
    padding-left: 40px;
    padding-right: 40px;
}


.form-body .btn {
    border-radius: 100px;
}

@media only screen and (max-width: 420px) {
.pt-6 {
    padding-top: 60% !important;
}
.btn-text3 {
    font-size: 14px !important; 
    letter-spacing: 4px;
    font-weight: 100 !important;
    padding-left: 40px;
    padding-right: 40px;
}


/* Thulasi ends */

</style>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="py-6 mx-auto left-pad">
					<Br>
					<Br>
					<!-- <h1 class="py-5 text-center">Reset Password</h1> -->
					
					<p class="pt-6 text-center" >Reset password</p>
					<!-- <p class="text-center body-text">Enter your registered email address below &amp; we'll send you a<br> link to reset your password</p> -->
					<p class="alert alert-success" style="display: none;" id="alert"></p>
					<div class="form-body py-6 mx-auto">
						<form class="form" action="" method="post" role="form" autocomplete="off" id="formrestpassword">
							<div class="form-group">
								<label class="sr-only" for="email">email</label>
								<input type="email" class="form-control form-control-lg rounded-0 round" name="email" id="email" placeholder="Email" required>
							</div>
							<button type="submit" class="btn btn-text3 btn-green btn-sz">Reset password</button>
						</form>
						<div class="form-url">
							
						</div>
					</div>
				</div>
				<!--/col-->
			</div>
			<!--/row-->
		
	<!--/row-->
</div>
<!--/container-->
<?php $this->load->view('layouts/footer'); ?>

<script type="text/javascript">
	$('#formrestpassword').validate({
		rules:{
			email:{
				required: true,
				email: true
			},
		},
		  
       messages: { 
email:{ 
required:'Please enter a valid email id ' 
}
},
		submitHandler: function() { 

			var formData=$("#formrestpassword").serialize();
			var email=$("#email").val();
			resetPass(formData,email);
		}

		
	});


	function resetPass(formData,email) {

		$.ajax({
			url: "<?php echo base_url('resetpass'); ?>",
			type: "POST",
			data: formData,
			dataType: 'json',
			beforeSend: function () {
				$('#loader').addClass('loader');
			},
			complete: function () {
				$('#loader').removeClass('loader');
			},
			success: function (responseText) {
				console.log(responseText.responseText);
			   $('#alert').text(responseText.responseText);
			    $("#alert").show();
			   $('#email').val(''); 
			},
			error: function (responseText) {
				console.log(responseText.responseText);
				 $('#alert').text(responseText.responseText);
				 $("#alert").show();
				 $('#email').val(''); 
			}
		});
	}

</script>
