<?php load_view('layouts/header'); ?>
</div>
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="row">
        <div class="py-6 mx-auto">
                    
                    <div class="form-body py-6 mx-auto">
                        <h1 class="py-5 text-center miss-you">YOUR CUSTOMERS WILL MISS YOU</h1>
                        <div class="sad mx-auto"><strong>:</strong><span class="sad-fa"> &#40;</span></div>
                        <form class="form " role="form" autocomplete="off" id="formLogin">
                            
                            <div class="form-group form-group3">
                                <div class="lr terms">
                                    <p class="text-center">Talk to us. We can sort things out. Don’t delete your account. All your customer rating and data will be lost.</p>
                                </div>
                            </div>
                            <button type="button"  onClick="window.location='<?php echo base_url('dashboard'); ?>'" class="btn btn-text2 btn-green btn-sz">I DONT WANT TO DELETE MY ACCOUN</button>
                            <button type="button" onClick="window.location='<?php echo base_url('deleteaccount'); ?>'" class="btn btn-text2 mt-2 btn-red btn-sz">DELETE MY ACCOUNT</button>
                        </form>
                    </div>
                </div>
                <!--/col-->
            </div>
            <!--/row-->
       
    <!--/row-->
</div>
<!--/container-->
<?php $this->load->view('layouts/footer'); ?>
