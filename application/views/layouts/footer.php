<!-- alert message popup -->

<div class="modal fade" id="notice_modal" role="dialog" data-backdrop="static" data-keyboard="false">

	<div class="modal-dialog modal-sm">

		<div class="modal-content">

			<div class="modal-header">

				<h4 class="modal-title" style="text-align: center;">Notice</h4>

			</div>

			<div class="modal-body">

				<div class="row">

					<div class="col-md-12 col-sm-12 col-xs-12">

						<h5 style="  line-height: 22px; text-align: center; font-size: 13px;">Thanks for visiting UpperCut. We are launching our operations soon, so please note that some service is inactive currently.</h5>

					</div>

				</div>

				<p><a href='<?php echo base_url(); ?>' class="btn btn-main btn-block green" id="continue_site">Continue to browse</a></p>

			</div>

		</div>

	</div>

</div>

<!-- end of alert popup -->



<!-- Bootstrap core JavaScript -->

<script src="<?php  echo base_url('assets/vendor/jquery/jquery.min.js'); ?>"></script>

<script src="<?php  echo base_url('assets/vendor/popper/popper.min.js'); ?>"></script>

<script src="<?php  echo base_url('assets/vendor/bootstrap/js/bootstrap.min.js'); ?>"></script>

<!-- Plugin JavaScript -->

<script src="<?php  echo base_url('assets/vendor/jquery-easing/jquery.easing.min.js'); ?>"></script>

<script src="<?php  echo base_url('assets/vendor/scrollreveal/scrollreveal.min.js'); ?>"></script>

<script src="<?php  echo base_url('assets/vendor/magnific-popup/jquery.magnific-popup.min.js'); ?>"></script>

<!-- Custom scripts for this template -->

<script type="text/javascript" src="<?php echo base_url('assets/admin/js/jquery-validation/dist/jquery.validate.js'); ?>"></script>

<script type="text/javascript" src="<?php echo base_url('assets/admin/js/validator.js'); ?>"></script>

<script type="text/javascript" src="<?php echo base_url('assets/admin/js/bootbox.min.js'); ?>"></script>

<script src="<?= base_url('assets/js/scroll.js'); ?>"></script>
<script>

  $(document).ready(function() {

    $('.toggleTab').on('click', function () {

      if($('.toggled-nav').hasClass('hide-nav')) {

        $('.toggled-nav').removeClass('hide-nav');

        $('.tog-icon').addClass('active');
        $('.left-nav').removeClass('height10');
        $(".left-nav").addClass('height100');

      } else {

        $('.toggled-nav').addClass('hide-nav'); 

        $('.tog-icon').removeClass('active');
        $('.left-nav').removeClass('height100');
        $(".left-nav").addClass('height10');


      }

    });


  });
  

</script>
<script >
  $(document).ready(function(){
   if (window.matchMedia('(max-width: 767px)').matches) {

   } else {
     $("#boxing").mCustomScrollbar({theme:"minimal",scrollButtons:{enable:true},scrollInertia:500});
      $(".toggled-nav ul").mCustomScrollbar({theme:"minimal",scrollButtons:{enable:true},scrollInertia:500});
   }

 });

</script>
<!--
<script>
  $(window).on('load',function() {
        var viewportWidth = $(window).width();
        if (viewportWidth < 900) {
            $("#oper").addClass("hide-nav");
            $('.left-nav').addClass('height10');
        }
     });

   $(window).on('resize',function() {
        var viewportW = $(window).width();
        if (viewport < 900) {
            $("#oper").removeClass("hide-nav");
        }
    });
</script>
-->

</div>
<?php if(get_session('type')=='owner'):?>
<script src="<?php echo base_url('assets/js/notification.js'); ?>"></script>
 <?php endif; ?>
</body>

</html>
