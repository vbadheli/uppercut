<!DOCTYPE html>

<html lang="en">

<head>

	<meta charset="UTF-8">
	<meta charset="utf-8">
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
   <meta name="viewport" content="width=device-width, minimum-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" id="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Shop</title>
	
	<link rel='shortcut icon' href='../uploads/favicon.ico' type='image/x-icon' />

	<!-- Bootstrap core CSS -->

	<link href="<?php  echo base_url('assets/vendor/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">

	<!-- Custom fonts for this template -->

	<link href="<?php  echo base_url('assets/vendor/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css">

	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600|Roboto:300" rel="stylesheet">

	<!-- Plugin CSS -->

	<link href="<?php  echo base_url('assets/vendor/magnific-popup/magnific-popup.css'); ?>" rel="stylesheet">

  <!--  Scroll bar for desktop version -->
  <link rel="stylesheet" type="text/css" href="<?php  echo base_url('assets/css/scroll.css'); ?>">
	<!-- Custom styles for this template -->


	<link href="<?php  echo base_url('assets/css/style.css'); ?>" rel="stylesheet">

	<link rel="shortcut icon" href="https://agora-file-storage-prod.s3.amazonaws.com/workplace/attachment/317131287979551419?response-content-disposition=inline%3B%20filename%3D%22favicon.png%22%3B%20filename%2A%3Dutf-8%27%27favicon.png&x-amz-security-token=FQoDYXdzEMP%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaDM7HuQ6%2F4ZVYJBoR2yK3A5tizl0YtJvGCWrbGAftFXCLf4ESwNR4rdg0036xpACNVCEf0z1hyD2NW88OH%2By0rKc0Bgmxk3pkCblb2D96sLaUKbHR5Mb%2BXpiTYInqzoTtRxNK%2BAd%2FcfxsanbWEHKaN1t%2Fps3v%2FrSax3T8xfZ3CigWu0evJZVEDWA19oI%2Bb8NZ7cNgv0lH1b2nCfgwG%2BAMIYmeBnysJX%2FAM4sgsPFnz3MFMSFeVvcJm374Z5rf6HMs9DEqfAnEQylDmprRjFw6JIsqF2XEAuSb8bxQJ3um1Qix6zXYgvNvgAeI1XBDHFMvfzz1r6z6CrIYGXChekn7dPw9PVCRog812Se1XTSgKr3a6pHWZpwUPdHtAYHfC62mmcLQy%2BfuwMWnfE676GlUOXvEPAEnQuXy7K7%2BOSOjcena8GSNvS3jpeCPUvZ4Ij3dHQNUTxqg9NPruBBrGM7Zebvl%2Ff2%2FmhUC%2F7LVfDbp2IFPmvfuBujlyek%2FjE3tGolKjMYVqf8%2BQ40fdKTIaBBvGqHBhDzJ8WKp8ZjvwJfhnM815UE1Xmd5l5FXVl9TaEgUgRtIPlXopITQVedma1hC7EJY8YNjFQUo4cC%2FzgU%3D&AWSAccessKeyId=ASIAJOLVEHYVE24Y2IAA&Expires=1506796613&Signature=wf%2F789LOywzc1a09jcHZBjZKEjw%3D" type="image/x-icon">



	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-100486865-1"></script>

	<script>

		window.dataLayer = window.dataLayer || [];

		function gtag(){dataLayer.push(arguments)};

		gtag('js', new Date());

		gtag('config', 'UA-100486865-1');

	</script>

	<script>

		(function(h,o,t,j,a,r){

			h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};

			h._hjSettings={hjid:649573,hjsv:6};

			a=o.getElementsByTagName('head')[0];

			r=o.createElement('script');r.async=1;

			r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;

			a.appendChild(r);

		})(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');

	</script>
  <?php if(user_logged_in()): ?>
<style >
  .nav ul {
    list-style: none;
    overflow: scroll;
    padding-left: 55px;
    padding-top: 5%;
    width: 100%;
}
.nav ul li {
    padding-bottom: 5px;
}
.nav ul li:nth-child(3) {
    margin-top: 0px;
}
</style>
<?php endif; ?>

</head>

<body class="row" style="margin:0; padding: 0;">

	<div id="loader"></div>

	<!--left nav-->

	<div class="left-nav"  >
    <div class="top-body">
      <div class="toggleTab">
        <div class="tog-icon"></div>
        <div class="tog-icon"></div>
      </div>
		<span class="brand-name">
		<a class="brand-logo" href="<?php echo base_url(); ?>"><img src="<?php echo base_url('assets/img/logo.svg'); ?>"></a>
		</span>
		
    </div>
      <div class="nav black-nav toggled-nav hide-nav">
        <ul>
          <?php if(!user_logged_in()): ?>
            <li class="login-wala" ><a href="<?php echo base_url('login'); ?>">Login</a><span class="spancol">|</span><a href="<?php echo base_url('signup'); ?>">Signup</a></li>
          <?php endif; ?>
          <?php if(user_logged_in()): ?>
            <li>
             <img id="image" style="    height: 50px;" src="<?= base_url(); ?>uploads/image.png">
            </li>
            <li>
              <span>BOOKING</span>
            </li>
            <li class="shop-name"><a href="#">MY BOOKINGs</a></li>
            <Br>
            <li>
              <span>FEEDBACK</span>
            </li>
            <li class="shop-name"><a href="#">RATE A SALON</a></li>
            <Br>
            <li>
              <span>Account</span>
            </li>
            <li class="shop-name"><a href="<?php echo base_url('dashboard'); ?>">PROFILE</a></li>

            <li class="shop-name"><a href="#">NOTIFICATION</a></li>
            <li class="shop-name"><a href="#">SETTING</a></li>
           
            <!-- <li><a href="<?php echo base_url('editprofile'); ?>">EDIT PROFILE</a></li>
            <li><a href="<?php echo base_url('newpass'); ?>">Change password</a></li> -->
            <li><a href="<?php echo base_url('logout'); ?>">Sign out</a></li>
            <!-- <li class="dacc"><a href="<?php echo base_url('missyou'); ?>">Delete Account</a></li> -->
          <?php endif; ?>
          <li class="bottom-3"><a href="<?= base_url(); ?>contact_us" >CONTACT</a><span class="contcolr">|</span>
          <a href="<?= base_url(); ?>about_us" >ABOUT</a><span class="contcolr">|</span>
          <a href="<?= base_url(); ?>terms_condition" >TERMS</a></li>
          </ul>
        </div>
      </div>