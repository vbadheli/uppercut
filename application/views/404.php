
<!DOCTYPE HTML>
<html lang="en-US">
<head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Page Not Found | CyberClinic</title>
    <link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.png'); ?>" type="image/x-icon"  sizes="16x16">
    <style>
      .link, .link:visited {
    cursor: pointer;
    color: rgba(218, 133, 39, 0.96);
    text-decoration: none;
}
      .link:hover{
          text-decoration: underline;
      }
      h1.error {
          font-size: 90px;
          color: rgba(175, 140, 0, 0.96);
          margin: 20px 0px;
      }
@media(min-width: 320px) and (max-width: 767px){
  h1.error {
    font-size: 40px;
    color:  rgba(175, 140, 0, 0.96);
}

}
    </style>
</head>
<body style="margin:50px; font-family:Arial; font-size:16px; color: #5f5f5f; text-align:center;" data-gr-c-s-loaded="true">
  <div style="/* max-height:76px; */max-width:396px;margin: 0px auto;">
    <img style="max-width:100%;max-height:100%;width: 240px;height:auto;margin: 0px auto;" src="<?php echo base_url('assets/img/TAT_web_logo.png'); ?>" alt="logo">

        
  </div>
 <h1 style="" class="error"> Error 404!</h1>
  <p style="margin-top:10px; font-size:24px;">Page Not Found</p>
  <p style="line-height:1.5;">Its unfortunate that we cannot find the page you were
looking for. But certainly we have a way out of this mess.</p>
  <a href="<?php echo base_url(); ?>" class="link">&laquo; Go to homepage</a>
  <span style="position:absolute; right:50px; bottom:50px; font-size:72px;
               color:#CCC;">404</span>
</body>
</html>