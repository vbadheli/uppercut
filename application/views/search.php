 <?php $this->load->view('public_header'); ?>
<style>
   body
    { 
    	   
      /* background: url(<?= base_url(); ?>assets/img/Bg_home_promo.png) no-repeat; */
    }
    .shop_list_area ul
  {
    list-style-type: none;
  }
  .shop_list_area ul li
  {
    border-bottom: 1px dashed grey;
    margin-bottom: 20px;
  }
  .greypara
  {
    color: grey;
    margin-top: 0;
    margin-bottom: 4px;
  }
  
  /* Thulasi CSS starts */
.c-padding {
	padding-left: 10%;
}

.pt-6 {
	padding-top: 110% !important;
}

.grey {
	font-size: 12px;
	color: #878787 !important;
	font-weight: 100;	
}

.shop-name {
	font-size: 14px;
	font-weight: 500;
	margin-bottom: -4px;
	margin-top: 5px;	
}

.text-info {
	font-size: 14px;
	color: #3D8DFB !important;
	line-height: 15px !important;		
}

.text-info-12 {
	font-size: 12px;
	color: #3a3a3a;
	line-height: 5px !important; 
}

.recent {
	padding-left:15%;
}

.font-chg {
	font-family: 'Comfortaa', cursive;
	font-weight: 100 !important;
	font-size: 28px;
	color: black;
}

.rate-1 {
	text-align: right;
	line-height: 15px;
	padding-right: 20px; 
}

.pad-btm-10 {
	margin-bottom: 10px;
}

.search-pad {
	width:100%; 
	padding-right:5%;	
}

.rem-pad {
	padding-left: 0 !important;	
}

.recent {
	padding-left: 0!important;
}

.row {
	margin-left: 0!important;
}

.col-6 {
	max-width: 45%;
	padding-top: 7px;
}



html {
    height: 100%;
    background-image: url('/assets/img/Bg_home_promo.png');
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
}

.form-body input[type=search] {
	border: 1px solid #ccc !important;
	height: 35px;
	font-size: 10px;
}

button.search-btn i.fa {
	font-weight: 100;
	font-size: 16px;
	color: #4C4C4C;	
}

button.search-btn {
	top: 4px;
}


@media only screen and (max-width: 600px) {

.c-padding {
	padding-left: 0;
}

.search-pad {
	width:100%; 
	padding-right:0 !important;	
	margin-right:0;
}

.rem-pad {
	padding-left: 0 !important;
}

.recent {
	padding-left: 0!important;
}

.text-info-12 {
	font-size: 11px;
	color: #3a3a3a;
	line-height: 5px !important; 
}


}


/* Thulasi CSS ends */


  
</style>


      <div class="container">
        <div class="row">
          <?php if(!user_logged_in()): ?>
          
          
        <?php endif; ?>
          <div class="col-md-12">
            <div class="row" style="padding-left:40px;">
              <div class="py-6 mx-auto search-pad" >
                <h1 class="pt-6 pb-1 font-chg"><b class="font-chg">Search <Br> Nearby <Br> Salon</b></h1>
                <p class="grey" style="margin-bottom: 3px;">Bengaluru</P>
                <div class="form-body">
                  <?php echo get_flashdata('message'); ?>
                  <form class="form" role="form" autocomplete="off"  id="formLogin" action="<?php echo base_url('search'); ?>" method="POST" >
                    <div class="form-group search-box <?php echo "hidden"; ?>">
                      <input type="hidden" name="lat" value="" id="lat">
                      <input type="hidden" name="lon" value="" id="lon">                
                      <input type="search" class="form-control form-control-lg search-input" name="search" id="address" placeholder="Search" required="">
                      <button type="submit" class="search-btn searchshop"><i class="fa fa-search"></i></button>
                    </div>
                  </form>
                  <div class="form-url">
                    <!-- <p class=""><span class="map rem-pad"><i class="fa fa-map-marker"></i> <a href="javascript:void(0)" id="currentlocation">Use my current location</a></span></p> -->
                  </div>
                  
          <br>        
          <span class="grey">Services</span>
          <?php
          if(!empty($_COOKIE['lat'])){
           $lat =  $_COOKIE['lat'];
            $lon =  $_COOKIE['lon'];
          }else
          {
            $lat = 0;
            $lon = 0;
          }
           $services = $this->db->get('services')->result(); ?>
           <?php if(!empty($services)){ ?>
           <?php foreach($services as $s){ ?>           
           <a class="text-info-12" href="<?php echo base_url(); ?>search/service/<?= urlencode($s->name); ?>/<?= $lat; ?>/<?= $lon; ?>"><?= $s->name; ?></a> <span style="font-size:9px; opacity:0.5; line-height: -5px; padding:0; margin:0;">|</span>           
            <?php } ?>
            <?php } ?>
            <Br>
            <Br>            
            
                  
                  <?php if(user_logged_in()){ ?>
                  <?php $recent = $this->db->select('user_id,shop_id')->where('user_id',get_session('userid'))->distinct('user_id')->distinct('shop_id')->get('recent_view')->result(); ?>
		  
		  

                  <?php if(!empty($recent)){ ?>
                  <div class="recent shop_list_area" >
                    <span class="greypara grey">Recent search</span>
                    
                    
                    <ul class="list-group">
                      <?php foreach($recent as $k): ?>
                    <?php $shop = $this->db->where('id',$k->shop_id)->get('shop_list')->row(); ?>
                    <?php $idfe = $this->db->where('user_id',$k->user_id)->where('shop_id',$shop->id)->get('feedback')->row(); ?>
                    
                    <?php if(empty($idfe)){ ?>
                    <?php if(!empty($shop)){ ?>
                      <li>
                        <div class="row" style="display: flex !important;">
                          <div class="col-6 pad-btm-10 rem-pad">
                            <a style="color: black" href="<?= base_url(); ?>shop/shop/profile/<?= urlencode($shop->name); ?>">
                              <h5 class="shop-name"><?= $shop->name;?></h5>
                            </a>
                            <span class="locality greypara grey"><?= $shop->locality_name; ?></span>
                          </div>
                          <div class="col-6">
                            <div class="row" style="display: flex !important;">
                              <div class="col-6">
                            <!--  <a href="" style="color: red;" ><i  class="fa fa-cut" aria-hidden="true"></i>
                              <br>Book</a> -->
                            </div>
                            <div class="col-6 rate-1">
                              <a class="text-info" href="<?= base_url(); ?>home/rate/<?= $shop->id; ?>" class="text-danger"><span class="fa fa-star" aria-hidden="true"></span><br>Rate</a>
                            </div>
                            
                          </div>
                        </div>
                        
                      </li>
                      <?php } ?>
                      <?php }else{ ?>
                      <li>
                        <div class="row" style="opacity: 0.3;display: flex !important;">
                          <div class="col-6 pad-btm-10 rem-pad">
                            <a style="color: black" href="<?= base_url(); ?>shop/shop/profile/<?= urlencode($shop->name); ?>">
                              <h5 class="shop-name"><?= $shop->name; ?></h5>
                            </a>
                            <span class="locality greypara grey pad-btm-10"><?= $shop->locality_name; ?></span>
                          </div>
                          <div class="col-6">
                            <div class="row" style="display: flex !important;">
                              <div class="col-6">
                             <!-- <a href="" style="color: red;" ><i  class="fa fa-cut" aria-hidden="true"></i>
                              <br>Book</a> -->
                            </div>
                            <div class="col-6 rate-1">
                              <a class="text-info" href="#" class="text-danger"><span class="fa fa-star" aria-hidden="true"></span><br>Rate</a>
                            </div>
                          </div>
                        </div>
                      </li>
                      <?php } ?>

                    <?php endforeach; ?>
                    </ul>
                  </div>
                  <?php } }  ?>
                </div>
              </div>
              <!--/col-->
            </div>
            <!--/row-->
          </div>
          <!--/col-->
        </div>
        <!--/row-->
      </div>

      <?php $this->load->view('layouts/footer'); ?>
      



      <script type='text/javascript' src='<?php echo base_url('assets/admin/js/backend/main.js'); ?>'></script>
      <script type="text/javascript">
        $("#currentlocation").click(function(){
          if ("geolocation" in navigator){
            navigator.geolocation.getCurrentPosition(function(position){ 
              var lat=position.coords.latitude; 
              var lang=position.coords.longitude;
              $("#lat").val(lat);
              $("#lon").val(lang);
            // console.log(lang);
            // console.log(lat);
            $("#formLogin").submit();
          });
          }else{
            console.log("Browser doesn't support geolocation!");
          }

        });
      </script>

