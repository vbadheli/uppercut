<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo SITE_NAME; ?> - Login</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.png'); ?>" type="image/x-icon" >


	<link rel="stylesheet" href="<?php echo base_url('assets/admin/css/bootstrap.min.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/admin/css/font-awesome.min.css'); ?>">

	<link rel="stylesheet" href="<?php echo base_url('assets/admin/css/constants.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/admin/css/backend/style.css'); ?>">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="hold-transition login-page">
		<div class="login-box">
			<div class="login-logo">
				<a href="<?php echo base_url('admin'); ?>"><img src="<?php echo base_url('assets/img/logo.png'); ?>" alt="<?php echo SITE_NAME; ?>"></a>
			</div>
			<!-- /.login-logo -->
			<div class="login-box-body">
				<?php echo get_flashdata('message'); ?>
				<div id="login-form">
					<p class="login-box-msg">Sign in to start your session</p>
					<form id="login_form" action="<?php echo base_url('admin'); ?>" method="post">
						<div class="form-group has-feedback">
							<input type="text" class="form-control" name="username" placeholder="email" required="required">
							<i class="fa fa-envelope form-control-feedback"></i>
						</div>
						<div class="form-group has-feedback">
							<input type="password" class="form-control" name="password" placeholder="Password" required="required">
							<i class="fa fa-lock form-control-feedback"></i>
						</div>
						<div class="row">
							<!-- <div class="col-md-8">
								<a href="javascript:void(0)" class="forgot_password">Forgot Password?</a>
							</div> -->
								<div class="col-md-12">
								<button type="submit" class="btn btn-primary btn-block btn-flat" name="sign_in" value="Sign In">Sign In</button>
							</div>
						</div>
					</form>
				</div>

				<div id="forgot-form">
					<p class="forgot-box-msg">In case you forgot your password, don't worry. We'll help you to get your account access back. Just enter your email address, we'll send you an email with change password link.</p>
					<form id="forgot_form" action="<?php echo base_url('admin/forgot'); ?>" method="post">
						<div class="form-group has-feedback">
							<input type="email" class="form-control" name="forgot_email" placeholder="Email" required="required">
							<i class="fa fa-envelope form-control-feedback"></i>
						</div>
						<div class="row">
							<div class="col-md-8">
								<a href="#" class="back_to_login">Back To Login</a>
							</div>
							<div class="col-md-4">
								<button type="submit" class="btn btn-primary btn-block btn-flat" name="sign_in" value="forgot">Submit</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<!-- /.login-box-body -->
		</div>
		<!-- /.login-box -->
		<script src="<?php echo base_url('assets/admin/js/jquery-2.1.1.min.js'); ?>"></script>
		<script src="<?php echo base_url('assets/admin/js/validator.js'); ?>"></script>
		
	</body>
	</html>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#login_form').validate({
				rules:{
					username:{
						required: true,
						email: true
					},
					password: {
						required: true
					}
				},
				errorPlacement: function(error, element) {},
				highlight: function(element) {
					$(element).parent('div').addClass('has-error');
				},
				unhighlight: function(element) {
					$(element).parent('div').removeClass('has-error');
				}
			});
			$('#forgot_form').validate({
				rules:{
					forgot_email:{
						required: true,
						email: true
					}
				},
				errorPlacement: function(error, element) {},
				highlight: function(element) {
					$(element).parent('div').addClass('has-error');
				},
				unhighlight: function(element) {
					$(element).parent('div').removeClass('has-error');
				}
			});

			$('#forgot-form').hide();
			
			$('.forgot_password').click(function() {
				$('.alert-danger').hide();
				$('#login-form').hide();
				$('#forgot-form').show();
			});

			$('.back_to_login').click(function() {
				$('#login-form').show();
				$('#forgot-form').hide();
			});
		});
	</script>