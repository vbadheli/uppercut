<!-- Left side column. contains the logo and sidebar -->

<?php

$items = array(

    array(

        'name' => 'Dashboard',

        'link' => base_url('admin/dashboard'),

        'icon-class' => 'fa-dashboard text-red',

        'active_on' => 'Dashboard'

        ),

    array(

        'name' => 'Deactivated users',

        'link' => base_url('admin/Deleteduser'),

        'icon-class' => 'fa-users',

        'active_on' => 'clients',

        ),



    array(

        'name' => 'Shop',

        'link' => '',

        'icon-class' => 'fa-calendar-plus-o',

        'active_on' => 'bookings',

        'subitems' => array(

            array(

             'name' => 'Shop user',

             'link' => base_url('admin/shopuser'),

             'icon-class' => 'fa-circle-o text-success',

             'active_on' => 'index',

             ),

            array(

                'name' => 'Shop List',

                'link' => base_url('shoplist'),

                'icon-class' => 'fa-circle-o text-warning',

                'active_on' => 'disputed_bookings',

                ),
            array(

                'name' => 'Services',

                'link' => base_url('services'),

                'icon-class' => 'fa-circle-o text-warning',

                'active_on' => 'disputed_bookings',

                ),
            array(

                'name' => 'Deleted Accounts',

                'link' => base_url('admin/deleteduser'),

                'icon-class' => 'fa-circle-o text-warning',

                'active_on' => 'deleteduser',

                ),
            array(

                'name' => 'Feedbacks',

                'link' => base_url('admin/feedbacks'),

                'icon-class' => 'fa-circle-o text-warning',

                'active_on' => 'feedback',

                ),
            array(

                'name' => 'Booking List',

                'link' => base_url('admin/Bookinglist'),

                'icon-class' => 'fa-circle-o text-warning',

                'active_on' => 'bookings',

                )

            ),

        ),

    // array(

    //     'name' => 'Settings',

    //     'link' => '',

    //     'icon-class' => 'fa-cog',

    //     'active_on' => 'profile',

    //     'subitems' => array(

    //         array(

    //             'name' => 'Password',

    //             'link' => base_url('admin/settings'),

    //             'icon-class' => 'fa-circle-o',

    //             'active_on' => 'index'

    //             ),           

    //         ),

    //     )

    );



    ?>

    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <section class="sidebar">    

            <ul class="sidebar-menu">

                <?php foreach ($items as $key => $item) : ?>

                    <li class="<?php echo (isset($item['subitems']) ? 'treeview' : '') . ' ' . (($controller == $item['active_on']) ? 'active' : ''); ?>">

                        <a href="<?php echo(isset($item['subitems']) ? '#' : $item['link']); ?>">

                            <i class="fa <?= $item['icon-class']; ?>"></i> <span><?= $item['name']; ?></span>

                            <?php if (isset($item['subitems'])) : ?>

                                <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>

                            <?php endif; ?>

                        </a>

                        <?php if (isset($item['subitems'])) : ?>

                            <ul class="treeview-menu">

                                <?php foreach ($item['subitems'] as $key => $subitems) : ?>

                                    <li class="<?php echo(($method == $subitems['active_on']) ? 'active' : ''); ?>">

                                        <a href="<?= $subitems['link']; ?>" class="<?php echo (isset($subitems['class'])) ? $subitems['class'] : ''; ?>"><i class="fa <?= $subitems['icon-class']; ?>"></i>

                                            <span><?= $subitems['name']; ?></span>

                                            <?php if (isset($subitems['right-side-value'])) : ?>

                                                <span class="pull-right-container">

                                                    <?php foreach ($subitems['right-side-value'] as $key => $side_right_item) : ?>

                                                        <small class="label pull-right <?= $side_right_item['class']; ?>" title="<?= $side_right_item['title']; ?>"> <?= $side_right_item['value']; ?> </small>

                                                    <?php endforeach; ?>

                                                </span>

                                            <?php endif; ?>

                                        </a>

                                    </li>

                                <?php endforeach; ?>

                            </ul>

                        <?php endif; ?>

                    </li>

                <?php endforeach; ?>

            </ul>

        </section>

        <!-- /.sidebar -->

    </aside>



