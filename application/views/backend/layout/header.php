<!DOCTYPE html>

<html>

<head>

	<meta charset="utf-8">
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" id="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<title><?php echo $page_title; ?></title>


	<link rel='shortcut icon' href='../uploads/favicon.ico' type='image/x-icon' />
	<!-- Tell the browser to be responsive to screen width -->

	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

	<link rel="shortcut icon" href="https://agora-file-storage-prod.s3.amazonaws.com/workplace/attachment/317131287979551419?response-content-disposition=inline%3B%20filename%3D%22favicon.png%22%3B%20filename%2A%3Dutf-8%27%27favicon.png&x-amz-security-token=FQoDYXdzEMP%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaDM7HuQ6%2F4ZVYJBoR2yK3A5tizl0YtJvGCWrbGAftFXCLf4ESwNR4rdg0036xpACNVCEf0z1hyD2NW88OH%2By0rKc0Bgmxk3pkCblb2D96sLaUKbHR5Mb%2BXpiTYInqzoTtRxNK%2BAd%2FcfxsanbWEHKaN1t%2Fps3v%2FrSax3T8xfZ3CigWu0evJZVEDWA19oI%2Bb8NZ7cNgv0lH1b2nCfgwG%2BAMIYmeBnysJX%2FAM4sgsPFnz3MFMSFeVvcJm374Z5rf6HMs9DEqfAnEQylDmprRjFw6JIsqF2XEAuSb8bxQJ3um1Qix6zXYgvNvgAeI1XBDHFMvfzz1r6z6CrIYGXChekn7dPw9PVCRog812Se1XTSgKr3a6pHWZpwUPdHtAYHfC62mmcLQy%2BfuwMWnfE676GlUOXvEPAEnQuXy7K7%2BOSOjcena8GSNvS3jpeCPUvZ4Ij3dHQNUTxqg9NPruBBrGM7Zebvl%2Ff2%2FmhUC%2F7LVfDbp2IFPmvfuBujlyek%2FjE3tGolKjMYVqf8%2BQ40fdKTIaBBvGqHBhDzJ8WKp8ZjvwJfhnM815UE1Xmd5l5FXVl9TaEgUgRtIPlXopITQVedma1hC7EJY8YNjFQUo4cC%2FzgU%3D&AWSAccessKeyId=ASIAJOLVEHYVE24Y2IAA&Expires=1506796613&Signature=wf%2F789LOywzc1a09jcHZBjZKEjw%3D

" type="image/x-icon">

	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">



	<?php

	foreach ($inclusions['css'] as $file) {

		echo "<link href='" . base_url($file . '.css') . "' rel='stylesheet' type='text/css' />\n";

	}



	foreach ($inclusions['header_js'] as $js) {

		echo "<script type='text/javascript' src='" . base_url($js . '.js') . "'></script>\n";

	}



	if (isset($inclusions['php_scripts'])) {

		foreach ($inclusions['php_scripts'] as $script) {

			include(FCPATH . $script);

		}

	}

	?>

</head>

<body class="hold-transition skin-blue sidebar-mini">

	<script type="text/javascript">

		window.base_url = '<?php echo base_url(); ?>'

	</script>

	<div class="wrapper">

		<header class="main-header">

			<!-- Logo -->

			<a href="<?php echo base_url('admin'); ?>" class="logo">

				<!-- mini logo for sidebar mini 50x50 pixels -->

				<span class="logo-mini">UC</span>

				<!-- logo for regular state and mobile devices -->

				<span class="logo-lg"><img src="https://agora-file-storage-prod.s3.amazonaws.com/workplace/attachment/351125378616831419?response-content-disposition=inline%3B%20filename%3D%22Uppercut%2520Logo.png%22%3B%20filename%2A%3Dutf-8%27%27Uppercut%2520Logo.png&x-amz-security-token=FQoDYXdzEMP%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaDM7HuQ6%2F4ZVYJBoR2yK3A5tizl0YtJvGCWrbGAftFXCLf4ESwNR4rdg0036xpACNVCEf0z1hyD2NW88OH%2By0rKc0Bgmxk3pkCblb2D96sLaUKbHR5Mb%2BXpiTYInqzoTtRxNK%2BAd%2FcfxsanbWEHKaN1t%2Fps3v%2FrSax3T8xfZ3CigWu0evJZVEDWA19oI%2Bb8NZ7cNgv0lH1b2nCfgwG%2BAMIYmeBnysJX%2FAM4sgsPFnz3MFMSFeVvcJm374Z5rf6HMs9DEqfAnEQylDmprRjFw6JIsqF2XEAuSb8bxQJ3um1Qix6zXYgvNvgAeI1XBDHFMvfzz1r6z6CrIYGXChekn7dPw9PVCRog812Se1XTSgKr3a6pHWZpwUPdHtAYHfC62mmcLQy%2BfuwMWnfE676GlUOXvEPAEnQuXy7K7%2BOSOjcena8GSNvS3jpeCPUvZ4Ij3dHQNUTxqg9NPruBBrGM7Zebvl%2Ff2%2FmhUC%2F7LVfDbp2IFPmvfuBujlyek%2FjE3tGolKjMYVqf8%2BQ40fdKTIaBBvGqHBhDzJ8WKp8ZjvwJfhnM815UE1Xmd5l5FXVl9TaEgUgRtIPlXopITQVedma1hC7EJY8YNjFQUo4cC%2FzgU%3D&AWSAccessKeyId=ASIAJOLVEHYVE24Y2IAA&Expires=1506796946&Signature=vzpwJSqhpH0LtaxXhBOH8%2FjuWdc%3D" alt="UPPERCUT"></span>

			</a>

			<!-- Header Navbar: style can be found in header.less -->

			<nav class="navbar navbar-static-top">

				<!-- Sidebar toggle button-->

				<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">

					<span class="sr-only">Toggle navigation</span>

				</a>



				<div class="navbar-custom-menu">

					<ul class="nav navbar-nav">

						<li class="dropdown user user-menu">

							<a href="<?php echo base_url('admin/logout') ?>" class="btn btn-flat" >

								<!-- <img src="<?php echo profile_image(get_session('image')); ?>" class="user-image" alt="User Image"> -->

								<span class="hidden-xs"><!-- <?php echo get_session('admin_fullname'); ?> -->Logout</span>

							</a>

						<!-- <ul class="dropdown-menu">

						<!-- User image -->

							<!--<li class="user-header">

								<img src="<?php echo profile_image(get_session('image')); ?>" class="img-circle" alt="User Image">

								<p class="marB0"><?php echo get_session('admin_fullname'); ?>

									<small>Administrator</small>

								</p>

							</li>

							<li class="user-footer">

								<div class="pull-left">

									<a href="<?php echo base_url('admin/settings'); ?>" class="btn btn-primary btn-flat">Profile Settings</a>

								</div>

								<div class="pull-right">

									<a href="<?php echo base_url('admin/logout') ?>" class="btn btn-danger btn-flat">Logout</a>

								</div>

							</li>

						</ul> -->

					</li>

				</ul>

			</div>

		</nav>

	</header>

	<div class="modal ajax"><!-- Place at bottom of page --></div>

