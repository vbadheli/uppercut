<div class="content-wrapper">
    <section class="content-header">
        <a href="javascript:void(0)" data-toggle="modal" data-target="#adduser" class="pull-right btn btn-success"><i class="fa fa-plus"></i> Add new user</a>

        <div class="row">
            <div class="col-md-4 col-sm-12">
                <h3><?php echo $page_title; ?></h3>
            </div>
        </div>
    </section>

    <section class="content">
        <?php echo get_flashdata('message'); ?>

        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div style="padding: 0px 15px;">
                    </div>
                    <div class="box-body dataTables_wrapper form-inline dt-bootstrap table-responsive">
                        <table class="table table-hover table-bordered" id="dataTable">
                            <thead>
                                <tr>
                                    <th>S.No.</th>
                                    <th>Email</th>
                                    <th>Mobile No.</th>
                                    <th>Username</th>
                                    <th>Created at</th>
                                    <th>Status</th>
                                    <th>Activation code</th>
                                    <th>Type</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                   <!-- <th>Disable</th> -->

                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if(isset($result)): 
                                    foreach ($result as $key => $value):  ?>
                                <tr>
                                    <td><?php echo $key+1; ?></td>
                                    <td><?php echo $value->email; ?></td>
                                    <td><?php echo $value->phone; ?></td>
                                    <td><?php echo $value->username; ?></td>
                                    <td><?php echo $value->created_at; ?></td>
                                    <td><?php echo $value->verified_status; ?></td>
                                    <td><?php echo $value->activation_code; ?></td>
                                    <td><?php echo $value->type; ?></td>
                                     <td><a href="javascript:void(0)" onclick="edit_book(<?php echo $value->id; ?>)"  data-id="<?php echo custom_encode($value->id); ?>" class="btn btn-warning btn-xs delete_user">Edit</a></td>
                                    <td>
                                        <a href="<?php echo base_url('admin/shopuser/deleteuser')."/".custom_encode($value->id); ?> " onclick="return confirm('Are you sure you want to delete?');"  data-id="<?php echo custom_encode($value->id); ?>" class="btn btn-danger btn-xs delete_user"><i class="fa fa-trash"></i></a>
                                    </td>
<!--                                    <?php if($value->verified_status == 0){ ?>
                                    <td><a href="<?php echo base_url('admin/shopuser/approve')."/".custom_encode($value->id); ?> " onclick="return confirm('Are you sure you want to Enable?');"  data-id="<?php echo custom_encode($value->id); ?>" class="btn btn-success btn-xs delete_user">Enable</a></td>
                                    <?php }else{ ?>
                                    <td><a href="<?php echo base_url('admin/shopuser/disable')."/".custom_encode($value->id); ?> " onclick="return confirm('Are you sure you want to Disable?');"  data-id="<?php echo custom_encode($value->id); ?>" class="btn btn-danger btn-xs delete_user">Disable</a></td>
                                    <?php } ?> 
-->                                    
                                    
                                </tr>
                            <?php  endforeach; endif; ?>
                        </tbody>
                    </table>
                </div>
            </div><!-- .box -->
        </div><!-- .col-md-12 -->
    </div><!-- .row -->
    <!-- modal start -->
    <div id="adduser" class="modal fade" role="dialog">
     <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-light-blue">Add new shop</h4>
            </div>
            <form role="form" method="post" action="<?php echo base_url('admin/shopuser/adduser'); ?>" enctype="multipart/form-data" id="formSignup">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">User Name</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Enter clinic name" required>
                        <input type="hidden" name="userid" id="userid">
                    </div>
                    <div class="form-group">
                        <label for="mobile">Contact Number</label>
                        <input type="text" class="form-control" id="mobile" name="mobileno" placeholder="(999) 999-9999"  data-inputmask='"mask": "(999) 999-9999"' data-mask required="required">
                    </div>
                    <div class="form-group">
                        <label for="state" class="">Password</label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="Password" value="" required="">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</form>
</div>

    <!-- modal end -->
    <!-- Edit modal start -->
    <div id="edituser" class="modal fade" role="dialog">
     <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-light-blue">Edit shop</h4>
            </div>
            <form role="form" method="post" action="<?php echo base_url('admin/shopuser/edituser'); ?>" enctype="multipart/form-data" id="form">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">User Name</label>
                        <input type="email"  class="form-control" id="user4" name="username" placeholder="Enter User name" required>
                        
                    </div>
                    <div class="form-group">
                        <label for="name">Email</label>
                        <input type="email"  class="form-control" id="email1" name="email" placeholder="Enter email" required>
                        
                    </div>
                    <div class="form-group">
                        <label for="mobile">Contact Number</label>
                        <input type="text" class="form-control" id="phone2" name="phone" placeholder="(999) 999-9999"   data-inputmask='"mask": "(999) 999-9999"' data-mask required="required">
                    </div>
                    
                    <div class="form-group">
                        <label for="verified_status">Status</label>
                        <input type="number"  class="form-control" id="verified_status" name="verified_status" placeholder="Change status" required>                        
                    </div>
                    
                    
                    
                    
                    <input type="text" name="id" id="id" value="" class="hidden">
                    <div class="form-group">
                        <label for="state" class="">Password</label>
                        <input type="password" class="form-control" id="pass3" name="password" placeholder="Password" value="" required="">
                        <a id="change" class="btn btn-success" onclick="changepass(<?php echo $value->id; ?>)"   href="javascript:void(0)">Change Password</a>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- modal end -->
</section>
</div>

<script type="text/javascript">


window.datatable = {};
var ajax = base_url + 'admin/shopuser/datalist';

$("#dataTable").dataTable();

</script>
<script >
     function edit_book(id)
    {
      
      $('#form')[0].reset(); // reset form on modals
 
      //Ajax Load data from ajax
      $.ajax({
        url : "<?php echo site_url('admin/shopuser/ajax_edit')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('#id').val(data.id);
            
            $('#email1').val(data.email);
            $('#user4').val(data.username);
            $('#phone2').val(data.phone);
            $('#pass3').val(data.password);
           
 
            $('#edituser').removeClass('fade');
            $('#edituser').modal('show'); // show bootstrap modal when complete loaded
            
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }
</script>
<script >
     function changepass(id)
    {
    var id = $('#id').val();
      var pp = $('#pass3').val();
     
      //Ajax Load data from ajax
      $.ajax({
        url : "<?php echo site_url('admin/shopuser/changepassword')?>/"+id+"/"+pp,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
           if(data == "TRUE")
           {
            alert('Password Changed');
           }else
           {
            alert('Fail to Change Password');
           }
            
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }
</script>

