<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1><?php echo $page_title; ?></h1>
        <?php echo get_flashdata('message'); ?>
        <!--
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Examples</a></li>
        <li class="active">Blank page</li>
      </ol>
        -->

    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-4">
                <div class="box box-widget widget-user-2">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-aqua">
                        <div class="widget-user-image">
                            <img class="img-circle" src="<?php echo profile_image($booking_info['user']['profile_pic']); ?>" alt="User Avatar">
                        </div>
                        <!-- /.widget-user-image -->
                        <h3 class="widget-user-username"><?php echo ucfirst($booking_info['name']); ?></h3>
                        <h5 class="widget-user-desc">Client</h5>
                    </div>
                    <div class="box-footer no-padding">
                        <ul class="nav nav-stacked">
                            <li><a href="#">Gender <span class="pull-right text-black"><?php echo ($booking_info['gender'] == 'MALE' || $booking_info['gender'] == 'Male')?'Male':'Female'; ?></span></a></li>
                            <li><a href="#">Email <span class="pull-right text-black"><?php echo $booking_info['email']; ?></span></a></li>
                            <li><a href="#">Contact Number<span class="pull-right text-black"><?php echo $booking_info['countrycode'].'-'.$booking_info['mobile']; ?></span></a></li>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- Widget: user widget style 1 -->
                <div class="box box-widget widget-user-2">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-green">
                        <div class="widget-user-image">
                            <img class="img-circle" src="<?php echo profile_image($booking_info['consultant']['profile_pic']); ?>" alt="User Avatar">
                        </div>
                        <!-- /.widget-user-image -->
                        <h3 class="widget-user-username"><?php echo $booking_info['consultant']['firstname'].' '.$booking_info['consultant']['lastname']; ?></h3>
                        <h5 class="widget-user-desc">Astrologer</h5>
                    </div>
                    <div class="box-footer no-padding">
                        <ul class="nav nav-stacked">
                            <li><a href="#">Email <span class="pull-right text-black"><?php echo $booking_info['consultant']['email']; ?></span></a></li>
                            <li><a href="#">Contact Number<span class="pull-right text-black"><?php echo $booking_info['consultant']['countrycode'].'-'.$booking_info['consultant']['mobileno']; ?></span></a></li>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- /.widget-user -->
            </div>
            <!-- /.col -->
            <div class="col-md-8">
                <div class="box box-success">
                    <div class="box-body">
                        <form class="form-horizontal" id="updateBooking" method="post" action="<?php echo base_url('admin/questions/update'); ?>">
                            <div class="form-group">
                                <label for="appointment_id" class="control-label col-md-3">Question ID</label>
                                <div class="col-md-9">
                                    <input type="number" class="form-control" disabled id="appointment_id" name="appointment_id" value="<?php echo $booking_info['id']; ?>">
                                    <input type="hidden" name="id" value="<?php echo $booking_info['id']; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="appointment_id" class="control-label col-md-3">Booking Date</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" disabled id="bookingDate" name="bookingDate" value="<?php echo date('d-m-Y, h:i A', $booking_info['creation_time']/1000); ?>">
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="control-label col-sm-3" for="email">Total Payble Amount : </label>
                                <div class="col-sm-9">
                                    <input type="number" class="form-control" name="total_payble_amount" id="total_payble_amount" placeholder="Total Payble Amount" required="" aria-required="true" value="<?php echo number_format($booking_info['questionPayment']['finalAmount'], 2); ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-3" for="email">Name : </label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="name" id="name" placeholder="Enter Your Name" required="" aria-required="true" value="<?php echo $booking_info['name']; ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3" for="gender">Gender : </label>
                                <div class="col-md-9 col-sm-9 col-xs-12 has-success">
                                    <select class="form-control" name="gender" id="gender" required="" aria-required="true" aria-invalid="false">
                                        <option <?php echo ($booking_info['gender'] == 'Male')?'selected="selected"':''; ?> value="Male">Male</option>
                                        <option <?php echo ($booking_info['gender'] != 'Male')?'selected="selected"':''; ?> value="Female">Female</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="pwd">Mobile Number : </label>
                                <div class="col-md-9">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <input type="tel" class="form-control" name="countrycode" id="countrycode" placeholder="+91" required="" aria-required="true" value="<?php echo $booking_info['countrycode']; ?>">
                                        </div>
                                        <div class="col-md-9">
                                            <input type="number" maxlength="10" class="form-control" name="mobile" id="mobile" placeholder="8802855152" required="required" aria-required="true" value="<?php echo $booking_info['mobile']; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="email">Email : </label>
                                <div class="col-sm-9">
                                    <input readonly type="email" class="form-control" name="email" id="email" placeholder="Enter Your Email" required="" aria-required="true" value="<?php echo $booking_info['email']; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="problemarea">Problem Area : </label>
                                <div class="col-sm-9 has-success">
                                    <select name="problemarea" id="problemarea" class="form-control" style="width: 100%;" required="" aria-required="true" aria-invalid="false">
                                        <option value="">Select</option>
                                        <option <?php echo ($booking_info['problemarea'] == 'Career and Business')?'selected="selected"':''; ?> value="Career and Business">Career and Business</option>
                                        <option <?php echo ($booking_info['problemarea'] == 'Marriage Obstacles')?'selected="selected"':''; ?> value="Marriage Obstacles">Marriage Obstacles</option>
                                        <option <?php echo ($booking_info['problemarea'] == 'Love and Relationship')?'selected="selected"':''; ?> value="Love and Relationship">love and relationship</option>
                                        <option <?php echo ($booking_info['problemarea'] == 'Wealth and Property')?'selected="selected"':''; ?> value="Wealth and Property">Wealth and Property</option>
                                        <option <?php echo ($booking_info['problemarea'] == 'Horoscope Matching')?'selected="selected"':''; ?> value="Horoscope Matching">Horoscope Matching</option>
                                        <option <?php echo ($booking_info['problemarea'] == 'legal Matters')?'selected="selected"':''; ?> value="legal Matters">legal Matters</option>
                                    </select>
                                </div>
                            </div>
                            <?php
                            $dobtimestamp = strtotime($booking_info['dob']);
                            $dob = date('d-m-Y', $dobtimestamp);
                            $time = date('h:i A', $dobtimestamp);
                            if($booking_info['partnerdob'] != ''){
                                $dobtimestampPart = strtotime($booking_info['partnerdob']);
                                $dobtimestampPartb = strtotime($booking_info['partnertimeofbirth']);

                                $dobPart = date('d-m-Y', $dobtimestampPart);
                                $timePart = date('h:i A', $dobtimestampPartb);
                            }else{
                                $dobPart = '';
                                $timePart = '';
                            }
                            ?>
                            <div class="form-group date">
                                <label class="control-label col-md-3 col-sm-3">Date of Birth : </label>
                                <div class=" col-md-9 col-sm-9  col-xs-12">
                                    <div class="input-group date has-success">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control" id="dob" name="dob" placeholder="dd/mm/yyyy" required="" readonly="" aria-required="true" aria-invalid="false" value="<?php echo $dob; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="email">Time of birth : </label>
                                <div class="col-sm-9">
                                    <div class="input-group time has-success">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control" id="dobtime" name="dobtime" placeholder="12:00 AM" required="" readonly="" aria-required="true" aria-invalid="false" value="<?php echo $time; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="email">Place of birth :  </label>
                                <div class="col-sm-9">
                                    <input type="hidden" name="lat" value="<?php echo $booking_info['lat']; ?>">
                                    <input type="hidden" name="lon" value="<?php echo $booking_info['lon']; ?>">
                                    <input type="text" class="form-control" id="address" name="address" placeholder="Place of birth" required="" aria-required="true" autocomplete="off" value="<?php echo $booking_info['placeOfBirth']; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="email">Question :  </label>
                                <div class="col-sm-9">
                                    <textarea name="comment" id="comment" cols="30" rows="5" class="form-control" placeholder="Comment..."><?php echo $booking_info['question']; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="email">Partner Name : </label>
                                <div class="col-sm-9">
                                    <input type="text" name="partner_name" id="partner_name" placeholder="Partner Name" class="form-control" value="<?php echo $booking_info['partnername']; ?>">
                                </div>
                            </div>
                            <div class="form-group date">
                                <label class="control-label col-md-3 col-sm-3">Partner Date of Birth : </label>
                                <div class=" col-md-9 col-sm-9  col-xs-12">
                                    <div class="input-group date has-success">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control" id="Partnerdob" name="Partnerdob" placeholder="DD-MM-YYYY" required="" readonly="" aria-required="true" aria-invalid="false" value="<?php echo $dobPart; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="email">Partner Time of birth : </label>
                                <div class="col-sm-9">
                                    <div class="input-group time has-success">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control" id="Partnerdobtime" name="Partnerdobtime" placeholder="HH:MM AM/PM" required="" readonly="" aria-required="true" aria-invalid="false" value="<?php echo $timePart; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="email">Partner Place of birth :  </label>
                                <div class="col-sm-9">
                                    <textarea placeholder="Partner Place of birth" name="PartnerLocation" id="PartnerLocation" cols="30" rows="2" class="form-control"><?php echo $booking_info['partnerplaceofbirth']; ?></textarea>
                                </div>
                            </div>
                            <!--<div class="form-group">
                                <label class="control-label col-sm-3" for="friend"></label>
                                <div class="col-sm-5">
                                    <label class="control1 control--checkbox"> <input type="checkbox" id="friend" name="friend" <?php /*echo ($booking_info['bookForFriend'] != '')?'checked="checked"':''; */?> > Book for friend/Gift a friend </label>
                                </div>
                                <div class="col-md-4">
                                    <label class="control1 control--checkbox"> <input type="checkbox" id="videoCall" name="video" <?php /*echo ($booking_info['isTelephonicCall'] != 1)?'checked="checked"':''; */?>> Is Video Call</label>
                                </div>
                            </div>-->

                            <div class="form-group">
                                <div class="col-sm-9 col-md-offset-3">
                                    <input type="hidden" name="user_id" value="<?php echo $booking_info['userId']; ?>">
                                    <input type="hidden" name="timezoneid" value="<?php echo $booking_info['timezoneid']; ?>">
                                    <button type="submit" class="btn btn-primary pull-right">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!--<div class="box-footer">
                        <button class="btn btn-danger pull-right ">Reject</button>
                        <button class="btn btn-success pull-right margin-r-5">Accept</button>
                    </div>-->
                </div>
                <!-- /.nav-tabs-custom -->
            </div>
            <!-- /.col -->
        </div>

        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- Modal -->

<script type="text/javascript">
    $('body').on('focus', '.input-group.datetime .form-control', function (event) {
        $(this).datetimepicker({
            format: "DD-MM-YYYY, hh:mm A",
            inline: true,
            sideBySide: true,
        });
    });

    $('body').on('click', '.input-group.datetime .input-group-addon', function () {
        $(this).closest('.input-group.date').find('.form-control').trigger('focus');
    });
    $('body').on('focus', '.input-group.time .form-control', function (event) {
        $(this).datetimepicker({
            format: "h:m A",
            pickTime: true,
            pickDate: false
        });
    });

    $('body').on('click', '.input-group.time .input-group-addon', function () {
        $(this).closest('.input-group.date').find('.form-control').trigger('focus');
    });
</script>
