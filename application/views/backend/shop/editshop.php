
<div class="content-wrapper">
<link href="<?php  echo base_url('assets/admin/datepicker/datetimepicker.min.css'); ?>" rel="stylesheet" type="text/css">
<script src="<?php  echo base_url('assets/admin/datepicker/moment.min.js'); ?>"></script>
<script src="<?php  echo base_url('assets/admin/datepicker/datetimepicker.min.js'); ?>"></script>


    <section class="content-header">
        <Br>
        <br>
        <div class="row">
            <div class="col-md-4 col-sm-12">
                <h3><?php echo $page_title; ?></h3>
            </div>
        </div>
    </section>
    <section class="content">
<script type='text/javascript' src='<?= base_url(); ?>assets/admin/js/jquery-3.1.1.min.js'></script>


<link href="<?php  echo base_url('assets/admin/datepicker/datetimepicker.min.css'); ?>" rel="stylesheet" type="text/css">

<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<style type="text/css">
.radio-inline,.checkbox-inline
{
	margin-left: 10px;
	margin-right: 10px;
}

   @media only screen and (max-width: 900px) {
  .px-6 {
    padding-left: 10px;
    padding-right: 10px;
    padding-top: 83px;
}

.ml-6 {
    margin-left: 0px;
}
.coll {
    display: inline-block;
    width: 100%;
}
.editform {
    margin-left: 0px;
}
}

</style>
<?php $num = $this->uri->segment(4); ?>

<?php $id = $this->db->where('id',$num)->get('shop_list')->row();  ?>
<?php $services = $this->db->get('services')->result(); ?>


<div class="container-fluid">
<form class="form" role="form" autocomplete="off"  method="post" id="addform" action="<?php echo base_url('admin/shopuser/edit_profile'); ?>" enctype="multipart/form-data" >
  <div class="row">

    <div class="col-md-8">

      <div class="ml-6 px-6">

   
          

<div class="container">

	<div class="row">

		<div class="col-md-8">

			<div class="row">

				<div class="editform">

								<div class="editform">
									

									<div class="form-body">

						<?php if( $feedback = $this->session->flashdata('feedback')): 

					           $feedback_class = $this->session->flashdata('feedback_class'); ?>
					                <div class="alert dark alert-<?= $feedback_class ?> alert-dismissible" role="alert">
					                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
					                    <span aria-hidden="true">&times;</span>
					                  </button>
					                 <?= $feedback ?>
					                </div>

					              <?php endif; ?>
					              <?php echo validation_errors(); ?>
					              
					              <?php //if(count($id)){ ?>
					              <?php if($id){ ?>


					              			<div class="form-group">

												<label class="sr-only" for="shopname">Type</label>

												<input type="text" class="form-control form-control-lg rounded-0" id="type" value="<?= $id->type; ?>" name="type" placeholder="Shop name" required >

											</div>
											<div class="form-group">

												<label class="sr-only" for="shopname">Shop name</label>

												<input type="text" class="form-control form-control-lg rounded-0" id="shopname" value="<?= $id->name; ?>" name="name" placeholder="Shop name" required >

											</div>
											<input type="text" name="id" value="<?= $id->id; ?>" class="hidden">
											<div class="form-group">

												<label class="sr-only" for="website">Website</label>

												<input type="text" class="form-control form-control-lg rounded-0" id="website" value="<?= $id->website; ?>" name="website" placeholder="Enter website" required >

											</div>

											<div class="form-group">

												<label class="sr-only" for="contact">Contact Number</label>

												<input type="number" class="form-control form-control-lg rounded-0" id="contact" value="<?= $id->mobileno; ?>" name="mobileno" placeholder="Enter contact" required >

											</div>	



											<div class="form-group">

												<label class="sr-only" for="location">Address</label>

												<input type="text" class="form-control" id="street" name="user_address" placeholder="street address" value="<?= $id->user_address; ?>" value="">

											</div>

											<div class="form-group">

												<label  class="sr-only" for="country" class="">Country</label>

												<input type="text" value="<?= $id->country; ?>" class="form-control" id="country" name="country" placeholder="country" value="">

											</div>

											<div class="form-group">

												<label class="sr-only"   for="city" class="">City</label>

												<input type="text" value="<?= $id->city; ?>" class="form-control" id="city" name="city" placeholder="city" value="">

											</div>

											<div class="form-group">

												<label class="sr-only" for="state" class="">State</label>

												<input type="text" value="<?= $id->state; ?>" class="form-control" id="state" name="state" placeholder="state" value="">

											</div>



											<div class="form-group">

												<label class="sr-only" for="postalcode" class="">Post Code</label>

												<input type="text" maxlength="6" value="<?= $id->postalcode; ?>" class="form-control" id="postalcode" name="postalcode" placeholder="post code" value="" >

											</div>					



											<div class="form-group">

												<label class="sr-only" for="about">About Shop</label>

												<textarea class="form-control" id="about" name="about_me" rows="3" placeholder="Enter company details" required><?= $id->about_me; ?></textarea>

											</div>

											<div class="form-group">

												<label class="" for="about">Service Offered</label>
										   <select name="service_offered[]" class="form-control" id="multiple" multiple="multiple">
											  <?php $arr = json_decode($id->service_offered); ?>
											  <?php foreach ($services as $jey ) { ?>
								
								              <?php if(in_array($jey->name, $arr)){ ?>
											  <option selected="selected"  value="<?= $jey->name; ?>"><?= $jey->name; ?></option>
											  <?php }else{ ?>
											  <option value="<?= $jey->name; ?>"><?= $jey->name; ?></option>
											  <?php  } ?>
											
											   <?php  } ?>
											  <option></option>
											</select>
												

											</div>

										</div>

										<div class="parking"> 



											<span><strong>Parking</strong></span>
											<br>
											<div>
												<?php if($id->parking == "parking"){ ?>
												<label class="radio-inline"><input type="radio" name="parking" checked value="Common Parking">Common Parking</label>
												<?php }else{ ?>
												<label class="radio-inline"><input type="radio" name="parking" value="Common Parking">Common Parking</label>
												<?php } ?>
												<?php if($id->parking =="Bus"){  ?>
												<label class="radio-inline"><input type="radio" name="parking" checked value="Bus">Bus</label>
												<?php }else{ ?>
												<label class="radio-inline"><input type="radio" name="parking" value="Bus">Bus</label>
												<?php  } ?>
												<?php if($id->parking == "Car"){  ?>
												<label class="radio-inline"><input type="radio" name="parking" checked value="Car">Car</label>
												<?php }else{ ?>
												<label class="radio-inline"><input type="radio" name="parking" value="Car">Car</label>
												<?php } ?>
												<?php if($id->parking == "No Parking"){  ?>
												<label class="radio-inline"><input type="radio" name="parking" checked value="No Parking">No Parking</label>
												<?php }else{ ?>
												<label class="radio-inline"><input type="radio" name="parking" value="No Parking">No Parking</label>
												<?php } ?>
											</div>
											



										</div>



										<div class="form-group"> 
											<label>Opening Time</label>
											<input type="text" value="<?= $id->opening_time; ?>" placeholder="9:30 AM" id="opening_time" name="opening_time" class="form-control">
										</div>
										<div class="form-group"> 
											<label>Closing Time</label>
											<input type="text" value="<?= $id->closing_time; ?>" placeholder="12:30 PM" id="closing_time" name="closing_time" class="form-control">
										</div>
										<div class="form-group">
											<label>Working Days</label>
											<Br>
											<?php if(in_array('Mo',json_decode($id->days))){ ?>
											<label class="checkbox-inline"><input  type="checkbox" name="days[]" value="Mo" checked="checked">Monday</label>
											<?php }else{ ?>
											<label class="checkbox-inline"><input  type="checkbox" name="days[]" value="Mo">Monday</label>
											<?php } ?> 
											<?php if(in_array('Tu',json_decode($id->days))){ ?>
											<label class="checkbox-inline"><input type="checkbox" name="days[]" value="Tu" checked="checked">Tuesday</label>
											<?php }else{ ?>
											<label class="checkbox-inline"><input type="checkbox" name="days[]" value="Tu">Tuesday</label>
											<?php } ?>
											<?php if(in_array('We',json_decode($id->days))){ ?>
											<label class="checkbox-inline"><input type="checkbox" name="days[]" value="We" checked="checked">Wednesday</label>
											<?php }else{ ?>
											<label class="checkbox-inline"><input type="checkbox" name="days[]" value="We">Wednesday</label>
											<?php } ?>
											<?php if(in_array('Th',json_decode($id->days))){ ?>
											<label class="checkbox-inline"><input type="checkbox" name="days[]"  value="Th" checked="checked">Thirsday</label>
											<?php }else{ ?>
											<label class="checkbox-inline"><input type="checkbox" name="days[]"  value="Th">Thirsday</label>
											<?php } ?>
											<?php if(in_array('Fr',json_decode($id->days))){ ?>
											<label class="checkbox-inline"><input type="checkbox" name="days[]" value="Fr" checked="checked">Friday</label>
											<?php }else{ ?>
											<label class="checkbox-inline"><input type="checkbox" name="days[]" value="Fr">Friday</label>
											<?php } ?>
											<?php if(in_array('Sa',json_decode($id->days))){ ?>
											<label class="checkbox-inline"><input type="checkbox" name="days[]" value="Sa" checked="checked">Saturday</label>
											<?php }else{ ?>
											<label class="checkbox-inline"><input type="checkbox" name="days[]" value="Sa">Saturday</label>
											<?php } ?>
											<?php if(in_array('Su',json_decode($id->days))){ ?>
											<label class="checkbox-inline"><input type="checkbox" name="days[]" value="Su" checked="checked" >Sunday</label>
											<?php }else{ ?>
											<label class="checkbox-inline"><input type="checkbox" name="days[]" value="Su" >Sunday</label>
											<?php } ?>
										</div>
										

										<div class="Picture"> 

											<p class="help-block" id="file-info">Shop Picture (max 1MB)</p>
											<div class="col-md-12" >
													<?php //if (count($id)) { ?>
													<?php if ($id) { ?>
														<img style="height: 200px;" src="<?= base_url(); ?>uploads/<?= $id->image; ?>">
												<?php 	} ?>
											</div>
										</div>

										<div class="form-group"  id="errordiv">



											<i class="fa fa-paperclip"></i><strong>Select Image</strong>

											<input type="file"  name="picture"  >



										</div>

										

								
									<div class="form-group">
										<label class=" control-label">CHoose Location From Below</label>
										<BR>
										<label>Latitude</label>
										<input type='text' class="form-control" value="<?= $id->lat; ?>" name='lat' id='lat'>  
									    <br>
									    <label>Longitude</label>
									    <input type='text' class="form-control" value="<?= $id->lon; ?>" name='lon' id='lng'> 
									    <Br>
										</div>
										<div class="form-group">
											<div id="googleMap" style="height: 400px;"></div>

									    Select your location


										</div>
									<div class="form-group">
										<input type="submit" name="submit" class="btn btn-lg btn-success">
									</div>
								</form>
								<?php }else{ ?>
									<form class="form" role="form" autocomplete="off"  method="post" id="addform" action="<?php echo base_url('edit_profile'); ?>" enctype="multipart/form-data" >

											<div class="form-group">

												<label class="sr-only" for="shopname">Shop name</label>

												<input type="text" class="form-control form-control-lg rounded-0" id="shopname" name="name" placeholder="Shop name" required >

											</div>

											<div class="form-group">

												<label class="sr-only" for="website">Website</label>

												<input type="text" class="form-control form-control-lg rounded-0" id="website" name="website" placeholder="Enter website" required >

											</div>

											<div class="form-group">

												<label class="sr-only" for="contact">Contact Number</label>

												<input type="number" class="form-control form-control-lg rounded-0" id="contact" name="mobileno" placeholder="Enter contact" required >

											</div>	



											<div class="form-group">

												<label class="sr-only" for="location">Address</label>

												<input type="text" class="form-control" id="street" name="user_address" placeholder="street address" value="">

											</div>

											<div class="form-group">

												<label  class="sr-only" for="country" class="">Country</label>

												<input type="text" class="form-control" id="country" name="country" placeholder="country" value="">

											</div>

											<div class="form-group">

												<label class="sr-only"   for="city" class="">City</label>

												<input type="text" class="form-control" id="city" name="city" placeholder="city" value="">

											</div>

											<div class="form-group">

												<label class="sr-only" for="state" class="">State</label>

												<input type="text" class="form-control" id="state" name="state" placeholder="state" value="">

											</div>



											<div class="form-group">

												<label class="sr-only" for="postalcode" class="">Post Code</label>

												<input class="form-control" id="postalcode" name="postalcode" placeholder="Postal code"  oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
    type = "number"
    maxlength = "6"
												 value="">

											</div>					



											<div class="form-group">

												<label class="sr-only" for="about">About Shop</label>

												<textarea class="form-control" id="about" name="about_me" rows="3" placeholder="Enter company details" required></textarea>

											</div>

											<div class="form-group">

												<label class="" for="about">Service Offered</label>

												  <select name="service_offered[]" class="form-control" id="multiple" multiple="multiple" required>
											  
											  <?php foreach ($services as $jey ) { ?>
											  <option value="<?= $jey->name; ?>"><?= $jey->name; ?></option>
											   <?php  } ?>
											  <option></option>
											</select>

											</div>

										</div>

										<div class="parking"> 



											<span><strong>Parking</strong></span>
											<br>
											<div>
												<label class="radio-inline"><input type="radio" name="parking" value="Common Parking">Common Parking</label>
												<label class="radio-inline"><input type="radio" name="parking" value="Bus">Bus</label>
												<label class="radio-inline"><input type="radio" name="parking" value="Car">Car</label>
												<label class="radio-inline"><input type="radio" name="parking" value="No Parking">No Parking</label>
											</div>
											



										</div>



										<div class="form-group"> 
											<label>Opening Time</label>
											<input type="text" id="opening_time" placeholder="9:30 AM" name="opening_time" class="form-control">
										</div>
										<div class="form-group"> 
											<label>Closing Time</label>
											<input type="text" id="closing_time" placeholder="12:30 PM" name="closing_time" class="form-control">
										</div>
										<div class="form-group">
											<label>Working Days</label>
											<Br>
											<label class="checkbox-inline"><input  type="checkbox" name="days[]" value="Mo">Monday</label>
											<label class="checkbox-inline"><input type="checkbox" name="days[]" value="Tu">Tuesday</label>
											<label class="checkbox-inline"><input type="checkbox" name="days[]" value="We">Wednesday</label>
											<label class="checkbox-inline"><input type="checkbox" name="days[]" value="Th">Thirsday</label>
											<label class="checkbox-inline"><input type="checkbox" name="days[]" value="Fr">Friday</label>
											<label class="checkbox-inline"><input type="checkbox" name="days[]" value="Sa">Saturday</label>
											<label class="checkbox-inline"><input type="checkbox" name="days[]" value="Su">Sunday</label>
										</div>
										

										<div class="Picture"> 

											<p class="help-block" id="file-info">Shop Picture (max 1MB)</p>

										</div>

										<div class="form-group"  id="errordiv">



											<i class="fa fa-paperclip"></i><strong>Select Image</strong>

											<input type="file"  name="picture" required  >



										</div>

										

								
									<div class="form-group">
												<label class="pull-right control-label">CHoose Location From Below</label>

												
										</div>
										<div class="form-group">
											<label>Latitude</label>
											<input type='text' class="form-control"  name='lat' id='lat'>  
									    
										</div>
										<div class="form-group">
											<label>Longitude</label>
											<input type='text' class="form-control"  name='lon' id='lng'> 
										</div>
										<div class="form-group">
											<div id="googleMap" style="height: 400px;"></div>

										</div>
									<div class="form-group">
										<input type="submit" name="submit" class="btn btn-lg btn-success">
									</div>
								</form>
								<?php } ?>



							</div>

						</div>

						<!--/col-->

					</div>

					<!--/row-->

				</div>	

				<!--/col-->

			</div>

			<!--/row-->

		</div>



		<!--/container-->      

	</div>

	<!--/col-->

</div>

<!--/col-->

</div>

<!--/row-->

</div>

<!--/container-->
</div>
</section>
</div>
<script src="<?php  echo base_url('assets/admin/datepicker/moment.min.js'); ?>"></script>
<script src="<?php  echo base_url('assets/admin/datepicker/datetimepicker.min.js'); ?>"></script>
<script >

	$("#opening_time").datetimepicker({
pickDate: false
});
</script>
<script >
	$("#closing_time").datetimepicker({
pickDate: false
});
</script>
<script type='text/javascript' src='<?php echo base_url('assets/admin/js/backend/main.js'); ?>'></script> 

<!-- <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8G1mE0PUuoN79b3iqE1PB27H0IisuCAg&callback=initMap"> -->

</script>

<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8G1mE0PUuoN79b3iqE1PB27H0IisuCAg&callback=myMap"></script> -->

<script src="https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyA8G1mE0PUuoN79b3iqE1PB27H0IisuCAg" type="text/javascript"></script>

<?php //if(count($id)){ ?>
<?php if($id){ ?>
<script >

	function initialize() {
	var latni = "<?= $id->lat; ?>";
	var longni = "<?= $id->lon; ?>";
    var myLatlng = new google.maps.LatLng(latni,longni);
  var mapProp = {
    center:myLatlng,
    zoom:5,
    mapTypeId:google.maps.MapTypeId.ROADMAP
      
  };
  var map=new google.maps.Map(document.getElementById("googleMap"), mapProp);
    var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      title: 'Upper Cut',
      draggable:true  
  });
    document.getElementById('lat').value= latni;
    document.getElementById('lng').value= longni; 
    // marker drag event
    google.maps.event.addListener(marker,'drag',function(event) {
        document.getElementById('lat').value = event.latLng.lat();
        document.getElementById('lng').value = event.latLng.lng();
    });

    //marker drag event end
    google.maps.event.addListener(marker,'dragend',function(event) {
        document.getElementById('lat').value = event.latLng.lat();
        document.getElementById('lng').value = event.latLng.lng();
      
      ReverseGeocode(event.latLng.lat(),event.latLng.lng());
      	document.getElementById('lat').value = event.latLng.lat();
      	document.getElementById('long').value = event.latLng.lng();
      
    });
}
function ReverseGeocode(latitude, longitude){
    var reverseGeocoder = new google.maps.Geocoder();
    var currentPosition = new google.maps.LatLng(latitude, longitude);
    reverseGeocoder.geocode({'latLng': currentPosition}, function(results, status) {
 
            if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                   document.getElementById('locate').value =  results[0].formatted_address;
                    }
            else {
                 document.getElementById('locate').value =  'Unable to detect your address.';
                    }
        } else {
          document.getElementById('locate').value =  'Unable to detect your address.';
        }
    });
}

google.maps.event.addDomListener(window, 'load', initialize);

</script>
<?php }else{ ?>
<script >

	function initialize() {

    var myLatlng = new google.maps.LatLng(23.483399978222792,79.74975546874998);
  var mapProp = {
    center:myLatlng,
    zoom:5,
    mapTypeId:google.maps.MapTypeId.ROADMAP
      
  };
  var map=new google.maps.Map(document.getElementById("googleMap"), mapProp);
    var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      title: 'Upper Cut',
      draggable:true  
  });
    document.getElementById('lat').value= 31.877557017361642
    document.getElementById('lng').value= 73.15795859374998  
    // marker drag event
    google.maps.event.addListener(marker,'drag',function(event) {
        document.getElementById('lat').value = event.latLng.lat();
        document.getElementById('lng').value = event.latLng.lng();
    });

    //marker drag event end
    google.maps.event.addListener(marker,'dragend',function(event) {
        document.getElementById('lat').value = event.latLng.lat();
        document.getElementById('lng').value = event.latLng.lng();
      
      ReverseGeocode(event.latLng.lat(),event.latLng.lng());
      	document.getElementById('lat').value = event.latLng.lat();
      	document.getElementById('long').value = event.latLng.lng();
      
    });
}
function ReverseGeocode(latitude, longitude){
    var reverseGeocoder = new google.maps.Geocoder();
    var currentPosition = new google.maps.LatLng(latitude, longitude);
    reverseGeocoder.geocode({'latLng': currentPosition}, function(results, status) {
 
            if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                   document.getElementById('locate').value =  results[0].formatted_address;
                    }
            else {
                 document.getElementById('locate').value =  'Unable to detect your address.';
                    }
        } else {
          document.getElementById('locate').value =  'Unable to detect your address.';
        }
    });
}

google.maps.event.addDomListener(window, 'load', initialize);

</script>
<?php } ?>
