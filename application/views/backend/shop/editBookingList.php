
<div class="content-wrapper">
<link href="<?php  echo base_url('assets/admin/datepicker/datetimepicker.min.css'); ?>" rel="stylesheet" type="text/css">
<script src="<?php  echo base_url('assets/admin/datepicker/moment.min.js'); ?>"></script>
<script src="<?php  echo base_url('assets/admin/datepicker/datetimepicker.min.js'); ?>"></script>


    <section class="content-header">
        <Br>
        <br>
        <div class="row">
            <div class="col-md-4 col-sm-12">
                <h3><?php echo $page_title; ?></h3>
            </div>
        </div>
    </section>
    <section class="content">
<script type='text/javascript' src='<?= base_url(); ?>assets/admin/js/jquery-3.1.1.min.js'></script>


<link href="<?php  echo base_url('assets/admin/datepicker/datetimepicker.min.css'); ?>" rel="stylesheet" type="text/css">

<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<style type="text/css">
.radio-inline,.checkbox-inline
{
	margin-left: 10px;
	margin-right: 10px;
}

   @media only screen and (max-width: 900px) {
  .px-6 {
    padding-left: 10px;
    padding-right: 10px;
    padding-top: 83px;
}

.ml-6 {
    margin-left: 0px;
}
.coll {
    display: inline-block;
    width: 100%;
}
.editform {
    margin-left: 0px;
}
}

</style>

<?php $num = $this->uri->segment(4); ?>

<?php $id = $this->db->where('id',$num)->get('bookings')->row(); 
//echo "<pre>";print_r($id->id);exit;?>


<div class="container-fluid">
<form class="form" role="form" autocomplete="off"  method="post" id="addform" name="addform" action="<?php echo base_url("admin/shopuser/edit_profile_admin/$id->id"); ?>" enctype="multipart/form-data" >
  <div class="row">

<div class="col-md-8">
<div class="ml-6 px-6">
<div class="container">
	<div class="row">
		<div class="col-md-8">
			<div class="row">
				<div class="editform">
					<!-- if(count($id)){-->
					<?php if(isset($id)){ ?>
								<div class="editform">
									<div class="form-body">
										<?php if( $feedback = $this->session->flashdata('feedback')): 

					           $feedback_class = $this->session->flashdata('feedback_class'); ?>
					                <div class="alert dark alert-<?= $feedback_class ?> alert-dismissible" role="alert">
					                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
					                    <span aria-hidden="true">&times;</span>
					                  </button>
					                 <?= $feedback ?>
					                </div>

					              <?php endif; ?>
					              		<input type='hidden' name='id' id='id' value="<?= $id->id; ?>">
										<div class="form-group"> 
											<label>Userid</label>
											<input type="text" id="userid" name="userid" class="form-control" readonly="" value="<?= $id->userid ?>">
										</div>
										<div class="form-group"> 
											<label>Salonid</label>

											<input type="text" id="salonid" name="salonid" class="form-control" readonly="" value="<?= $id->salonid ?>">
										</div>

										<div class="service-combo-wrap" id='servicecombowrapid'>
											<select class="combo form-control" id='service_type' name='service_type' required="">
												 <option value="">Select a service</option>
                   								<option  value="MensHairCut" <?= $id->service_type =="MensHairCut"? 'selected="selected"':""?>>Men's Hair Cut</option>
                   								<option  value="WomensHairCut" <?= $id->service_type =="WomensHairCut"? 'selected="selected"':""?>>Women's Hair Cut</option>
                   								<option  value="HairCutKids" <?= $id->service_type =="HairCutKids"? 'selected="selected"':""?>>Hair Cut Kids</option>
                   								<option  value="EyebrowThreading" <?= $id->service_type =="EyebrowThreading"? 'selected="selected"':""?>>Eyebrow Threading</option>										
                   							</select>											
										</div><br>
										 <div class="form-group date-wrap" id="datewrapide">
											<input type='date' name='date' id='bookingdate' value="<?= $id->date ?>" value="" class="form-control"> 
											
										</div>
										 <div class="form-group time-wrap" id="etimewrapide">
											<span class="time-label">Time</span><br>
                      						<select class="combo form-control" id='time' name='time' required="">
                							   <option selected="disabled">Select Time</option>
                							   <option value="8 AM" <?= $id->time =="8 AM"? 'selected="selected"':""?>>8 AM</option>                   
                							   <option value="9 AM" <?= $id->time =="9 AM"? 'selected="selected"':""?>>9 AM</option>                   
                							   <option  value="10 AM" <?= $id->time =="10 AM"? 'selected="selected"':""?>>10 AM</option>                   
                							   <option  value="11 AM" <?= $id->time =="11 AM"? 'selected="selected"':""?>>11 AM</option>                   
                							   <option  value="12 AM" <?= $id->time =="12 AM"? 'selected="selected"':""?>>12 AM</option>                   
                							   <option value="1 PM" <?= $id->time =="1 PM"? 'selected="selected"':""?>>1 PM</option>                   
                							   <option value="2 PM" <?= $id->time =="2 PM"? 'selected="selected"':""?>>2 PM</option>                   
                							   <option value="3 PM" <?= $id->time =="3 PM"? 'selected="selected"':""?>>3 PM</option>                   
                							   <option value="4 PM" <?= $id->time =="4 PM"? 'selected="selected"':""?>>4 PM</option>                   
                							   <option value="5 PM" <?= $id->time =="5 PM"? 'selected="selected"':""?>>5 PM</option>                   
                							   <option value="6 PM" <?= $id->time =="6 PM"? 'selected="selected"':""?>>6 PM</option>                   
                							   <option value="7 PM" <?= $id->time =="7 PM"? 'selected="selected"':""?>>7 PM</option>                   
                							   <option value="8 PM" <?= $id->time =="8 PM"? 'selected="selected"':""?>>8 PM</option>                   
                							   <option value="9 PM" <?= $id->time =="9 PM"? 'selected="selected"':""?>>9 PM</option>                   
                							   <option  value="10 PM" <?= $id->time =="10 PM"? 'selected="selected"':""?>>10 PM</option>                   
                							 </select> 
										</div> 	
										<div class="form-group"> 
											<label>Status</label>

											<input type="text" id="status" name="status" class="form-control" value="<?= $id->status ?>">
										</div>
										<div class="form-group"> 
											<label>Remark</label>
											<textarea type="text"cols="30" rows="3" id="remark" name="remark" class="form-control"><?= $id->remark ?></textarea>
										</div>
																			
									<div class="form-group">
										<input type="submit" name="submit" class="btn btn-lg btn-success">
									</div>
									<?php  } ?>	
								</form>
							</div>

						</div>

						<!--/col-->

					</div>

					<!--/row-->

				</div>	

				<!--/col-->

			</div>

			<!--/row-->

		</div>



		<!--/container-->      

	</div>

	<!--/col-->

</div>

<!--/col-->

</div>

<!--/row-->

</div>

<!--/container-->
</div>
</section>
</div>
<script src="<?php  echo base_url('assets/admin/datepicker/moment.min.js'); ?>"></script>
<script src="<?php  echo base_url('assets/admin/datepicker/datetimepicker.min.js'); ?>"></script>
<script >

	$("#opening_time").datetimepicker({
pickDate: false
});
</script>
<script >
	$("#closing_time").datetimepicker({
pickDate: false
});
</script>
<script type='text/javascript' src='<?php echo base_url('assets/admin/js/backend/main.js'); ?>'></script> 

<!-- <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8G1mE0PUuoN79b3iqE1PB27H0IisuCAg&callback=initMap"> -->

</script>

<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8G1mE0PUuoN79b3iqE1PB27H0IisuCAg&callback=myMap"></script> -->

<script type="text/javascript">



    window.datatable = {};
    var ajax = base_url + 'admin/shopuser/datalist';

    $("#dataTable").dataTable({
        "aoColumns": [
        {"bSortable": true},
        {"bSortable": true},
        {"bSortable": true},
        {"bSortable": true},
        {"bSortable": true},
        {"bSortable": true},
        {"bSortable": true},
        {"bSortable": true},
        {"bSortable": true},
        {"bSortable": true},
        {"bSortable": true},
        {"bSortable": true},
        {"bSortable": true},
        {"bSortable": true},
        {"bSortable": true},
        {"bSortable": true},
        {"bSortable": true},
        {"bSortable": true},
        {"bSortable": true},
        {"bSortable": false},
        ]
    });

    

    $(document).on("click",".edit_user",function(event) {

       var $id=$(this).data('id');
       $("#userid").val($id);
       var data=$(this).closest('tr');

       $("#salonid").val(data[0].cells[1].innerText);
       $("#service_type").val(data[0].cells[2].innerText);
       $("#date").attr("readonly","readonly");
       $("#time").val(data[0].cells[3].innerText);
       $("#remark").val(data[0].cells[4].innerText);
       
       $("#addform").modal("show");
   });
    $(document).ready(function() {
        $(".fancybox").fancybox();
    });
    $('#opening_time').datetimepicker({
    autoclose: true,
    todayHighlight: true
});
    $('#closing_time').datetimepicker({
    autoclose: true,
    todayHighlight: true
});
</script>