<?php //debug($profile, false); ?>
<div class="modal-dialog booking-detail-popup modal-lg">
    <!-- Modal content-->
    <div class="modal-content ">
        <div class="modal-header">
            <button type="button" class="close close-bt" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Question</h4>
            <div id="saveCommentMessage"></div>
        </div>
        <div class="modal-body">
            <div class="booking_message"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 res-dai">
                   <span class="table-responsive">
                      <table class="table" style="margin-bottom: 0px;">
                        <tr>
                            <th>Question ID</th>
                            <td><?php echo $profile['id']; ?></td>
                        </tr>
                        <?php if(isset($profile['problemarea'])): ?>
                            <?php if($profile['problemarea'] != ''): ?>
                                <tr>
                                  <th>Problem Area</th>
                                  <td><?php echo $profile['problemarea']; ?></td>
                              </tr>
                          <?php endif; ?>
                      <?php endif; ?>
                      <?php if(isset($profile['question'])): ?>
                        <?php if($profile['question'] != ''): ?>
                            <tr>
                              <th>Question</th>
                              <td><p><?php echo $profile['question']; ?></p></td>
                          </tr>
                      <?php endif; ?>
                  <?php endif; ?>
                  <tr>
                    <th colspan="2">
                       <span style="text-align: center;" class="text-bold text-info text-info">Client Profile</span>
                   </th>
               </tr>
               <tr>
                <th>Name</th>
                <td><?php echo ucfirst($profile['name']); ?></td>
            </tr>
            <tr>
                <th>Gender</th>
                <td><?php echo ucfirst($profile['gender']); ?></td>
            </tr>
            <tr>
                <th>Email</th>
                <td><?php echo $profile['email']; ?></td>
            </tr>
            <tr>
                <th>Mobile</th>
                <td><?php echo (isset($profile['mobile']))?$profile['countrycode'].'-'.$profile['mobile']:''; ?></td>
            </tr>
            <tr>
                <th class="nowrap">Date of birth</th>
                <td><?php echo ucfirst($profile['dob']); ?></td>
            </tr>
            <tr>
                <th class="nowrap">Place of Birth</th>
                <td><?php echo $profile['placeOfBirth']; ?></td>
            </tr>
            <?php if(isset($profile['partnername'])): ?>
                <?php if($profile['partnername'] != "" || $profile['partnerdob'] != "" || $profile['partnertimeofbirth'] != "" || $profile['partnerplaceofbirth'] != "") : ?>
                    <tr>
                      <th colspan="2">
                         <span style="text-align: center;" class="text-bold text-info text-info">Partner Profile</span>
                     </th>
                 </tr>
                 <?php if($profile['partnername'] != '') : ?>
                    <tr>
                      <th>Name</th>
                      <td><?php echo (isset($profile['partnername']))?$profile['partnername']:''; ?></td>
                  </tr>
              <?php endif; ?>
              <?php if($profile['partnerdob'] != '') : ?>
                <tr>
                  <th>Date of birth</th>
                  <td><?php echo ucfirst($profile['partnerdob']); ?></td>
              </tr>
          <?php endif; ?>
          <?php if($profile['partnertimeofbirth'] != '') : ?>
            <tr>
              <th>Time of Birth</th>
              <td><?php echo $profile['partnertimeofbirth']; ?></td>
          </tr>
      <?php endif; ?>
      <?php if($profile['partnerplaceofbirth'] != '') : ?>
        <tr>
          <th>Place of Birth</th>
          <td><?php echo $profile['partnerplaceofbirth']; ?></td>
      </tr>
  <?php endif; ?>

<?php endif; ?>
<?php endif; ?>
<tr>
    <th colspan="2">
       <span style="text-align: center;" class="text-bold text-info text-info">Payment Details</span>
   </th>
</tr>
<tr>
    <th>Amount</th>
    <td><?php echo get_price_html($profile['questionPayment']['amount'] + $profile['questionPayment']['credits']); ?></td>
</tr>
<tr>
    <th>Credits</th>
    <td><?php echo get_price_html($profile['questionPayment']['credits']); ?></td>
</tr>
<tr>
    <th class="nowrap">Final Amount</th>
    <td><?php echo get_price_html($profile['questionPayment']['finalAmount']); ?></td>
</tr>
<tr>
    <th class="nowrap">Service-Tax/Bank-Charge</th>
    <td><?php echo get_price_html($profile['questionPayment']['serviceText']).' / '.number_format($profile['questionPayment']['bankCharge'], 2); ?></td>
</tr>

<tr>
    <th class="nowrap">Consultant Part</th>
    <td>
        <div class="row">
            <div class="col-md-6">
                <div id="conPartUpdate">
                    <input type="number" id="conPartAmount" class="form-control input-sm" readonly value="<?php echo $profile['questionPayment']['consultantPart']; ?>">
                    <input type="hidden" id="question_id" value="<?php echo $profile['id']; ?>">
                </div>
            </div>
            <div class="col-md-6">
                <button class="btn btn-sm btn-default" disabled id="conPart">Update</button>
            </div>
        </div>
    </td>
</tr>

<tr>
    <th>Status</th>
    <td><?php echo question_status($profile['astrologerAnswer'], $profile['askQuestionAnswers']); ?></td>
</tr>
<?php if(!empty($profile['astrologerAnswer'])): ?>
    <tr>
        <th colspan="2">
            <span style="text-align: center;" class="text-bold text-info text-info">Astrologer Details</span>
        </th>
    </tr>
    <tr>
        <th>Name</th>
        <td><?php echo (isset($profile['consultant']['firstname']))?$profile['consultant']['firstname']:'NA'; ?></td>
    </tr>
    <tr>
        <th>Answer Datetime</th>
        <td><?php echo (isset($profile['astrologerAnswer']['astrologerAnswer']))?format_datetime($profile['astrologerAnswer']['creationTime']):'NA'; ?></td>
    </tr>
    <th colspan="2">
        <span style="text-align: center;" class="text-bold text-warning text ">Answer &nbsp;&nbsp; <a href="javascript:void(0);" class="text text-success" onclick="copyToClipboard('#astroAns');" title="Copy Astrologer Answer"><i class="fa fa-copy"></i></a></span>
    </th>
<?php endif; ?>
</table>
</span>
</div>
</div>
<?php if(!empty($profile['astrologerAnswer'])): ?>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 res-dai" style="padding-left: 23px">
            <textarea disabled readonly="" name="astroAns" id="astroAns" cols="30" rows="16" class="form-control"><?php echo (isset($profile['astrologerAnswer']['astrologerAnswer']))?$profile['astrologerAnswer']['astrologerAnswer']:''; ?></textarea>
        </div>
    </div>
<?php endif; ?>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12 res-dai" style="padding-left: 23px; margin-top: 10px;">
        <div class="form-group">
            <label for="ConsultantComment" class="text-bold text-success text "> Verified Answer<!-- &nbsp;&nbsp; <a href="javascript:void(0);" class="text text-success" onclick="paste('ConsultantComment');" title="Past Astrologer Answer"><i class="fa fa-paste"></i></a>--></label>
            <textarea name="comment" id="ConsultantComment" rows="16" class="form-control" placeholder="Answer..."><?php echo ($profile['askQuestionAnswers']['answer'])?$profile['askQuestionAnswers']['answer']:''; ?></textarea>
        </div>
        <div class="form-group">
            <div class="col-sm-6">
              <button  class="btn btn-primary btn-xs " data-toggle="modal" data-target="#modalmotivation"  >View motivational line</button>
              <button  class="btn btn-primary btn-xs "  data-toggle="modal"  data-target="#endinglines"  >View ending line</button>
          </div>
          <div class="col-sm-6">
                <button id="saveAnswer" class="btn btn-primary btn-xs pull-right" data-user_id="<?php echo $profile['userId']; ?>" data-id="<?php echo $profile['id']; ?>">Submit Answer</button>
          </div>
        </div>
  </div>
</div>
<div class="clear"></div>
</div>
        <!--<div class="modal-footer">
            <div class="row text-center">
                <button class="btn btn-default btn_trans" id="reject_booking" data-id="<?php /*echo $profile['id']; */?>"> Reject</button>
            </div>
        </div>-->
    </div>
</div>
<script type="text/javascript">
    function copyToClipboard(element) {
        var $temp = $("<textarea>");
        $("body").append($temp);
        $temp.val($(element).text()).select();
        document.execCommand("copy");
        $temp.remove();
    }

    function paste(target) {
        var editor = document.getElementById(target);
        editor.focus();
        editor.select();
        document.execCommand('paste');
    }

</script>
