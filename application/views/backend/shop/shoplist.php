<div class="content-wrapper">
<link href="<?php  echo base_url('assets/admin/datepicker/datetimepicker.min.css'); ?>" rel="stylesheet" type="text/css">
<script src="<?php  echo base_url('assets/admin/datepicker/moment.min.js'); ?>"></script>
<script src="<?php  echo base_url('assets/admin/datepicker/datetimepicker.min.js'); ?>"></script>


    <section class="content-header">
        <a href="javascript:void(0);" data-target="#addShop" data-toggle="modal" class="pull-right btn btn-success"><i class="fa fa-plus"></i> Add new shop</a>
        <div class="row">
            <div class="col-md-4 col-sm-12">
                <h3><?php echo $page_title; ?></h3>
            </div>
        </div>
    </section>
    <section class="content">
        <?php echo get_flashdata('message'); ?>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div style="padding: 0px 15px;">
                    </div>
                    <div class="box-body dataTables_wrapper form-inline dt-bootstrap table-responsive">
                        <table class="table table-hover table-bordered" id="dataTable">
                            <thead>
                                <tr>
                                    <th >S.No.</th>
                                    <th>Name</th>
                                    <th class="nowrap">View Counter</th>
                                    <th class="nowrap">Mobile No.</th>
                                    <th class="nowrap">Website </th>
                                    <th class="nowrap">About</th>
                                    <th class="nowrap">Image</th>
                                    <th class="nowrap">Address</th>
                                    <th class="nowrap">Country</th>
                                    <th class="nowrap">State</th>
                                   
                                    <th class="nowrap">City</th>
                                    <th class="nowrap">Postal code</th>
                                    <th class="nowrap">Lat</th>
                                    <th class="nowrap" >Lang</th>
                                    <th class="nowrap">Created By</th>
                                    <th class="nowrap">service offered</th>
                                    <th class="nowrap">days</th>
                                    <th class="nowrap">opening time</th>
                                    <th class="nowrap">closing time</th>
                                    <th class="nowrap">parking</th>
                                    <th class="nowrap">Type</th>
                                    <th class="nowrap"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php

                                if(isset($result)): 
                                    foreach ($result as $key => $value):  ?>
                                <tr>
                                    <td><?php echo $key+1; ?></td>
                                    <td><?php echo $value->name; ?></td>
                                      <td><?php echo $value->counter; ?></td>
                                    <td><?php echo $value->mobileno; ?></td>
                                    <td><?php echo $value->website; ?></td>
                                    <td><?php echo $value->about_me; ?></td>
                                    <td><a class="fancybox" rel="group" href="<?php echo $value->profile_pic; ?>"><img src="<?= base_url(); ?>uploads/<?= $value->image; ?>" class="img img-responsive" style="width:100px;height:50px; "> </a></td>
                                    <td><?php echo $value->user_address; ?></td>
                                    <td><?php echo $value->country; ?></td>
                                    <td><?php echo $value->state; ?></td>
                                  
                                    <td><?php echo $value->city; ?></td>
                                    <td><?php echo $value->postalcode; ?></td>
                                    <td><?php echo $value->lat; ?></td>
                                    <td><?php echo $value->lon; ?></td>
                                    <td><?php if($value->is_created_by_admin){ echo "Admin"; }else{ echo "Shop user"; } ?></td>
                                    <td><?php echo $value->service_offered; ?></td>
                                    <td><?php echo $value->days; ?></td>
                                    <td><?php echo $value->opening_time; ?></td>
                                    <td><?php echo $value->closing_time; ?></td>
                                    <td><?php echo $value->parking; ?></td>
                                    <td><?php echo $value->type; ?></td>
                                    <td class="nowrap">
                                       <a href="<?= base_url(); ?>admin/shopuser/editshop/<?= $value->id; ?>"  data-id="<?php echo custom_encode($value->id); ?>" class="btn btn-info btn-xs"><i class="fa fa-edit"></i></a>
                                       <a  href="<?php echo base_url('admin/shopuser/deleteAccount')."/".custom_encode($value->id); ?>" onclick="return confirm('Are you sure you want to delete?');" class="btn btn-danger btn-xs delete_user"><i class="fa fa-trash"></i></a>
                                   </td>
                               </tr>
                           <?php  endforeach; endif; ?>
                       </tbody>
                   </table>
               </div>
           </div><!-- .box -->
       </div><!-- .col-md-12 -->
   </div><!-- .row -->

   <!-- modal start -->
   <div id="addShop" class="modal fade" role="dialog">
       <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-light-blue">Add new shop</h4>
            </div>
            <form role="form" method="post" action="<?php echo base_url('admin/shopuser/addShop'); ?>" enctype="multipart/form-data" id="cinic-form">
                <div class="modal-body">                    
                    <div class="form-group">
                        <label for="name">Shop Name</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Enter shop name" required>
                        <input type="hidden" name="userid" id="userid">
                    </div>
                    <div class="form-group">
                        <label for="name">Shop Website</label>
                        <input type="text" class="form-control" id="website" name="website" placeholder="Enter shop website url" required>
                    </div>                      
                    <div class="form-group">
                        <label for="mobile">Contact Number</label>
                        <input  class="form-control" id="mobile" name="mobileno" placeholder="(999) 999-9999"  data-inputmask='"mask": "(999) 999-9999"' oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
    type = "number"
    maxlength = "10" data-mask>
                    </div>
                    <div class="form-group">
                        <label for="address" class="">Search On Google</label>
                        <input type="hidden" name="lat" value="" id="lat">
                        <input type="hidden" name="lon" value="" id="lon">
                        <input class="form-control" id="address" name="address" placeholder="address" autocomplete/>
                    </div>
                    <div class="form-group">
                        <label for="location">Address</label>
                        <input type="text" class="form-control" id="street" name="street" placeholder="street address" value="">
                    </div>
                    <div class="form-group">
                        <label for="country" class="">Country</label>
                        <input type="text" class="form-control" id="country" name="country" placeholder="country" value="">
                    </div>
                    <div class="form-group">
                        <label for="city" class="">City</label>
                        <input type="text" class="form-control" id="city" name="city" placeholder="city" value="">
                    </div>
                    <div class="form-group">
                        <label for="state" class="">State</label>
                        <input type="text" class="form-control" id="state" name="state" placeholder="state" value="">
                    </div>
                    <div class="form-group">
                        <label for="state" class="">Post Code</label>
                        <input type="text" class="form-control" id="postalcode" name="postalcode" placeholder="post code" value="">
                    </div>
                    <div class="form-group">
                        <label for="state" class="">Service Offered</label>
                        <input type="text" class="form-control" id="service_offered" name="service_offered" placeholder="Service Offered" value="">
                    </div>
                    <div class="form-group">
                        <label for="state" class="">Days</label>
                        <input type="text" class="form-control" id="days" name="days" placeholder="Days" value="">
                    </div>
                    <div class="form-group">
                        <label for="state" class="">Parking</label>
                        <input type="text" class="form-control" id="parking" name="parking" placeholder="Parking" value="">
                    </div>
                     <div class="form-group">
                        <label for="state" class="">Opening Time</label>
                        <input type="text" class="form-control" id="opening_time" name="opening_time" placeholder="Opening Time" value="">
                    </div>
                    <div class="form-group">
                        <label for="state" class="">Closing Time</label>
                        <input type="text" class="form-control" id="closing_time" name="closing_time" placeholder="Closing Time" value="">
                    </div>
                    <div class="form-group">
                        <label for="name">Type</label>
                        <input type="text" class="form-control" id="type" name="type" placeholder="Enter Unisex or Women" required>                        
                    </div>
                    <div class="form-group">
                        <label for="about">About Shop</label>
                        <textarea class="form-control" id="about" name="about_me" rows="3" required></textarea>
                    </div>
                    <div class="form-group"  id="errordiv">
                        <div class="btn btn-default btn-file">
                            <i class="fa fa-paperclip"></i> Select Image
                            <input type="file" id="fUpload" name="attachment" required>
                        </div>
                        <div> 
                           <p class="help-block" id="file-info">Max. 1MB</p>
                       </div>
                   </div>
                   <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- modal end -->
</section>
</div>
<script type="text/javascript">


window.datatable = {};
var ajax = base_url + 'admin/shopuser/datalist';

$("#dataTable").dataTable();

</script>
<script type="text/javascript">



    // window.datatable = {};
    // var ajax = base_url + 'admin/shopuser/datalist';

    // $("#dataTable").dataTable({
    //     "aoColumns": [
    //     {"bSortable": true},
    //     {"bSortable": true},
    //     {"bSortable": true},
    //     {"bSortable": true},
    //     {"bSortable": true},
    //     {"bSortable": true},
    //     {"bSortable": true},
    //     {"bSortable": true},
    //     {"bSortable": true},
    //     {"bSortable": true},
    //     {"bSortable": true},
    //     {"bSortable": true},
    //     {"bSortable": true},
    //     {"bSortable": true},
    //     {"bSortable": true},
    //     {"bSortable": true},
    //     {"bSortable": true},
    //     {"bSortable": true},
    //     {"bSortable": true},
    //     {"bSortable": false},
    //     ]
    // });

    

    $(document).on("click",".edit_user",function(event) {

       var $id=$(this).data('id');
       $("#userid").val($id);
       var data=$(this).closest('tr');

       $("#name").val(data[0].cells[1].innerText);
       $("#mobile").val(data[0].cells[2].innerText);
       $("#mobile").attr("readonly","readonly");
       $("#website").val(data[0].cells[3].innerText);
       $("#street").val(data[0].cells[6].innerText);
       $("#country").val(data[0].cells[7].innerText);
       $("#state").val(data[0].cells[8].innerText);
       $("#city").val(data[0].cells[9].innerText);
       $("#postalcode").val(data[0].cells[10].innerText);
       $("#about").val(data[0].cells[4].innerText);
       $("#lat").val(data[0].cells[11].innerText);
       $("#lon").val(data[0].cells[12].innerText);
       $("#service_offered").val(data[0].cells[14].innerText);
       $("#days").val(data[0].cells[15].innerText);
       $("#opening_time").val(data[0].cells[16].innerText);
       $("#closing_time").val(data[0].cells[17].innerText);
       $("#parking").val(data[0].cells[18].innerText);
       $("#type").val(data[0].cells[19].innerText);
       $("#addShop").modal("show");
   });
    $(document).ready(function() {
        $(".fancybox").fancybox();
    });
    $('#opening_time').datetimepicker({
    autoclose: true,
    todayHighlight: true
});
    $('#closing_time').datetimepicker({
    autoclose: true,
    todayHighlight: true
});
</script>





