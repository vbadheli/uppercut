
<div class="content-wrapper">
    <section class="content-header">
        <a href="javascript:void(0)" data-toggle="modal" data-target="#adduser" class="pull-right btn btn-success"><i class="fa fa-plus"></i> Add new Service</a>

        <div class="row">
            <div class="col-md-4 col-sm-12">
                <h3><?php echo $page_title; ?></h3>
            </div>
        </div>
    </section>

    <section class="content">
        <?php echo get_flashdata('message'); ?>

        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div style="padding: 0px 15px;">
                    </div>
                    <div class="box-body dataTables_wrapper form-inline dt-bootstrap table-responsive">
                        <table class="table table-hover table-bordered" id="dataTable">
                            <thead>
                                <tr>
                                    <th>S.No.</th>
                                    <th>Name</th>
                                  
                                    <th>Delete</th>
                                   
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if(isset($result)): 
                                    foreach ($result as $key => $value):  ?>
                                <tr>
                                    <td><?php echo $key+1; ?></td>
                                    <td><?php echo $value->name; ?></td>
                                  
                                    <td>
                                        <a href="<?php echo base_url('admin/shopuser/deleteservice')."/".custom_encode($value->id); ?> " onclick="return confirm('Are you sure you want to delete?');"  data-id="<?php echo custom_encode($value->id); ?>" class="btn btn-danger btn-xs delete_user"><i class="fa fa-trash"></i></a>
                                    </td>
                                   
                                </tr>
                            <?php  endforeach; endif; ?>
                        </tbody>
                    </table>
                </div>
            </div><!-- .box -->
        </div><!-- .col-md-12 -->
    </div><!-- .row -->
    <!-- modal start -->
    <div id="adduser" class="modal fade" role="dialog">
     <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-light-blue">Add new Service</h4>
            </div>
            <form role="form" method="post" action="<?php echo base_url('admin/shopuser/addservice'); ?>" enctype="multipart/form-data" id="formSignup">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">Name of Service</label>
                        <input type="text" class="form-control"  name="name" placeholder="Enter Name" required>
                        
                    </div>
                   <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    <!-- modal end -->
</section>
</div>

<script type="text/javascript">


window.datatable = {};
var ajax = base_url + 'admin/shopuser/services_detail';

$("#dataTable").dataTable();

</script>


