<div class="content-wrapper">
<link href="<?php  echo base_url('assets/admin/datepicker/datetimepicker.min.css'); ?>" rel="stylesheet" type="text/css">
<script src="<?php  echo base_url('assets/admin/datepicker/moment.min.js'); ?>"></script>
<script src="<?php  echo base_url('assets/admin/datepicker/datetimepicker.min.js'); ?>"></script>



    <section class="content-header">
        <a href="javascript:void(0);" data-target="#addShop" data-toggle="modal" class="pull-right btn btn-success"><i class="fa fa-plus"></i> Add new shop</a>
        <div class="row">
            <div class="col-md-4 col-sm-12">
                <h3><?php echo $page_title; ?></h3>
            </div>
        </div>
    </section>
    <section class="content">
        <?php echo get_flashdata('message'); ?>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div style="padding: 0px 15px;">
                    </div>
                    <div class="box-body dataTables_wrapper form-inline dt-bootstrap table-responsive">
                        <table class="table table-hover table-bordered" id="dataTable">
                            <thead>
                                <tr>
                                    <th>S.No.</th>
									                   <th>Salon Name</th>
									                   <th>User Name</th>
									                   <th>Service Type</th>
									                   <th>Date</th>
									                   <th>time</th>
									                   <th>status</th>
									                   <th>remark</th>
									                   <th>created_at</th>
									                   <th>updated_at</th>
									                   <th>Edit</th>
									                   <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php

                                if(isset($result)): 
                                    foreach ($result as $key => $value):  ?>
                                       <?php $id = $this->db->where('id',$value->salonid)->get('shop_list')->row(); ?>
                                       <?php $id1 = $this->db->where('id',$value->userid)->get('shop_signup')->row(); ?>
                                <tr>
                                   <td><?php echo $key+1; ?></td>									                                                                                      
                                   <td><?php echo $id->name; ?></td>                                   
									                 <td><?php echo $id1->username; ?></td>
									                 <td><?php echo $value->service_type; ?></td>
									                 <td><?php echo $value->date; ?></td>
									                 <td><?php echo $value->time; ?></td>
									                 <td><?php echo $value->status; ?></td>
									                 <td><?php echo $value->remark; ?></td>
									                 <td><?php echo $value->created_at; ?></td>
									                 <td><?php echo $value->updated_at; ?></td>
									                 <td class="nowrap">
                                       <a href="<?= base_url(); ?>admin/shopuser/editBookingListAdmin/<?= $value->id; ?>"  data-id="<?php echo custom_encode($value->id); ?>" class="btn btn-info btn-xs"><i class="fa fa-edit"></i></a></td>
                                     <td class="nowrap"> 
                                       <a  href="<?php echo base_url('admin/shopuser/deleteAccountAdmin')."/".custom_encode($value->id); ?>" onclick="return confirm('Are you sure you want to delete?');" class="btn btn-danger btn-xs delete_user"><i class="fa fa-trash"></i></a>
                                   </td>
                               </tr>
                           <?php  endforeach; endif; ?>
                       </tbody>
                   </table>
               </div>
           </div><!-- .box -->
       </div><!-- .col-md-12 -->
   </div><!-- .row -->

   <!-- modal start -->
   <div id="addShop" class="modal fade" role="dialog">
       <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-light-blue">Add Service Booking</h4>
            </div>
            <form role="form" method="post" action="<?php echo base_url('shop/shop/sbookingAdmin'); ?>" enctype="multipart/form-data" id="cinic-form">
                <div class="modal-body">
                    <div class="service-combo-wrap" id='servicecombowrapid'>
						<select class="combo form-control" id='service_type' name='service_type' required="">
							<option value="">Select a service</option>
							<option name="MensHairCut" value="MensHairCut">Men's Hair Cut</option>
							<option name="WomensHairCut" value="WomensHairCut">Women's Hair Cut</option>
							<option name="HairCutKids" value="HairCutKids">Hair Cut Kids</option>
							<option name="EyebrowThreading" value="EyebrowThreading">Eyebrow Threading</option>
						</select>
						<span><span class="fa fa-angle-down"></span></span>
					</div>
                    <div class="form-group date-wrap" id="datewrapid">
                        <span class="date-label">Date</span>
						<div class="week-day-wrap">
							<input type='hidden' name='date' id='bookingdate' value="">
							<input type='hidden' name='time' id='bookingtime' value="">
							<input type='hidden' name='salonid' id='salonid' value="">
							<?php
								for($i=0; $i<=6; $i++)
								{
										echo "<span>".date('D',strtotime("$i day"))."</span>";
								}
							?>
                    </div>  
                      <div class="form-group day-num-wrap" id="datewrapid">
                        <?php
								for($i=0; $i<=6; $i++)
								{
									echo "<span onclick='selectBookingDate(this,\"" . date('Y-m-d', strtotime("$i day")) . "\")'>".date('d',strtotime("$i day"))."</span>";
									//echo date('Y-m-d: D',strtotime("$i day"))."<br>";

								}
							?>
                    </div>                      
                    <div class="form-group time-wrap" id="timewrapid">
                    	<span class="time-label">Time</span><br>
                       <span onclick="selectBookingTime(this,'8 AM')">8 AM</span>
						<span onclick="selectBookingTime(this, '9 AM')">9 AM</span>
						<span onclick="selectBookingTime(this, '10 AM')">10 AM</span>
						<span onclick="selectBookingTime(this, '11 AM')">11 AM</span>
						<span onclick="selectBookingTime(this, '12 AM')">12 AM</span>
						<span onclick="selectBookingTime(this, '1 PM')">1 PM</span>
						<span onclick="selectBookingTime(this, '2 PM')" class="publish">2 PM</span>
						<span onclick="selectBookingTime(this, '3 PM')">3 PM</span>
						<span onclick="selectBookingTime(this, '4 PM')">4 PM</span>
						<span onclick="selectBookingTime(this, '5 PM')">5 PM</span>
						<span onclick="selectBookingTime(this, '6 PM')">6 PM</span>
						<span onclick="selectBookingTime(this, '7 PM')">7 PM</span>
						<span onclick="selectBookingTime(this, '8 PM')">8 PM</span>
						<span onclick="selectBookingTime(this, '9 PM')">9 PM</span>
						<span onclick="selectBookingTime(this, '10 PM')">10 PM</span>
						<!--div class="time-label">Time</div-->
                    </div>                   
                    <div class="form-group">
                        <label for="remark">Remark</label>
                        <textarea class="form-control" id="remark" name="remark" rows="3" required></textarea>
                    </div>
                    
                   <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- modal end -->
</section>
</div>
<script type="text/javascript">


window.datatable = {};
var ajax = base_url + 'admin/shopuser/datalist';

$("#dataTable").dataTable();

</script>
<script type="text/javascript">



   //  window.datatable = {};
   //  var ajax = base_url + 'admin/shopuser/datalist';

   //  $("#dataTable").dataTable({
   //      "aoColumns": [
   //      {"bSortable": true},
   //      {"bSortable": true},
   //      {"bSortable": true},
   //      {"bSortable": true},
   //      {"bSortable": true},
   //      {"bSortable": true},
   //      {"bSortable": true},
   //      {"bSortable": true},
   //      {"bSortable": true},
   //      {"bSortable": true},
   //      {"bSortable": true},
   //      {"bSortable": true},
   //      {"bSortable": true},
   //      {"bSortable": true},
   //      {"bSortable": true},
   //      {"bSortable": true},
   //      {"bSortable": true},
   //      {"bSortable": true},
   //      {"bSortable": true},
   //      {"bSortable": false},
   //      ]
   //  });

    

   //  $(document).on("click",".edit_user",function(event) {

   //     var $id=$(this).data('id');
   //     $("#userid").val($id);
   //     var data=$(this).closest('tr');

   //     $("#name").val(data[0].cells[1].innerText);
   //     $("#mobile").val(data[0].cells[2].innerText);
   //     $("#mobile").attr("readonly","readonly");
   //     $("#website").val(data[0].cells[3].innerText);
   //     $("#street").val(data[0].cells[6].innerText);
   //     $("#country").val(data[0].cells[7].innerText);
   //     $("#state").val(data[0].cells[8].innerText);
   //     $("#city").val(data[0].cells[9].innerText);
   //     $("#postalcode").val(data[0].cells[10].innerText);
   //     $("#about").val(data[0].cells[4].innerText);
   //     $("#lat").val(data[0].cells[11].innerText);
   //     $("#lon").val(data[0].cells[12].innerText);
   //     $("#service_offered").val(data[0].cells[14].innerText);
   //     $("#days").val(data[0].cells[15].innerText);
   //     $("#opening_time").val(data[0].cells[16].innerText);
   //     $("#closing_time").val(data[0].cells[17].innerText);
   //     $("#parking").val(data[0].cells[18].innerText);
   //     $("#addShop").modal("show");
   // });
    $(document).ready(function() {
        $(".fancybox").fancybox();
    });
    $('#opening_time').datetimepicker({
    autoclose: true,
    todayHighlight: true
});
    $('#closing_time').datetimepicker({
    autoclose: true,
    todayHighlight: true
});
function selectBookingDate(current, date)
{
	// alert(date);
	// console.log($(current));
	$(".day-num-wrap").children().each(function(e,i){
		$(i).removeClass('publish');
	})
	$(current).addClass('publish');
	$("#bookingdate").val(date);

}
function selectBookingTime(current, time)
{
	// alert(date);
	// console.log($(current));
	$(".time-wrap").children().each(function(u,t){
		$(t).removeClass('publish');
	})
	$(current).addClass('publish');
	$("#bookingtime").val(time);

}
</script>





