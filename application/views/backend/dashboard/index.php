

<div class="content-wrapper">

    <!-- Content Header (Page header) -->

    <section class="content-header">

        <h1>

            Dashboard

        </h1>

    </section>

    <!-- Main content -->

    <section class="content">

        <?php echo get_flashdata('message'); ?>

        <!-- Small boxes (Stat box) -->

        <div class="row">

            <div class="col-md-12">

                <div class="box box-primary">

                    <div class="box-header">

                        <h4>Statics</h4>

                    </div>

                    <div class="box-body">

                        <div class="row">

                            <div class="col-lg-3 col-xs-6">

                                <!-- small box -->

                                <div class="small-box bg-light-blue color-palette">

                                    <div class="inner">

                                        <h3><?= $this->db->get('shop_signup')->num_rows(); ?></h3>

                                        <p>Total Users</p>

                                    </div>

                                    <div class="icon">

                                        <i class="fa fa-users"></i>

                                    </div>

                                    <a href="<?= base_url(); ?>admin/shopuser" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>

                                </div>

                            </div>

                            <!-- ./col -->

                            <div class="col-lg-3 col-xs-6">

                                <!-- small box -->

                                <div class="small-box bg-green">

                                    <div class="inner">

                                        <h3><?= $this->db->get('shop_signup')->num_rows(); ?></h3>

                                        <p>Total Shops</p>

                                    </div>

                                    <div class="icon">

                                        <i class="fa fa-medkit"></i>

                                    </div>

                                    <a href="<?= base_url(); ?>shoplist" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>

                                </div>

                            </div>

                            <!-- ./col 

                            <div class="col-lg-3 col-xs-6">

                               

                                <div class="small-box bg-olive">

                                    <div class="inner">

                                        <h3>100</h3>

                                        <p>Total Appointments</p>

                                    </div>

                                    <div class="icon">

                                        <i class="fa fa-calendar"></i>

                                    </div>

                                    <a href="javascript:void(0)" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>

                                </div>

                            </div>

                        ./col 

                            <div class="col-lg-3 col-xs-6">



                                <div class="small-box bg-purple-gradient">

                                    <div class="inner">

                                        <h3>100</h3>

                                        <p>Total Questions</p>

                                    </div>

                                    <div class="icon">

                                        <i class="fa fa-question"></i>

                                    </div>

                                    <a href="javascript:void(0);" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>

                                </div>

                            </div>

                          ./col -->

                        </div>                        

                    </div>

                </div>

            </div>

        </div>       

    </section>

    <!-- /.content -->

</div>



<script type="text/javascript">

    $(document).ready(function () {

        validate();

    });

    function validate() {

        $('#mail-form').validate({

            errorPlacement: function (error, element) {},

            highlight: function (element) {

                $(element).parent('div').addClass('has-error');

            },

            unhighlight: function (element) {

                $(element).parent('div').removeClass('has-error');

            }

        });

    }



    $(document).on('click', '#send', function () {

        $("[name^=name]").each(function () {

            $(this).rules("add", {

                required: true,

            });

        });



        $("[name^=email]").each(function () {

            $(this).rules("add", {

                required: true,

                email: true

            });

        });

        $('#mail-form-submit').trigger('click');

    });

    var index = <?php echo $i; ?>;

    $(document).on('click', '#add', function () {

        var html = '<tr>';

        html += '<td><div class="form-group"><input type="text" class="form-control name" placeholder="name" name="name['+index+']"></div></td>';

        html += '<td><div class="form-group"><input type="email" class="form-control email" placeholder="email" name="email['+index+']"></div></td>';

        html += '<td class="center"><a href="#" class="btn btn-danger btn-sm remove"><i class="fa fa-trash"></i></a></td>';

        html += '</tr>';

        index++;

        $('#add-new-row').append(html);



    });

    $(document).on('click', '.remove', function () {

        $(this).closest('tr').remove();

    });

</script>

