
<div class="content-wrapper">
	<section class="content-header">
	<br>
		<!-- <a href="javascript:void(0)" data-toggle="modal" data-target="#adduser" class="pull-right btn btn-success"><i class="fa fa-plus"></i> Add new user</a> -->

		<div class="row">
			<div class="col-md-4 col-sm-12">
				<h3><?php echo $page_title; ?></h3>
			</div>
		</div>
	</section>

	<section class="content">
		<?php echo get_flashdata('message'); ?>

		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div style="padding: 0px 15px;">
					</div>
					<div class="box-body dataTables_wrapper form-inline dt-bootstrap table-responsive">
						<table class="table table-hover table-bordered" id="dataTable">
							<thead>
								<tr>
									<th>S.No.</th>
									<th>Email</th>
									<th>Username</th>
									<th>Phone No</th>
									<th>Reason</th>
									<th>Date</th>
								</tr>
							</thead>
							<tbody>
								<?php
								 // echo '<pre>';
								 // print_r($result);
								 // echo '</pre>';
								if(isset($result)): 

									foreach ($result as $key => $value): ?>
								<tr>
									<td><?php echo $key+1; ?></td>
									<td><?php echo $value->name; ?></td>
									<td><?php echo $value->email; ?></td>
									<td><?php echo $value->number; ?></td>
									<td><?php echo $value->reason; ?></td>
									<td><?php echo $value->date; ?></td>
									
								</tr>
							<?php  endforeach; endif; ?>
						</tbody>
					</table>
				</div>
			</div><!-- .box -->
		</div><!-- .col-md-12 -->
	</div><!-- .row -->
	<!-- modal start -->
	<div id="adduser" class="modal fade" role="dialog">
		<div class="modal-dialog modal-lg">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title text-light-blue">Add new shop</h4>
				</div>
				<form role="form" method="post" action="<?php echo base_url('admin/shopuser/adduser'); ?>" enctype="multipart/form-data" id="formSignup">
					<div class="modal-body">
						<div class="form-group">
							<label for="name">User Name</label>
							<input type="email" class="form-control" id="email" name="email" placeholder="Enter clinic name" required>
							<input type="hidden" name="userid" id="userid">
						</div>
						<div class="form-group">
							<label for="mobile">Contact Number</label>
							<input type="text" class="form-control" id="mobile" name="mobileno" placeholder="(999) 999-9999"  data-inputmask='"mask": "(999) 999-9999"' data-mask required="required">
						</div>
						<div class="form-group">
							<label for="state" class="">Password</label>
							<input type="password" class="form-control" id="password" name="password" placeholder="Password" value="" required="">
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-success">Submit</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- modal end -->
	</section>
</div>

<script type="text/javascript">


	window.datatable = {};
	var ajax = base_url + 'admin/shopuser/datalist';

	$("#dataTable").dataTable();

</script>


