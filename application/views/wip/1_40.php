<?php $this->load->view('public_header'); ?>
<style >
.font-chg{
	font-family: 'Comfortaa', cursive;
}
.row{
	margin-left:0 !important;
}
.col-md-12 .row{
	padding-left:25px;
}
.note-head-wrap{
	position: absolute;
	top: 0;
	left: 0;
	right: 0;
	padding-bottom: 15px;
}
.note-head-arr{
	width: 55px;
	display: inline-block;
	float: left;
}
.note-head-arr span::before{
	content: '';
	border-right: 16px solid #0b0b0b;
	border-top: 14px  solid transparent;
	border-bottom: 14px  solid transparent;
	float: left;
	position: absolute;
	margin-top: 20px;
	margin-left: 20px;
}
.note-head-arr span::after{
	content: '';
	border-right: 16px solid #fff;
	border-top: 14px  solid transparent;
	border-bottom: 14px  solid transparent;
	float: left;
	position: absolute;
	margin-top: 20px;
	margin-left: 17px;
}
.note-head-text{
	width: calc(100% - 55px);
	text-transform: uppercase;
	color: #a1a1a1;
	display: inline-block;
	float: left;
	padding-top: 20px;
	font-size: 20px;
}
.item-wrap{
	padding-top: 70px;
	padding-right: 5%;
}
.item{
	border-bottom: 2px dashed #999999;
	margin-bottom:30px;
	padding-bottom: 30px;
}
.my-badge-wrap{
	display: inline-block;
	width: 100%;
	padding: 15px 0;
}
.my-badge{
	background: #ff3f2e;
	color: #fff;
	padding: 4px 18px 5px 10px;
	font-size: 12px;
	float: left;
	position: relative;
	text-transform: uppercase;
}
.my-badge::after{
	content: '';
	float: right;
	position: absolute;
	border-right: 18px solid white;
	border-top: 13px solid #ff3f2e;
	border-bottom: 14px solid #ff3f2e;
	top: 0;
	border-radius: 0px 0px 0px 0px;
	margin-left: 15px;
}
.my-badge.ongoing{background:#ffa402;}
.my-badge.ongoing::after{border-top-color:#ffa402;border-bottom-color:#ffa402;}
.my-badge.refund{background:#ff3f2e;}
.my-badge.refund::after{border-top-color:#ff3f2e;border-bottom-color:#ff3f2e;}
.my-badge.cancelled{background:#ff3f2e;}
.my-badge.cancelled::after{border-top-color:#ff3f2e;border-bottom-color:#ff3f2e;}
.my-badge.completed{background:#70b12d;}
.my-badge.completed::after{border-top-color:#70b12d;border-bottom-color:#70b12d;}
.stats{
	margin-bottom: 30px;
}
.stats .heading{
	color:#dcdcdc;
	font-size: 14px;
	padding: 6px 0;
}
.stats .action-wrap span{
	padding-right: 25px;
}
.stats .action-wrap span a{
	text-decoration:none;
	color:#64a4fc;
	font-size: 14px;
}
.stats .action-wrap span.active a{
	color:#000000;
	border-bottom:5px solid #000000;
}
.item .time{
	color:#000000;
	font-size: 20px;
	font-weight: bold;
}
.item .cut-type{
	color: #999999;
	font-size: 15px;
	padding: 6px 0;
}
.item .price{
	color: #999999;	
	font-size: 15px;
}
.item .salon-name{
	color: #000000;
	font-size: 16px;
	padding-top: 40px;
}
.item .salon-city{
	color: #999999;	
	font-size: 14px;
	padding: 6px 0;
}
.item .salon-note{
	color: #999999;	
	font-size: 14px;
	padding: 0 0 6px 0;
}
.item .salon-desc{
	color: #515151;
	font-size: 14px;
	line-height: 20px;
}
.item .controls{
	padding-top: 30px;
}
.item .controls a{
	margin-right: 40px;
	color:#3d8dfb;
}
</style>


<?php //$this->load->view('open'); ?>

<div class="note-head-wrap">
	<div class="note-head-arr"><span>&nbsp;</span></div>
	<div class="note-head-text font-chg">Bookings</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="row">


				

				<div class="item-wrap">

					<div class="stats">
						<div class="heading font-chg">Today's booking</div>
						<div class="action-wrap">
							<span class="active font-chg"><a href="#">All</a></span>
							<span class="font-chg"><a href="#">Upcoming</a></span>
							<span class="font-chg"><a href="#">Ongoing</a></span>
						</div>
					</div>

					<div class="item">
						<div class="my-badge-wrap"><span class="my-badge ongoing">Ongoing</span></div>
						<div class="time font-chg">5:00 PM</div>
						<div class="cut-type font-chg">Men's - hair cut</div>
						<div class="price font-chg">₹ 199 | 30 mins</div>
						<div class="salon-name font-chg">Salon Name</div>
						<div class="salon-city font-chg">Salon Locality</div>
						<div class="salon-note font-chg">Note from salon</div>
						<div class="salon-desc font-chg">The resulting cornea flap is created at a precise depth and diameter pre-determined by the surgeon. As occures with a mechanical microkeratome, a small section of tissue at one edge Let us know if you have any questoin about pricing.</div>
						<div class="controls">
							<a href="#"><span class="fa fa-phone fa-2x"></span></a>
							<a href="#"><span class="fa fa-close fa-2x"></span></a>
						</div>
					</div>

					<div class="item">
						<div class="time font-chg">5:00 PM</div>
						<div class="cut-type font-chg">Men's - hair cut</div>
						<div class="price font-chg">₹ 199 | 30 mins</div>
						<div class="salon-name font-chg">Salon Name</div>
						<div class="salon-city font-chg">Salon Locality</div>
						<div class="salon-note font-chg">Note from salon</div>
						<div class="salon-desc font-chg">The resulting cornea flap is created at a precise depth and diameter pre-determined by the surgeon. As occures with a mechanical microkeratome, a small section of tissue at one edge Let us know if you have any questoin about pricing.</div>
						<div class="controls">
							<a href="#"><span class="fa fa-map fa-2x"></span></a>
							<a href="#"><span class="fa fa-phone fa-2x"></span></a>
							<a href="#"><span class="fa fa-close fa-2x"></span></a>
						</div>
					</div>

					<div class="item">
						<div class="my-badge-wrap"><span class="my-badge refund">REFUND INIATED</span></div>
						<div class="time font-chg">5:00 PM</div>
						<div class="cut-type font-chg">Men's - hair cut</div>
						<div class="price font-chg">₹ 199 | 30 mins</div>
						<div class="salon-name font-chg">Salon Name</div>
						<div class="salon-city font-chg">Salon Locality</div>
						<div class="salon-note font-chg">Note from salon</div>
						<div class="salon-desc font-chg">The resulting cornea flap is created at a precise depth and diameter pre-determined by the surgeon. As occures with a mechanical microkeratome, a small section of tissue at one edge Let us know if you have any questoin about pricing.</div>
						<div class="controls">
							<a href="#"><span class="fa fa-map fa-2x"></span></a>
							<a href="#"><span class="fa fa-phone fa-2x"></span></a>
							<a href="#"><span class="fa fa-close fa-2x"></span></a>
						</div>
					</div>

					<div class="item">
						<div class="my-badge-wrap"><span class="my-badge cancelled">CANCELLED</span></div>
						<div class="time font-chg">5:00 PM</div>
						<div class="cut-type font-chg">Men's - hair cut</div>
						<div class="price font-chg">₹ 199 | 30 mins</div>
						<div class="salon-name font-chg">Salon Name</div>
						<div class="salon-city font-chg">Salon Locality</div>
						<div class="salon-note font-chg">Note from salon</div>
						<div class="salon-desc font-chg">The resulting cornea flap is created at a precise depth and diameter pre-determined by the surgeon. As occures with a mechanical microkeratome, a small section of tissue at one edge Let us know if you have any questoin about pricing.</div>
						<div class="controls">
							<a href="#"><span class="fa fa-map fa-2x"></span></a>
							<a href="#"><span class="fa fa-phone fa-2x"></span></a>
							<a href="#"><span class="fa fa-close fa-2x"></span></a>
						</div>
					</div>

					<div class="item">
						<div class="my-badge-wrap"><span class="my-badge completed">COMPLETED</span></div>
						<div class="time font-chg">5:00 PM</div>
						<div class="cut-type font-chg">Men's - hair cut</div>
						<div class="price font-chg">₹ 199 | 30 mins</div>
						<div class="salon-name font-chg">Salon Name</div>
						<div class="salon-city font-chg">Salon Locality</div>
						<div class="salon-note font-chg">Note from salon</div>
						<div class="salon-desc font-chg">The resulting cornea flap is created at a precise depth and diameter pre-determined by the surgeon. As occures with a mechanical microkeratome, a small section of tissue at one edge Let us know if you have any questoin about pricing.</div>
						<div class="controls">
							<a href="#"><span class="fa fa-map fa-2x"></span></a>
							<a href="#"><span class="fa fa-phone fa-2x"></span></a>
							<a href="#"><span class="fa fa-close fa-2x"></span></a>
						</div>
					</div>

					<div class="item">
						<div class="my-badge-wrap"><span class="my-badge completed">COMPLETED</span></div>
						<div class="time font-chg">5:00 PM</div>
						<div class="cut-type font-chg">Men's - hair cut</div>
						<div class="price font-chg">₹ 199 | 30 mins</div>
						<div class="salon-name font-chg">Salon Name</div>
						<div class="salon-city font-chg">Salon Locality</div>
						<div class="salon-note font-chg">Note from salon</div>
						<div class="salon-desc font-chg">The resulting cornea flap is created at a precise depth and diameter pre-determined by the surgeon. As occures with a mechanical microkeratome, a small section of tissue at one edge Let us know if you have any questoin about pricing.</div>
						<div class="controls">
							<a href="#"><span class="fa fa-map fa-2x"></span></a>
							<a href="#"><span class="fa fa-phone fa-2x"></span></a>
							<a href="#"><span class="fa fa-close fa-2x"></span></a>
						</div>
					</div>

				</div>




			</div>
		</div>
	</div>
</div>

 <?php //$this->load->view('close'); ?>
 
<?php //$this->load->view('layouts/footer'); ?>

