<?php $this->load->view('public_header'); ?>
<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/ToggleSwitch.css">
<style >
.font-chg{
	font-family: 'Comfortaa', cursive;
}
.row{
	margin-left:0 !important;
}
.col-md-12 .row{
	padding-left:25px;
}
.note-head-wrap{
	position: absolute;
	top: 0;
	left: 0;
	right: 0;
	padding-bottom: 15px;
}
.note-head-arr{
	width: 55px;
	display: inline-block;
	float: left;
}
.note-head-arr span::before{
	content: '';
	border-right: 16px solid #0b0b0b;
	border-top: 14px  solid transparent;
	border-bottom: 14px  solid transparent;
	float: left;
	position: absolute;
	margin-top: 20px;
	margin-left: 20px;
}
.note-head-arr span::after{
	content: '';
	border-right: 16px solid #fff;
	border-top: 14px  solid transparent;
	border-bottom: 14px  solid transparent;
	float: left;
	position: absolute;
	margin-top: 20px;
	margin-left: 17px;
}
.note-head-text{
	width: calc(100% - 55px);
	text-transform: uppercase;
	color: #a1a1a1;
	display: inline-block;
	float: left;
	padding-top: 24px;
	font-size: 16px;
}
.note-head-text span{
	color: #000000;
	float: right;
	margin-right: 10px;
}
.item-wrap{
	padding-top: 100px;
	padding-right: 5%;
	width: 100%;
}
.input-wrap{
	margin-bottom: 120px;
}
.input-wrap .left{
	display: inline-block;
	width: 80%;
}
.input-wrap .left input{
	border:none;
	border-bottom: 1px solid #c2c2c2;
	padding:7px 0;
	font-size: 16px;
	width: 100%;
	color:#aaaaaa;
}
.input-wrap .right{
	display: inline-block;
	width: 18%;
	color:#2896d3;
	font-size: 16px;
	text-transform: uppercase;
	text-align: right;
}
.item{
	display: inline-block;
	width: 100%;
	border-bottom: 2px dashed #ccc;
	padding:10px 0;
}
.item .item-label{
	display: inline-block;
	width: 75%;
	float: left;
	color: #a2a2a2;
	text-transform: uppercase;
}
.item .item-chk{
	display: inline-block;
	width: 24%;
	float: left;
}

</style>

<?php //$this->load->view('open'); ?>

<div class="note-head-wrap">
	<div class="note-head-arr"><span>&nbsp;</span></div>
	<div class="note-head-text font-chg">Barber Chair<span><span class="fa fa-plus"></span></span></div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="row">

				<div class="item-wrap font-chg">
					<div class="input-wrap">
						<div class="left">
							<input type="text" placeholder="New chair name">
						</div>
						<div class="right">
							Add
						</div>
					</div>
					<div class="item">
						<div class="item-label">
							Chair 1
						</div> 
						<div class="item-chk">
							<input type="checkbox" id="chk1">
						</div>
					</div>
				</div>




			</div>
		</div>
	</div>
</div>

<script src="<?php echo base_url(); ?>/assets/js/ToggleSwitch.js"></script>
<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery("#chk1").toggleSwitch();
});
</script>

<?php //$this->load->view('close'); ?>

<?php //$this->load->view('layouts/footer'); ?>

