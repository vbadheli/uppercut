<?php $this->load->view('public_header'); ?>
<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/ToggleSwitch.css">
<style >
.font-chg{
	font-family: 'Comfortaa', cursive;
}
.row{
	margin-left:0 !important;
}
.col-md-12 .row{
	padding-left:25px;
}
.note-head-wrap{
	position: absolute;
	top: 0;
	left: 0;
	right: 0;
	padding-bottom: 15px;
}
.note-head-arr{
	width: 55px;
	display: inline-block;
	float: left;
}
.note-head-arr span::before{
	content: '';
	border-right: 16px solid #0b0b0b;
	border-top: 14px  solid transparent;
	border-bottom: 14px  solid transparent;
	float: left;
	position: absolute;
	margin-top: 20px;
	margin-left: 20px;
}
.note-head-arr span::after{
	content: '';
	border-right: 16px solid #fff;
	border-top: 14px  solid transparent;
	border-bottom: 14px  solid transparent;
	float: left;
	position: absolute;
	margin-top: 20px;
	margin-left: 17px;
}
.note-head-text{
	width: calc(100% - 55px);
	text-transform: uppercase;
	color: #a1a1a1;
	display: inline-block;
	float: left;
	padding-top: 24px;
	font-size: 16px;
}
.note-head-text span.clock{
	color: #3d8dfb;
	float: right;
	margin-right: 40px;
	font-size: 23px;
}
.note-head-text span.edit{
	color: #464646;
	float: right;
	margin-right: 10px;
	font-size: 22px;
	border-bottom: 3px solid red;
	padding-bottom: 3px;
}
.item-wrap{
	padding-top: 100px;
	padding-right: 5%;
	width: 100%;
}
.input-wrap{
	margin-bottom: 120px;
}
.input-wrap p{
	color: #999999;
	font-size: 14px;
	margin: 0;
}
.input-wrap input{
	border:none;
	border-bottom: 2px solid #000000;
	padding:7px 0;
	font-size: 16px;
	width: 100%;
	color:#000000;
}
.save-button{
	background: #3d8dfb;
	color: #fff;
	border: none;
	padding: 10px 0;
	width: 100%;
	border-radius: 50px;
	text-transform: uppercase;
	margin-top: 120px;
}
.delete-button{
	color: #ff3f2e !important;
	text-transform: uppercase;
	padding: 30px 0;
	display: inline-block;
	width: 100%;
	text-align: center;
	text-decoration: none !important;
}

</style>

<?php //$this->load->view('open'); ?>

<div class="note-head-wrap">
	<div class="note-head-arr"><span>&nbsp;</span></div>
	<div class="note-head-text font-chg">New Chair<span class="edit"><span class="fa fa-edit"></span></span><span class="clock"><span class="fa fa-compass"></span></span></div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="row">

				<div class="item-wrap font-chg">
					<div class="input-wrap">
						<p>Desk name(20 characters)</p>
						<input type="text">
					</div>
					<button class="save-button">Save</button>
					<a href="#" class="delete-button">Delete</a>
				</div>




			</div>
		</div>
	</div>
</div>

<script src="<?php echo base_url(); ?>/assets/js/ToggleSwitch.js"></script>
<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery("#chk1").toggleSwitch();
});
</script>

<?php //$this->load->view('close'); ?>

<?php //$this->load->view('layouts/footer'); ?>

