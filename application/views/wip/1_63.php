<?php $this->load->view('public_header'); ?>
<style >
.font-chg{
	font-family: 'Comfortaa', cursive;
}
.row{
	margin-left:0 !important;
}
.col-md-12 .row{
	padding-left:25px;
}
.note-head-wrap{
	position: absolute;
	top: 0;
	left: 0;
	right: 0;
	padding-bottom: 15px;
}
.note-head-arr{
	width: 55px;
	display: inline-block;
	float: left;
	padding-top: 18px;
	padding-left: 15px;
}
.note-head-text{
	width: calc(100% - 55px);
	text-transform: uppercase;
	color: #a1a1a1;
	display: inline-block;
	float: left;
	padding-top: 15px;
	font-size: 16px;
}
.note-head-text span{
	display: inline-block;
	width: 100%;
	font-size: 12px;
	color:#3b3b3b;
}
.item-wrap{
	padding-top: 60px;
	padding-right: 5%;
}
.customer{
	font-size: 22px;
	color:#4a4a4a;
	margin-top:30px;
}
.customrid{
	font-size: 16px;
	color:#878787;
	margin-bottom: 30px;
}
.hair-cut{
	font-size: 14px;
	margin-bottom: 5px;
	color:#000000;
}
.duration{
	font-size: 14px;
	margin-bottom: 5px;
	color:#000000;
}
.date{
	font-size: 14px;
	margin-bottom: 5px;
	color:#000000;
}
.time{
	font-size: 14px;
	margin-bottom: 5px;
	color:#000000;
}
.note{
	font-size: 16px;
	margin-top:30px;
	margin-bottom: 5px;
	color:#888888;
}
.desc{
	font-size: 14px;
	color:#0a0a0a;
}
.start-service{
	display:inline-block;
	width:100%;
	text-align: center;
	color: #000000 !important;
	text-transform: uppercase;
	margin-top: 40px;
	padding:8px;
	text-decoration: none !important;
	letter-spacing: 1px;
	border:1px solid #000000;
}
.cancel-booking{
	color:#ff5050  !important;
	display:inline-block;
	width:100%;
	text-align: center;
	text-transform: uppercase;
	margin-top: 15px;
	margin-bottom: 10px;
	padding:10px;
	text-decoration: none !important;
	letter-spacing:1px;
}
</style>

<?php //$this->load->view('open'); ?>

<div class="note-head-wrap">
	<div class="note-head-arr"><span class="fa fa-home fa-2x"></span></div>
	<div class="note-head-text font-chg">Desk 1<span>25 Marc 2018</span></div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="row">

				<div class="item-wrap font-chg">
					<div class="customer">Custome Name</div>
					<div class="customrid">28748 48283</div>
					<div class="hair-cut">Hair cut - Men</div>
					<div class="duration">199 | 30 mins</div>
					<div class="date">5 June 2018</div>
					<div class="time">5 PM</div>
					<div class="note">Note from customer</div>
					<div class="desc">The resulting cornea flap is created at a precise depth and diameter pre-determined by the surgeon. As occures with a mechanical microkeratome, a small section of tissue at one edge Let us know if you have any questoin about pricing.</div>
					<a href="#" class="start-service">Stop Service</a>
					<a href="#" class="cancel-booking">Cancel Booking</a>
				</div>

			</div>
		</div>
	</div>
</div>

<?php //$this->load->view('close'); ?>

<?php //$this->load->view('layouts/footer'); ?>

