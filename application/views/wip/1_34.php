<?php $this->load->view('public_header'); ?>
<style >
.font-chg{
	font-family: 'Comfortaa', cursive;
}
.row{
	margin-left:0 !important;
}
.col-md-12 .row{
	padding-left:25px;
}
.note-head-wrap{
	position: absolute;
	top: 0;
	left: 0;
	right: 0;
	padding-bottom: 15px;
}
.note-head-arr{
	width: 55px;
	display: inline-block;
	float: left;
}
.note-head-arr span::before{
	content: '';
	border-right: 16px solid #0b0b0b;
	border-top: 14px  solid transparent;
	border-bottom: 14px  solid transparent;
	float: left;
	position: absolute;
	margin-top: 20px;
	margin-left: 20px;
}
.note-head-arr span::after{
	content: '';
	border-right: 16px solid #fff;
	border-top: 14px  solid transparent;
	border-bottom: 14px  solid transparent;
	float: left;
	position: absolute;
	margin-top: 20px;
	margin-left: 17px;
}
.note-head-text{
	width: calc(100% - 55px);
	text-transform: uppercase;
	color: #a1a1a1;
	display: inline-block;
	float: left;
	padding-top: 24px;
	font-size: 16px;
}
.item-wrap{
	padding-top: 80px;
	padding-right: 5%;
}
.item{
	border-bottom: 2px dashed #999999;
	margin-bottom: 25px;
	padding-bottom: 25px;
}
.item-date{
	color:#999999;
	line-height: 50px;
	font-size:13px;
}
.item-desc{
	color: #464646;
	font-size: 14px;
	padding: 5px 0;
	display: inline-block;
	width: 100%;
	line-height: 24px;
}
</style>

<?php //$this->load->view('open'); ?>

<div class="note-head-wrap">
	<div class="note-head-arr"><span>&nbsp;</span></div>
	<div class="note-head-text font-chg">Feedback</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="row">

				<div class="item-wrap font-chg">
					<div class="item">
						<div class="item-date">5 June 2018</div>
						<div class="item-desc">The resulting cornea flap is created at a precise depth and diameter pre-determined by the surgeon. As occures with a mechanical microkeratome, a small section of tissue at one edge Let us know if you have any questoin about pricing.</div>
					</div>
					<div class="item">
						<div class="item-date">1 Jan 2018</div>
						<div class="item-desc">The resulting cornea flap is created at a precise depth and diameter pre-determined by the surgeon. As occures with a mechanical microkeratome, a small section of tissue at one edge Let us know if you have any questoin about pricing.</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>

<?php //$this->load->view('close'); ?>

<?php //$this->load->view('layouts/footer'); ?>

