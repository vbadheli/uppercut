<?php $this->load->view('public_header'); ?>
<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/ToggleSwitch.css">
<style >
.font-chg{
	font-family: 'Comfortaa', cursive;
}
.row{
	margin-left:0 !important;
}
.col-md-12 .row{
	padding-left:25px;
}
.note-head-wrap{
	position: absolute;
	top: 0;
	left: 0;
	right: 0;
	padding-bottom: 15px;
}
.note-head-arr{
	width: 55px;
	display: inline-block;
	float: left;
}
.note-head-arr span{
	position: absolute;
	top: 18px;
	left: 20px;
	font-size: 20px;
}
.note-head-text{
	width: calc(100% - 55px);
	text-transform: uppercase;
	color: #a1a1a1;
	display: inline-block;
	float: left;
	padding-top: 15px;
	font-size: 16px;
	margin-left: 55px;
}
.note-head-text span.edit{
	color: #3d8dfb;
	float: right;
	margin-right: 10px;
	font-size: 22px;
}
.note-head-text span.clock{
	color: #464646;
	float: right;
	margin-right: 40px;
	font-size: 23px;
	border-bottom: 3px solid red;
	padding-bottom: 3px;
}
.note-head-text span.date{
	color: #000000;
	font-size: 12px;
	position: absolute;
	top: 36px;
	left: 56px;
}
.item-wrap{
	padding-top: 80px;
	padding-right: 5%;
	width: 100%;
}
.input-wrap{
	margin-bottom: 120px;
}
.save-button{
	background: #3d8dfb;
	color: #fff;
	border: none;
	padding: 10px 0;
	width: 100%;
	border-radius: 50px;
	text-transform: uppercase;
	margin-top: 30px;
	margin-bottom: 30px;
	font-size: 13px;
	letter-spacing: 1px;
}
.half{
	display: inline-block;
	width: 50%;
	float: left;
}
.half .time-type{
	color:#000000;
	font-size: 12px;
}
.half > div{
	display: inline-block;width: 100%;
	padding:10px 0;
}
.half > div > span.time-label{
	display: inline-block;
	width: 30%;
	color: #000000;
	font-size: 12px;
	vertical-align: top;
}
.half > div > span.checkbox-wrap{
	display: inline-block;
	width: 60%;
}

</style>

<?php //$this->load->view('open'); ?>

<div class="note-head-wrap">
	<div class="note-head-arr"><span>X</span></div>
	<div class="note-head-text font-chg">Barber Chair<span class="date">5 Mar 2018</span><span class="edit"><span class="fa fa-edit"></span></span><span class="clock"><span class="fa fa-compass"></span></span></div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="row">

				<div class="item-wrap font-chg">
					<div class="half">
						<div class="time-type">AM</div>
						<div><span class="time-label">7</span><span class="checkbox-wrap"><input type="checkbox" id="chk1" checked></span></div>
						<div><span class="time-label">8</span><span class="checkbox-wrap"><input type="checkbox" id="chk2" checked></span></div>
						<div><span class="time-label">9</span><span class="checkbox-wrap"><input type="checkbox" id="chk3" checked></span></div>
						<div><span class="time-label">10</span><span class="checkbox-wrap"><input type="checkbox" id="chk4" checked></span></div>
						<div><span class="time-label">11</span><span class="checkbox-wrap"><input type="checkbox" id="chk5" checked></span></div>
					</div>
					<div class="half">
						<div class="time-type">PM</div>
						<div><span class="time-label">12</span><span class="checkbox-wrap"><input type="checkbox" id="chk6" checked></span></div>
						<div><span class="time-label">1</span><span class="checkbox-wrap"><input type="checkbox" id="chk7" checked></span></div>
						<div><span class="time-label">2:30</span><span class="checkbox-wrap"><input type="checkbox" id="chk8" checked></span></div>
						<div><span class="time-label">3</span><span class="checkbox-wrap"><input type="checkbox" id="chk9" checked></span></div>
						<div><span class="time-label">4</span><span class="checkbox-wrap"><input type="checkbox" id="chk10" checked></span></div>
						<div><span class="time-label">5</span><span class="checkbox-wrap"><input type="checkbox" id="chk11" checked></span></div>
						<div><span class="time-label">6</span><span class="checkbox-wrap"><input type="checkbox" id="chk12" checked></span></div>
						<div><span class="time-label">7</span><span class="checkbox-wrap"><input type="checkbox" id="chk13" checked></span></div>
						<div><span class="time-label">8</span><span class="checkbox-wrap"><input type="checkbox" id="chk14" checked></span></div>
						<div><span class="time-label">9</span><span class="checkbox-wrap"><input type="checkbox" id="chk15" checked></span></div>
						<div><span class="time-label">10</span><span class="checkbox-wrap"><input type="checkbox" id="chk16" checked></span></div>
					</div>
					<button class="save-button">Submit Schedule</button>
				</div>

			</div>
		</div>
	</div>
</div>

<script src="<?php echo base_url(); ?>/assets/js/ToggleSwitch.js"></script>
<script type="text/javascript">
jQuery(document).ready(function(){
	for(var i=1;i<=16;i++){
		jQuery("#chk"+i).toggleSwitch();
	}
});
</script>

<?php //$this->load->view('close'); ?>

<?php //$this->load->view('layouts/footer'); ?>

