<?php $this->load->view('public_header'); ?>
<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/ToggleSwitch.css">
<style >
.font-chg{
	font-family: 'Comfortaa', cursive;
}
.row{
	margin-left:0 !important;
}
.col-md-12 .row{
	padding-left:25px;
}
.note-head-wrap{
	position: absolute;
	top: 0;
	left: 0;
	right: 0;
	padding-bottom: 15px;
}
.note-head-arr{
	width: 55px;
	display: inline-block;
	float: left;
}
.note-head-arr span::before{
	content: '';
	border-right: 16px solid #0b0b0b;
	border-top: 14px  solid transparent;
	border-bottom: 14px  solid transparent;
	float: left;
	position: absolute;
	margin-top: 20px;
	margin-left: 20px;
}
.note-head-arr span::after{
	content: '';
	border-right: 16px solid #fff;
	border-top: 14px  solid transparent;
	border-bottom: 14px  solid transparent;
	float: left;
	position: absolute;
	margin-top: 20px;
	margin-left: 17px;
}
.note-head-text{
	width: calc(100% - 55px);
	text-transform: uppercase;
	color: #a1a1a1;
	display: inline-block;
	float: left;
	padding-top: 24px;
	font-size: 16px;
}
.item-wrap{
	padding-top: 130px;
	padding-right: 5%;
}
.item{
	margin-bottom:30px;
}
.check-box{

}
.check-label{
	font-size: 14px;
	color: #4a4a4a;
}
</style>

<?php //$this->load->view('open'); ?>

<div class="note-head-wrap">
	<div class="note-head-arr"><span>&nbsp;</span></div>
	<div class="note-head-text font-chg">Settings</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="row">


				

				<div class="item-wrap font-chg">
					<div class="item">
						<div class="check-box">
							<input type="checkbox" id="chk1" checked>
						</div>
						<div class="check-label">Email notifications</div>
					</div>

					<div class="item">
						<div class="check-box">
							<input type="checkbox" id="chk2">
						</div>
						<div class="check-label">Promotional emails</div>
					</div>

				</div>




			</div>
		</div>
	</div>
</div>

<script src="<?php echo base_url(); ?>/assets/js/ToggleSwitch.js"></script>
<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery("#chk1").toggleSwitch();
	jQuery("#chk2").toggleSwitch();
});
</script>

<?php //$this->load->view('close'); ?>

<?php //$this->load->view('layouts/footer'); ?>

