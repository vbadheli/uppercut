<?php $this->load->view('public_header'); ?>
<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/ToggleSwitch.css">
<style >
.font-chg{
	font-family: 'Comfortaa', cursive;
}
.row{
	margin-left:0 !important;
}
.col-md-12 .row{
	padding-left:25px;
}
.note-head-wrap{
	position: absolute;
	top: 0;
	left: 0;
	right: 0;
	padding-bottom: 15px;
}
.note-head-arr{
	width: 55px;
	display: inline-block;
	float: left;
}
.note-head-arr span::before{
	content: '';
	border-right: 16px solid #0b0b0b;
	border-top: 14px  solid transparent;
	border-bottom: 14px  solid transparent;
	float: left;
	position: absolute;
	margin-top: 20px;
	margin-left: 20px;
}
.note-head-arr span::after{
	content: '';
	border-right: 16px solid #fff;
	border-top: 14px  solid transparent;
	border-bottom: 14px  solid transparent;
	float: left;
	position: absolute;
	margin-top: 20px;
	margin-left: 17px;
}
.note-head-text{
	width: calc(100% - 55px);
	text-transform: uppercase;
	color: #a1a1a1;
	display: inline-block;
	float: left;
	padding-top: 14px;
	font-size: 16px;
}
.note-head-text span.icon{
	color: #000000;
	float: right;
	margin-right: 20px;
	margin-top: 5px;
}
.note-head-text span.date{
	display: inline-block;
	width: 100%;
	color: #000000;
	font-size: 12px;
}
.item-wrap{
	padding-top: 100px;
	padding-right: 5%;
	width: 100%;
}
.item{
	display: inline-block;
	width: 100%;
	border-bottom: 2px dashed #ccc;
	padding:10px 0 20px 0;
}
.item .item-label{
	display: inline-block;
	width: 75%;
	float: left;
	color: #000000;
	text-transform: uppercase;
	margin-bottom: 15px;
}
.item .item-label span{
	color:#ffa402;
	display: inline-block;
	width: 100%;
	text-transform: none;
	margin-top: 10px;
	line-height: 20px;
}
.item .item-chk{
	display: inline-block;
	width: 24%;
	float: left;
}
.timeline-item{
	display: inline-block;
	width: 100%;
	padding:10px 0;
}
.status-img{
	display: inline-block;
	width: 35px;
	float: left;
}
.status-img span{
	color:#1bb015;
}
.status-label{
	display: inline-block;
	width: calc(100% - 35px);
	float: left;
}

</style>

<?php //$this->load->view('open'); ?>

<div class="note-head-wrap">
	<div class="note-head-arr"><span>&nbsp;</span></div>
	<div class="note-head-text font-chg">Barber Chair<span class="icon"><span class="fa fa-plus"></span></span>
		<span class="date">5 Mar 2018</span></div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="row">

				<div class="item-wrap font-chg">
					<div class="item">
						<div class="item-label">
							Chair 1
						</div> 
						<div class="item-chk">
							<input type="checkbox" id="chk1" checked>
						</div>

						<div class="timeline-item">
							<div class="status-img"><span class="fa fa-check"></span></div>
							<div class="status-label">Today <a href="#">Edit</a></div>
						</div>
						<div class="timeline-item">
							<div class="status-img">&nbsp;</div>
							<div class="status-label">Tomorrow <a href="#">Edit</a></div>
						</div>

					</div>
				</div>




			</div>
		</div>
	</div>
</div>

<script src="<?php echo base_url(); ?>/assets/js/ToggleSwitch.js"></script>
<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery("#chk1").toggleSwitch();
});
</script>

<?php //$this->load->view('close'); ?>

<?php //$this->load->view('layouts/footer'); ?>

