<?php $this->load->view('public_header'); ?>
<style >
.font-chg{
	font-family: 'Comfortaa', cursive;
}
.row{
	margin-left:0 !important;
}
.col-md-12 .row{
	padding-left:25px;
}
.note-head-wrap{
	position: absolute;
	top: 0;
	left: 0;
	right: 0;
	padding-bottom: 15px;
}
.note-head-arr{
	width: 55px;
	display: inline-block;
	float: left;
}
.note-head-arr span::before{
	content: '';
	border-right: 16px solid #0b0b0b;
	border-top: 14px  solid transparent;
	border-bottom: 14px  solid transparent;
	float: left;
	position: absolute;
	margin-top: 20px;
	margin-left: 20px;
}
.note-head-arr span::after{
	content: '';
	border-right: 16px solid #fff;
	border-top: 14px  solid transparent;
	border-bottom: 14px  solid transparent;
	float: left;
	position: absolute;
	margin-top: 20px;
	margin-left: 17px;
}
.note-head-text{
	width: calc(100% - 55px);
	text-transform: uppercase;
	color: #a1a1a1;
	display: inline-block;
	float: left;
	padding-top: 24px;
	font-size: 16px;
}
.note-head-text span{
	color: #000000;
	float: right;
	margin-right: 10px;
}
.item-wrap{
	padding-top: 70px;
	padding-right: 5%;
	width: 100%;
}
.item{
	display: inline-block;
	width: 100%;
	border-bottom: 2px dashed #ccc;
	padding:10px 0;
}
input{
	width:100%;
	border:none;
	color:#aeaeae;
	border-bottom: 2px solid #aeaeae;
	padding:10px 0;
	margin-bottom: 25px;
}
.save-button{
	width: 100%;
	padding:10px;
	text-align: center;
	background-color: #3d8dfb;
	color: #fff;
	border:none;
	border-radius: 50px;
	margin-top: 40px;
}
.cancel-btn{
	display: inline-block;
	width: 100%;
	padding:9px;
	text-align: center;
	border:1px solid #3d8dfb;
	color: #3d8dfb;
	border-radius: 50px;
	margin-top: 15px;
}

</style>

<?php //$this->load->view('open'); ?>

<div class="note-head-wrap">
	<div class="note-head-arr"><span>&nbsp;</span></div>
	<div class="note-head-text font-chg">Barber Chair</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="row">

				<div class="item-wrap font-chg">
					<input type="text" placeholder="Account number">
					<input type="text" placeholder="Bank name">
					<input type="text" placeholder="IFSC code">
					<input type="text" placeholder="Account holder's name">
					<button class="save-button">Save</button>
					<a href="#" class="cancel-btn">Cancel</a>
				</div>




			</div>
		</div>
	</div>
</div>

<?php //$this->load->view('close'); ?>

<?php //$this->load->view('layouts/footer'); ?>

