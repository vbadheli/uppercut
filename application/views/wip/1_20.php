<?php $this->load->view('public_header'); ?>
<style >
.font-chg{
	font-family: 'Comfortaa', cursive;
}
.row{
	margin-left:0 !important;
}
.col-md-12 .row{
	padding-left:25px;
}
.note-head-wrap{
	position: absolute;
	top: 0;
	left: 0;
	right: 0;
	padding-bottom: 15px;
}
.note-head-arr{
	width: 55px;
	display: inline-block;
	float: left;
}
.note-head-arr span::before{
	content: '';
	border-right: 16px solid #0b0b0b;
	border-top: 14px  solid transparent;
	border-bottom: 14px  solid transparent;
	float: left;
	position: absolute;
	margin-top: 20px;
	margin-left: 20px;
}
.note-head-arr span::after{
	content: '';
	border-right: 16px solid #fff;
	border-top: 14px  solid transparent;
	border-bottom: 14px  solid transparent;
	float: left;
	position: absolute;
	margin-top: 20px;
	margin-left: 17px;
}
.note-head-text{
	width: calc(100% - 55px);
	text-transform: uppercase;
	color: #a1a1a1;
	display: inline-block;
	float: left;
	padding-top: 24px;
	font-size: 16px;
}
.item-wrap{
	padding-top: 100px;
	padding-right: 5%;
}
.item{
	border-bottom: 2px dashed #999999;
	margin-bottom:30px;
}
.my-badge-wrap{
	display: inline-block;
	width: 100%;
}
.my-badge{
	background: #ff3f2e;
	color: #fff;
	padding: 4px 18px 5px 10px;
	border-radius: 6px 0px 0px 6px;
	font-size: 14px;
	float: left;
	position: relative;
}
.my-badge::after{
	content: '';
	float: right;
	position: absolute;
	border-right: 18px solid white;
	border-top: 15px solid #ff3f2e;
	border-bottom: 15px solid #ff3f2e;
	top: 0;
	border-radius: 0px 0px 0px 0px;
	margin-left: 15px;
}
.my-badge.pro{background:#e6a105;}
.my-badge.pro::after{border-top-color:#e6a105;border-bottom-color:#e6a105;}
.my-badge.mem{background:#ff3f2e;}
.my-badge.mem::after{border-top-color:#ff3f2e;border-bottom-color:#ff3f2e;}
.my-badge.book{background:#70b12d;}
.my-badge.book::after{border-top-color:#70b12d;border-bottom-color:#70b12d;}

.time{
	float: right;
	color:#999999;
	font-size: 12px;
}
.item-desc{
	color: #464646;
	font-size: 14px;
	padding: 5px 0;
	display: inline-block;
	width: 100%;
	line-height: 25px;
}
</style>

<?php //$this->load->view('open'); ?>

<div class="note-head-wrap">
	<div class="note-head-arr"><span>&nbsp;</span></div>
	<div class="note-head-text font-chg">Notification</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="row">


				

				<div class="item-wrap font-chg">
					<div class="item">
						<div class="my-badge-wrap">
							<span class="my-badge mem">Membership</span>
							<span class="time">1 hr ago</span>
						</div>
						<div class="item-desc">PRO membership activated</div>
					</div>

					<div class="item">
						<div class="my-badge-wrap">
							<span class="my-badge book">Booking</span>
							<span class="time">1 hr ago</span>
						</div>
						<div class="item-desc">New booking</div>
					</div>

					<div class="item">
						<div class="my-badge-wrap">
							<span class="my-badge pro">Promotion</span>
							<span class="time">1 hr ago</span>
						</div>
						<div class="item-desc">Launching service booking feature. Activate the membership</div>
					</div>
				</div>




			</div>
		</div>
	</div>
</div>

<?php //$this->load->view('close'); ?>

<?php //$this->load->view('layouts/footer'); ?>

