<?php $this->load->view('public_header'); ?>
<style >
.font-chg{
	font-family: 'Comfortaa', cursive;
}
.row{
	margin-left:0 !important;
}
.col-md-12 .row{
	padding-left:25px;
}
.note-head-wrap{
	position: absolute;
	top: 0;
	left: 0;
	right: 0;
	padding-bottom: 15px;
}
.note-head-arr{
	width: 55px;
	display: inline-block;
	float: left;
}
.note-head-arr span::before{
	content: '';
	border-right: 16px solid #0b0b0b;
	border-top: 14px  solid transparent;
	border-bottom: 14px  solid transparent;
	float: left;
	position: absolute;
	margin-top: 20px;
	margin-left: 20px;
}
.note-head-arr span::after{
	content: '';
	border-right: 16px solid #fff;
	border-top: 14px  solid transparent;
	border-bottom: 14px  solid transparent;
	float: left;
	position: absolute;
	margin-top: 20px;
	margin-left: 17px;
}
.note-head-text{
	width: calc(100% - 55px);
	text-transform: uppercase;
	color: #a1a1a1;
	display: inline-block;
	float: left;
	padding-top: 24px;
	font-size: 16px;
}
.note-head-withdraw{
	position: absolute;
	right: 3%;
	top: 24px;
	background: #000000;
	color: #fff;
	padding: 5px 10px;
	text-transform: uppercase;
	font-size: 10px;
	letter-spacing: 3px;
}
.item-wrap{
	padding-top: 80px;
	padding-right: 5%;
}
.item{
	border-bottom: 2px dashed #999999;
	width: 100%;
	display: inline-block;
	margin-bottom: 30px;
	padding-bottom: 10px;
}
.item .amount{
	width: 60px;
	display: inline-block;
	color: #2b2b2b;
	font-size: 16px;
	margin-bottom: 6px;
}
.item .my-badge-wrap{
	width: 70px;
	display: inline-block;
}
.item .my-badge{
	padding: 5px 10px;
	border-radius: 11px;
	font-size: 8px;
	text-transform: uppercase;
	color: #fff;
	letter-spacing: 2px;
	vertical-align: middle;
}
.item .my-badge.debit{
	background: #ff3f2e;
}
.item .my-badge.credit{
	background: #70b12d;
}
.item .date{
	width: calc(100% - 140px);
	display: inline-block;
	text-align: right;
	color: #999999;
	font-size: 12px;
}
.item p{
	margin: 0;
	line-height: 22px;
	font-size: 13px;
	color: #464646;
}
.stats{
	margin-bottom: 20px;
}
.stats-top{
	font-size: 12px;
	color:#a4a4a4;
}
.stats-bottom{
	border: 1px solid #b9b9b9;
	border-radius: 40px;
	display: inline-block;
	margin-left: -35px;
	padding: 8px 10px 8px 30px;
	margin-bottom: 40px;
}
.stats-block{
	display: inline-block;
	width: 33.3333%;
	float: left;
}
.stats-block .figur{
	display: inline-block;
	width: 100%;
	color:#292929;
	font-size: 18px;
}
.stats-block .desc{
	display: inline-block;
	width: 100%;
	color:#a4a4a4;
	font-size: 12px;
}
</style>

<?php //$this->load->view('open'); ?>

<div class="note-head-wrap">
	<div class="note-head-arr"><span>&nbsp;</span></div>
	<div class="note-head-text font-chg">Transaction</div>
	<span class="note-head-withdraw font-chg">Withdraw</span>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="row">

				<div class="item-wrap font-chg">
					<div class="stats">
						<div class="stats-top">
							Life time
						</div>
						<div class="stats-bottom">
							<div class="stats-block">
								<div class="figur">50,000</div>
								<div class="desc">Revenue</div>
							</div>
							<div class="stats-block">
								<div class="figur">20K</div>
								<div class="desc">Bookings</div>
							</div>
							<div class="stats-block">
								<div class="figur">4</div>
								<div class="desc">Cancellation</div>
							</div>
						</div>

						<div class="item">
							<div class="amount">₹ 3,500</div>
							<div class="my-badge-wrap">
								<span class="my-badge debit">Debit</span>
							</div>
							<div class="date"> 5 Jan 2018</div>
							<p>Membership activation</p>
							<p>PRO Membership activation</p>
						</div>

						<div class="item">
							<div class="amount">₹ 500</div>
							<div class="my-badge-wrap">
								<span class="my-badge credit">Credit</span>
							</div>
							<div class="date"> 15 Dec 2017</div>
							<p>PRO Membership refund</p>
						</div>

						<div class="item">
							<div class="amount">₹ 3,500</div>
							<div class="my-badge-wrap">
								<span class="my-badge debit">Debit</span>
							</div>
							<div class="date"> 25 Nov 2017</div>
							<p>Membership activation</p>
							<p>PRO Membership</p>
						</div>

						<div class="item">
							<div class="amount">₹ 500</div>
							<div class="my-badge-wrap">
								<span class="my-badge credit">Credit</span>
							</div>
							<div class="date"> 15 Dec 2017</div>
							<p>Men's Hair cut</p>
						</div>

						<div class="item">
							<div class="amount">₹ 3,500</div>
							<div class="my-badge-wrap">
								<span class="my-badge debit">Debit</span>
							</div>
							<div class="date"> 25 Nov 2017</div>
							<p>Women's Hair cut</p>
						</div>

					</div>
				</div>

			</div>
		</div>
	</div>
</div>

<?php //$this->load->view('close'); ?>

<?php //$this->load->view('layouts/footer'); ?>

