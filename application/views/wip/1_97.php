<?php $this->load->view('public_header'); ?>
<style>
.font-chg{
	font-family: 'Comfortaa', cursive;
}
.row{
	margin-left:0 !important;
}
.col-md-12 .row{
	padding-left:25px;
}
.note-head-wrap{
	position: absolute;
	top: 0;
	left: 0;
	right: 0;
	padding-bottom: 15px;
}
.note-head-arr{
	width: 55px;
	display: inline-block;
	float: left;
}
.note-head-arr span{
	border: 2px solid #272727;
	display: inline-block;
	width: 30px;
	height: 12px;
	border-left: none;
	border-right: none;
	margin: 28px 0 0 12px;
}
.note-head-text{
	width: calc(100% - 55px);
	text-transform: uppercase;
	color: #a1a1a1;
	display: inline-block;
	float: left;
	padding-top: 24px;
	font-size: 16px;
}
.note-head-text span.one{
	position: absolute;
	right: 10px;
	font-size: 20px;
	color: #000000;
	top: 22px;
}
.note-head-text span.two{
	position: absolute;
	right: 12px;
	font-size: 16px;
	color: #fff;
	top: 24px;
}
.note-head-text span.notif{
	color: #fff;
	background: red;
	border-radius: 50%;
	font-size: 8px;
	padding: 3px;
	position: absolute;
	right: 5px;
	top: 18px;
	border: none;
}
.item-wrap{
	padding-top:300px;
	padding-right: 5%;
	width: 100%;
}
.success-msg{
	color: #000000;
	background: #d3fb00;
	position: absolute;
	left: -15px;
	right: 0;
	padding: 20px 15px 20px 55px;
	font-size: 12px;
	top: 75px;
}
.success-msg p{
	font-size: 16px;
	margin: 0;
}
.success-msg p:first-child{
	margin-top: 10px;
}
.success-msg .accept-button{
	color: #fff;
	background: #000000;
	padding: 7px;
	width: calc(50% - 5px);
	margin-top: 20px;
	border: none;
	text-transform: uppercase;
	letter-spacing: 1px;
	font-size: 10px;
	border-radius: 7px;
	margin-right: 10px;
	float: left;
}
.success-msg .reject-button{
	color: #000000;
	background: transparent;
	padding: 6px;
	width: calc(50% - 5px);
	margin-top: 20px;
	border: none;
	text-transform: uppercase;
	letter-spacing: 1px;
	font-size: 10px;
	border-radius: 7px;
	float: left;
	border:1px solid #000000;
}
.stats{
	margin-bottom: 25px;
}
.stats-top{
	font-size: 12px;
	color:#a4a4a4;
}
.stats-bottom{
	border: 1px solid #b9b9b9;
	border-radius: 40px;
	display: inline-block;
	margin-left: -35px;
	padding:5px 10px 5px 30px;
	width: calc(100% + 35px);
}
.stats-block{
	display: inline-block;
	width: 33.3333%;
	float: left;
}
.stats-block .figur{
	display: inline-block;
	width: 100%;
	color:#292929;
	font-size: 14px;
}
.stats-block .desc{
	display: inline-block;
	width: 100%;
	color:#a4a4a4;
	font-size: 12px;
}
.search-otp-label{
	margin-top: 10px;
	color: #000000;
	font-size: 13px;
	margin-bottom: 10px;
}
.or-label{
	margin-top: 10px;
	color: #000000;
	font-size: 13px;
	margin-bottom: 10px;
}
.otp-input-wrap{
	display: inline-block;
	width: 100%;
}
.otp-input-wrap input, .otp-input-wrap button{
	display: inline-block;
	float: left;
	height: 50px;
	width: calc(20% - 10px);
	border:1px solid #8e8e8e;
	border-radius: 5px;
	margin-right: 10px;
	text-align: center;
}
.otp-input-wrap button{
	background: #000000;
	color: #fff;
	border-color:#000000;
}
.phone-wrap input{
	display: inline-block;
	float: left;
	height: 40px;
	width: calc(80% - 10px);
	border:1px solid #8e8e8e;
	border-radius: 5px;
	margin-right: 10px;
	text-align: center;
	padding:0px 10px;
}
.phone-wrap button{
	display: inline-block;
	float: left;
	height: 40px;
	width: calc(20% - 10px);
	border:1px solid #8e8e8e;
	border-radius: 5px;
	background: #000000;
	color: #fff;
	border-color:#000000;
}
.icon-links{
	display: inline-block;
	width: 100%;
	margin-bottom: 30px;
}
.icon-links .section{
	padding-top: 30px;
	display: inline-block;
	width: 25%;
}
.icon-links .section .icon{
	text-align: left;
}
.icon-links .section .text{
	text-align: left;
	font-size: 11px;
	padding-top: 4px;
	text-transform: uppercase;
}
.gray-label{
	font-size: 14px;
	color:#cdcdcd;
	padding:4px 0;
}
.desk{
	font-size: 14px;
	color:#70b12d;
	text-transform: uppercase;
	margin-bottom: 20px;
}
.no-booking{
	font-size: 10px;
	margin-bottom: 30px;
	margin-top: 5px;
	color: #939393;
}


</style>

<!-- <?php //$this->load->view('open'); ?>  -->

<div class="container">
  <div class="">
          
        <!-- <div class="col-md-12">
          <div class="row">
            <div class="py-6 mx-auto">
	-->

<div class="note-head-wrap">
	<div class="note-head-arr"><span>&nbsp;</span></div>
	<div class="note-head-text font-chg">
		<img src="https://uppercut.shop/assets/img/logo.svg">
		<span class="one"><span class="fa fa-bell"></span></span>
		<span class="two"><span class="fa fa-bell"></span></span>
		<span class="notif">58</span>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="row">

				<div class="success-msg font-chg">
					Test Booking
					<p>Hair Cut</p>
					<p>June 5 (today)</p>
					<p>10 AM</p>
					<button class="accept-button">Accept</button>
					<button class="reject-button">Reject</button>
				</div>

				<div class="item-wrap font-chg">
					<div class="stats">
						<div class="stats-top">
							Today
						</div>
						<div class="stats-bottom">
							<div class="stats-block">
								<div class="figur">50,000</div>
								<div class="desc">Revenue</div>
							</div>
							<div class="stats-block">
								<div class="figur">20K</div>
								<div class="desc">Bookings</div>
							</div>
							<div class="stats-block">
								<div class="figur">5</div>
								<div class="desc">Chairs</div>
							</div>
						</div>
					</div>
					<div class="search-otp-label">Search booking with OTP</div>
					<div class="otp-input-wrap">
						<input type="text">
						<input type="text">
						<input type="text">
						<input type="text">
						<button><span class="fa fa-search"></span></button>
					</div>
					<div class="or-label">(or)</div>
					<div class="phone-wrap">
						<input type="text" placeholder="Search with phone number">
						<button><span class="fa fa-search"></span></button>
					</div>
					<div class="icon-links">
						<div class="section">
							<div class="icon"><?php echo str_repeat('&nbsp;',2); ?><span class="fa fa-asterisk"></span></div>
							<div class="text">Chair</div>
						</div>
						<div class="section">
							<div class="icon"><?php echo str_repeat('&nbsp;',4); ?><span class="fa fa-list-alt"></span></div>
							<div class="text">Bookings</div>
						</div>
						<div class="section">
							<div class="icon"><?php echo str_repeat('&nbsp;',4); ?><span class="fa fa-comment"></span></div>
							<div class="text">Feedback</div>
						</div>
					</div>
					<div class="gray-label">Barber chair slots</div>
					<div class="desk">Desk 1</div>
					<div class="gray-label">On-going bookings</div>
					<div class="no-booking">No booking yet</div>
				</div>

			</div>
		</div>
	</div>
</div>

</div>
</div>
</div>
</div>
</div>


<?php //$this->load->view('close'); ?>

<?php //$this->load->view('layouts/footer'); ?>

