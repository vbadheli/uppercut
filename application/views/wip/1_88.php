<?php $this->load->view('public_header'); ?>
<style >
.font-chg{
	font-family: 'Comfortaa', cursive;
}
.row{
	margin-left:0 !important;
}
.col-md-12 .row{
	padding-left:25px;
}
.note-head-wrap{
	position: absolute;
	top: 0;
	left: 0;
	right: 0;
	padding-bottom: 15px;
}
.note-head-arr{
	width: 55px;
	display: inline-block;
	float: left;
}
.note-head-arr span{
	border: 2px solid #272727;
	display: inline-block;
	width: 30px;
	height: 12px;
	border-left: none;
	border-right: none;
	margin: 28px 0 0 12px;
}
.note-head-text{
	width: calc(100% - 55px);
	text-transform: uppercase;
	color: #a1a1a1;
	display: inline-block;
	float: left;
	padding-top: 24px;
	font-size: 16px;
}
.note-head-text span.one{
	position: absolute;
	right: 10px;
	font-size: 20px;
	color: #000000;
	top: 22px;
}
.note-head-text span.two{
	position: absolute;
	right: 12px;
	font-size: 16px;
	color: #fff;
	top: 24px;
}
.item-wrap{
	padding-top: 90px;
	padding-right: 5%;
	width: 100%;
}
.item{
	display: inline-block;
	width: 100%;
	border-bottom: 2px dotted #ccc;
	padding: 10px 55px 4px 0;
	margin-bottom: 10px;
	position: relative;
}
.item .carot{
	position: absolute;
	right: 5px;
	top: 18%;
}
.item .carot::after{
	content: '';
	border-left: 16px solid #fff;
	border-top: 14px  solid transparent;
	border-bottom: 14px  solid transparent;
	float: right;
	position: absolute;
	margin-top: 20px;
	right: 16px;
}
.item .carot::before{
	content: '';
	border-left: 16px solid #0b0b0b;
	border-top: 14px  solid transparent;
	border-bottom: 14px  solid transparent;
	float: right;
	position: absolute;
	margin-top: 20px;
	right: 15px;
}
.item .title{
	font-weight: bold;
	font-size: 16px;
	color:#000000;
}
.item .desc{
	color: #999999;
	font-size: 15px;
	line-height: 22px;
}
.item.active .title{
	color: #999999;
}
.item.active .carot{
	display: none;
}
.item.active span.fa{
	color: #70b12d;
	position: absolute;
	left: -35px;
	font-size: 25px;
}
.account-msg{
	color:#000000;
	margin-bottom: 20px;
	font-size: 18px;
}



</style>

<?php //$this->load->view('open'); ?>

<div class="note-head-wrap">
	<div class="note-head-arr"><span>&nbsp;</span></div>
	<div class="note-head-text font-chg">
		<img src="https://uppercut.shop/assets/img/logo.svg">
		<span class="one"><span class="fa fa-bell"></span></span>
		<span class="two"><span class="fa fa-bell"></span></span>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="row">

				<div class="item-wrap font-chg">
					<div class="account-msg">Let's setup your account</div>
					<div class="item active">
						<span><span class="fa fa-check-circle"></span></span>
						<div class="title">Salon Profile</div>
						<div class="desc">Customer search for a salon, your salon profile will be displayed</div>
						<span class="carot">&nbsp;</span>
					</div>
					<div class="item">
						<div class="title">Barber Chair</div>
						<div class="desc">Chairs are your resource to provide a service. Booking is based on chairs.</div>
						<span class="carot">&nbsp;</span>
					</div>
					<div class="item">
						<div class="title">Chair time slot</div>
						<div class="desc">Booking is based on availability of slots everyday</div>
						<span class="carot">&nbsp;</span>
					</div>
					<div class="item">
						<div class="title">Bank Account</div>
						<div class="desc">Provide your salons band account to withdraw funds</div>
						<span class="carot">&nbsp;</span>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>

<?php //$this->load->view('close'); ?>

<?php //$this->load->view('layouts/footer'); ?>

