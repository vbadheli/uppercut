<?php $this->load->view('public_header'); ?>
<style >
.font-chg{
	font-family: 'Comfortaa', cursive;
}
.row{
	margin-left:0 !important;
}
.col-md-12 .row{
	padding-left:25px;
}
.note-head-wrap{
	position: absolute;
	top: 0;
	left: 0;
	right: 0;
	padding-bottom: 15px;
}
.note-head-arr{
	width: 55px;
	display: inline-block;
	float: left;
}
.note-head-arr span::before{
	content: '';
	border-right: 16px solid #0b0b0b;
	border-top: 14px  solid transparent;
	border-bottom: 14px  solid transparent;
	float: left;
	position: absolute;
	margin-top: 20px;
	margin-left: 20px;
}
.note-head-arr span::after{
	content: '';
	border-right: 16px solid #fff;
	border-top: 14px  solid transparent;
	border-bottom: 14px  solid transparent;
	float: left;
	position: absolute;
	margin-top: 20px;
	margin-left: 17px;
}
.note-head-text{
	width: calc(100% - 55px);
	text-transform: uppercase;
	color: #a1a1a1;
	display: inline-block;
	float: left;
	padding-top: 24px;
	font-size: 16px;
}
.item-wrap{
	padding-top: 75px;
	padding-right: 5%;
	width: 100%;
}
.reason{
	color:#676767;
	font-size: 14px;
	margin-bottom: 30px;
}
a.ignore-button{
	display: inline-block;
	width: 100%;
	padding:15px 0;
	border-radius: 50px;
	color:#fff;
	background: #70b12d;
	text-transform: uppercase;
	font-size: 12px;
	text-align: center;
	text-decoration: none;
	border:none;
	margin-top: 150px;
}
button.cancel-button{
	display: inline-block;
	width: 100%;
	padding:15px 0;
	border-radius: 50px;
	color:#fff;
	background: #ff5050;
	text-transform: uppercase;
	font-size: 12px;
	text-align: center;
	text-decoration: none;
	border:none;
	margin-top: 15px;
}
.combo{
	width: 100%;
	border:none;
	border-bottom: 2px solid #ccc;
	padding:5px 0;
	color:#a4a4a4;
}

.service-combo-wrap{
	position: relative;
}
.service-combo-wrap::after{
	content: '';
	width: 20px;
	background: #fff;
	position: absolute;
	right: 0;
	bottom: 2px;
	height: 35px;
}
.service-combo-wrap > span{
	position: absolute;
	right: 0;
	top: 6px;
	z-index: 1;
}
</style>

<?php //$this->load->view('open'); ?>

<div class="note-head-wrap">
	<div class="note-head-arr"><span>&nbsp;</span></div>
	<div class="note-head-text font-chg">Cancel Booking</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="row">


				

				<div class="item-wrap font-chg">
					<div class="reason">Alrighty, before you go, please let us know the reason. Your feedback will help us improve our service.</div>
					<div class="service-combo-wrap">
						<select class="combo">
							<option>Select a reason</option>
							<option>reason 1</option>
							<option>reason 2</option>
							<option>reason 3</option>
							<option>reason 4</option>
						</select>
						<span><span class="fa fa-angle-down"></span></span>
					</div>

					<a href="#" class="ignore-button">I don't wanna cancel my booking</a>
					<button class="cancel-button">I wanna cancel my booking</button>
					
				</div>




			</div>
		</div>
	</div>
</div>

<?php //$this->load->view('close'); ?>

<?php //$this->load->view('layouts/footer'); ?>

