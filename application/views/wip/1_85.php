<?php $this->load->view('public_header'); ?>
<style >
.font-chg{
	font-family: 'Comfortaa', cursive;
}
.row{
	margin-left:0 !important;
}
.col-md-12 .row{
	padding-left:25px;
}
.note-head-wrap{
	position: absolute;
	top: 0;
	left: 0;
	right: 0;
	padding-bottom: 15px;
}
.note-head-arr{
	width: 55px;
	display: inline-block;
	float: left;
}
.note-head-arr span{
	border: 2px solid #272727;
	display: inline-block;
	width: 30px;
	height: 12px;
	border-left: none;
	border-right: none;
	margin: 28px 0 0 12px;
}
.note-head-text{
	width: calc(100% - 55px);
	text-transform: uppercase;
	color: #a1a1a1;
	display: inline-block;
	float: left;
	padding-top: 24px;
	font-size: 16px;
}
.note-head-text span{
	color: #0183cb;
	float: right;
	margin-right: 15px;
	text-transform: none;
}
.item-wrap{
	padding-top: 80px;
	padding-right: 5%;
	width: 100%;
}
.item{
	display: inline-block;
	width: 100%;
	border-bottom: 2px dashed #ccc;
	padding: 10px 0 10px 0;
	margin-bottom: 10px;
}
.item .title{
	font-weight: bold;
	font-size: 18px;
	color:#000000;
}
.item .price{
	color:#464646;
}
.item .price span{
	color: #ff3f2e;
	text-transform: uppercase;
}
.item .desc{
	color: #999999;
	margin-top: 10px;
	font-size: 15px;
}
.item .circle{
	border: 1px solid #ccc;
	width: 31px;
	border-radius: 51%;
	height: 31px;
	display: inline-block;
	margin-left: -42px;
	margin-bottom: -35px;
}
.item .circle.active{
	border-color:#d4e7c0;
	position: relative;
}
.item .circle.active::after{
	content: 'Active';
	color: #70b12d;
	position: absolute;
	left: -5px;
	top: 31px;
	font-size: 10px;
	text-transform: uppercase;
}
.item .circle.active span.fa{
	color:#d4e7c0;
	transform: translate(40%,40%);
}
.next-button{
	width: 100%;
	color: #fff;
	background:#3d8dfb;
	padding:10px;
	text-transform: uppercase;
	text-align: center;
	border:none;
	border-radius: 50px;
	margin-top: 30px;
	margin-bottom: 30px;
}

</style>

<?php //$this->load->view('open'); ?>

<div class="note-head-wrap">
	<div class="note-head-arr"><span>&nbsp;</span></div>
	<div class="note-head-text font-chg">Membership<span>skip</span></div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="row">

				<div class="item-wrap font-chg">
					<div class="item">
						<span class="circle active"><span class="fa fa-check"></span></span>
						<div class="title">Starter</div>
						<div class="price"><span>Free</span> for lifetime</div>
						<div class="desc">One salon profile</div>
					</div>
					<div class="item">
						<span class="circle">&nbsp;</span>
						<div class="title">Booking</div>
						<div class="price"><span>₹ 999</span> one salon per year</div>
						<div class="desc">Accept booking from customers at 19% commission per booking</div>
					</div>
					<div class="item">
						<span class="circle">&nbsp;</span>
						<div class="title">Analytics</div>
						<div class="price"><span>₹ 499</span> per salon for an year</div>
						<div class="desc">Page visit counter<br>Phone click counter</div>
					</div>
					<button class="next-button">Next</button>
				</div>

			</div>
		</div>
	</div>
</div>

<?php //$this->load->view('close'); ?>

<?php //$this->load->view('layouts/footer'); ?>

