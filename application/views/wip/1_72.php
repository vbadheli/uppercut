<?php $this->load->view('public_header'); ?>
<style >
.font-chg{
	font-family: 'Comfortaa', cursive;
}
.row{
	margin-left:0 !important;
}
.col-md-12 .row{
	padding-left:25px;
}
.note-head-wrap{
	position: absolute;
	top: 0;
	left: 0;
	right: 0;
	padding-bottom: 15px;
}
.note-head-arr{
	width: 55px;
	display: inline-block;
	float: left;
}
.note-head-arr span::before{
	content: '';
	border-right: 16px solid #0b0b0b;
	border-top: 14px  solid transparent;
	border-bottom: 14px  solid transparent;
	float: left;
	position: absolute;
	margin-top: 20px;
	margin-left: 20px;
}
.note-head-arr span::after{
	content: '';
	border-right: 16px solid #fff;
	border-top: 14px  solid transparent;
	border-bottom: 14px  solid transparent;
	float: left;
	position: absolute;
	margin-top: 20px;
	margin-left: 17px;
}
.note-head-text{
	width: calc(100% - 55px);
	text-transform: uppercase;
	color: #a1a1a1;
	display: inline-block;
	float: left;
	padding-top: 24px;
	font-size: 16px;
}
.item-wrap{
	padding-top: 80px;
	padding-right: 5%;
	width: 100%;
}
.booking-request-wrap{
	width: 100%;
	margin-top: 250px;
}
.booking-request-wrap a{
	background: #e5e5e5;
	color:#fff;
	border-radius: 50px;
	display: inline-block;
	width: 100%;
	padding:13px 10px;
	text-align: center;
	text-transform: uppercase;
	font-size: 14px;
	letter-spacing: 2px;
}
.combo{
	width: 100%;
	border:none;
	border-bottom: 2px solid #ccc;
	padding:5px 0;
	color:#a4a4a4;
}

.service-combo-wrap{
	position: relative;
}
.service-combo-wrap::after{
	content: '';
	width: 20px;
	background: #fff;
	position: absolute;
	right: 0;
	bottom: 2px;
	height: 35px;
}
.service-combo-wrap > span{
	position: absolute;
	right: 0;
	top: 6px;
	z-index: 1;
}
.date-wrap{
	display: inline-block;
	width: 100%;
	margin-top: 30px;
	position: relative;
}
.date-wrap .date-label{
	color: #cccccc;
	transform: rotate(-90deg);
	display: inline-block;
	margin-left: -46px;
	top: 13px;
	position: absolute;
	font-size: 14px;
}
.date-wrap .week-day-wrap{
	display: inline-block;
	width: 100%;
}
.date-wrap .week-day-wrap span{
	display: inline-block;
	width: 15.99%;
	float: left;
	color:#7f7f7f;
	font-size: 16px;
	text-align: center;
}
.date-wrap .week-day-wrap span:first-child{
	text-align: left;
	width: 10%;
}
.date-wrap .week-day-wrap span:last-child{
	text-align: right;
	width: 10%;
}
.date-wrap .week-day-wrap span.disabled{
	color:#e0e0e0;
}

.date-wrap .day-num-wrap{
	display: inline-block;
	width: 100%;
}
.date-wrap .day-num-wrap span{
	display: inline-block;
	width: 15.99%;
	float: left;
	color:#000000;
	font-size: 16px;
	text-align: center;
	text-decoration: underline;
}
.date-wrap .day-num-wrap span:first-child{
	text-align: left;
	width: 10%;
}
.date-wrap .day-num-wrap span:last-child{
	text-align: right;
	width: 10%;
}
.date-wrap .day-num-wrap span.disabled{
	color:#e0e0e0;
	text-decoration: none;
}
.date-wrap .day-num-wrap span.publish{
	color: #ffffff;
	background: #7fba41;
	text-decoration: none;
	border-radius: 50%;
	padding: 3px 0px 3px 9px;
	margin: 0px 6px 0 -6px;
}
.time-wrap{
	display: inline-block;
	width: 100%;
	margin-top: 30px;
	position: relative;
}
.time-wrap .time-label{
	color: #cccccc;
	transform: rotate(-90deg);
	display: inline-block;
	left: -46px;
	top: 13px;
	position: absolute;
	font-size: 14px;
}
.time-wrap span{
	display: inline-block;
	width: 30%;
	float: left;
	text-align: center;
	color: #000000;
	font-size: 15px;
	margin-bottom: 15px;
}
.time-wrap span:nth-child(4n+1){
	text-align: left;
	width: 20%;
}
.time-wrap span:nth-child(4n+4){
	text-align: right;
	width: 20%;
}
.time-wrap span.disabled{
	color:#cccccc;
}
</style>

<?php //$this->load->view('open'); ?>

<div class="note-head-wrap">
	<div class="note-head-arr"><span>&nbsp;</span></div>
	<div class="note-head-text font-chg">Service Booking</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="row">


				

				<div class="item-wrap font-chg">
					<div class="service-combo-wrap">
						<select class="combo">
							<option>Select a service</option>
							<option>Service 1</option>
							<option>Service 2</option>
							<option>Service 3</option>
							<option>Service 4</option>
						</select>
						<span><span class="fa fa-angle-down"></span></span>
					</div>
					<div class="date-wrap">
						<span class="date-label">Date</span>
						<div class="week-day-wrap">
							<span>M</span>
							<span>T</span>
							<span>W</span>
							<span>T</span>
							<span class="disabled">F</span>
							<span class="disabled">S</span>
							<span class="disabled">S</span>
						</div>
						<div class="day-num-wrap">
							<span>5</span>
							<span>6</span>
							<span>7</span>
							<span>8</span>
							<span class="disabled">9</span>
							<span class="disabled">10</span>
							<span class="disabled">11</span>
						</div>
					</div>
					<div class="booking-request-wrap">
						<a href="#">Send Booking Request</a>
					</div>
				</div>




			</div>
		</div>
	</div>
</div>


<?php //$this->load->view('close'); ?>

<?php //$this->load->view('layouts/footer'); ?>

