<?php $this->load->view('public_header'); ?>
<style >
.font-chg{
	font-family: 'Comfortaa', cursive;
}
.row{
	margin-left:0 !important;
}
.col-md-12 .row{
	padding-left:25px;
}
.note-head-wrap{
	position: absolute;
	top: 0;
	left: 0;
	right: 0;
	padding-bottom: 15px;
}
.note-head-text{
	width: calc(100% - 55px);
	text-transform: uppercase;
	color: #a1a1a1;
	display: inline-block;
	float: left;
	padding-top: 15px;
	padding-left: 55px;
	font-size: 16px;
}
.note-head-text span{
	display: inline-block;
	width: 100%;
	font-size: 12px;
	color:#3b3b3b;
}
.item-wrap{
	padding-top: 80px;
	padding-right: 5%;
}
.stat-section{
	display: inline-block;
	width: 50%;
	float: left;
}
.stat-label{
	font-size: 15px;
	color:#a2a2a2;
}
.stat-val{
	font-size: 14px;
	color:#272727;
}
.start-service{
	display:inline-block;
	width:100%;
	text-align: center;
	background: #000000;
	color: #fff !important;
	text-transform: uppercase;
	margin-top: 40px;
	padding:10px;
	text-decoration: none !important;
	letter-spacing: 2px;
}
.cancel-booking{
	display:inline-block;
	width:100%;
	text-align: center;
	background: #fff;
	color: #000000 !important;
	text-transform: uppercase;
	margin-top: 25px;
	padding:10px;
	text-decoration: none !important;
	letter-spacing: 2px;
	border:1px solid #000000;
}
</style>

<?php //$this->load->view('open'); ?>

<div class="note-head-wrap">
	<div class="note-head-text font-chg">Withdraw Funds</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="row">

				<div class="item-wrap font-chg">
					<div class="stat-section">
						<div class="stat-label">
							As of
						</div>
						<div class="stat-val">
							5 Apr 2018
						</div>
					</div>
					<div class="stat-section">
						<div class="stat-label">
							Revenue
						</div>
						<div class="stat-val">
							₹ 3,500
						</div>
					</div>
					<a href="#" class="start-service">Send Request</a>
					<a href="#" class="cancel-booking">Cancel</a>
				</div>

			</div>
		</div>
	</div>
</div>

<?php //$this->load->view('close'); ?>

<?php //$this->load->view('layouts/footer'); ?>

