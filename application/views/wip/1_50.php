<?php $this->load->view('public_header'); ?>
<style >
.font-chg{
	font-family: 'Comfortaa', cursive;
}
.row{
	margin-left:0 !important;
}
.col-md-12 .row{
	padding-left:25px;
}
.note-head-wrap{
	position: absolute;
	top: 0;
	left: 0;
	right: 0;
	padding-bottom: 15px;
}
.note-head-arr{
	width: 55px;
	display: inline-block;
	float: left;
}
.note-head-arr span::before{
	content: '';
	border-right: 16px solid #0b0b0b;
	border-top: 14px  solid transparent;
	border-bottom: 14px  solid transparent;
	float: left;
	position: absolute;
	margin-top: 20px;
	margin-left: 20px;
}
.note-head-arr span::after{
	content: '';
	border-right: 16px solid #fff;
	border-top: 14px  solid transparent;
	border-bottom: 14px  solid transparent;
	float: left;
	position: absolute;
	margin-top: 20px;
	margin-left: 17px;
}
.note-head-text{
	width: calc(100% - 55px);
	text-transform: uppercase;
	color: #a1a1a1;
	display: inline-block;
	float: left;
	padding-top: 24px;
	font-size: 16px;
}
.item-wrap{
	padding-top: 100px;
	padding-right: 5%;
	width: 100%;
}
.booking-request-wrap{
	width: 100%;
	margin-top: 300px;
}
.booking-request-wrap a{
	background: #e5e5e5;
	color:#fff;
	border-radius: 50px;
	display: inline-block;
	width: 100%;
	padding:10px 10px;
	text-align: center;
}
.combo{
	width: 100%;
	border:none;
	border-bottom: 2px solid #ccc;
	padding:5px 0;
	color:#a4a4a4;
}

.service-combo-wrap{
	position: relative;
}
.service-combo-wrap::after{
	content: '';
	width: 20px;
	background: #fff;
	position: absolute;
	right: 0;
	bottom: 2px;
	height: 35px;
}
.service-combo-wrap > span{
	position: absolute;
	right: 0;
	top: 6px;
	z-index: 1;
}
</style>

<?php //$this->load->view('open'); ?>

<div class="note-head-wrap">
	<div class="note-head-arr"><span>&nbsp;</span></div>
	<div class="note-head-text font-chg">Service Booking</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="row">


				

				<div class="item-wrap font-chg">
					<div class="service-combo-wrap">
						<select class="combo">
							<option>Select a service</option>
							<option>Service 1</option>
							<option>Service 2</option>
							<option>Service 3</option>
							<option>Service 4</option>
						</select>
						<span><span class="fa fa-angle-down"></span></span>
					</div>
					<div class="booking-request-wrap">
						<a href="#">Send Booking Request</a>
					</div>
				</div>




			</div>
		</div>
	</div>
</div>

<?php //$this->load->view('close'); ?>

<?php //$this->load->view('layouts/footer'); ?>

