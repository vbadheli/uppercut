<?php $this->load->view('public_header');  ?>
<style >
  .left-nav {
    position: fixed;
    overflow: hidden;
    width: 295px;
    z-index: 3333;
    height: 100%;
    /* background: rgb(247, 247, 247); */
    /* background: rgba(255, 0, 0, 0.26); */
   }
   .hide-nav .bottom-3 {
    
  }
  .height100
  {
    height: 100% !important;
  }
  .height10{
    height: 10%;
   }
  .bottom-3
  {
    bottom: 16%;
  }
  
  /* Thulasi CSS */
.h3-mod {
	font-size: 18px;
	color: grey;
	font-weight: 100;
	margin-top: 20px;
}  
  
.contact-p {
	padding-left: 25%;
	padding-right: 10px;
	font-size: 12px;
	color: #464646;
}

.heading-1 {
	font-size: 18px !important;
	font-weight: 700;
	letter-spacing: -0.5px;	
	margin-bottom: 15px;
	margin-top: 30%;
	color: black;
	
}

.body-text {
	font-size: 12px;
	color: #888 !important;
	font-weight: 100;
	line-height: 16px;
	margin-bottom: 5px;
	padding-right: 5%;
}

.heading-4 {
	font-size: 13px !important;
	font-weight: 500;
	letter-spacing: 0px;	
}

/* Thulasi CSS ends */
  
   @media only screen and (max-width: 900px) {
   .left-nav {
    position: fixed;
    width: 100%;
    z-index: 3333;
    height: 12%;
   }
   .height10{
    height: 14%;
   }
   .mx-auto {
   padding-top: 59px;
}
   }
</style>
<div class="container">
        <div class="row">
        
        
              <div class="mx-auto">
                <Br>
                
                <div class="form-body mx-auto contact-p">
              <h1 class="py-2 pb-1 heading-1">Terms & Conditions</h1>

<p class="body-text">These terms and conditions ("Terms", "Agreement") are an agreement between Website Operator ("Website Operator", "us", "we" or "our") and you ("User", "you" or "your"). This Agreement sets forth the general terms and conditions of your use of the www.uppercut.shop website and any of its products or services (collectively, "Website" or "Services").</p>

<h2 class="h3-mod">Accounts and membership</h2>

<p class="body-text">If you create an account on the Website, you are responsible for maintaining the security of your account and you are fully responsible for all activities that occur under the account and any other actions taken in connection with it. Providing false contact information of any kind may result in the termination of your account. You must immediately notify us of any unauthorized uses of your account or any other breaches of security. We will not be liable for any acts or omissions by you, including any damages of any kind incurred as a result of such acts or omissions. We may suspend, disable, or delete your account (or any part thereof) if we determine that you have violated any provision of this Agreement or that your conduct or content would tend to damage our reputation and goodwill. If we delete your account for the foregoing reasons, you may not re-register for our Services. We may block your email address and Internet protocol address to prevent further registration.</p>

<h2 class="h3-mod">User content</h2>

<p class="body-text">We do not own any data, information or material ("Content") that you submit on the Website in the course of using the Service. You shall have sole responsibility for the accuracy, quality, integrity, legality, reliability, appropriateness, and intellectual property ownership or right to use of all submitted Content. We may monitor Content on the Website submitted or created using our Services by you. Unless specifically permitted by you, your use of the Website does not grant us the license to use, reproduce, adapt, modify, publish or distribute the Content created by you or stored in your user account for commercial, marketing or any similar purpose. But you grant us permission to access, copy, distribute, store, transmit, reformat, display and perform the Content of your user account solely as required for the purpose of providing the Services to you. Without limiting any of those representations or warranties, we have the right, though not the obligation, to, in our own sole discretion, refuse or remove any Content that, in our reasonable opinion, violates any of our policies or is in any way harmful or objectionable.</p>

<h2 class="h3-mod">Backups</h2>

<p class="body-text">We perform regular backups of the Website and Content and will do our best to ensure completeness and accuracy of these backups. In the event of the hardware failure or data loss we will restore backups automatically to minimize the impact and downtime.</p>

<h2 class="h3-mod">Links to other websites</h2>

<p class="body-text">Although this Website may be linked to other websites, we are not, directly or indirectly, implying any approval, association, sponsorship, endorsement, or affiliation with any linked website, unless specifically stated herein. We are not responsible for examining or evaluating, and we do not warrant the offerings of, any businesses or individuals or the content of their websites. We do not assume any responsibility or liability for the actions, products, services, and content of any other third-parties. You should carefully review the legal statements and other conditions of use of any website which you access through a link from this Website. Your linking to any other off-site websites is at your own risk.</p>

<h2 class="h3-mod">Prohibited uses</h2>

<p class="body-text">In addition to other terms as set forth in the Agreement, you are prohibited from using the Website or its Content: (a) for any unlawful purpose; (b) to solicit others to perform or participate in any unlawful acts; (c) to violate any international, federal, provincial or state regulations, rules, laws, or local ordinances; (d) to infringe upon or violate our intellectual property rights or the intellectual property rights of others; (e) to harass, abuse, insult, harm, defame, slander, disparage, intimidate, or discriminate based on gender, sexual orientation, religion, ethnicity, race, age, national origin, or disability; (f) to submit false or misleading information; (g) to upload or transmit viruses or any other type of malicious code that will or may be used in any way that will affect the functionality or operation of the Service or of any related website, other websites, or the Internet; (h) to collect or track the personal information of others; (i) to spam, phish, pharm, pretext, spider, crawl, or scrape; (j) for any obscene or immoral purpose; or (k) to interfere with or circumvent the security features of the Service or any related website, other websites, or the Internet. We reserve the right to terminate your use of the Service or any related website for violating any of the prohibited uses.</p>

<h2 class="h3-mod">Intellectual property rights</h2>

<p class="body-text">This Agreement does not transfer to you any intellectual property owned by Website Operator or third-parties, and all rights, titles, and interests in and to such property will remain (as between the parties) solely with Website Operator. All trademarks, service marks, graphics and logos used in connection with our Website or Services, are trademarks or registered trademarks of Website Operator or Website Operator licensors. Other trademarks, service marks, graphics and logos used in connection with our Website or Services may be the trademarks of other third-parties. Your use of our Website and Services grants you no right or license to reproduce or otherwise use any Website Operator or third-party trademarks.</p>

<h2 class="h3-mod">Limitation of liability</h2>

<p class="body-text">To the fullest extent permitted by applicable law, in no event will Website Operator, its affiliates, officers, directors, employees, agents, suppliers or licensors be liable to any person for (a): any indirect, incidental, special, punitive, cover or consequential damages (including, without limitation, damages for lost profits, revenue, sales, goodwill, use or content, impact on business, business interruption, loss of anticipated savings, loss of business opportunity) however caused, under any theory of liability, including, without limitation, contract, tort, warranty, breach of statutory duty, negligence or otherwise, even if Website Operator has been advised as to the possibility of such damages or could have foreseen such damages. To the maximum extent permitted by applicable law, the aggregate liability of Website Operator and its affiliates, officers, employees, agents, suppliers and licensors, relating to the services will be limited to an amount greater of one dollar or any amounts actually paid in cash by you to Website Operator for the prior one month period prior to the first event or occurrence giving rise to such liability. The limitations and exclusions also apply if this remedy does not fully compensate you for any losses or fails of its essential purpose.</p>

<h2 class="h3-mod">Indemnification</h2>

<p class="body-text">You agree to indemnify and hold Website Operator and its affiliates, directors, officers, employees, and agents harmless from and against any liabilities, losses, damages or costs, including reasonable attorneys' fees, incurred in connection with or arising from any third-party allegations, claims, actions, disputes, or demands asserted against any of them as a result of or relating to your Content, your use of the Website or Services or any willful misconduct on your part.</p>

<h2 class="h3-mod">Severability</h2>

<p class="body-text">All rights and restrictions contained in this Agreement may be exercised and shall be applicable and binding only to the extent that they do not violate any applicable laws and are intended to be limited to the extent necessary so that they will not render this Agreement illegal, invalid or unenforceable. If any provision or portion of any provision of this Agreement shall be held to be illegal, invalid or unenforceable by a court of competent jurisdiction, it is the intention of the parties that the remaining provisions or portions thereof shall constitute their agreement with respect to the subject matter hereof, and all such remaining provisions or portions thereof shall remain in full force and effect.</p>

<h2 class="h3-mod">Dispute resolution</h2>

<p class="body-text">The formation, interpretation, and performance of this Agreement and any disputes arising out of it shall be governed by the substantive and procedural laws of Karnataka, India without regard to its rules on conflicts or choice of law and, to the extent applicable, the laws of India. The exclusive jurisdiction and venue for actions related to the subject matter hereof shall be the state and federal courts located in Karnataka, India, and you hereby submit to the personal jurisdiction of such courts. You hereby waive any right to a jury trial in any proceeding arising out of or related to this Agreement. The United Nations Convention on Contracts for the International Sale of Goods does not apply to this Agreement.</p>

<h2 class="h3-mod">Changes and amendments</h2>

<p class="body-text">We reserve the right to modify this Agreement or its policies relating to the Website or Services at any time, effective upon posting of an updated version of this Agreement on the Website. When we do we will revise the updated date at the bottom of this page. Continued use of the Website after any such changes shall constitute your consent to such changes. Policy was created with <a style="color:#222" target="_blank" title="Terms and conditions generator" href="https://www.websitepolicies.com/terms-and-conditions-generator">WebsitePolicies.com</a></p>

<h2 class="h3-mod">Acceptance of these terms</h2>

<p class="body-text">You acknowledge that you have read this Agreement and agree to all its terms and conditions. By using the Website or its Services you agree to be bound by this Agreement. If you do not agree to abide by the terms of this Agreement, you are not authorized to use or access the Website and its Services.</p>

<h2 class="h3-mod">Contacting us</h2>

<p class="body-text">If you have any questions about this Agreement, please contact us.</p>

<p class="body-text">This document was last updated on March 9, 2018</p>

<p class="body-text">This Privacy Policy governs the manner in which uppercut collects, uses, maintains and discloses information collected from users (each, a "User") of the http://www.uppercut.shop website ("Site").</p>

<h3 class="h3-mod">Personal identification information</h3>
<p class="body-text">We may collect personal identification information from Users in a variety of ways, including, but not limited to, when Users visit our site, register on the site, and in connection with other activities, services, features or resources we make available on our Site. Users may be asked for, as appropriate, name, email address, mailing address, phone number. Users may, however, visit our Site anonymously. We will collect personal identification information from Users only if they voluntarily submit such information to us. Users can always refuse to supply personally identification information, except that it may prevent them from engaging in certain Site related activities.</p>

<h3 class="h3-mod">Non-personal identification information</h3>
<p>We may collect non-personal identification information about Users whenever they interact with our Site. Non-personal identification information may include the browser name, the type of computer and technical information about Users means of connection to our Site, such as the operating system and the Internet service providers utilized and other similar information.</p>

<h3 class="h3-mod">Web browser cookies</h3>
<p>Our Site may use "cookies" to enhance User experience. User's web browser places cookies on their hard drive for record-keeping purposes and sometimes to track information about them. User may choose to set their web browser to refuse cookies, or to alert you when cookies are being sent. If they do so, note that some parts of the Site may not function properly.</p>

<h3 class="h3-mod">How we use collected information</h3>
<p class="body-text">uppercut may collect and use Users personal information for the following purposes:</p>
<ul>
  <li>
    <i>To run and operate our Site</i><br/>
    We may need your information display content on the Site correctly.
  </li>
  <li>
    <i>To improve customer service</i><br/>
    Information you provide helps us respond to your customer service requests and support needs more efficiently.
  </li>
  <li>
    <i>To personalize user experience</i><br/>
    We may use information in the aggregate to understand how our Users as a group use the services and resources provided on our Site.
  </li>
  <li>
    <i>To improve our Site</i><br/>
    We may use feedback you provide to improve our products and services.
  </li>
  <li>
    <i>To run a promotion, contest, survey or other Site feature</i><br/>
    To send Users information they agreed to receive about topics we think will be of interest to them.
  </li>
  <li>
    <i>To send periodic emails</i><br/>
    We may use the email address to send User information and updates pertaining to their order. It may also be used to respond to their inquiries, questions, and/or other requests. 
  </li>
</ul>

<h3 class="h3-mod">How we protect your information</h3>
<p class="body-text">We adopt appropriate data collection, storage and processing practices and security measures to protect against unauthorized access, alteration, disclosure or destruction of your personal information, username, password, transaction information and data stored on our Site.</p>

<h3 class="h3-mod">Sharing your personal information</h3>
<p class="body-text">We do not sell, trade, or rent Users personal identification information to others. We may share generic aggregated demographic information not linked to any personal identification information regarding visitors and users with our business partners, trusted affiliates and advertisers for the purposes outlined above. We may use third party service providers to help us operate our business and the Site or administer activities on our behalf, such as sending out newsletters or surveys. We may share your information with these third parties for those limited purposes provided that you have given us your permission.</p>

<h3 class="h3-mod">Electronic newsletters</h3>
<p class="body-text">If User decides to opt-in to our mailing list, they will receive emails that may include company news, updates, related product or service information, etc. If at any time the User would like to unsubscribe from receiving future emails, we include detailed unsubscribe instructions at the bottom of each email or User may contact us via our Site. We may use third party service providers to help us operate our business and the Site or administer activities on our behalf, such as sending out newsletters or surveys. We may share your information with these third parties for those limited purposes provided that you have given us your permission.</p>

<h3 class="h3-mod">Third party websites</h3>
<p class="body-text">Users may find advertising or other content on our Site that link to the sites and services of our partners, suppliers, advertisers, sponsors, licensors and other third parties. We do not control the content or links that appear on these sites and are not responsible for the practices employed by websites linked to or from our Site. In addition, these sites or services, including their content and links, may be constantly changing. These sites and services may have their own privacy policies and customer service policies. Browsing and interaction on any other website, including websites which have a link to our Site, is subject to that website's own terms and policies.</p>

<h3 class="h3-mod">Changes to this privacy policy</h3>
<p class="body-text">uppercut has the discretion to update this privacy policy at any time. When we do, we will post a notification on the main page of our Site. We encourage Users to frequently check this page for any changes to stay informed about how we are helping to protect the personal information we collect. You acknowledge and agree that it is your responsibility to review this privacy policy periodically and become aware of modifications.</p>

<h3 class="h3-mod">Your acceptance of these terms</h3>
<p class="body-text">By using this Site, you signify your acceptance of this policy. If you do not agree to this policy, please do not use our Site. Your continued use of the Site following the posting of changes to this policy will be deemed your acceptance of those changes. This privacy policy was built <a href="https://privacypolicies.com/" target="_blank">using the https://PrivacyPolicies.com generator</a>.</p>

<h3 class="h3-mod">Contacting us</h3>
<p class="body-text">If you have any questions about this Privacy Policy, the practices of this site, or your dealings with this site, please contact us.</p>

<p class="body-text">This document was last updated on March 09, 2018</p>



                </div>
              </div>
              <!--/col-->
            </div>
            <!--/row-->
          </div>
          <!--/col-->
        </div>
        <!--/row-->
      </div>
     <?php $this->load->view('layouts/footer'); ?>
      <script type='text/javascript' src='<?php echo base_url('assets/admin/js/backend/main.js'); ?>'></script>
    

