<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Uppercut</title>
  <link rel='shortcut icon' href='/uploads/favicon.ico' type='image/x-icon' />

   <meta name="viewport" content="width=device-width, minimum-scale=1.0" />
  <link href="<?php  echo base_url('assets/vendor/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
  <link href="<?php  echo base_url('assets/vendor/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600|Roboto:300" rel="stylesheet">
  <link href="<?php  echo base_url('assets/vendor/magnific-popup/magnific-popup.css'); ?>" rel="stylesheet">
  <link rel="stylesheet" href="<?php  echo base_url('assets/css/style.css'); ?>" >
   <!--  Scroll bar for desktop version -->
 <base id="base_url" value="<?php echo base_url(); ?>" name="base_url">
  <link rel="stylesheet" media="screen and (min-width: 900px)" type="text/css" href="<?php  echo base_url('/assets/css/scroll.css'); ?>">

<!--  <link rel="stylesheet" media="screen and (min-width: 900px)" type="text/css" href="<?php  echo base_url('/assets/css/scroll.css'); ?>"> -->
  <link rel="stylesheet" media="screen and (min-width: 900px)" href="<?php  echo base_url(); ?>/assets/css/desktop.css">
  
  <link href="https://fonts.googleapis.com/css?family=Comfortaa:300,700" rel="stylesheet">

  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-100486865-1"></script>
  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-100486865-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-100486865-1');
</script>

  <style type="text/css">
   
    
   b, strong {
    font-weight: 100;
  }
  h1 {
    font-weight: 300;
    font-size: 42px;
    color: #4a4a4a;
    letter-spacing: 0px;
  }
  .serviceli
  {
    padding-bottom: 0px !important;
  }
  </style>
    <?php if(user_logged_in()): ?>
<style >
  .nav ul {
    list-style: none;
    overflow: scroll;
    padding-left: 55px;
    padding-top: 5%;
    width: 100%;
}
.alter
{
  width: 100%;
}
.nav ul li {
    padding-bottom: 5px;
}
.nav ul li:nth-child(3) {
    margin-top: 0px;
}
</style>
<?php endif; ?>
</head>
<body>
  <div class="boxing" id="boxing">
  <div id="loader"></div>
       <?php $this->load->view('left_nav'); ?>