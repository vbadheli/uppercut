<?php

defined('BASEPATH') OR exit('No direct script access allowed');



/*

| -------------------------------------------------------------------------

| URI ROUTING

| -------------------------------------------------------------------------

| This file lets you re-map URI requests to specific controller functions.

|

| Typically there is a one-to-one relationship between a URL string

| and its corresponding controller class/method. The segments in a

| URL normally follow this pattern:

|

|	example.com/class/method/id/

|

| In some instances, however, you may want to remap this relationship

| so that a different class/function is called than the one

| corresponding to the URL.

|

| Please see the user guide for complete details:

|

|	https://codeigniter.com/user_guide/general/routing.html

|

| -------------------------------------------------------------------------

| RESERVED ROUTES

| -------------------------------------------------------------------------

|

| There are three reserved routes:

|

|	$route['default_controller'] = 'welcome';

|

| This route indicates which controller class should be loaded if the

| URI contains no data. In the above example, the "welcome" class

| would be loaded.

|

|	$route['404_override'] = 'errors/page_missing';

|

| This route will tell the Router which controller/method to use if those

| provided in the URL cannot be matched to a valid route.

|

|	$route['translate_uri_dashes'] = FALSE;

|

| This is not exactly a route, but allows you to automatically route

| controller and method names that contain dashes. '-' isn't a valid

| class or method name character, so it requires translation.

| When you set this option to TRUE, it will replace ALL dashes in the

| controller and method URI segments.

|

| Examples:	my-controller/index	-> my_controller/index

|		my-controller/my-method	-> my_controller/my_method

*/

$route['default_controller'] = 'home';

$route['login'] = 'home/login';

$route['signup'] = 'home/signup';
$route['usersignup'] = 'home/usersignup';

$route['restpass'] = 'home/restpassword';

$route['newpass'] ='home/newpassword';

$route['addshop'] ='home/addshop';



$route['missyou']='home/missyou';

$route['deleteaccount']='home/will_missyou';

$route['shopsignup']='shop/shop/signup';
$route['profile']='shop/shop/userprofile';

$route['editprofile']='shop/shop/editprofile';
$route['edit_profile']='shop/shop/edit_profile';
$route['change_pass']='home/change_pass';
$route['about_us']='home/about';
$route['contact_us']='home/contact';
$route['terms_condition']='home/terms';

$route['shoplogin']='shop/shop/login';
$route['rate_a_salon']='shop/shop/rating';
$route['mybooking']='shop/shop/mybooking';
$route['service_booking/(:id)']='shop/shop/service_booking';
//$route['feedback/(:id)']='shop/shop/feedback';

/*$route['dashboard']='home/dashboard';*/
$route['shop/shop/homesalon']='home/owner';
//$route['bookingacceptreject']='shop/shop/bookingacceptreject';

$route['dashboard2/(:any)']='home/dashboard2/$1';

$route['logout']='shop/shop/logout';

$route['resetpass']='shop/shop/resetPass';
$route['edituser/(:any)']='shop/shop/edituser/$1';
$route['edit_profile_admin/(:any)']='admin/shopuser/edit_profile_admin';

$route['changepass']='shop/shop/changepass';

$route['delAccount']='shop/shop/deleteAccount';

$route['map']='home/map';
$route['shopmap/(:any)/(:any)']='home/shopmap/$1/$2';

$route['mapdetail/(:any)']='home/mapbyid/$1';

$route['search']='search';

// --------------------shoppp-------



/*$route['fb_callback'] = 'auth/fb_callback';

$route['gp_callback'] = 'auth/gp_callback';*/



$route['admin'] = "admin/login";

//$route['admin/settings'] = "admin/settings";

$route['shoplist']="admin/shopuser/shop";
$route['services']="admin/shopuser/services";
//$route['booking_confirm']="admin/shopuser/booking_confirm";
$route['admin/feedbacks']="admin/shopuser/feedback";
$route['shop/bookings/(:any)']="shop/shop/shopbookinglist/$1";

$route['admin/forgot'] = "admin/login/forgot";

$route['admin/change_password'] = "admin/login/change_password";

$route['admin/login'] = "admin/login";

$route['admin/logout'] = "admin/login/logout";



