<?php defined('BASEPATH') OR exit('No direct script access allowed');



class Home_model extends CI_Model {



	public function __construct() {

		parent::__construct();

	}



	public function search_result($request){

		// debug($_SESSION['data']['search']);

		$this->db->select('*'); 

		$this->db->from('shop_list');

		// $this->db->or_where('lon', $_POST['lon']);
		$this->db->where('status',1);
		$this->db->like('name', $request);
		$this->db->or_like('user_address', $request);
		$this->db->or_like('service_offered', $request);
		$this->db->or_like('locality_name', $request);
		$this->db->or_like('city', $request);
		$this->db->or_like('state', $request);
		$this->db->or_like('country', $request);
		
		 //debug($this->db->get()->result());

		return $this->db->get()->result();

	}
	public function search_customer($request){

		// debug($_SESSION['data']['search']);

		$this->db->select('*'); 

		$this->db->from('shop_signup');

		// $this->db->or_where('lon', $_POST['lon']);
		$this->db->where('phone',$request);
		$this->db->like('username', $request);
		$this->db->or_like('email', $request);
		$this->db->or_like('phone', $request);
		/*$this->db->or_like('locality_name', $request);
		$this->db->or_like('city', $request);
		$this->db->or_like('state', $request);
		$this->db->or_like('country', $request);
		*/
		 //debug($this->db->get()->result());

		return $this->db->get()->result();

	}
	public function search_result_lt($request){
		// debug($_SESSION['data']['search']);
	
		$this->db->select('*'); 
		$this->db->from('shop_list');		
		$data = explode(',', $request);
		// $this->db->where('status',1);
		$this->db->like('user_address', $request); 

		$this->db->or_like('city', trim($data[1]));
		$this->db->or_like('country', trim($data[2]));
		// debug($this->db->get()->result());

		return $this->db->get()->result();

	}
	public function search_service($service,$request){
		// debug($_SESSION['data']['search']);
		//echo "<pre>";print_r($service);exit;
		$this->db->select('*'); 
		$this->db->from('shop_list');
		//$this->db->where('status',1);
		$this->db->like('user_address', $request);  
		if(!empty($request)){
		$data = explode(',', $request);
		$this->db->or_like('city', trim($data[1]));
		$this->db->or_like('country', trim($data[2]));
	    }	
		$this->db->or_like('service_offered', $service); 
		return $this->db->get()->result();
	}

	public function getshopbyid($data){
		$data = array(
			'id' =>base64_decode($data)
		);
		$this->db->where($data);
		$query = $this->db->get('shop_list');
		return $query->result_array();
	}
}