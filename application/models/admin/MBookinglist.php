<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MBookinglist extends CI_Model {
	public function __construct() {
		parent::__construct();
		$this->load->model('admin/login_model');
	}

	function get_data() {		

		$query = $this->db->select('*')->from('bookings')->get();
		// debug($query);
		if ($query->num_rows() >=1) {
			$result = $query->result();
			return $result;
		}
		return false;	
	}
}