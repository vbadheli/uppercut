<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Booking_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}


	function addBooking($value) {
		//echo "<pre>";print_r($value);exit;
		if(empty($value['id'])) {
			$query = $this->db->insert('bookings', $value);					
		}else{
			$this->db->where('id',$value['id']);
			$query = $this->db->update('bookings', $value);
		}
		if($query) {			
			return true;
		}else{
			return false;
		}
	}
	function addBookingajax($value) {
		//echo "<pre>";print_r($value);exit;
		if(empty($value['id'])) {
			$query = $this->db->insert('bookings', $value);
			$insert_id = $this->db->insert_id();			
			//$data['insert_id'] = $insert_id;				
		}else{
			$this->db->where('id',$value['id']);
			$query = $this->db->update('bookings', $value);
		}
		if($query) {			
			return $insert_id;
		}else{
			return false;
		}
	}
	function addBookingAdmin($value) {
		//echo "<pre>";print_r($value);exit;
		if(empty($value['id'])) {
			$query = $this->db->insert('bookings', $value);			
		}else{
			$this->db->where('id',$value['id']);
			$query = $this->db->update('bookings', $value);
		}
		if($query) {			
			return true;
		}else{
			return false;
		}
	}
	function BookingList($value) {
		//echo "<pre>";print_r($value);exit;
		if(!empty($value['id'])) {
			$this->db->where('id',$value['id']);
			$query = $this->db->update('bookings', $value);
			
		}else{
			$query = $this->db->insert('bookings', $value);			
		}
		if($query) {			
			return true;
		}else{
			return false;
		}
	}
	
	function EditshopList() {
		$query = $this->db->select('*')->from('bookings')->get();
		// debug($query);
		if ($query->num_rows() >=1) {
			$result = $query->result();
			return $result;
		}
		return false;	
	}
	function Notification() {		
		$shop_id = -1;		
		$this->db->where('shop_user', get_session('userid'));	
		$query = $this->db->select('*')->from('shop_list')->get();
		
		if ($query->num_rows() >=1) {
			$results = $query->result();
			$shop_id = $results[0]->id;
		}
		if($shop_id != -1 )
		{
			$this->db->select('*');
			$this->db->from('bookings');
			$this->db->where('status','0')->where('salonid',$shop_id);
			$query = $this->db->get();
			return $query->num_rows();
		}
		return 0;		
	}


	function BookingAcceptReject() {
		// $this->db->select('*');
		$shop_id = -1;
		$this->db->where('shop_user', get_session('userid'));	
		$query = $this->db->select('*')->from('shop_list')->get();
		
		if ($query->num_rows() >=1) {
			$results = $query->result();
			$shop_id = $results[0]->id;
		}
		if($shop_id != -1 )
		{
			$this->db->where('salonid',$shop_id);
			$query = $this->db->select('*')->from('bookings')->where('status',1)->order_by('date','asc')->get();
			if ($query->num_rows() >=1) {
				$result = $query->result();
				return  $result;
			}
		}
		return [];	
	}

	function getUpcomingBookings() {
		$shop_id = -1;
		// $this->db->select('*');	
		$this->db->where('shop_user',get_session('userid'));	
		$query = $this->db->select('*')->from('shop_list')->get();
		if ($query->num_rows() >=1) {
			$results = $query->result();
			$shop_id = $results[0]->id;
		}
		if($shop_id != -1 )
		{
			$this->db->where('salonid',$shop_id);	
			$query = $this->db->select('*')->from('bookings')->where('date > CURDATE()')->where('status',1)->order_by('date','asc')->get();
			// debug($query);
			if ($query->num_rows() >=1) {
				$result = $query->result();
				return  $result;
			}
		}
		return [];	
	}

	function getOnGoingBookings() {
		$shop_id = -1;
		$this->db->where('shop_user',get_session('userid'));	
		$query = $this->db->select('*')->from('shop_list')->get();
		if ($query->num_rows() >=1) {
			$results = $query->result();
			$shop_id = $results[0]->id;
		}

		if($shop_id != -1 )
		{
			$this->db->where('salonid',$shop_id);	
			$query = $this->db->select('*')->from('bookings')->where('date=CURDATE()')->where('status',1)->order_by('date','asc')->get();
			// debug($query);
			if ($query->num_rows() >=1) {
				$result = $query->result();
				return  $result;
			}
		}
		return [];	
	}
	function BookingRecords() {
		// $this->db->select('*');	
		$this->db->where('shop_user',get_session('userid'));	
		$query = $this->db->select('*')->from('shop_list')->get();
		if ($query->num_rows() >=1) {
			$results = $query->result();
			$shop_id = $results[0]->id;
		}
		$this->db->where('salonid',$shop_id);	
		$query = $this->db->select('*')->from('bookings')->where('status = 0')->order_by('date','asc')->get();
		// debug($query);
		if ($query->num_rows() >=1) {
			$result = $query->result();
			return  array("result" =>$result);
		}
		return false;	
	}

	function shopBookingAccept($value) {
		
		//echo "<pre>";print_r($value);exit;
		if(!empty($value['id'])) {
			$this->db->where('id',$value['id']);
			$query = $this->db->update('bookings', $value);
		}else
		{
			
		}
		if($query) {			
			return true;
		}else{
			return false;
		}
	}
	function shopBookingRejectF($value) {
		
		//echo "<pre>";print_r($value);exit;
		if(!empty($value['id'])) {
			$this->db->where('id',$value['id']);
			$query = $this->db->update('bookings', $value);
		}else
		{
			
		}
		if($query) {			
			return true;
		}else{
			return false;
		}
	}

	function mybookingM() {
		// $this->db->select('*');
		$this->db->where('userid',get_session('userid'));	
		$query = $this->db->select('*')->from('bookings')->order_by('date','asc')->order_by('time','asc')->get();
		// debug($query);
		if ($query->num_rows() >=1) {
			$result = $query->result();
			return  $result;
		}

		return [];	
	}
	
	function BookingStatus($id) {
		
		$this->db->where('id',$id);	
		$query = $this->db->select('*')->from('bookings')->get();
		if ($query->num_rows() >=1) {
			$result = $query->row();
			return  $result;
		}

		return null;	
	}
	function MembershipStatus() {
		
		$query = $this->db->select('*')->from('tbl_packages')->get();
		// debug($query);
		 //echo "<pre>";print_r($query);exit;
		if ($query->num_rows() >=1) {
			$result = $query->result();
			return $result;
		}
		return null;	
	}
	function MembershipData($value) {
		
		//echo "<pre>";print_r($value);exit;
		if(empty($value['id'])) {
			$query = $this->db->insert('tbl_membership', $value);
		}
	}

}