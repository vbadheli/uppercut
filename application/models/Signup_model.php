<?php defined('BASEPATH') OR exit('No direct script access allowed');



class Signup_model extends CI_Model {



	public function __construct() {

		parent::__construct();

	}



	function login() {

		$username = $this->input->post('email');

		$password = md5($this->input->post('password'));

		$where = array(
			'username' => $username,
			'password' => $password,
			'verified_status' => 1,
			);

		$this->db->where($where);

		$query = $this->db->get('shop_signup');

		if ($query->num_rows() == 1) {

			$result = $query->row_array();

			// debug($result);

			$session_data = array(

				'userid' => $result['id'],
				'email'=>$result['email'],
				'type' =>$result['type'],
				'logged_in' => 1

				); 

			set_sessions($session_data);

			return true;

		}

		return false;

	}

	public function calculate_rating($id)
            {
              $total = 0;
              $data = $this->db->where('shop_id',$id)->get('feedback')->result();
              if(!empty($data[0]->id))
              {
                $i = 0;
                foreach($data as $r)
                {
                  $i++;
                  $total = $total + $r->rating;
                }

                $av = $total / $i  ;
                $av = number_format($av, 1, '.', '');
                return $av;
              }
              return 0;
              

            }

	function signup() {

		$post = $this->input->post();

		$where = array(

			'verified_status' => 0,

			'username' => $post['email'],

			'password' => md5($post['password']),

			'activation_code' => $post['activation_code'],

			'email' => $post['email'],

			'phone' => $post['phone'],

			'created_at' => updated_at(),

			'is_admincreated'=>0,
			//'verified_status'=>1,
			'type' => 'user'

			);

		$query = $this->db->insert('shop_signup', $where);

		if($query) {

			return true;	

		}

		return false;

	}
	function usersignup() {

		$post = $this->input->post();

		$where = array(

			'verified_status' => 0,

			'username' => $post['email'],

			'password' => md5($post['password']),

			'activation_code' => $post['activation_code'],

			'email' => $post['email'],

			'phone' => $post['phone'],

			'created_at' => updated_at(),

			'is_admincreated'=>0,

			'type' => 'owner'

			);

		$query = $this->db->insert('shop_signup', $where);

		if($query) {
			$session_data = array(

				'code' => $post['activation_code'],

				'email'=>$post['email'],

				'logged_in' => 0

				); 

			set_sessions($session_data);
			return true;
		
		}

		return false;

	}


	public function change_passd($id,$new)
	{
		return $this->db->set('password',md5($new))->where('id',$id)->update('shop_signup');
	}
	public function change_dd($email,$new)
	{
		return $this->db->set('activation_code',$new)->where('email',$email)->update('shop_signup');
	}
	function resetPass() {



		$post = $this->input->post();

		$this->db->where('email', $post['email']);

		$query = $this->db->get('shop_signup');

		if ($query->num_rows() == 1) {

			return true;

		}else{

			return false;

		}

	}



	function change_password() {

		$data = array(

			'password' => md5($_POST['newpassword']),

			);

		$this->db->where('email', $_POST['email']);

		$result = $this->db->update('shop_signup', $data);

		if ($result) {

			return true;

		}

		return false;

	}



	function deleteAccount($id) {

		

		$where = array(

			'id' =>$id

			);

		$data = $this->db->select('username,email,phone')->where('id',$id)->get('shop_signup')->row();
		$result = $this->db->delete('shop_signup', $where);
		$result2 = $this->db->where('shop_user',$id)->delete('shop_list');
		print_r($data);
		if ($result) {
		  return $data;

		}else{
			return false;
		}

		

	}	



}