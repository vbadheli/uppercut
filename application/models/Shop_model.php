<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Shop_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}


	function addShop($value) {

		if(empty($value['id'])) {
			array_pop($value);
			$query = $this->db->insert('shop_list', $value);
		}else{
			$this->db->where('id',$value['id']);
			$query = $this->db->update('shop_list', $value);
		}
		if($query) {			
			return true;
		}else{
			return false;
		}
	}
	
	public function get_user_id($id)
	{
		return $this->db->where('id',$id)->get('shop_signup')->result();
	}
}