-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 16, 2018 at 08:54 AM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `thepikhs_shopfinder`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(20) NOT NULL,
  `admin_username` varchar(60) NOT NULL,
  `admin_pass` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_username`, `admin_pass`) VALUES
(1, 'c6gJJV@gmail.com', '+T7_:Xk?pyhR`5uu');

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE `bookings` (
  `id` int(11) NOT NULL,
  `salonid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `service_type` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `time` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bookings`
--

INSERT INTO `bookings` (`id`, `salonid`, `userid`, `service_type`, `date`, `time`, `status`, `remark`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Mens Hair Cut', '2018-06-17', '2 PM', 1, 'Confirm Your Booking', '2018-06-16 10:18:51', '2018-06-16 10:18:53'),
(2, 1, 1, 'MensHairCut', '2018-06-17', '10 AM', 0, NULL, '2018-06-16 10:30:34', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `deleted_account`
--

CREATE TABLE `deleted_account` (
  `id` int(11) NOT NULL,
  `reason` longtext NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `number` varchar(100) NOT NULL,
  `date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `feedback` longtext NOT NULL,
  `rating` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `recent_view`
--

CREATE TABLE `recent_view` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recent_view`
--

INSERT INTO `recent_view` (`id`, `user_id`, `shop_id`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `name`, `active`) VALUES
(5, 'Hair cut - Men', 0),
(6, 'Hair cut - Women', 0),
(7, 'Hair cut - Kids', 0),
(8, 'Eyebrow Threading', 0);

-- --------------------------------------------------------

--
-- Table structure for table `shop_list`
--

CREATE TABLE `shop_list` (
  `id` int(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `locality_name` varchar(255) NOT NULL,
  `mobileno` varchar(50) NOT NULL,
  `website` varchar(50) NOT NULL,
  `about_me` varchar(250) NOT NULL,
  `service_offered` varchar(255) NOT NULL,
  `profile_pic` varchar(100) NOT NULL,
  `user_address` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `postalcode` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL,
  `days` longtext NOT NULL,
  `lat` decimal(8,0) NOT NULL,
  `lon` decimal(8,0) NOT NULL,
  `is_created_by_admin` tinyint(1) NOT NULL DEFAULT '0',
  `userid` int(20) DEFAULT NULL,
  `parking` varchar(100) NOT NULL,
  `opening_time` time NOT NULL,
  `closing_time` time NOT NULL,
  `image` varchar(100) NOT NULL,
  `shop_user` int(11) NOT NULL,
  `counter` int(11) NOT NULL,
  `rating` varchar(255) NOT NULL,
  `color` varchar(255) NOT NULL,
  `status` int(1) NOT NULL,
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shop_list`
--

INSERT INTO `shop_list` (`id`, `name`, `locality_name`, `mobileno`, `website`, `about_me`, `service_offered`, `profile_pic`, `user_address`, `city`, `state`, `postalcode`, `country`, `days`, `lat`, `lon`, `is_created_by_admin`, `userid`, `parking`, `opening_time`, `closing_time`, `image`, `shop_user`, `counter`, `rating`, `color`, `status`, `type`) VALUES
(1, 'test', 'Solapur', '9970609808', 'test.com', 'this is a test description for salon', '[\"Hair cut - Men\",\"Hair cut - Women\"]', '', '205/26 Telangi Pachha Peth, Padmashli Chowk', '', '', '', '', '[\"Su\",\"Mo\",\"Tu\",\"We\",\"Th\",\"Fr\"]', '1', '2', 0, NULL, '[\"car\",\"bike\"]', '09:00:00', '21:00:00', 'download1.png', 2, 1, '', '', 0, 'Unisex'),
(2, 'tes', 'Solapur', '1234567890', 'test', 'asa', '[\"Hair cut - Kids\"]', '', '205/26 Telangi Pachha Peth, Padmashli Chowk', '', '', '', '', '[\"Su\",\"Mo\",\"Tu\",\"We\",\"Th\",\"Fr\",\"Sa\"]', '12', '12', 0, NULL, '', '01:01:00', '01:01:00', 'images3.jpg', 3, 0, '', '', 0, 'Unisex'),
(3, 'Sai Harbal Salon', 'SOLAPUR', '9876543210', 'sai.com', 'th', '[\"Hair cut - Men\",\"Hair cut - Women\",\"Hair cut - Kids\",\"Eyebrow Threading\"]', '', '205/26 Telangi Pachha Peth, Padmahsali chowk', '', '', '', '', '[\"Su\",\"Mo\",\"Tu\",\"We\",\"Th\",\"Fr\",\"Sa\"]', '15', '42', 0, NULL, '[\"car\",\"bike\"]', '05:00:00', '17:00:00', 'images_(1)5.jpg', 4, 0, '', '', 0, 'Unisex'),
(4, 'Sai Harbal Salon', 'SOLAPUR', '9876543210', 'sai.com', 'th', '[\"Hair cut - Men\",\"Hair cut - Women\",\"Hair cut - Kids\",\"Eyebrow Threading\"]', '', '205/26 Telangi Pachha Peth, Padmahsali chowk', '', '', '', '', '[\"Su\",\"Mo\",\"Tu\",\"We\",\"Th\",\"Fr\",\"Sa\"]', '15', '42', 0, NULL, '[\"car\",\"bike\"]', '05:00:00', '17:00:00', 'images_(1)6.jpg', 4, 0, '', '', 0, 'Unisex'),
(5, 'd', 'SOLAPUR', '9', 'd', 'd', '[\"Hair cut - Men\",\"Hair cut - Women\",\"Hair cut - Kids\",\"Eyebrow Threading\"]', '', '205/26 Telangi Pachha Peth, Padmahsali chowk', '', '', '', '', '[\"Su\",\"Mo\",\"Tu\",\"We\",\"Th\",\"Fr\",\"Sa\"]', '4', '4', 0, NULL, '[\"car\"]', '04:00:00', '16:00:00', 'images_(1)7.jpg', 4, 0, '', '', 0, 'Unisex'),
(6, 'd', 'SOLAPUR', '9', 'd', 'd', '[\"Hair cut - Men\",\"Hair cut - Women\",\"Hair cut - Kids\",\"Eyebrow Threading\"]', '', '205/26 Telangi Pachha Peth, Padmahsali chowk', '', '', '', '', '[\"Su\",\"Mo\",\"Tu\",\"We\",\"Th\",\"Fr\",\"Sa\"]', '4', '4', 0, NULL, '[\"car\"]', '04:00:00', '16:00:00', 'images_(1)8.jpg', 4, 0, '', '', 0, 'Unisex'),
(7, 'd', 'SOLAPUR', '9', 'd', 'd', '[\"Hair cut - Men\",\"Hair cut - Women\",\"Hair cut - Kids\",\"Eyebrow Threading\"]', '', '205/26 Telangi Pachha Peth, Padmahsali chowk', '', '', '', '', '[\"Su\",\"Mo\",\"Tu\",\"We\",\"Th\",\"Fr\",\"Sa\"]', '4', '4', 0, NULL, '[\"car\"]', '04:00:00', '16:00:00', 'images_(1)9.jpg', 4, 0, '', '', 0, 'Unisex');

-- --------------------------------------------------------

--
-- Table structure for table `shop_signup`
--

CREATE TABLE `shop_signup` (
  `id` int(110) NOT NULL,
  `verified_status` tinyint(1) NOT NULL DEFAULT '0',
  `username` varchar(50) NOT NULL,
  `password` varchar(50) CHARACTER SET utf8 NOT NULL,
  `activation_code` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `created_at` varchar(100) NOT NULL,
  `is_admincreated` tinyint(1) NOT NULL DEFAULT '0',
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shop_signup`
--

INSERT INTO `shop_signup` (`id`, `verified_status`, `username`, `password`, `activation_code`, `email`, `phone`, `created_at`, `is_admincreated`, `type`) VALUES
(1, 1, 'demo@demo.com', '14d6c370d42320e4b6870be8d306fd7c', 'gZqHPWeUFa3ufQ72', 'demo@demo.com', '9999999999', '2018-06-16 10:25:59', 0, 'user'),
(2, 1, 'vbadheli@gmail.com', '5f4dcc3b5aa765d61d8327deb882cf99', '2688', 'vbadheli@gmail.com', '9976098080', '2018-06-16 10:16:49', 0, 'owner'),
(3, 1, 'tt@gmail.com', '5f4dcc3b5aa765d61d8327deb882cf99', '4642', 'tt@gmail.com', '2232342342', '2018-06-16 11:16:56', 0, 'owner'),
(4, 1, 'lucky@gmail.com', '339a65e93299ad8d72c42b263aa23117', '1522', 'lucky@gmail.com', '9876543210', '2018-06-16 12:04:46', 0, 'owner');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_membership`
--

CREATE TABLE `tbl_membership` (
  `id` int(11) NOT NULL,
  `prices` float NOT NULL,
  `user_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `validity_period` int(11) NOT NULL,
  `expiry_date` date DEFAULT NULL,
  `created_on` date NOT NULL,
  `updated_on` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_membership`
--

INSERT INTO `tbl_membership` (`id`, `prices`, `user_id`, `package_id`, `validity_period`, `expiry_date`, `created_on`, `updated_on`) VALUES
(1, 0, 1, 1, 0, NULL, '2018-06-16', '0000-00-00'),
(2, 0, 1, 20, 3, '2018-09-16', '2018-06-16', '0000-00-00'),
(3, 0, 3, 1, 0, NULL, '2018-06-16', '0000-00-00'),
(4, 0, 4, 1, 0, NULL, '2018-06-16', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_packages`
--

CREATE TABLE `tbl_packages` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `valid_period` int(11) NOT NULL,
  `prices` float NOT NULL,
  `description` text NOT NULL,
  `status` enum('Active','Deactive') NOT NULL,
  `created_on` date NOT NULL,
  `updated_on` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_packages`
--

INSERT INTO `tbl_packages` (`id`, `title`, `valid_period`, `prices`, `description`, `status`, `created_on`, `updated_on`) VALUES
(1, 'Starter', 0, 0, 'One Salon Profile', 'Active', '2018-06-11', '0000-00-00'),
(20, 'Booking Request', 3, 0, 'Accept booking from customer at 0 % commission per booking.', 'Deactive', '2018-06-11', '0000-00-00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deleted_account`
--
ALTER TABLE `deleted_account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recent_view`
--
ALTER TABLE `recent_view`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_list`
--
ALTER TABLE `shop_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_signup`
--
ALTER TABLE `shop_signup`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `tbl_membership`
--
ALTER TABLE `tbl_membership`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_packages`
--
ALTER TABLE `tbl_packages`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `bookings`
--
ALTER TABLE `bookings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `deleted_account`
--
ALTER TABLE `deleted_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `recent_view`
--
ALTER TABLE `recent_view`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `shop_list`
--
ALTER TABLE `shop_list`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `shop_signup`
--
ALTER TABLE `shop_signup`
  MODIFY `id` int(110) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_membership`
--
ALTER TABLE `tbl_membership`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_packages`
--
ALTER TABLE `tbl_packages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
