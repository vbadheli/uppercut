-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 11, 2018 at 03:06 PM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `thepikhs_shopfinder`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(20) NOT NULL,
  `admin_username` varchar(60) NOT NULL,
  `admin_pass` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_username`, `admin_pass`) VALUES
(1, 'c6gJJV@gmail.com', '+T7_:Xk?pyhR`5uu');

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE `bookings` (
  `id` int(11) NOT NULL,
  `salonid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `service_type` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `time` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bookings`
--

INSERT INTO `bookings` (`id`, `salonid`, `userid`, `service_type`, `date`, `time`, `status`, `remark`, `created_at`, `updated_at`) VALUES
(12, 8, 44, 'MensHairCut', '2018-05-31', '1', 0, NULL, '2018-05-28 19:19:17', '0000-00-00 00:00:00'),
(13, 8, 44, 'MensHairCut', '2018-05-31', '2', 0, NULL, '2018-05-28 19:27:56', '0000-00-00 00:00:00'),
(14, 8, 44, 'Select a service', '2018-05-28', '', 0, NULL, '2018-05-28 19:33:05', '0000-00-00 00:00:00'),
(15, 8, 44, 'MensHairCut', '2018-05-28', '2 PM', 0, 'OK', '2018-05-28 19:35:57', '0000-00-00 00:00:00'),
(16, 11, 44, 'MensHairCut', '2018-06-02', '2 PM', 1, 'Confirm Your Booking', '2018-05-29 10:36:17', '2018-06-02 19:05:13'),
(17, 8, 44, 'MensHairCut', '2018-05-30', '1 PM', 0, 'Set Data', '2018-05-29 10:36:57', '0000-00-00 00:00:00'),
(19, 11, 16, 'HairCutKids', '2018-06-04', '11 AM', 0, '', '2018-05-29 18:06:28', '2018-06-02 17:15:42'),
(20, 11, 21, 'MensHairCut', '2018-05-31', '', 1, '', '2018-05-29 18:10:45', '2018-06-02 19:03:13'),
(21, 10, 43, 'MensHairCut', '2018-06-04', '11 AM', 0, 'Currect Bset Data', '2018-05-29 18:21:03', '0000-00-00 00:00:00'),
(22, 10, 43, 'WomensHairCut', '2018-05-29', '8 AM', 0, 'OK', '2018-05-29 18:22:52', '0000-00-00 00:00:00'),
(23, 10, 44, 'MensHairCut', '2018-06-04', '11 AM', 0, 'da', '2018-05-29 18:41:56', '0000-00-00 00:00:00'),
(24, 13, 51, 'WomensHairCut', '2018-06-04', '10 AM', 1, 'Confirm Your Booking', '2018-05-29 18:49:51', '2018-06-05 19:06:58'),
(25, 12, 47, 'MensHairCut', '2018-06-05', '1 PM', 1, 'Confirm Your Booking', '2018-06-04 11:39:13', '2018-06-04 11:42:50'),
(26, 12, 47, 'MensHairCut', '2018-06-06', '1 PM', 2, 'Holiday On This Day', '2018-06-04 11:39:50', '2018-06-04 11:43:29'),
(27, 8, 49, 'MensHairCut', '2018-06-05', '1 PM', 1, 'Confirm Your Booking', '2018-06-05 11:09:23', '0000-00-00 00:00:00'),
(28, 8, 49, 'MensHairCut', '2018-06-05', '2 PM', 2, NULL, '2018-06-05 11:20:19', '0000-00-00 00:00:00'),
(29, 8, 49, 'MensHairCut', '2018-06-05', '2 PM', 2, NULL, '2018-06-05 11:22:22', '0000-00-00 00:00:00'),
(30, 13, 51, 'WomensHairCut', '2018-06-05', '12 AM', 1, 'Confirm Your Booking', '2018-06-05 11:23:25', '2018-06-05 19:10:25'),
(31, 8, 58, 'MensHairCut', '2018-06-09', '9 AM', 0, NULL, '2018-06-06 11:45:11', '0000-00-00 00:00:00'),
(32, 14, 58, 'MensHairCut', '2018-06-10', '10 AM', 1, 'Confirm Your Booking', '2018-06-06 11:45:42', '2018-06-06 11:49:48'),
(33, 14, 58, 'WomensHairCut', '2018-06-12', '11 AM', 0, 'Already Book This Scheduled.', '2018-06-06 11:50:31', '2018-06-06 12:03:18'),
(34, 14, 58, 'WomensHairCut', '2018-06-12', '3 PM', 1, 'Confirm Your Booking', '2018-06-06 11:51:32', '2018-06-06 11:54:56'),
(35, 15, 44, 'MensHairCut', '2018-06-06', '1 PM', 2, 'Already Book This Scheduled.', '2018-06-06 12:16:13', '2018-06-06 12:29:02'),
(36, 8, 44, 'WomensHairCut', '2018-06-11', '', 0, NULL, '2018-06-11 13:48:54', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `deleted_account`
--

CREATE TABLE `deleted_account` (
  `id` int(11) NOT NULL,
  `reason` longtext NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `number` varchar(100) NOT NULL,
  `date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `deleted_account`
--

INSERT INTO `deleted_account` (`id`, `reason`, `name`, `email`, `number`, `date`) VALUES
(1, 'I don’t find this website useful', 'saad@gmail.com', 'saad@gmail.com', '3993499349', ''),
(2, 'Too many email notifications', 'saad@gmail.com', 'saad@gmail.com', '9993939393', '28-02-2018'),
(3, 'I have created another account for my shop', 'askfj3@gmail.com', 'askfj3@gmail.com', '3993949349', '28-02-2018'),
(4, 'Too many email notifications', 'tulsi.black@gmail.com', 'tulsi.black@gmail.com', '2394234234', '24-03-2018'),
(5, 'Less number of customers from website', 'tulsi.black@gmail.com', 'tulsi.black@gmail.com', '3838584949', '25-03-2018'),
(6, 'Privacy', 'uppercut', 'uppercut.neo@gmail.com', '9840439385', '25-04-2018');

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `feedback` longtext NOT NULL,
  `rating` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`id`, `shop_id`, `user_id`, `feedback`, `rating`, `date`) VALUES
(3, 1, 2, 'testing', '4', '20-02-2018'),
(4, 1, 15, 'comment here', '5', '03-03-2018'),
(5, 2, 21, 'Test', '4', '24-03-2018'),
(6, 8, 21, 'Sdvgd', '2', '25-04-2018'),
(7, 8, 35, 'Xjsjx', '3', '25-04-2018'),
(8, 8, 38, 'It\'s k', '1', '25-04-2018'),
(9, 8, 39, 'Df', '5', '26-04-2018');

-- --------------------------------------------------------

--
-- Table structure for table `recent_view`
--

CREATE TABLE `recent_view` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recent_view`
--

INSERT INTO `recent_view` (`id`, `user_id`, `shop_id`) VALUES
(1, 13, 1),
(2, 13, 1),
(3, 13, 1),
(4, 13, 1),
(5, 13, 1),
(6, 13, 1),
(7, 13, 1),
(8, 14, 1),
(9, 14, 1),
(10, 14, 1),
(11, 14, 1),
(12, 14, 1),
(13, 14, 1),
(14, 14, 1),
(15, 14, 1),
(16, 14, 1),
(17, 14, 1),
(18, 14, 1),
(19, 14, 1),
(20, 15, 1),
(21, 15, 1),
(22, 15, 1),
(23, 15, 1),
(31, 21, 1),
(44, 21, 1),
(45, 21, 1),
(46, 21, 1),
(47, 21, 1),
(48, 21, 1),
(49, 21, 1),
(50, 21, 2),
(51, 21, 2),
(52, 21, 2),
(53, 21, 2),
(54, 21, 2),
(55, 21, 2),
(56, 21, 2),
(57, 21, 2),
(58, 21, 2),
(59, 21, 2),
(60, 21, 2),
(61, 21, 2),
(62, 21, 2),
(63, 21, 2),
(64, 21, 2),
(65, 21, 2),
(66, 21, 2),
(67, 21, 2),
(68, 21, 2),
(69, 21, 2),
(70, 21, 2),
(71, 21, 2),
(72, 21, 2),
(73, 21, 2),
(74, 21, 2),
(75, 21, 2),
(76, 21, 2),
(77, 21, 2),
(78, 21, 2),
(79, 21, 2),
(80, 21, 2),
(81, 21, 2),
(82, 21, 2),
(83, 21, 2),
(84, 21, 2),
(85, 21, 2),
(86, 21, 2),
(87, 21, 2),
(88, 21, 2),
(89, 21, 2),
(90, 21, 2),
(91, 21, 2),
(92, 21, 2),
(93, 21, 2),
(94, 21, 2),
(95, 21, 2),
(96, 21, 2),
(97, 21, 2),
(98, 21, 2),
(99, 21, 2),
(100, 21, 2),
(101, 21, 2),
(102, 21, 2),
(103, 21, 2),
(104, 21, 2),
(105, 21, 2),
(106, 21, 2),
(107, 21, 2),
(108, 21, 2),
(109, 21, 2),
(110, 21, 2),
(111, 21, 2),
(112, 21, 2),
(113, 21, 2),
(114, 21, 2),
(115, 21, 2),
(116, 21, 2),
(117, 21, 2),
(118, 21, 2),
(119, 21, 2),
(120, 21, 2),
(121, 21, 2),
(122, 21, 2),
(123, 21, 2),
(124, 21, 2),
(125, 21, 2),
(126, 21, 2),
(127, 21, 2),
(128, 21, 2),
(129, 21, 2),
(130, 21, 2),
(131, 21, 2),
(132, 21, 2),
(133, 21, 2),
(134, 21, 2),
(135, 21, 2),
(136, 21, 2),
(137, 21, 2),
(138, 21, 2),
(139, 21, 2),
(140, 21, 2),
(141, 21, 2),
(142, 21, 2),
(143, 21, 2),
(144, 21, 2),
(145, 21, 2),
(146, 21, 2),
(147, 21, 2),
(148, 21, 2),
(149, 21, 2),
(150, 21, 2),
(151, 21, 2),
(152, 21, 2),
(153, 21, 2),
(154, 21, 2),
(155, 21, 2),
(156, 21, 2),
(157, 21, 2),
(158, 21, 2),
(159, 21, 2),
(160, 21, 2),
(161, 21, 2),
(162, 21, 2),
(163, 21, 2),
(164, 21, 2),
(165, 21, 2),
(166, 21, 2),
(167, 21, 2),
(168, 21, 2),
(169, 21, 2),
(170, 21, 2),
(171, 21, 2),
(172, 21, 2),
(173, 21, 2),
(174, 21, 2),
(175, 21, 2),
(176, 21, 2),
(177, 21, 2),
(178, 21, 2),
(179, 21, 2),
(180, 21, 2),
(181, 21, 2),
(182, 21, 2),
(183, 21, 2),
(184, 21, 2),
(185, 21, 2),
(186, 21, 2),
(187, 21, 2),
(188, 21, 2),
(189, 21, 2),
(190, 21, 2),
(191, 21, 2),
(192, 21, 2),
(193, 21, 2),
(194, 21, 2),
(195, 21, 2),
(196, 21, 2),
(197, 21, 2),
(198, 21, 2),
(199, 21, 2),
(200, 21, 2),
(201, 21, 2),
(202, 21, 2),
(203, 21, 2),
(204, 21, 2),
(205, 21, 2),
(206, 21, 2),
(207, 21, 2),
(208, 21, 2),
(209, 21, 2),
(210, 21, 2),
(211, 21, 2),
(212, 21, 2),
(213, 21, 2),
(214, 21, 2),
(215, 21, 2),
(216, 21, 2),
(217, 21, 2),
(218, 21, 2),
(219, 21, 2),
(220, 21, 2),
(221, 21, 2),
(222, 21, 2),
(223, 21, 2),
(224, 21, 2),
(225, 21, 2),
(226, 21, 2),
(227, 21, 2),
(228, 21, 2),
(229, 21, 2),
(230, 21, 2),
(231, 21, 2),
(232, 21, 2),
(233, 21, 2),
(234, 21, 2),
(235, 21, 2),
(236, 21, 2),
(237, 21, 2),
(238, 21, 2),
(239, 21, 2),
(240, 21, 2),
(241, 21, 2),
(242, 21, 2),
(243, 21, 2),
(244, 21, 2),
(245, 21, 2),
(246, 21, 2),
(247, 21, 2),
(248, 21, 2),
(249, 21, 2),
(250, 21, 2),
(251, 21, 2),
(252, 21, 2),
(253, 21, 2),
(254, 21, 2),
(255, 21, 2),
(256, 32, 2),
(257, 32, 2),
(258, 21, 3),
(259, 21, 3),
(260, 21, 3),
(261, 21, 3),
(262, 21, 3),
(263, 32, 2),
(264, 32, 2),
(265, 32, 2),
(266, 32, 2),
(267, 32, 2),
(268, 32, 2),
(269, 32, 2),
(270, 32, 2),
(271, 32, 2),
(272, 32, 2),
(273, 32, 2),
(274, 32, 2),
(275, 32, 2),
(276, 32, 2),
(277, 32, 2),
(278, 32, 2),
(279, 32, 2),
(280, 32, 2),
(281, 32, 2),
(282, 21, 2),
(283, 21, 2),
(284, 32, 2),
(285, 21, 2),
(286, 21, 2),
(287, 32, 2),
(288, 21, 2),
(289, 21, 2),
(290, 33, 2),
(291, 33, 2),
(292, 21, 2),
(293, 21, 2),
(294, 21, 2),
(295, 21, 2),
(296, 21, 2),
(297, 21, 2),
(298, 21, 2),
(299, 21, 2),
(300, 21, 2),
(301, 21, 2),
(302, 21, 2),
(303, 21, 2),
(304, 21, 2),
(305, 21, 2),
(306, 21, 2),
(307, 21, 2),
(308, 21, 2),
(309, 21, 2),
(310, 21, 2),
(311, 21, 2),
(312, 21, 2),
(313, 21, 2),
(314, 21, 2),
(315, 21, 2),
(316, 32, 2),
(317, 32, 2),
(318, 32, 2),
(319, 32, 2),
(320, 32, 2),
(321, 32, 2),
(322, 32, 2),
(323, 32, 2),
(324, 21, 8),
(325, 21, 8),
(326, 21, 8),
(327, 21, 8),
(328, 35, 8),
(329, 38, 8),
(330, 38, 8),
(331, 38, 8),
(332, 38, 8),
(333, 39, 8),
(334, 39, 8),
(335, 39, 8),
(336, 39, 8),
(337, 40, 8),
(338, 21, 8),
(339, 21, 8),
(340, 21, 8),
(341, 40, 8),
(342, 40, 8),
(343, 40, 8),
(344, 32, 8),
(345, 32, 8),
(346, 32, 8),
(347, 32, 8),
(348, 32, 8),
(349, 32, 8),
(350, 32, 8),
(351, 32, 8),
(352, 32, 8),
(353, 32, 8),
(354, 32, 8),
(355, 32, 8),
(356, 32, 8),
(357, 32, 8),
(358, 32, 8),
(359, 32, 8),
(360, 32, 8),
(361, 32, 8),
(362, 32, 8),
(363, 32, 8),
(364, 32, 8),
(365, 32, 8),
(366, 32, 8),
(367, 32, 8),
(368, 32, 8),
(369, 32, 8),
(370, 32, 8),
(371, 32, 8),
(372, 32, 8),
(373, 32, 8),
(374, 32, 8),
(375, 32, 8),
(376, 32, 8),
(377, 32, 8),
(378, 32, 8),
(379, 32, 8),
(380, 32, 8),
(381, 32, 8),
(382, 32, 8),
(383, 32, 8),
(384, 32, 8),
(385, 32, 9),
(386, 32, 8),
(387, 32, 9),
(388, 32, 10),
(389, 32, 10),
(390, 32, 10),
(391, 32, 10),
(392, 32, 10),
(393, 32, 10),
(394, 32, 10),
(395, 32, 10),
(396, 32, 10),
(397, 32, 11),
(398, 32, 11),
(399, 32, 11),
(400, 32, 11),
(401, 41, 8),
(402, 41, 8),
(403, 41, 8),
(404, 41, 8),
(405, 41, 8),
(406, 41, 8),
(407, 32, 10),
(408, 32, 11),
(409, 32, 8),
(410, 21, 8),
(411, 32, 8),
(412, 32, 8),
(413, 32, 8),
(414, 21, 10),
(415, 21, 10),
(416, 21, 10),
(417, 21, 8),
(418, 21, 10),
(419, 21, 10),
(420, 21, 10),
(421, 32, 8),
(422, 32, 8),
(423, 32, 11),
(424, 32, 8),
(425, 32, 8),
(426, 32, 8),
(427, 44, 8),
(428, 44, 8),
(429, 44, 8),
(430, 47, 12);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `name`, `active`) VALUES
(5, 'Hair cut - Men', 0),
(6, 'Hair cut - Women', 0),
(7, 'Hair cut - Kids', 0),
(8, 'Eyebrow Threading', 0);

-- --------------------------------------------------------

--
-- Table structure for table `shop_list`
--

CREATE TABLE `shop_list` (
  `id` int(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `locality_name` varchar(255) NOT NULL,
  `mobileno` varchar(50) NOT NULL,
  `website` varchar(50) NOT NULL,
  `about_me` varchar(250) NOT NULL,
  `service_offered` varchar(255) NOT NULL,
  `profile_pic` varchar(100) NOT NULL,
  `user_address` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `postalcode` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL,
  `days` longtext NOT NULL,
  `lat` decimal(8,0) NOT NULL,
  `lon` decimal(8,0) NOT NULL,
  `is_created_by_admin` tinyint(1) NOT NULL DEFAULT '0',
  `userid` int(20) DEFAULT NULL,
  `parking` varchar(100) NOT NULL,
  `opening_time` time NOT NULL,
  `closing_time` time NOT NULL,
  `image` varchar(100) NOT NULL,
  `shop_user` int(11) NOT NULL,
  `counter` int(11) NOT NULL,
  `rating` varchar(255) NOT NULL,
  `color` varchar(255) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shop_list`
--

INSERT INTO `shop_list` (`id`, `name`, `locality_name`, `mobileno`, `website`, `about_me`, `service_offered`, `profile_pic`, `user_address`, `city`, `state`, `postalcode`, `country`, `days`, `lat`, `lon`, `is_created_by_admin`, `userid`, `parking`, `opening_time`, `closing_time`, `image`, `shop_user`, `counter`, `rating`, `color`, `status`) VALUES
(8, 'Uppercut ', 'Bangalore ', '9840439385', 'www.uppercut.shop', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore ', '[\"Hair cut - Men\",\"Hair cut - Women\"]', '', 'Electronic city ', '', '', '', '', '[\"Tu\",\"We\",\"Th\",\"Fr\",\"Sa\"]', '23', '24', 0, NULL, '[\"bike\",\"car\",\"common\",\"no\"]', '06:31:00', '06:31:00', 'Salon_profile_template1.png', 35, 169, '', '', 0),
(10, 'abc', 'Chennai', '2342342342', 'www.uppercut.shop', 'asdfasdf ghdhdghdghfhgjbjgjgjgjgjhjbjbjbjjbjbjb', '[\"Eyebrow Threading\"]', '', 'Asdff', '', '', '', '', '[\"Su\",\"Tu\"]', '23', '42', 0, NULL, '[\"car\",\"bike\",\"common\",\"no\"]', '10:10:00', '22:10:00', 'Salon_profile_template11.png', 32, 17, '', '', 0),
(11, 'upper', 'bangalore', '2342342342', 'asdf', 'adfaasdfasdfadsfsdffasdfsfasdfadlfkajsdflajsldfalsdfjalsjdlfalsjfdasdfkasldfjalsdjfljalsdjflajsdlfjlajsdlfjasjdfljsljflsfdjklasjdlfjlajsdfljasldfjaslfasdflasdf;asdflasdlfaskdflksfjlajldjflasjfljsldfjlsdlksflkjasljfklksjdflsldflsjflksjfljal;df;f;afasd', '[\"Hair cut - Men\",\"Eyebrow Threading\"]', '', 'bangalore', '', '', '', '', '[\"We\",\"Th\",\"Fr\",\"Sa\"]', '23', '12', 0, NULL, '[\"car\"]', '10:10:00', '22:10:00', 'no_image.png', 45, 8, '', '', 0),
(12, 'VishalSaloon', 'VishalSaloon', '9999999999', 'vishalsaloon.COM', 'asdadasd', '', '', '', '', '', '', '', '', '0', '0', 0, NULL, '', '00:00:00', '00:00:00', '', 46, 1, '', '', 1),
(13, 'Success Hair Studio professional', 'Solapur', '8625952203', 'www.SuccessHairStudioprofessional.com', 'Solapur Best Hydrabad Special All type of working.', '[\"Hair cut - Men\",\"Hair cut - Kids\"]', '', 'Ekatha Nagar,WIT leadies hostel frant,solapur', '', '', '', '', '[\"Su\",\"Mo\",\"Tu\",\"We\",\"Th\",\"Fr\"]', '18', '76', 0, NULL, '[\"bike\"]', '07:00:00', '21:00:00', 'images_(1).jpg', 51, 0, '', '', 0),
(14, 'vpacetest', 'Solapur', '9090909090', 'vpace.com', 'this is a test', '[\"Hair cut - Men\"]', '', '205/26 Telangi Pachha Peth, Padmashli Chowk', '', '', '', '', '[\"Su\",\"Mo\",\"Tu\",\"We\",\"Th\",\"Fr\",\"Sa\"]', '1', '2', 0, NULL, '[\"car\"]', '09:00:00', '21:00:00', 'download1.png', 57, 0, '', '', 0),
(15, 'bilu barbor', 'Solapur', '7585956585', 'bilu.com', 'This is solapur', '[\"Hair cut - Men\",\"Hair cut - Kids\"]', '', '205/26 Telangi Pachha Peth, Padmashli Chowk', '', '', '', '', '[\"Su\",\"Mo\",\"Tu\",\"We\",\"Th\",\"Fr\"]', '13', '16', 0, NULL, '[\"car\",\"bike\",\"common\"]', '10:00:00', '22:00:00', 'images_(1)1.jpg', 59, 0, '', '', 0),
(16, 'Ak Salon', 'Solapur', '8758487848', 'ak.com', 'This is fake data', '[\"Hair cut - Men\",\"Hair cut - Women\",\"Hair cut - Kids\",\"Eyebrow Threading\"]', '', '205/26 Telangi Pachha Peth, Padmashli Chowk', '', '', '', '', '[\"Su\",\"Mo\",\"Tu\",\"We\",\"Th\",\"Fr\",\"Sa\"]', '18', '155', 0, NULL, '[\"car\",\"bike\"]', '07:00:00', '19:00:00', 'images_(1)2.jpg', 61, 0, '', '', 0),
(17, 'Dan Hair Studio', 'Solapur', '9874556665', 'dan.com', 'Thisi is best.', '[\"Hair cut - Men\",\"Hair cut - Women\"]', '', '205/26 Telangi Pachha Peth, Padmashli Chowk', '', '', '', '', '[\"Su\",\"Mo\",\"Tu\",\"We\",\"Th\",\"Fr\",\"Sa\"]', '15', '155', 0, NULL, '[\"bike\"]', '07:45:00', '19:50:00', 'images.jpg', 62, 0, '', '', 0),
(18, 'jar style', 'Solapur', '9856412541', 'jar.com', 'dsafsafsda', '[\"Hair cut - Men\"]', '', '205/26 Telangi Pachha Peth, Padmashli Chowk', '', '', '', '', '[\"Su\",\"Mo\",\"Tu\",\"We\",\"Th\",\"Fr\",\"Sa\"]', '17458', '146', 0, NULL, '[\"bike\"]', '07:54:00', '19:50:00', 'images1.jpg', 62, 0, '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `shop_signup`
--

CREATE TABLE `shop_signup` (
  `id` int(110) NOT NULL,
  `verified_status` tinyint(1) NOT NULL DEFAULT '0',
  `username` varchar(50) NOT NULL,
  `password` varchar(50) CHARACTER SET utf8 NOT NULL,
  `activation_code` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `created_at` varchar(100) NOT NULL,
  `is_admincreated` tinyint(1) NOT NULL DEFAULT '0',
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shop_signup`
--

INSERT INTO `shop_signup` (`id`, `verified_status`, `username`, `password`, `activation_code`, `email`, `phone`, `created_at`, `is_admincreated`, `type`) VALUES
(16, 1, 'mariavincydhivya@gmail.com', 'f6edd2ea35beabdac297063eb3d1dca2', 'sYcBvV8eZ6z4wa32', 'mariavincydhivya@gmail.com', '(991) 616-9020', '2018-03-04 21:29:01', 0, 'user'),
(21, 1, '122meters@gmail.com', 'f970e2767d0cfe75876ea857f92e319b', 'VqmtDPAGfdjT6e7K', '122meters@gmail.com', '(984) 043-9385', '2018-03-12 07:53:04', 0, 'user'),
(32, 1, 'tulsi.black@gmail.com', 'f970e2767d0cfe75876ea857f92e319b', '1627', 'tulsi.black@gmail.com', '9840439385', '2018-03-25 15:36:42', 0, 'owner'),
(35, 1, 'uppercut.neo@gmail.com', 'f970e2767d0cfe75876ea857f92e319b', 'S1mBbFfx8wpIvXVa', 'uppercut.neo@gmail.com', '9840439385', '2018-04-25 18:21:24', 0, 'owner'),
(37, 0, 'b.muthubtech@gmail.com', '48357ed31bc839e233ced829cce6cf49', 'KBoZGzf1dROIPL6j', 'b.muthubtech@gmail.com', '0000000000', '2018-04-25 19:09:19', 0, 'user'),
(38, 1, 'bmuthubtech@gmail.com', 'bbcc9d65589193c8eb98782bc39e303d', 'tsAN2iZKawuWxCSk', 'bmuthubtech@gmail.com', '9566976107', '2018-04-25 19:23:36', 0, 'user'),
(39, 1, 'uppercut@uppercut.shop', 'f970e2767d0cfe75876ea857f92e319b', 'GSmWrR45xjYyQ8ct', 'upadmincut@uppercut.shop', '(984) 043-9384', '2018-04-26 03:46:14', 0, 'owner'),
(40, 1, 'kushaalsingla@gmail.com', '25f9e794323b453885f5181f1b624d0b', '8709', 'kushaalsingla@gmail.com', '9599110203', '2018-04-27 00:38:26', 0, 'owner'),
(41, 1, 'test_owner@gmail.com', 'f970e2767d0cfe75876ea857f92e319b', 'g5XVRNluJD8bjLWv', 'test_owner@gmail.com', '9849384958', '2018-05-12 06:57:23', 1, 'owner'),
(42, 0, 'imprabalthakur@gmail.com', '4d5855b59760200da464d7fe3098d9ff', 'dRBbK4a8JN1Hp9ks', 'imprabalthakur@gmail.com', '7807376810', '2018-05-17 20:56:01', 0, 'user'),
(43, 0, 'prabalthakur02@gmail.com', '4d5855b59760200da464d7fe3098d9ff', 'w1gzLcdXv4kJtEmp', 'prabalthakur02@gmail.com', '7807376810', '2018-05-17 20:58:08', 0, 'user'),
(44, 1, 'urgonda143@gmail.com', '339a65e93299ad8d72c42b263aa23117', 'wsGaRWHjFkX2Ldez', 'urgonda143@gmail.com', '8149686090', '2018-05-28 10:47:33', 0, 'user'),
(45, 1, 'navin@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '6062', 'navin@gmail.com', '9876543210', '2018-05-28 13:38:07', 1, 'owner'),
(46, 1, 'vishtest@saloon.com', '5f4dcc3b5aa765d61d8327deb882cf99', '7795', 'vishtest@saloon.com', '9970609808', '2018-06-04 10:40:43', 0, 'owner'),
(47, 1, 'vishtest@user.com', '5f4dcc3b5aa765d61d8327deb882cf99', 'BtvO1xcnfkg3Zouh', 'vishtest@user.com', '1234567890', '2018-06-04 11:36:29', 0, 'user'),
(48, 0, 'ganga@gmail.com', '731309c4bb223491a9f67eac5214fb2e', 'GQYRNx3I5KztnvrA', 'ganga@gmail.com', '9876543210', '2018-06-05 11:04:00', 0, 'user'),
(49, 1, 'oppo@gmail.com', '731309c4bb223491a9f67eac5214fb2e', '4VL5uzMqtUKdylsA', 'oppo@gmail.com', '(848) 484-4649', '2018-06-05 11:04:45', 0, 'user'),
(50, 1, 'bangaram@gmail.com', '202cb962ac59075b964b07152d234b70', '6347', 'bangaram@gmail.com', '8545758484', '2018-06-05 16:26:55', 0, 'owner'),
(51, 1, 'chicku@gmail.com', '202cb962ac59075b964b07152d234b70', '8252', 'chicku@gmail.com', '8787878787', '2018-06-05 16:34:39', 0, 'owner'),
(52, 0, 'kill@gmail.com', '202cb962ac59075b964b07152d234b70', '7474', 'kill@gmail.com', '8565456584', '2018-06-05 19:21:06', 0, 'owner'),
(53, 0, 'gili@gmail.com', '6a30e32e56fce5cf381895dfe6ca7b6f', '4030', 'gili@gmail.com', '8787878787', '2018-06-05 19:27:35', 0, 'owner'),
(54, 0, 'yeldandi123@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '1617', 'yeldandi123@gmail.com', '9860154814', '2018-06-05 19:41:18', 1, 'owner'),
(57, 1, 'ji@gmail.com', '5f4dcc3b5aa765d61d8327deb882cf99', '8526', 'ji@gmail.com', '1919191919', '2018-06-06 11:26:10', 0, 'owner'),
(58, 1, 'visuser@gmail.com', '5f4dcc3b5aa765d61d8327deb882cf99', 'DTrzIjLiF7nYl9QK', 'visuser@gmail.com', '8080808080', '2018-06-06 11:42:13', 0, 'user'),
(59, 1, 'billa@gmail.com', '202cb962ac59075b964b07152d234b70', '8027', 'billa@gmail.com', '7475859565', '2018-06-06 12:06:19', 0, 'owner'),
(60, 1, 'banga@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '3078', 'banga@gmail.com', '7585484857', '2018-06-11 12:23:20', 0, 'owner'),
(61, 1, 'bakki@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '2582', 'bakki@gmail.com', '9875461203', '2018-06-11 13:01:57', 0, 'owner'),
(62, 1, 'nama@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '5035', 'nama@gmail.com', '8149867878', '2018-06-11 17:42:06', 0, 'owner');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_membership`
--

CREATE TABLE `tbl_membership` (
  `id` int(11) NOT NULL,
  `prices` float NOT NULL,
  `user_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `validity_period` int(11) NOT NULL,
  `expiry_date` date DEFAULT NULL,
  `created_on` date NOT NULL,
  `updated_on` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_membership`
--

INSERT INTO `tbl_membership` (`id`, `prices`, `user_id`, `package_id`, `validity_period`, `expiry_date`, `created_on`, `updated_on`) VALUES
(1, 0, 62, 1, 0, NULL, '2018-06-11', '0000-00-00'),
(2, 0, 62, 1, 0, NULL, '2018-06-11', '0000-00-00'),
(3, 0, 62, 20, 3, '2018-09-11', '2018-06-11', '0000-00-00'),
(4, 0, 62, 1, 0, NULL, '2018-06-11', '0000-00-00'),
(5, 0, 62, 20, 3, '2018-09-11', '2018-06-11', '0000-00-00'),
(6, 0, 62, 1, 0, NULL, '2018-06-11', '0000-00-00'),
(7, 0, 62, 20, 3, '2018-09-11', '2018-06-11', '0000-00-00'),
(8, 0, 62, 1, 0, NULL, '2018-06-11', '0000-00-00'),
(9, 0, 62, 1, 0, NULL, '2018-06-11', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_packages`
--

CREATE TABLE `tbl_packages` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `valid_period` int(11) NOT NULL,
  `prices` float NOT NULL,
  `description` text NOT NULL,
  `status` enum('Active','Deactive') NOT NULL,
  `created_on` date NOT NULL,
  `updated_on` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_packages`
--

INSERT INTO `tbl_packages` (`id`, `title`, `valid_period`, `prices`, `description`, `status`, `created_on`, `updated_on`) VALUES
(1, 'Starter', 0, 0, 'One Salon Profile', 'Active', '2018-06-11', '0000-00-00'),
(20, 'Booking Request', 3, 0, 'Accept booking from customer at 0 % commission per booking.', 'Deactive', '2018-06-11', '0000-00-00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deleted_account`
--
ALTER TABLE `deleted_account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recent_view`
--
ALTER TABLE `recent_view`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_list`
--
ALTER TABLE `shop_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_signup`
--
ALTER TABLE `shop_signup`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `tbl_membership`
--
ALTER TABLE `tbl_membership`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_packages`
--
ALTER TABLE `tbl_packages`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `bookings`
--
ALTER TABLE `bookings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `deleted_account`
--
ALTER TABLE `deleted_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `recent_view`
--
ALTER TABLE `recent_view`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=431;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `shop_list`
--
ALTER TABLE `shop_list`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `shop_signup`
--
ALTER TABLE `shop_signup`
  MODIFY `id` int(110) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `tbl_membership`
--
ALTER TABLE `tbl_membership`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbl_packages`
--
ALTER TABLE `tbl_packages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
