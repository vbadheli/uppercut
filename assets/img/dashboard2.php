<!DOCTYPE html>

<html lang="en">

<head>

  <meta charset="UTF-8">
  <meta charset="UTF-8">
  <meta charset="utf-8">
  <meta http-equiv="content-type" content="text/html;charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" id="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title>Shop</title>

  <!-- Bootstrap core CSS -->

  <link href="<?php  echo base_url('assets/vendor/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">

  <!-- Custom fonts for this template -->

  <link href="<?php  echo base_url('assets/vendor/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css">

  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600|Roboto:300" rel="stylesheet">

  <!-- Plugin CSS -->

  <link href="<?php  echo base_url('assets/vendor/magnific-popup/magnific-popup.css'); ?>" rel="stylesheet">

  <!-- Custom styles for this template -->

  <link href="<?php  echo base_url('assets/css/style.css'); ?>" rel="stylesheet">

  <link rel="shortcut icon" href="https://agora-file-storage-prod.s3.amazonaws.com/workplace/attachment/317131287979551419?response-content-disposition=inline%3B%20filename%3D%22favicon.png%22%3B%20filename%2A%3Dutf-8%27%27favicon.png&x-amz-security-token=FQoDYXdzEMP%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaDM7HuQ6%2F4ZVYJBoR2yK3A5tizl0YtJvGCWrbGAftFXCLf4ESwNR4rdg0036xpACNVCEf0z1hyD2NW88OH%2By0rKc0Bgmxk3pkCblb2D96sLaUKbHR5Mb%2BXpiTYInqzoTtRxNK%2BAd%2FcfxsanbWEHKaN1t%2Fps3v%2FrSax3T8xfZ3CigWu0evJZVEDWA19oI%2Bb8NZ7cNgv0lH1b2nCfgwG%2BAMIYmeBnysJX%2FAM4sgsPFnz3MFMSFeVvcJm374Z5rf6HMs9DEqfAnEQylDmprRjFw6JIsqF2XEAuSb8bxQJ3um1Qix6zXYgvNvgAeI1XBDHFMvfzz1r6z6CrIYGXChekn7dPw9PVCRog812Se1XTSgKr3a6pHWZpwUPdHtAYHfC62mmcLQy%2BfuwMWnfE676GlUOXvEPAEnQuXy7K7%2BOSOjcena8GSNvS3jpeCPUvZ4Ij3dHQNUTxqg9NPruBBrGM7Zebvl%2Ff2%2FmhUC%2F7LVfDbp2IFPmvfuBujlyek%2FjE3tGolKjMYVqf8%2BQ40fdKTIaBBvGqHBhDzJ8WKp8ZjvwJfhnM815UE1Xmd5l5FXVl9TaEgUgRtIPlXopITQVedma1hC7EJY8YNjFQUo4cC%2FzgU%3D&AWSAccessKeyId=ASIAJOLVEHYVE24Y2IAA&Expires=1506796613&Signature=wf%2F789LOywzc1a09jcHZBjZKEjw%3D

  " type="image/x-icon">

  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-100486865-1"></script>

  <script>

    window.dataLayer = window.dataLayer || [];

    function gtag(){dataLayer.push(arguments)};

    gtag('js', new Date());

    gtag('config', 'UA-100486865-1');

  </script>

  <style type="text/css">

    i {

      border: solid white;

      border-width: 0 3px 3px 0;

      display: inline-block;

      padding: 3px;

    }

    .left {

      transform: rotate(135deg);

      -webkit-transform: rotate(135deg);

    }

    .cross_top{

          margin-top: 15px;

    }

    .close-icon

    {

      display:block;

      box-sizing:border-box;

      width:20px;

      height:20px;

      border-width:3px;

      background: -webkit-linear-gradient(-45deg, transparent 0%, transparent 46%, black 46%,  black 56%,transparent 56%, transparent 100%), -webkit-linear-gradient(45deg, transparent 0%, transparent 46%, black 46%,  black 56%,transparent 56%, transparent 100%);

      background-color:white;

      transition: all 0.3s ease;

      margin-top: 20px;

    }

  </style>

</head>

<body>

  <div id="loader"></div>

   <div class="left-nav" style="padding: 0;background: white;" >
    <div class="top-body">
      <div class="toggleTab">
        <div class="tog-icon"></div>
        <div class="tog-icon"></div>
      </div>
    <span class="brand-name">
    <a class="brand-logo" href="<?php echo base_url('home'); ?>"><img src="<?php echo base_url('assets/img/logo.png'); ?>"></a>
    </span>
    </div>

   <div class="form-dash toggled-nav">

    <form class="form" role="form" autocomplete="off" method="post" action="<?php //echo base_url('search'); ?>" id="formLogin">

      <div class="form-group search-box">

        <input type="search" class="form-control form-control-lg" name="search" id="address" placeholder="Search by shop name or locality" >

        <input type="hidden" name="lat" value="" id="lat">

        <input type="hidden" name="lon" value="" id="lon">    

        <button type="submit" class="search-dash-btn"><i class="fa fa-search"></i></button>

      </div>

    </form>

  </div>

  <div class="row dashhrow">

    <div class="col-sm-8 search_result">Search result <span class="num_of_search_result">  <?php  echo count($result); ?></span></div>

    <div class="col-sm-4"><i class="fa fa-filter marg35" aria-hidden="true"></i></div>

  </div>

  <div class="leftbar">

    <?php 

    if(isset($result)):     foreach ($result as $key => $value):    ?>

    <div Onclick="window.location.href='<?php echo base_url('dashboard2'); ?>/<?php echo base64_encode($value->id);?>' " style="cursor: pointer;" >

     <div class="col-md-12 marg10">

      <div class="Shop_name"><div class="left_border"></div> <span><a href="<?php echo base_url('dashboard2'); ?>/<?php echo base64_encode($value->id); ?>" style="color: black;font-weight: 600 "> <?php echo $value->name; ?></a></span></div>

    </div>

    <div class="row dashhrow">

      <div class="col-sm-6 shop_area"><?php echo $value->city; ?></div>

      <div class="col-sm-6 kms"></div>

    </div>

  </div>

  <hr>

<?php endforeach; endif; ?>

</div>

</div>

<div class="main">
  <div class="">
    <div class="col-md-12">
      <div class="">
        <div class="py-2 header-body">
          <div class="coll" href="" >
            <div class="oval mr-3" id="oval" style="    width: 50px;
    height: 50px;
    background-color: #D8D8D8;
    border: 1px solid #979797;
    border-radius: 50%;
    vertical-align: middle;padding-left: 0px;">
              <div class="toggleTab man">
            <div class="tog-icon"></div>
            <div class="tog-icon"></div>
          </div>
            </div>
            <div class="s-name">
              <h1><a href="" ><?= $getbyid[0]['name']; ?></a></h1>
              <p><?= $getbyid[0]['user_address']; ?></p>
            </div>
          </div>
          <!-- <div class="coll">
            <div class="btn-body">
              <button class="bttn bttn-blue">SAVE</button>
              <button class="bttn bttn-border-blue">CANCEL</button>
            </div>
          </div> -->
        </div>

        <div class="row no-gutters mt-4 fa-icon">

          <div class="col-sm-4">

            <div class="map-kms">

              <p style="padding-left: 26px;"><i class="fa fa-map-o"></i><span></span></p>

              <p class="map-link"><a href="" id="link" target="__BLANK">Get Directions <img src="<?= base_url(); ?>assets/img/download.png" ></a></p>

            </div>

            <div class="service">

              <p><span>Service offered</span></p>

             <?php foreach (json_decode($getbyid[0]['service_offered']) as $sey ) { ?>
               <p><?= $sey; ?></p>
              <?php  } ?>

            </div>

            <div class="parking">

              <p><span>Parking</span></p>

              <p>
                <?php if($getbyid[0]['parking'] == "Bike") { ?>
                 <i class="fa fa-motorcycle"></i>
                <?php }else if($getbyid[0]['parking'] == "Car"){ ?>
                <i class="fa fa-car"></i>
                 <?php }else if($getbyid[0]['parking'] == "Bus"){ ?>
                <i class="fa fa-bus"></i>
                <?php }else{ ?>
                <?= $getbyid[0]['parking']; ?>
                <?php } ?>
                </p>

              </div>

              <div class="address">

                <p><span>Address</span></p>

                <p><?php echo $getbyid[0]['user_address']; ?>, <?php echo $getbyid[0]['city']; ?></p>

              </div>

            </div>

            <div class="col-sm-4">

              <div class="contact">

                <p><i class="fa fa-phone"></i><span>Contact</span></p>

                <span><?php echo $getbyid[0]['mobileno']; ?></span>   

                

              </div>

            </div>

            <div class="col-sm-4">

              <div class="img">

                <img src="<?= base_url(); ?>uploads/<?= $getbyid[0]['image']; ?>" class="img-responsive" style="height: 250px; width: 100%;" alt="">

              </div>

              <div class="mt-4 result-text">

                <p><?php echo $getbyid[0]['about_me']; ?></p>

              </div>

              <div class="date-time">

                <p><i class="fa fa-clock-o"></i><span><?= $getbyid[0]['opening_time']; ?> - <?= $getbyid[0]['closing_time']; ?></span></p>

                <?php if($getbyid[0]['days'] != "null" ){  ?>
                <p><?php $dayss =  json_decode($getbyid[0]['days']); ?>
                  <?php foreach ($dayss as $key ) {
                 echo $key.'  ';
                } }else { } ?></p>

              </div>

              <div class="site-url">

                <p><i class="fa fa-globe"></i><span><a href="<?php echo $getbyid[0]['website'];?>" target="__BLANK"><?php echo $getbyid[0]['website'];?></a></span></p>

              </div>

            </div>

          </div>

        </div>

        <!--/col-->

      </div>

      <!--/col-->

    </div>

    <!--/row-->

  </div>

  <!--/container-->

  <?php $this->load->view('layouts/footer'); ?>

  <script type='text/javascript' src='<?php echo base_url('assets/admin/js/backend/main.js'); ?>'></script>



  <script type="text/javascript">



    $('#formLogin').validate({

      rules:{

        address:{

          required: true,

        },

      },

      errorPlacement: function(error, element) {},

      highlight: function(element) {

        $(element).parent('div').addClass('has-error');

      },

      unhighlight: function(element) {

        $(element).parent('div').removeClass('has-error');

      },

    });

  </script>
   <script type="text/javascript">
        
          if ("geolocation" in navigator){
            
            navigator.geolocation.getCurrentPosition(function(position){ 
              var lat=position.coords.latitude; 
              var lang=position.coords.longitude;
              $('#link').attr("href", "https://www.google.com/maps/dir/<?php echo $getbyid[0]['lat'].','.$getbyid[0]['lon']; ?>/"+lat+","+lang);
              console.log(<?php $lang; ?>);
              $("#lon").val(lang);
              $("#lat").val(lat);
            
            
          });
          }else{
            console.log("Browser doesn't support geolocation!");
            $('#link').attr("href", "https://www.google.com/maps/dir/<?php echo $getbyid[0]['lat'].','.$getbyid[0]['lon']; ?>/");
          }

      
      </script>