$(document).on('click', '.try-login', function () {
    $('#log').click();
});
$(document).on('click', '.try-register', function () {
    $('#reg').click();
});
$('#otp_check').click(function () {
    var $input = $(this);
    if ($input.prop('checked')) $('#password-field').hide('slow');
    else $('#password-field').show('slow');
})
$(document).on('click', '#change-email', function () {
    $('#login-box').removeClass('hide').fadeIn('slow');
    $('#otp-login').addClass('hide').fadeOut('slow');
    $('#login-otp-form').find('.message').html('');
    $('#login-form').find('.message').html('');
});

$(document).on('click', '#forgot-button', function () {
    $('#login-process').addClass('hide').fadeOut('slow');
    $('#forgot-box').removeClass('hide').fadeIn('slow');
});
$(document).on('click', '#try-login', function () {
    $('#login-process').removeClass('hide').fadeIn('slow');
    $('#forgot-box').addClass('hide').fadeOut('slow');
})
$.validator.addMethod("pwcheck", function(value) {
    return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) // consists of only these
        && /[a-z]/.test(value) // has a lowercase letter
        && /\d/.test(value) // has a digit
});
$.validator.addMethod('mobileNumber', function (value, element) {
    return this.optional(element) || /([0-9]{10})|(\([0-9]{3}\)\s+[0-9]{3}\-[0-9]{4})/.test(value);
});

$(document).ready(function(){
    $('#registration-form').validate({
        onkeyup: false,
        rules: {
            name: {
                required: true,
            },
            email: {
                required: true,
                email: true,
                remote: {
                    url: base_url+"auth/check_email"
                }
            },
            mobile: {
                required: true,
                mobileNumber: true,
                remote: {
                    url: base_url+"auth/check_mobile"
                }
            },
            password: {
                required: true,
                //pwcheck: true,
                minlength: 8
            },
            terms: {
                required: true,
            }
        },
        messages: {
            name: {
                required: "Please enter your name"
            },
            mobile: {
                required: "Please enter your valid mobile number",
                mobileNumber: "Enter valid mobile number",
                remote: "This mobile number is already used"
            },
            email: {
                required: "Please enter your valid email id",
                email: "This is not a valid email id",
                remote: "This email id is already registered"
            },
            password: {
                required: "Create your password",
               // pwcheck: "The password must be minimum one char latter and one numeric digit",
                minlength: "Password must have atleast 8 characters"
            },
            terms: {
                required: "Please agree to terms & conditions and Privacy Policy",
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "terms") {
                error.insertAfter(".terms.control--checkbox");
            }else{
                error.insertAfter(element);
            }
        },

        highlight: function (element) {
            $(element).parent('div').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).parent('div').removeClass('has-error');
        },
        submitHandler: function(form) {
            $('#registration-form').find(':submit').attr('disabled','disabled');
            $.ajax({
                url: form.action,
                type: form.method,
                dataType: 'JSON',
                data: $(form).serialize(),
                success: function(response) {
                    $('#registration-form').find('.message').html(response.message);
                    $('#registration-form')[0].reset();
                    $('#registration-form').find(':submit').removeAttr('disabled');
                }
            });
        }
    });

    $('#login-form').validate({
        errorPlacement: function (error, element) {},
        highlight: function (element) {
            $(element).parent('div').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).parent('div').removeClass('has-error');
        },
        submitHandler: function(form) {
            $('#login-form').find(':submit').attr('disabled','disabled');
            var data = $(form).serialize();
            $.ajax({
                url: form.action,
                type: form.method,
                dataType: 'JSON',
                data: data +"&timeZone=" + getTimeZone(),
                success: function(response) {
                    if(response.error == true){
                        $('#login-form').find('.message').html(response.message);
                    } else {
                        if(response.login == false) {
                            $('#login-otp-form').find('.message').html(response.message);
                            $('#login-box').addClass('hide').fadeOut('slow');
                            $('#otp-login').removeClass('hide').fadeIn('slow');
                        } else {
                            window.location.href = base_url+'dashboard';
                        }
                    }
                    $('#login-form').find(':submit').removeAttr('disabled');
                }
            });
        }
    });

    $('#forgot-form').validate({
        errorPlacement: function (error, element) {},
        highlight: function (element) {
            $(element).parent('div').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).parent('div').removeClass('has-error');
        },
        submitHandler: function(form) {
            $('#forgot-form').find(':submit').attr('disabled','disabled');
            $.ajax({
                url: form.action,
                type: form.method,
                dataType: 'JSON',
                data: $(form).serialize(),
                success: function(response) {
                    $('#forgot-form').find('.message').html(response.message);
                    $('#forgot-form').find(':submit').removeAttr('disabled');
                }
            });
        }
    });

    $('#login-otp-form').validate({
        errorPlacement: function (error, element) {},
        highlight: function (element) {
            $(element).parent('div').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).parent('div').removeClass('has-error');
        },
        submitHandler: function(form) {
            $('#login-otp-form').find(':submit').attr('disabled','disabled');
            var otp = $('#otp').val();
            var email = $('#login-form').find('#email').val();
            $.ajax({
                url: form.action,
                type: form.method,
                dataType: 'JSON',
                data: {email: email, otp:otp},
                success: function(response) {
                    if(response.error == true) {
                        $('#login-otp-form').find('.message').html(response.message);
                    } else {
                        window.location.href = base_url+'dashboard';
                    }
                    $('#login-otp-form').find(':submit').removeAttr('disabled');
                }
            });
        }
    })

    $('#resend-otp').click(function () {
        var email = $('#login-form').find('#email').val();
        $.ajax({
            url: base_url+'auth/resend_otp',
            type: 'POST',
            dataType: 'JSON',
            data: {email: email},
            success: function(response) {
                $('#login-otp-form').find('.message').html(response.message);
            }
        });
    })
});