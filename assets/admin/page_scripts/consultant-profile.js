$(document).ready(function () {
    $('#dob').datetimepicker({
        format: "DD-MM-YYYY",
        pickTime: false
    });
    $('#pass').datetimepicker({
        //format: "DD-MM-YYYY",
        format: "YYYY", // Notice the Extra space at the beginning
        viewMode: "years",
        minViewMode: "years",
        pickTime: false
    });
    $('#start').datetimepicker({
        format: "YYYY", // Notice the Extra space at the beginning
        viewMode: "years",
        minViewMode: "years",
        pickTime: false
    });
    $('#clinicsTable').dataTable({
        "processing": false,
        "ordering": true,
        "searching": true,
        "bLengthChange":true,
        "bInfo": true,
        "order": [[1, "desc"]],
        "aoColumns": [
            {"bSortable": true},
            {"bSortable": true},
            {"bSortable": true},
            {"bSortable": true},
            {"bSortable": true},
            {"bSortable": true},
            {"bSortable": false},
        ]
    });
});
$(document).on('click', '#change-mobile', function () {
    $('#mobile').removeAttr('readonly').focus();
});
$(document).on('blur', '#mobile', function () {
    $(this).attr('readonly', 'readonly');
})

$(document).on('click', '.browse', function () {
    $(this).siblings('.file').trigger('click');
});
$(document).on('change', '.file', function () {
    var input = $(this);
    file = input[0].files[0];
    var name = input[0].files[0].name;
    var d = 0;
    if (input.data('type') == 'video') {
        var validExtensions = ['mp4', 'MP4', 'mov', 'MOV'];
    } else if (input.data('type') == 'doc_img') {
        var validExtensions = ['jpg', 'png', 'JPG', 'JPEG', 'jpeg', 'PNG', 'docx', 'doc', 'pdf']; //array of valid extensions
    } else if (input.data('type') == 'image') {
        var validExtensions = ['jpg', 'png', 'JPG', 'JPEG', 'jpeg', 'PNG'];
    }
    var fileNameExt = name.substr(name.lastIndexOf('.') + 1);
    if ($.inArray(fileNameExt, validExtensions) == -1) {
        input.closest('.form-group').addClass('has-error');
        input.parent().siblings('input[type=text]').val('File you are select is not a valide type');
    } else {
        if (input.data('type') == 'video') {
            var vid = document.createElement('video');
            var fileURL = URL.createObjectURL(file);
            vid.src = fileURL;
            vid.ondurationchange = function () {
                d = Math.round((vid.duration / 60) * 100) / 100;
                if (parseFloat(d) > 12.8) {
                    input.closest('.form-group').addClass('has-error');
                    input.parent().siblings('input[type=text]').val('Introduction video duration is maximum 120 seconds');
                } else {
                    input.closest('.form-group').removeClass('has-error');
                    input.parent().siblings('input[type=text]').val(name);
                    var button = input.data('class');
                    insuranceFile(file, button);
                }
            };
        } else {
            input.closest('.form-group').removeClass('has-error');
            input.parent().siblings('input[type=text]').val(name);
            var button = input.data('class');
            insuranceFile(file, button);
        }
    }
});
function createCORSRequest(method, url) {
    var xhr = new XMLHttpRequest();
    if ("withCredentials" in xhr) {
        // Check if the XMLHttpRequest object has a "withCredentials" property.
        // "withCredentials" only exists on XMLHTTPRequest2 objects.
        xhr.open(method, url, true);

    } else if (typeof XDomainRequest != "undefined") {
        // XDomainRequest only exists in IE, and is IE's way of making CORS requests.
        xhr = new XDomainRequest();
        xhr.open(method, url);

    } else {
        // Otherwise, CORS is not supported by the browser.
        xhr = null;

    }
    return xhr;
}

function insuranceFile(file, button) {

    var file_data = file;   // Getting the properties of file from file field
    var form_data = new FormData();                  // Creating object of FormData class
    form_data.append("image", file_data);             // Appending parameter named file with properties of file_field to form_data
    $(".progress-bar-striped." + button).parent().show();
    $(".progress-bar-striped." + button).width('0%').text('0%');
    $(".progress-bar-striped." + button).addClass('progress-bar-info');
    $(".progress-bar-striped." + button).removeClass('progress-bar-success');
    $(".progress-bar-striped." + button).removeClass('progress-bar-danger');
    var xhr = createCORSRequest('POST', base_url+'dashboard/profile/'+button);
    (xhr.upload || xhr).addEventListener('progress', function (e) {
        var done = e.position || e.loaded
        var total = e.totalSize || e.total;
        var com = Math.round(done / total * 100) - 1;
        if (com == 1) {
            $(".progress-bar-striped." + button).parent().show();
        } else if (com > 30) {
            $(".progress-bar-striped." + button).width(com + '%').text(com + '% Please Wait');
        } else {
            $(".progress-bar-striped." + button).width(com + '%').text(com + '%');
        }
    });
    xhr.addEventListener('load', function (e) {
        var response = JSON.parse(this.responseText);
        if (response.error == true) {
            $(".progress-bar-striped." + button).width('100%').text(response.message);
            $(".progress-bar-striped." + button).removeClass('progress-bar-info');
            $(".progress-bar-striped." + button).removeClass('progress-bar-success');
            $(".progress-bar-striped." + button).addClass('progress-bar-danger');
        } else {
            $(".progress-bar-striped." + button).width('100%').text(response.message);
            $(".progress-bar-striped." + button).removeClass('progress-bar-info');
            $(".progress-bar-striped." + button).removeClass('progress-bar-danger');
            $(".progress-bar-striped." + button).addClass('progress-bar-success');
            $(".progress-bar-striped." + button).parent().delay(1000).fadeOut();
            if (button == 'introduction') {
                $('#video').attr('src', response.url);
                $('#' + button + 'Url').val(response.url);
            }else if(button == 'clinic') {
                $('#clinic_pics').attr('src',response.url);
                $('#' + button + 'Url').val(response.url);
            } else {
                $('#' + button + 'Url').val(response.url);
            }
        }

    });
    // xhr.open('post', upload, true);
    /* xhr.setRequestHeader(
     "Content-type",'Content-Type: multipart/form-data'
     );*/
    xhr.send(form_data);

}


$('.clinic_details').click(function () {
    var clinic_id = $(this).data('id');
    $('#clinic_modal').html('');
    $.ajax({
        url: base_url + "dashboard/profile/clinic_details",
        dataType: 'json',
        data: {id: clinic_id},                         // Setting the data attribute of ajax with file_data
        type: 'post',
        success: function (response) {
            if(response.error == false) {
                $('#clinic_modal').html(response.data);
            }else{
                $('#clinic_modal').modal('hide');
                bootbox.alert('Some error found please try again!');
            }
        }
    });
});
/***********add education details**********/
$('#addEducationForm').validate({
    errorPlacement: function (error, element) {
    },
    highlight: function (element) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function (element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    submitHandler: function (form) {
        $('#addEducationForm').find(':submit').attr('disabled', 'disabled');
        $.ajax({
            url: form.action,
            type: form.method,
            dataType: 'JSON',
            data: $(form).serialize(),
            success: function (response) {
                if (response.error == true) {
                    $('#addEducationForm').find('.message').html(response.message);
                } else {
                    var education_id = $('#addEducationForm').find('#education_id').val();
                    if(education_id != '') {
                        var x = $('.bt-close1.addEducation[data-id="'+education_id+'"]');
                        x.closest('.add-edu.education').parent().remove();
                        //alert(education_id);
                    }
                    $('#add-education').prepend(response.message);
                    $('#addEducationForm')[0].reset();
                    $('#add-education-modal').modal('hide');
                }
                $('#addEducationForm').find(':submit').removeAttr('disabled');
            }
        });
    }
});
/********************** add skills **********************/
$('#skills-info-form').validate({
    errorPlacement: function (error, element) {
    },
    highlight: function (element) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function (element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    submitHandler: function (form) {
        $('#skills-info-form').find(':submit').attr('disabled', 'disabled');
        $.ajax({
            url: form.action,
            type: form.method,
            dataType: 'JSON',
            data: $(form).serialize(),
            success: function (response) {
                if (response.error == true) {
                    $('#skills-info-form').find('.message').html(response.message);
                } else {
                    $('#add-skills').html(response.message);
                    //$('#skills-info-form')[0].reset();
                    $('#skills-info').modal('hide');
                }
                $('#skills-info-form').find(':submit').removeAttr('disabled');
            }
        });
    }
});
/*------------------------------------add category ----------------------------------------------*/
$('#category-info-form').validate({
    errorPlacement: function (error, element) {
    },
    highlight: function (element) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function (element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    submitHandler: function (form) {
        $('#category-info-form').find(':submit').attr('disabled', 'disabled');
        $.ajax({
            url: form.action,
            type: form.method,
            dataType: 'JSON',
            data: $(form).serialize(),
            success: function (response) {
                if (response.error == true) {
                    $('#category-info-form').find('.message').html(response.message);
                } else {
                    $('#add-category').html(response.message);
                    //$('#category-info-form')[0].reset();
                    $('#category-info').modal('hide');
                }
                $('#category-info-form').find(':submit').removeAttr('disabled');
            }
        });
    }
});

$('#certificateForm').validate({
    errorPlacement: function (error, element) {
    },
    highlight: function (element) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function (element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    submitHandler: function (form) {
        var url = $('#certificatesUrl').val();
        if(url == ''){
            $('#certificatesUrl').closest('.form-group').addClass('has-error');
        }else{
            $('#certificatesUrl').closest('.form-group').removeClass('has-error');
            $('#certificateForm').find(':submit').attr('disabled', 'disabled');
            $.ajax({
                url: form.action,
                type: form.method,
                dataType: 'JSON',
                data: $(form).serialize(),
                success: function (response) {
                    if (response.error == true) {
                        $('#certificateForm').find('.message').html(response.message);
                    } else {
                        $('#append-registration').prepend(response.message);
                        $('#certificateForm')[0].reset();
                        $('#registration-modal').modal('hide');
                    }
                    $('#certificateForm').find(':submit').removeAttr('disabled');
                }
            });
        }
    }
});

$('#profile-update-form').validate({
    errorPlacement: function (error, element) {
    },
    highlight: function (element) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function (element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    submitHandler: function (form) {
        $('#profile-update-form').find(':submit').attr('disabled', 'disabled');
        $.ajax({
            url: form.action,
            type: form.method,
            dataType: 'JSON',
            data: $(form).serialize(),
            success: function (response) {
                if (response.error == true) {
                    $('#profile-update-form').find('.message').html(response.message);
                } else {
                    $('#append-registration').prepend(response.message);
                    $('#profile-update-form')[0].reset();
                }
                $('html, body').animate({
                    scrollTop: $(".breadcrumb.dash").offset().top
                }, 2000);
                $('#profile-update-form').find(':submit').removeAttr('disabled');
            }
        });
    }
});

$('#primaryClinic').on('change', '#category', function () {
    var selected = $(this).val();
    var clinic_id = $('#clinic_id').val();
    if(clinic_id != ''){
        $.ajax({
            url: base_url+'dashboard/profile/primaryCategory',
            type: 'post',
            dataType: 'JSON',
            data: {selected: selected, clinic_id:clinic_id},
            success: function (response) {

            }
        });
    }
});

$('#primaryClinic').on('change', '#skills', function () {
    var selected = $(this).val();
    var clinic_id = $('#clinic_id').val();
    if(clinic_id != '') {
        $.ajax({
            url: base_url + 'dashboard/profile/primarySkills',
            type: 'post',
            dataType: 'JSON',
            data: {selected: selected, clinic_id: clinic_id},
            success: function (response) {

            }
        });
    }
});

$('#primaryClinic').validate({
    errorPlacement: function (error, element) {
    },
    highlight: function (element) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function (element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    submitHandler: function (form) {
        $('#primaryClinic').find(':submit').attr('disabled', 'disabled');
        $.ajax({
            url: form.action,
            type: form.method,
            dataType: 'JSON',
            data: $(form).serialize(),
            success: function (response) {
                if (response.error == true) {
                    $('#primaryClinic').find('.message').html(response.message);
                } else {
                    $('#append-registration').prepend(response.message);
                    $('#primaryClinic')[0].reset();
                }
                $('html, body').animate({
                    scrollTop: $(".breadcrumb.dash").offset().top
                }, 2000);
                $('#primaryClinic').find(':submit').removeAttr('disabled');
            }
        });
    }
});

$(document).on('click', '.bt-close.certificates', function () {
    var self = $(this);
    var reg_id = $(this).data('id');
    $.ajax({
        url: base_url+'dashboard/profile/delete_certificate',
        type: 'post',
        dataType: 'JSON',
        data: {reg_id: reg_id},
        success: function (response) {
            if (response.error == false) {
                self.closest('.regi-certi').parent().remove().fadeOut('slow');
            }
        }
    });
});
$(document).on('click', '.bt-close1.addEducation', function () {
    var self = $(this);
    var reg_id = $(this).data('id');
    $.ajax({
        url: base_url+'dashboard/profile/deleteEducation',
        type: 'post',
        dataType: 'JSON',
        data: {reg_id: reg_id},
        success: function (response) {
            if (response.error == false) {
                self.closest('.add-edu').parent().remove().fadeOut('slow');
            }
        }
    });
});

$(document).on('click', '.education', function () {
    var self = $(this).find('.bt-close1.addEducation');
    var start = self.data('start');
    var end = self.data('end');
    var eduction_id = self.data('id');
    var college = self.data('college');
    var degree = self.data('degree');
    $('#education_id').val(eduction_id);
    $('#collagename').val(college);
    $('#degree').val(degree);
    $('#start').val(start);
    $('#pass').val(end);
    $('#add-education-modal').modal('show');

});

$(document).on('click', '.bt-close1.skills', function () {
    var text = $.trim($(this).closest('.add-edu').parent().text());
    var replaced = $(this).data('id');
    $('#skills-info-form #skills').find("option[value="+replaced+"]").removeAttr("selected").trigger('change');
    $('#skills-info-form').submit();
});
$(document).on('click', '.bt-close1.category', function () {
    var text = $.trim($(this).closest('.add-edu').parent().text());
    var replaced = $(this).data('id');
    $('#category-info-form #category').find("option[value="+replaced+"]").removeAttr("selected").trigger('change');
    $('#category-info-form').submit();
});
function file_upload(file) {
    var file_data = file;   // Getting the properties of file from file field
    var form_data = new FormData();                  // Creating object of FormData class
    form_data.append("image", file_data);              // Appending parameter named file with properties of file_field to form_data
    $.ajax({
        url: base_url + "dashboard/profile/profile_pic",
        dataType: 'json',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,                         // Setting the data attribute of ajax with file_data
        type: 'post',
        success: function (response) {
            if(response.error == false){
                $('#profile_pic').attr('src', response.message);
            }
        }
    });
}

$("#profile_pic_file").change(function () {
    readURL(this);
});

function readURL(input) {
    if (input.files && input.files[0]) {
        var validExtensions = ['jpg','png', 'JPG', 'JPEG', 'jpeg', 'PNG']; //array of valid extensions
        var fileName = input.files[0].name;
        var fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1);
        if ($.inArray(fileNameExt, validExtensions) == -1) {
            bootbox.alert("Invalid file type");
            return false;
        } else {
            var img = base_url + 'assets/img/ajax-loader.gif';
            $('#profile_pic').attr('src', img);
            var reader = new FileReader();
            reader.readAsDataURL(input.files[0]);
            file_upload(input.files[0]);
        }
    }
}
