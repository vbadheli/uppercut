var videoClient;
var activeRoom;
var previewMedia;
var identity;
var roomName;
var sessionCount = 0;
var localMedia;
var remoteUsers = 0;
// Check for WebRTC

if (!navigator.webkitGetUserMedia && !navigator.mozGetUserMedia) {
    bootbox.alert('WebRTC is not available in your browser.');
}else{
    initializeTwilio();
}
// When we are about to transition away from this page, disconnect
// from the room, if joined.
window.addEventListener('beforeunload', leaveRoomIfJoined);
function initializeTwilio() {
    $.getJSON(base_url+'twilio/token', function (data) {
        identity = data.identity;
        // Create a Video Client and connect to Twilio
        videoClient = new Twilio.Video.Client(data.token);
        // Bind button to join room
        document.getElementById('button-join').onclick = function () {
            var token = $('#room-name').data('token');
            if((token == 1 || token == '1') && sessionCount == 0) {
                sessionCount = sessionCount + 1;
                roomName = document.getElementById('room-name').value;
                if (roomName) {
                    videoClient.connect({ to: roomName}).then(roomJoined,
                        function(error) {
                            document.getElementById('video-error').style.display = 'block';
                            document.getElementById('button-join').style.display = 'inline';
                            document.getElementById('video-error-message').innerHTML = 'Could not connect to server: ' + error.message;
                        });
                }
            } else {
                var id = $('#button-join').data('id');
                $.ajax({
                    url: base_url + 'my_dashboard/video-conference/sendConnectionRequest',
                    type: 'post',
                    dataType: 'JSON',
                    data: {booking_id: id},
                    success: function (response) {
                        if (response.error == false) {
                            $('#room-name').val(response.token);
                            roomName = document.getElementById('room-name').value;
                            videoClient.connect({ to: roomName}).then(roomJoined,
                                function(error) {
                                    document.getElementById('video-error').style.display = 'block';
                                    document.getElementById('button-join').style.display = 'inline';
                                    document.getElementById('video-error-message').innerHTML = 'Could not connect to server: ' + error.message;
                                });
                        }
                    }
                });
            }
        };
        // Bind button to leave room
        document.getElementById('button-leave').onclick = function () {
            document.getElementById('video-error').style.display = 'block';
            document.getElementById('video-error-message').text = 'Disconnecting';
            activeRoom.disconnect();
            document.getElementById('video-error').style.display = 'none';
        };
    });
}
// Successfully connected!
function roomJoined(room) {
    activeRoom = room;
    document.getElementById('video-error').style.display = 'none';
    document.getElementById('button-join').style.display = 'none';
    document.getElementById('button-leave').style.display = 'inline';
    if (!previewMedia) {
        room.localParticipant.media.attach('#local-media');
    }

    room.participants.forEach(function(participant) {
        participant.media.attach('#remote-media');
        setTimeout( function() {
            connected();
            remoteUsers = remoteUsers + 1;
        },2000);
    });
    // When a participant joins, draw their video on screen
    room.on('participantConnected', function (participant) {
        document.getElementById('video-error').style.display = 'none';
        participant.media.attach('#remote-media');
        setTimeout( function() {
            remoteUsers = remoteUsers + 1;
            connected();
        },2000);
    });

    // When a participant disconnects, note in log
    room.on('participantDisconnected', function (participant) {
        remoteUsers = remoteUsers - 1;
        document.getElementById('video-error').style.display = 'block';
        document.getElementById('video-error-message').innerHTML = participant.identity + "' left the room";
        participant.media.detach();
    });

    // When we are disconnected, stop capturing local video
    // Also remove media for all remote participants
    room.on('disconnected', function () {
        remoteUsers = 0;
        document.getElementById('video-error').style.display = 'none';
        room.localParticipant.media.detach();
        room.participants.forEach(function(participant) {
            participant.media.detach();
        });
        activeRoom = null;
        document.getElementById('button-join').style.display = 'inline';
        document.getElementById('button-leave').style.display = 'none';
    });
}

function connected() {
    var remoteMedia = $('#remote-media');
    if(remoteUsers <= 1) {
        remoteMedia.children('video').removeClass();
        remoteMedia.children('video').addClass('user1');
    }else if(remoteUsers == 2) {
        remoteMedia.children('video').removeClass();
        remoteMedia.children('video').addClass('user2');
    }else if(remoteUsers > 2 && remoteUsers <= 4) {
        remoteMedia.children('video').removeClass();
        remoteMedia.children('video').addClass('user3');
    }else if(remoteUsers > 4 && remoteUsers < 9) {
        remoteMedia.children('video').removeClass();
        remoteMedia.children('video').addClass('user9');
    }
}

//  Local video preview
// document.getElementById('button-preview').onclick = function () {
//     if (!previewMedia) {
//         previewMedia = new Twilio.Video.LocalMedia();
//         Twilio.Video.getUserMedia().then(
//             function (mediaStream) {
//                 previewMedia.addStream(mediaStream);
//                 previewMedia.attach('#local-media');
//             },
//             function (error) {
//                 console.error('Unable to access local media', error);
//
//                 document.getElementById('video-error').style.display = 'block';
//                 document.getElementById('video-error-message').innerHTML = "Unable to access Camera and Microphone";
//             });
//     };
// };


function leaveRoomIfJoined() {
    if (activeRoom) {
        activeRoom.stop();
        activeRoom.disconnect();
    }
}

function leaveMicrophone(self) {
    if(self.hasClass('cross')) {
        localMedia.addMicrophone();
        self.removeClass('cross');
        self.closest('fa').addClass('fa-microphone').removeClass('fa-microphone-slash');
    } else {
        localMedia.removeMicrophone();
        self.addClass('cross');
        self.closest('fa').addClass('fa-microphone-slash').removeClass('fa-microphone');
    }
}

function leaveCamera(self) {
// Remove the camera.
    /*  if(self.hasClass('cross')) {
     localMedia.removeCamera();
     self.removeClass('cross');
     } else {
     localMedia.removeCamera();
     self.addClass('cross');
     }*/
    localMedia.pause();
}

$(document).on('click', '#leaveMicrophone', function () {
    var self = $(this);
    leaveMicrophone(self);
});
$(document).on('click', '#leaveCamera', function () {
    var self = $(this);
    leaveCamera(self);
});
/**
 * Created by Aditya on 07-02-2017.
 */
$(document).on('click', '#request', function () {
    var self = $(this);
    var id = self.data('id');
    self.attr('disabled', true);
    $('#video-wait').show('inline');
    $('#video-error').hide();
    /*$('#coonecting').html("Sending request to client for join this confrence!");*/
    $.ajax({
        url: base_url + 'my_dashboard/video-conference/sendConnectionRequest',
        type: 'post',
        dataType: 'JSON',
        data: {booking_id: id},
        success: function (response) {
            if (response.error == false) {
                /* $('#coonecting').html("Your chat request successfully send to client now you able to hit start button!");*/
                $('#room-name').val(response.token);
                $('#button-join').trigger('click');
                $('#video-wait').hide();
                self.hide();
            } else {
                self.removeAtt('disabled');
                /*$('#coonecting').html("Connection failed please try again!");*/
            }
        }
    });
});

$(document).on('click', '.close_btn', function () {
    document.getElementById('video-error').style.display = 'none';
});

$(document).ready(function () {
    if (navigator.userAgent.match(/(iPod|iPhone|iPad|Android)/)) {
        $('html, body').animate({
            scrollTop: 65
        }, 800);
    } else {
        $('html, body').animate({
            scrollTop: 65
        }, 800);
    }

    startTime();
    var token = $('#room-name').data('token');
    if(token == 1 || token == '1') {
        $("#button-join").click();
    }

    $('#saveNote').click(function () {
        var self = $(this);
        self.attr('disabled', 'disabled');
        var id = self.data('id');
        var notes = $('.note-editable').html();
        $.ajax({
            url: base_url + 'my_dashboard/video-conference/saveNote',
            type: 'post',
            dataType: 'JSON',
            data: {booking_id: id, note: notes},
            success: function (response) {
                if (response.error == false) {
                    $('#noteMessage').html(response.message);
                }
                self.removeAtt('disabled');
            }
        });
    });
});

function startTime() {
    var last_hour = $('#last_hour').val();
    var last_minut = $('#last_minut').val();
    var last_day = $('#last_day').val();
    var countDownDate = new Date(last_day+last_hour+":"+last_minut+":00").getTime();

    var x = setInterval(function() {
        var now = new Date().getTime();
        var distance = countDownDate - now;
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        if(hours == 0) {
            if(minutes == 3 ) {
                $('#clockdiv').addClass('animate');
            } else if(minutes == 0) {
                $('#clockdiv').removeClass('animate');
                $('#clockdiv').addClass('expired');
            }
        }

        if (distance < 0) {
            $('.hours').text('00');
            $('.minutes').text('00');
            $('.seconds').text('00');
            $('#clockdiv').removeClass('animate');
            $('#clockdiv').addClass('expired');
        } else {
            $('.hours').text(hours);
            $('.minutes').text(minutes);
            $('.seconds').text(seconds);
        }
    }, 1000);
}
