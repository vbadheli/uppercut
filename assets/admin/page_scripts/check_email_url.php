<script type="text/javascript">
$(document).ready(function() {
	(function($) {
		var charsReg = /^[a-zA-Z0-9-\s]+$/;

		var ajax_call= new Array();
		var default_value = $('form .check_urlname_field').val();
		var last_value = $('form .check_urlname_field').val();
		var error = false;
	
		$('.check_urlname_field').keyup(function() {
			error = false;
			var curr = $(this);
			var val = $.trim(curr.val());
			curr.next('i').show();

			if( val == '' ) { error = true; }
	
			if( !charsReg.test(val) ) { error = true; console.log('error2');}
			
			if( val.indexOf(' ') >= 0 ) { error = true; }
			
			if( error ) {
				curr.parents('form').find('[type="submit"]').attr('disabled', 'disabled');
				curr.next('i').attr('class', 'fa fa-times-circle error');
			}
			
			if( !error ) {
				curr.parents('form').find('[type="submit"]').attr('disabled', 'disabled');
	
				curr.next('i').attr('class', 'fa fa-spinner spin');
				last_value = curr.val();
	
				if( ajax_call.length > 0 ) ajax_call[0].abort();
				ajax_call[0] = $.ajax({
					type: "POST",
					url: "<?php echo base_url(); ?>admin/resources/ajax_urlname_exists",
					data: { urlname: val, default_urlname: default_value },
					success: function(response) {
						if( response == "1" ) {
							curr.next('i').attr('class', 'fa fa-times-circle error');
						} else {
							curr.parents('form').find('[type="submit"]').removeAttr('disabled');
							curr.next('i').attr('class', 'fa fa-check-circle success');
						}
					}
				});
			}
		});
	})(jQuery);

});
</script>

