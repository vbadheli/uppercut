'use strict';

//var Video = require('twilio-video');

var activeRoom;
var previewTracks;
var identity;
var roomName;

// Attach the Tracks to the DOM.
function attachTracks(tracks, container) {
    tracks.forEach(function(track) {
        container.appendChild(track.attach());
    });
}

// Attach the Participant's Tracks to the DOM.
function attachParticipantTracks(participant, container) {
    var tracks = Array.from(participant.tracks.values());
    attachTracks(tracks, container);
}

// Detach the Tracks from the DOM.
function detachTracks(tracks) {
    tracks.forEach(function(track) {
        track.detach().forEach(function(detachedElement) {
            detachedElement.remove();
        });
    });
}

// Detach the Participant's Tracks from the DOM.
function detachParticipantTracks(participant) {
    var tracks = Array.from(participant.tracks.values());
    detachTracks(tracks);
}

// Check for WebRTC
if (!navigator.webkitGetUserMedia && !navigator.mozGetUserMedia) {
    //bootbox.alert('WebRTC is not available in your browser.');
    document.getElementById('video-error').style.display = 'block';
    $('#unsupported').show();
}

// When we are about to transition away from this page, disconnect
// from the room, if joined.
window.addEventListener('beforeunload', leaveRoomIfJoined);

// Obtain a token from the server in order to connect to the Room.
$.getJSON(base_url+'twilio/token', function(data) {
    identity = data.identity;
    //document.getElementById('room-controls').style.display = 'block';

    // Bind button to join Room.
    document.getElementById('button-join').onclick = function() {
        roomName = document.getElementById('room-name').value;
        var connectOptions = {
            name: roomName,
            /*audio: true,
            video: true*/
        };
        if (previewTracks) {
            connectOptions.tracks = previewTracks;
        }
        var id = $('#button-join').data('id');
        $.ajax({
            url: base_url + 'dashboard/video-conference/sendConnectionRequest',
            type: 'post',
            dataType: 'JSON',
            data: {booking_id: id},
            success: function (response) {
                if (response.error == false) {
                    Twilio.Video.connect(data.token, connectOptions).then(roomJoined, function(error) {
                        document.getElementById('video-error').style.display = 'block';
                        document.getElementById('button-join').style.display = 'inline';
                        document.getElementById('video-error-message').innerHTML = 'Could not connect to server: ' + error.message;
                    });
                }
            }
        });
    };
    // Bind button to leave Room.
    document.getElementById('button-leave').onclick = function() {
        document.getElementById('video-error').style.display = 'block';
        document.getElementById('video-error-message').text = 'Disconnecting';
        document.getElementById('video-error').style.display = 'none';
        activeRoom.disconnect();
    };
});

// Successfully connected!
function roomJoined(room) {
    window.room = activeRoom = room;
    document.getElementById('video-error').style.display = 'block';
    document.getElementById('button-join').style.display = 'none';
    document.getElementById('button-leave').style.display = 'inline';
    document.getElementById('video-error-message').innerHTML = 'Call Connecting Please Wait...';
    $('#video-error img').attr('src', base_url + 'assets/img/loading_gif.gif');
    // Attach LocalParticipant's Tracks, if not already attached.
    var previewContainer = document.getElementById('local-media');
    //alert('start preview');
    if (!previewContainer.querySelector('video')) {
        //alert('start preview active');
        attachParticipantTracks(room.localParticipant, previewContainer);
    }

    // Attach the Tracks of the Room's Participants.
    room.participants.forEach(function(participant) {
        var previewContainer = document.getElementById('remote-media');
        attachParticipantTracks(participant, previewContainer);
    });

    // When a Participant joins the Room, log the event.
    room.on('participantConnected', function(participant) {
        //$('#video-error img').attr('src', base_url + 'assets/img/error.png');
    });

    // When a Participant adds a Track, attach it to the DOM.
    room.on('trackAdded', function(track, participant) {
        document.getElementById('video-error').style.display = 'none';
        $('#video-error img').attr('src', base_url + 'assets/img/error.png');
        var previewContainer = document.getElementById('remote-media');
        attachTracks([track], previewContainer);
    });

    // When a Participant removes a Track, detach it from the DOM.
    room.on('trackRemoved', function(track, participant) {
        detachTracks([track]);
    });

    // When a Participant leaves the Room, detach its Tracks.
    room.on('participantDisconnected', function(participant) {
        document.getElementById('video-error').style.display = 'block';
        document.getElementById('video-error-message').innerHTML = participant.identity + "' Disconect this call";
        detachParticipantTracks(participant);
    });

    // Once the LocalParticipant leaves the room, detach the Tracks
    // of all Participants, including that of the LocalParticipant.
    room.on('disconnected', function() {
        if (previewTracks) {
            previewTracks.forEach(function(track) {
                track.stop();
            });
        }
        detachParticipantTracks(room.localParticipant);
        room.participants.forEach(detachParticipantTracks);
        activeRoom = null;
        document.getElementById('video-error').style.display = 'none';
        document.getElementById('button-join').style.display = 'inline';
        document.getElementById('button-leave').style.display = 'none';
    });
}

// Preview LocalParticipant's Tracks.
/*document.getElementById('button-preview').onclick = function() {
    var localTracksPromise = previewTracks
        ? Promise.resolve(previewTracks)
        : Video.createLocalTracks();

    localTracksPromise.then(function(tracks) {
        window.previewTracks = previewTracks = tracks;
        var previewContainer = document.getElementById('local-media');
        if (!previewContainer.querySelector('video')) {
            attachTracks(tracks, previewContainer);
        }
    }, function(error) {
        console.error('Unable to access local media', error);
        log('Unable to access Camera and Microphone');
    });
};*/

// Activity log.
function log(message) {
    /*var logDiv = document.getElementById('log');
    logDiv.innerHTML += '<p>&gt;&nbsp;' + message + '</p>';
    logDiv.scrollTop = logDiv.scrollHeight;*/
   // console.log(message);
}

// Leave Room.
function leaveRoomIfJoined() {
    if (activeRoom) {
        activeRoom.disconnect();
    }
}

$(document).on('click', '#request', function () {
    var self = $(this);
    var id = self.data('id');
    self.attr('disabled', true);
    $('#video-wait').show('inline');
    $('#video-error').hide();
    /*$('#coonecting').html("Sending request to client for join this confrence!");*/
    $.ajax({
        url: base_url + 'dashboard/video-conference/sendConnectionRequest',
        type: 'post',
        dataType: 'JSON',
        data: {booking_id: id},
        success: function (response) {
            if (response.error == false) {
                /* $('#coonecting').html("Your chat request successfully send to client now you able to hit start button!");*/
                $('#room-name').val(response.token);
                $('#button-join').trigger('click');
                $('#video-wait').hide();
                self.hide();
            } else {
                self.removeAtt('disabled');
                /*$('#coonecting').html("Connection failed please try again!");*/
            }
        }
    });
});

$(document).on('click', '.close_btn', function () {
    document.getElementById('video-error').style.display = 'none';
});

$(document).ready(function () {
    if (navigator.userAgent.match(/(iPod|iPhone|iPad|Android)/)) {
        $('html, body').animate({
            scrollTop: 65
        }, 800);
    } else {
        $('html, body').animate({
            scrollTop: 65
        }, 800);
    }

    startTime();
    /*var token = $('#room-name').data('token');
    if(token == 1 || token == '1') {
        $("#button-join").click();
    }*/

    /*$('#saveNote').click(function () {
        var self = $(this);
        self.attr('disabled', 'disabled');
        var id = self.data('id');
        var notes = $('.note-editable').html();
        $.ajax({
            url: base_url + 'dashboard/video-conference/saveNote',
            type: 'post',
            dataType: 'JSON',
            data: {booking_id: id, note: notes},
            success: function (response) {
                if (response.error == false) {
                    $('#noteMessage').html(response.message);
                }
                self.removeAttr('disabled');
            }
        });
    });*/
});

function startTime() {
    var last_hour = $('#last_hour').val();
    var last_minut = $('#last_minut').val();
    var last_day = $('#last_day').val();
    var countDownDate = new Date(last_day+last_hour+":"+last_minut+":00").getTime();
    var x = setInterval(function() {
        var now = new Date().getTime();
        var distance = countDownDate - now;
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        $('.minutes').text(minutes);
        $('.seconds').text(seconds);
        if(hours == 0) {
            if(minutes == 3 ) {
                $('#clockCount').addClass('animate');
            } else if(minutes == 0) {
                $('#clockCount').removeClass('animate');
                $('#clockCount').addClass('expired');
            }
        }
        //console.log(hours + ' ' + minutes + ' ' + seconds);
        if (distance < 0) {
            $('#clockCount').text('EXPIRED');
            $('#clockCount').removeClass('animate');
            $('#clockCount').addClass('expired');
        }
    }, 1000);
}