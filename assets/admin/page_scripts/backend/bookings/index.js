$(document).ready(function ($) {
    callData('', '', '');
    tableOptions = {
        "processing": false,
        "serverSide": false,
        "ordering": true,
        "searching": false,
        "bLengthChange": false,
        "iDisplayLength": 10,
        "bAutoWidth": false,
        "aoColumns": [
            {"bSortable": true},
            {"bSortable": true},
            {"bSortable": true},
            {"bSortable": true},
            {"bSortable": true},
            {"bSortable": true},
            {"bSortable": true},
            {"bSortable": true},
            {"bSortable": false},
        ]
    };
    var table = $('#book');
    table.DataTable(tableOptions);
    function DatatableInitialize(data) {
        table.DataTable().destroy()
        $('#book tbody').html(data);
        table.DataTable(tableOptions);
    }
    $("#filter").on("click", function (event) {
        var searchText = $('#search').val();
        var dateFrom = $('#datefrom').val();
        var dateTo = $('#dateto').val();
        callData(searchText, dateFrom, dateTo);
    });

    function callData(searchText, dateFrom, dateTo) {
        $.ajax({
            url: "<?php echo base_url('admin/bookings/loadDatatable'); ?>",
            type: "post",
            data: {
                search: searchText,
                dateFrom: dateFrom,
                dateTo: dateTo
            },
            dataType: 'JSON'
        }).done(function (result) {
            DatatableInitialize(result.output);
        }).fail(function (jqXHR, textStatus, errorThrown) {

        });
    }
});